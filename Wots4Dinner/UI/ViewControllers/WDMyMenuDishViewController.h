//
//  WDMyMenuDishViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/21/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDMyMenuDishViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) BOOL isToday;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItemMyMenu;
@end
