//
//  SignInViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/12/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDSignInViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbAreaCode;
@property (weak, nonatomic) IBOutlet UITextField *tfPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignin;
@property (weak, nonatomic) IBOutlet UIButton *btnShowHide;
@property (weak, nonatomic) IBOutlet UIView *viewSearch;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;

- (IBAction)touchDown:(id)sender;
- (IBAction)clickedSignin:(id)sender;
- (IBAction)textFieldDidChaned:(id)sender;
- (IBAction)clickedShowHide:(id)sender;
- (IBAction)clickedForgot:(id)sender;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedCountry:(id)sender;

@end
