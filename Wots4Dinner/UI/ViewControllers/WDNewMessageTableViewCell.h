//
//  WDNewMessageTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDNewMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UIButton *btnChecked;

@end
