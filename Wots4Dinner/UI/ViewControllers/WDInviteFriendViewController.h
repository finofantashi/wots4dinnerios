//
//  InviteFriendViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDInviteFriendViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnInviteContact;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
- (IBAction)clickedInvite:(id)sender;
- (IBAction)clickedShare:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

@end
