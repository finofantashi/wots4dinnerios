//
//  WDBecomeSellerStepTwoTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBecomeSellerStepTwoTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "EBPhotoPagesController.h"
#import "EBPhotoPagesDataSource.h"
#import "EBPhotoPagesDelegate.h"
#import "UIColor+Style.h"
#import "TOCropViewController.h"
#import "SBPickerSelector.h"
#import "LEAlertController.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDFoodType.h"
#import "WDDish.h"
#import "UIImage+ResizeMagick.h"
#import "WDMainViewController.h"
#import "WDBecomeSellerStepThreeTableViewController.h"
#import "WDSourceConfig.h"
#import "SKTaskManager.h"
#import "WDCurrency.h"
#import "WDGallery.h"
#import "WDUserTemp.h"
#import "WDDelivery.h"

@interface WDBecomeSellerStepTwoTableViewController ()<UINavigationControllerDelegate, EBPhotoPagesDataSource, EBPhotoPagesDelegate, UIImagePickerControllerDelegate, TOCropViewControllerDelegate, SBPickerSelectorDelegate>
@property (nonatomic, strong) NSMutableArray *dishImages;
@property (nonatomic, strong) NSMutableArray *dishURLImages;

@property (nonatomic, strong) NSMutableArray *arrTasks;

@property (nonatomic, strong) UIImage *image;           // The image we'll be cropping

@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (nonatomic, assign) CGRect croppedFrame;
@property (strong, nonatomic) SBPickerSelector *pickerTimeFrom;
@property (strong, nonatomic) SBPickerSelector *pickerTimeTo;
@property (strong, nonatomic) SBPickerSelector *pickerServingDate;

@property (strong, nonatomic) NSDate *fromDate;
@property (strong, nonatomic) NSDate *toDate;
@property (strong, nonatomic) NSDate *servingDate;
@property (nonatomic, strong) WDDish *dish;

@end

@implementation WDBecomeSellerStepTwoTableViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  if(self.isFromSlideMenu){
    self.lbTitle.text = @"Step 1: Add Your Dish";
    self.barBtnBack.image = [UIImage imageNamed:@"ic_menu"];
  }
  
  if(self.isUpdate){
    self.lbTitle.text = @"Step 1: Update Your Dish";
    self.barBtnBack.image = [UIImage imageNamed:@"ic_back"];
    self.title = @"Edit Dish";
  }
  self.navigationItem.title = @"Dish";
  
  self.btnNext.layer.cornerRadius = self.btnNext.frame.size.height/2; // this value vary as per your desire
  self.btnNext.layer.borderWidth = 1.0f;
  self.btnNext.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
  self.btnNext.clipsToBounds = YES;
  
  self.btnAddMore.layer.cornerRadius = self.btnAddMore.frame.size.height/2; // this value vary as per your desire
  self.btnAddMore.layer.borderWidth = 1.0f;
  self.btnAddMore.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
  self.btnAddMore.clipsToBounds = YES;
  
  self.dishImages = [[NSMutableArray alloc] init];
  self.dishURLImages = [[NSMutableArray alloc] init];
  self.arrTasks = [[NSMutableArray alloc] init];
  
  UITapGestureRecognizer *dishImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectViewDishImage:)];
  dishImageTap.numberOfTapsRequired = 1;
  [self.ivDish setUserInteractionEnabled:YES];
  [self.ivDish addGestureRecognizer:dishImageTap];
  
  UITapGestureRecognizer *pickupFromTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedPickupTimeFrom:)];
  dishImageTap.numberOfTapsRequired = 1;
  [self.viewPickFrom setUserInteractionEnabled:YES];
  [self.viewPickFrom addGestureRecognizer:pickupFromTap];
  
  
  UITapGestureRecognizer *pickupToTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedPickupTimeTo:)];
  dishImageTap.numberOfTapsRequired = 1;
  [self.viewPickTo setUserInteractionEnabled:YES];
  [self.viewPickTo addGestureRecognizer:pickupToTap];
  
  UITapGestureRecognizer *servingDateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedServingDate:)];
  servingDateTap.numberOfTapsRequired = 1;
  [self.viewServingDate setUserInteractionEnabled:YES];
  [self.viewServingDate addGestureRecognizer:servingDateTap];
  
  [self setupPickerCurrency];
  [self getListFoodTypeAPI];
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"hh:mm a"];
  NSDate *date = [NSDate date];
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSDateComponents *components = [calendar components: NSUIntegerMax fromDate: date];
  [components setHour: 8];
  [components setMinute: 0];
  [components setSecond: 0];
  self.fromDate = [calendar dateFromComponents: components];
  self.tfPickFrom.text = [dateFormat stringFromDate:self.fromDate];
  
  [components setHour: 22];
  [components setMinute: 0];
  [components setSecond: 0];
  self.toDate = [calendar dateFromComponents: components];
  self.tfPickTo.text = [dateFormat stringFromDate:self.toDate];
  
  [self setupRepeat];
  
  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
  [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Private
- (void) updateLayout {
  if(self.dishImages.count<4){
    [self.btnAddMore setEnabled:YES];
  }else{
    [self.btnAddMore setEnabled:NO];
  }
  if(self.dishImages.count>0){
    WDGallery *gallery = [self.dishImages lastObject];
    [self.ivDish setImage:gallery.image];
  }else{
    [self.ivDish setImage:[UIImage imageNamed:@"placeholder.jpg"]];
  }
}

- (void) setData:(WDDish*) dish{
  NSDictionary *localeInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"USD", NSLocaleCurrencyCode,
                              [[NSLocale preferredLanguages] objectAtIndex:0], NSLocaleLanguageCode,
                              nil];
  NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:
                      [NSLocale localeIdentifierFromComponents:localeInfo]];
  NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
  [fmt setNumberStyle:NSNumberFormatterCurrencyStyle];
  [fmt setLocale:locale];
  [fmt setCurrencySymbol:@""];
  
  self.tfDishName.text = dish.dishName;
  self.tfPrice.text = [fmt stringFromNumber:[NSNumber numberWithFloat:dish.price]];
  self.pickerCurrency.text = [self getCurrencyName:dish.currencyID];
  self.lbPrice.text = [self getCurrencySymbol:self.pickerCurrency.text];
  self.tfQuantity.text = [NSString stringWithFormat:@"%ld",dish.quantity];
  self.pickerFoodType.text = [self getfoodTypeName:dish.foodTypeID];
  self.tvDescription.text = dish.dishDescription;
  [self.tvDescription setContentOffset:CGPointZero animated:NO];
  
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  dateFormatter.dateFormat = @"HH:mm:ss";
  
  NSDate *dateFrom = [dateFormatter dateFromString:dish.pickupTimeFrom];
  NSDate *dateTo = [dateFormatter dateFromString:dish.pickupTimeTo];
  dateFormatter.dateFormat = @"hh:mm a";
  NSString *strDateFrom = [dateFormatter stringFromDate:dateFrom];
  NSString *strDateTo = [dateFormatter stringFromDate:dateTo];
  self.tfPickTo.text = strDateTo;
  self.tfPickFrom.text = strDateFrom;
  _pickerTimeFrom.defaultDate = dateFrom;
  _pickerTimeTo.defaultDate = dateTo;
  dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
  NSDate *servingDate = [dateFormatter dateFromString:dish.servingDate];
  dateFormatter.dateFormat = @"yyyy-MM-dd";
  NSString *strServingDate = [dateFormatter stringFromDate:servingDate];
  self.tfDate.text = strServingDate;
  _pickerServingDate.defaultDate = servingDate;
  
  if([dish.repeat rangeOfString:@"Monday"].location != NSNotFound){
    [self.rbtnMonday setSelected:YES];
  }
  for (DLRadioButton *object in self.rbtnMonday.otherButtons) {
    if([object.titleLabel.text isEqualToString:@"Tue"]&&
       [dish.repeat rangeOfString:@"Tuesday"].location != NSNotFound){
      [object setSelected:YES];
    }
    if([object.titleLabel.text isEqualToString:@"Wed"]&&
       [dish.repeat rangeOfString:@"Wednesday"].location != NSNotFound){
      [object setSelected:YES];
    }
    if([object.titleLabel.text isEqualToString:@"Thu"]&&
       [dish.repeat rangeOfString:@"Thursday"].location != NSNotFound){
      [object setSelected:YES];
    }
    if([object.titleLabel.text isEqualToString:@"Fri"]&&
       [dish.repeat rangeOfString:@"Friday"].location != NSNotFound){
      [object setSelected:YES];
    }
    if([object.titleLabel.text isEqualToString:@"Sat"]&&
       [dish.repeat rangeOfString:@"Saturday"].location != NSNotFound){
      [object setSelected:YES];
    }
    if([object.titleLabel.text isEqualToString:@"Sun"]&&
       [dish.repeat rangeOfString:@"Sunday"].location != NSNotFound){
      [object setSelected:YES];
    }
  }
}

- (void)dismissKeyboard {
  [self.tfDishName resignFirstResponder];
  [self.tfQuantity resignFirstResponder];
  [self.tvDescription resignFirstResponder];
}

- (void)setupRepeat {
  self.rbtnMonday.multipleSelectionEnabled = YES;
}

- (void)setupPickerCurrency {
  // bind yourTextField to DownPicker
  UIColor *color = [UIColor colorFromHexString:@"636363" withAlpha:1.0]; // select needed color
  NSString *string = @"Currency"; // the string to colorize
  NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
  NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];
  self.picker1 = [[DownPicker alloc] initWithTextField:self.pickerCurrency  withData:self.currenciesData];
  [self.pickerCurrency setFont:[UIFont fontWithName:@"Poppins-Medium" size:14]];
  [self.pickerCurrency setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:1.0]];
  [self.picker1  setPlaceholder:@"Currency"];
  [self.picker1  setAttributedPlaceholder:attrStr];
  [self.picker1 addTarget:self
                   action:@selector(currencySelected:)
         forControlEvents:UIControlEventValueChanged];
}

- (void)setupPickerFoodType {
  // bind yourTextField to DownPicker
  // bind yourTextField to DownPicker
  UIColor *color = [UIColor colorFromHexString:@"636363" withAlpha:1.0]; // select needed color
  NSString *string = @"Food Type"; // the string to colorize
  NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
  NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];
  self.picker2 = [[DownPicker alloc] initWithTextField:self.pickerFoodType  withData:self.foodTypesData];
  [self.pickerFoodType setFont:[UIFont fontWithName:@"Poppins-Medium" size:14]];
  [self.pickerFoodType setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:1.0]];
  [self.picker2 setPlaceholder:@"Food Type"];
  [self.picker2 setAttributedPlaceholder:attrStr];
  [self.picker2 addTarget:self
                   action:@selector(foodTypeSelected:)
         forControlEvents:UIControlEventValueChanged];
}

- (NSInteger)getfoodTypeID:(NSString*)foodTypeName {
  for (WDFoodType *object in self.foodTypes) {
    if([object.foodTypeName isEqualToString:foodTypeName]){
      return object.foodTypeID;
    }
  }
  return 0;
}

- (NSString*)getfoodTypeName:(NSInteger)foodTypeID {
  for (WDFoodType *object in self.foodTypes) {
    if(object.foodTypeID == foodTypeID){
      return object.foodTypeName;
    }
  }
  return @"";
}

- (NSInteger)getCurrencyID:(NSString*)currencyCode {
  for (WDCurrency *object in self.currencies) {
    if([object.code isEqualToString:currencyCode]){
      return object.currencyID;
    }
  }
  return 0;
}

- (NSString*)getCurrencyName:(NSInteger)currencyID {
  for (WDCurrency *object in self.currencies) {
    if(object.currencyID == currencyID){
      return object.code;
    }
  }
  return @"";
}

- (NSString*)getCurrencySymbol:(NSString*)currencyCode {
  for (WDCurrency *object in self.currencies) {
    if([object.code isEqualToString:currencyCode]){
      return object.symbol;
    }
  }
  
  return @"";
}

- (BOOL)validate {
  if(self.dishImages.count == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Dish image is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tfDishName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Dish name is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tfPrice.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Price is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.pickerCurrency.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Currency is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tfQuantity.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Quantity is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.pickerFoodType.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Foodtype is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tfPickFrom.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Pick-up time from is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tfPickTo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Pick-up time to is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tfDate.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Date is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  return YES;
}

#pragma mark - SBPickerSelectorDelegate

-(void) pickerSelector:(SBPickerSelector *)selector dateSelected:(NSDate *)date{
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"hh:mm a"];
  
  unsigned int flags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
  NSCalendar* calendar = [NSCalendar currentCalendar];
  NSDateComponents* component1 = [calendar components:flags fromDate:date];
  NSDate* date1 = [calendar dateFromComponents:component1];
  
  if(selector == self.pickerTimeFrom){
    NSDateComponents* component2 = [calendar components:flags fromDate:self.toDate];
    NSDate* date2 = [calendar dateFromComponents:component2];
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending||result == NSOrderedSame) {
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"\"To\" time must be greater than \"From\" time. Please try again!"] preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
      
    } else {
      self.tfPickFrom.text = [dateFormat stringFromDate:date];
    }
    
  }else if(selector == self.pickerTimeTo){
    NSDateComponents* component2 = [calendar components:flags fromDate:self.fromDate];
    NSDate* date2 = [calendar dateFromComponents:component2];
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedAscending||result == NSOrderedSame) {
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"\"To\" time must be greater than \"From\" time. Please try again!"] preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
      
    } else {
      self.tfPickTo.text = [dateFormat stringFromDate:date];
    }
  }else{
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    self.tfDate.text = [dateFormat stringFromDate:date];
  }
  
  [self.tableView reloadData];
}

#pragma mark - Image Picker Delegate -

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
  TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:image];
  cropController.delegate = self;
  cropController.aspectRatioLockEnabled = YES;
  cropController.resetAspectRatioEnabled = NO;
  cropController.customAspectRatio = CGSizeMake(1200.0, 855.0);
  cropController.aspectRatioPreset  = TOCropViewControllerAspectRatioPresetCustom;
  cropController.toolbarPosition = TOCropViewControllerToolbarPositionBottom;
  self.image = image;
  
  //If profile picture, push onto the same navigation stack
  if (self.croppingStyle == TOCropViewCroppingStyleDefault) {
    [picker pushViewController:cropController animated:YES];
  }else { //otherwise dismiss, and then present from the main controller
    [picker dismissViewControllerAnimated:YES completion:^{
      [self presentViewController:cropController animated:YES completion:nil];
    }];
  }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Cropper Delegate -

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
  self.croppedFrame = cropRect;
  [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
  self.croppedFrame = cropRect;
  [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController {
  WDGallery *gallery = [[WDGallery alloc] init];
  gallery.galleryID = -1;
  gallery.image = [image resizedImageWithMaximumSize:CGSizeMake(1200, 515)];
  [self.dishImages addObject:gallery];
  [self.ivDish setImage:image];
  if(self.dishImages.count<4){
    [self.btnAddMore setEnabled:YES];
  }else{
    [self.btnAddMore setEnabled:NO];
  }
  //[self uploadAvatar:image];
  [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Bar Button Items -

- (void)showCropViewController {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
  UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Take Photo"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];
  }];
  
  UIAlertAction *profileAction = [UIAlertAction actionWithTitle:@"Choose Photo"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *profilePicker = [[UIImagePickerController alloc] init];
    profilePicker.modalPresentationStyle = UIModalPresentationPopover;
    profilePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    profilePicker.allowsEditing = NO;
    profilePicker.delegate = self;
    profilePicker.preferredContentSize = CGSizeMake(512,512);
    [self presentViewController:profilePicker animated:YES completion:nil];
  }];
  
  UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleCancel
                                                 handler:^(UIAlertAction *action) {
    
  }];
  
  [alertController addAction:defaultAction];
  [alertController addAction:profileAction];
  [alertController addAction:cancel];
  [alertController setModalPresentationStyle:UIModalPresentationPopover];
  [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
  if(self.isFromSlideMenu&&!self.isUpdate){
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
      [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
  }else{
    [self.navigationController popViewControllerAnimated:YES];
  }
}

- (IBAction)clickedAddMore:(id)sender {
  [self showCropViewController];
}

- (IBAction)clickedNext:(id)sender {
  if([self validate]){
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //Set 12 hours date format
    dateFormatter.dateFormat = @"hh:mm a";
    
    NSDate *dateFrom = [dateFormatter dateFromString:self.tfPickFrom.text];
    NSDate *dateTo = [dateFormatter dateFromString:self.tfPickTo.text];
    //Set 24 hours date format
    dateFormatter.dateFormat = @"HH:mm";
    NSString *strDateFrom;
    NSString *strDateTo;
    if(dateFrom){
      strDateFrom = [dateFormatter stringFromDate:dateFrom];
    }
    if(dateTo){
      strDateTo= [dateFormatter stringFromDate:dateTo];
    }
    WDDish *dish = [[WDDish alloc] init];
    dish.dishName = self.tfDishName.text;
    dish.foodTypeID = [self getfoodTypeID:self.pickerFoodType.text];
    dish.currencyID = [self getCurrencyID:self.pickerCurrency.text];
    dish.price = [self.tfPrice.text floatValue];
    dish.quantity = [self.tfQuantity.text integerValue];
    dish.dishDescription = self.tvDescription.text;
    dish.pickupTimeFrom = strDateFrom;
    dish.pickupTimeTo = strDateTo;
    dish.servingDate = self.tfDate.text;
    NSString *repeat = @"";
    if (self.rbtnMonday.isMultipleSelectionEnabled) {
      for (DLRadioButton *button in self.rbtnMonday.selectedButtons) {
        if([button.titleLabel.text isEqualToString:@"Mon"]){
          repeat = [repeat stringByAppendingString:@"Monday"];
        }
        if([button.titleLabel.text isEqualToString:@"Tue"]){
          repeat = [repeat stringByAppendingString:@" Tuesday"];
        }
        if([button.titleLabel.text isEqualToString:@"Wed"]){
          repeat = [repeat stringByAppendingString:@" Wednesday"];
        }
        if([button.titleLabel.text isEqualToString:@"Thu"]){
          repeat = [repeat stringByAppendingString:@" Thursday"];
        }
        if([button.titleLabel.text isEqualToString:@"Fri"]){
          repeat = [repeat stringByAppendingString:@" Friday"];
        }
        if([button.titleLabel.text isEqualToString:@"Sat"]){
          repeat = [repeat stringByAppendingString:@" Saturday"];
        }
        if([button.titleLabel.text isEqualToString:@"Sun"]){
          repeat = [repeat stringByAppendingString:@" Sunday"];
        }
      }
      
    }
    if(repeat.length>0){
      repeat = [repeat stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
      repeat = [repeat stringByReplacingOccurrencesOfString:@" " withString:@", "];
    }
    dish.repeat = repeat;
    UIImage *dishImage;
    NSMutableArray *galleries = [[NSMutableArray alloc] init];
    for (WDGallery *object in self.dishImages) {
      if(object.galleryID==0){
        dishImage = object.image;
      }else{
        [galleries addObject:object.image];
      }
    }
    
    if(!dishImage){
      dishImage = ((WDGallery *)[self.dishImages objectAtIndex:0]).image;
      [galleries removeObjectAtIndex:0];
    }
    
    [self addDish:dish image:dishImage galleries:self.dishImages];
  }
}

- (IBAction)selectedRepeat:(DLRadioButton *)radioButton {
  
}

- (IBAction)didSelectViewDishImage:(id)sender {
  if(self.dishImages.count>0){
    EBPhotoPagesController *photoPagesController = [[EBPhotoPagesController alloc] initWithDataSource:self delegate:self];
    [self presentViewController:photoPagesController animated:YES completion:nil];
  }else{
    [self clickedAddMore:nil];
  }
}

- (IBAction)clickedPickupTimeFrom:(id)sender {
  [self dismissKeyboard];
  _pickerTimeFrom = [SBPickerSelector picker];
  _pickerTimeFrom.delegate = self;
  _pickerTimeFrom.doneButtonTitle = @"Done";
  _pickerTimeFrom.cancelButtonTitle = @"Cancel";
  _pickerTimeFrom.pickerType = SBPickerSelectorTypeDate;
  _pickerTimeFrom.datePickerType = SBPickerSelectorDateTypeOnlyHour;
  _pickerTimeFrom.defaultDate = self.fromDate;
  [_pickerTimeFrom showPickerOver:self];
}

- (IBAction)clickedPickupTimeTo:(id)sender {
  [self dismissKeyboard];
  _pickerTimeTo= [SBPickerSelector picker];
  _pickerTimeTo.delegate = self;
  _pickerTimeTo.doneButtonTitle = @"Done";
  _pickerTimeTo.cancelButtonTitle = @"Cancel";
  _pickerTimeTo.pickerType = SBPickerSelectorTypeDate;
  _pickerTimeTo.datePickerType = SBPickerSelectorDateTypeOnlyHour;
  _pickerTimeTo.defaultDate = self.toDate;
  [_pickerTimeTo showPickerOver:self];
}

- (IBAction)clickedServingDate:(id)sender {
  [self dismissKeyboard];
  _pickerServingDate= [SBPickerSelector picker];
  _pickerServingDate.delegate = self;
  _pickerServingDate.doneButtonTitle = @"Done";
  _pickerServingDate.cancelButtonTitle = @"Cancel";
  _pickerServingDate.pickerType = SBPickerSelectorTypeDate;
  _pickerServingDate.datePickerType = SBPickerSelectorDateTypeOnlyDay;
  if(self.servingDate){
    _pickerServingDate.defaultDate = self.servingDate;
  }
  [_pickerServingDate showPickerOver:self];
}

#pragma mark - EBPhotoPagesDataSource

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
    shouldExpectPhotoAtIndex:(NSInteger)index {
  if(index < self.dishImages.count){
    return YES;
  }
  
  return NO;
}

- (void)photoPagesController:(EBPhotoPagesController *)controller
                imageAtIndex:(NSInteger)index
           completionHandler:(void (^)(UIImage *))handler {
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  dispatch_async(queue, ^{
    WDGallery *gallery = [self.dishImages objectAtIndex:index];
    UIImage *image = gallery.image;
    handler(image);
  });
}

- (void)photoPagesController:(EBPhotoPagesController *)photoPagesController
       didDeletePhotoAtIndex:(NSInteger)index {
  WDGallery *gallery = [self.dishImages objectAtIndex:index];
  if(gallery.galleryID > 0){
    [self deleteImageDish:gallery isDelete:YES];
  }else if(gallery.galleryID==0){
    [self.dishImages removeObjectAtIndex:index];
    if(self.dishImages.count>0){
      for (WDGallery *object in self.dishImages) {
        if(object.galleryID > 0){
          [self deleteImageDish:object isDelete:NO];
          break;
        }
      }
    }
    [self updateLayout];
  }else{
    [self.dishImages removeObjectAtIndex:index];
    [self updateLayout];
  }
  
}

#pragma mark - User Permissions

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowTaggingForPhotoAtIndex:(NSInteger)index {
  return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)controller
 shouldAllowDeleteForComment:(id<EBPhotoCommentProtocol>)comment
             forPhotoAtIndex:(NSInteger)index {
  return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowCommentingForPhotoAtIndex:(NSInteger)index {
  return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowActivitiesForPhotoAtIndex:(NSInteger)index {
  if(!self.dishImages.count){
    return NO;
  }
  return YES;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowMiscActionsForPhotoAtIndex:(NSInteger)index {
  if(!self.dishImages.count){
    return NO;
  }
  return YES;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowDeleteForPhotoAtIndex:(NSInteger)index {
  if(!self.dishImages.count){
    return NO;
  }
  return YES;
  
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
     shouldAllowDeleteForTag:(EBTagPopover *)tagPopover
              inPhotoAtIndex:(NSInteger)index {
  return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController
    shouldAllowEditingForTag:(EBTagPopover *)tagPopover
              inPhotoAtIndex:(NSInteger)index {
  return NO;
}

- (BOOL)photoPagesController:(EBPhotoPagesController *)photoPagesController shouldAllowReportForPhotoAtIndex:(NSInteger)index {
  return NO;
}

#pragma mark - EBPPhotoPagesDelegate

- (void)photoPagesControllerDidDismiss:(EBPhotoPagesController *)photoPagesController{
  NSLog(@"Finished using %@", photoPagesController);
}

#pragma mark - Picker

-(void)currencySelected:(id)dp {
  self.lbPrice.text = [self getCurrencySymbol:self.pickerCurrency.text];
}
-(void)foodTypeSelected:(id)dp {
  // do what you want
}

#pragma mark - API

- (void) getListFoodTypeAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
      [self getCurrenciesAPI];
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getFoodTypes:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          [self getCurrenciesAPI];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        [self getCurrenciesAPI];
        if(responseObject) {
          self.foodTypes = [[NSMutableArray alloc] init];
          self.foodTypesData = [[NSMutableArray alloc] init];
          
          NSDictionary *dataFoodType = [[responseObject valueForKey:@"data"] valueForKey:@"food_types"];
          for (NSDictionary *object in dataFoodType) {
            WDFoodType *foodType = [[WDFoodType alloc] init];
            [foodType setFoodTypeObject:object];
            [self.foodTypes addObject:foodType];
            [self.foodTypesData addObject:foodType.foodTypeName];
          };
          [self setupPickerFoodType];
          [self.tableView reloadData];
        }
      }
    }];
  }
}

- (void) getCurrenciesAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getCurrencies:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          if(self.isUpdate){
            [self loadData];
          }
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          if(self.isUpdate){
            [self loadData];
          }
          self.currencies = [[NSMutableArray alloc] init];
          self.currenciesData = [[NSMutableArray alloc] init];
          
          NSDictionary *dataCurrencies = [responseObject valueForKey:@"data"];
          for (NSDictionary *object in dataCurrencies) {
            WDCurrency *currency = [[WDCurrency alloc] init];
            [currency setCurrencyObject:object];
            [self.currencies addObject:currency];
            [self.currenciesData addObject:currency.code];
          };
          [self setupPickerCurrency];
          [self.tableView reloadData];
        }
      }
    }];
  }
}

- (void)addDish:(WDDish*)dish
          image:(UIImage*)image
      galleries:(NSMutableArray*)galleries {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    if(self.dishID!=0){
      [[WDFoodMenuAPIController sharedInstance] updateDish:self.dishID dish:dish image:image galleries:galleries completion:^(NSString *errorString, id responseObject) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        if(errorString){
          LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
          LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
          }];
          [alertController addAction:defaultAction];
          [self presentAlertController:alertController animated:YES completion:nil];
        }else{
          if(responseObject) {
            self.dishID = [[[responseObject valueForKey:@"data"] valueForKey:@"id"] integerValue];
            WDBecomeSellerStepThreeTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepThreeTableViewController"];
            viewController.dishID = self.dishID;
            viewController.isFromSlideMenu = self.isFromSlideMenu;
            viewController.isUpdate = self.isUpdate;
            viewController.currencyID = [self getCurrencyID:self.pickerCurrency.text];
            
            if (self.dish.deliveries.count > 0) {
              WDDelivery *delivery = [self.dish.deliveries objectAtIndex:0];
              viewController.specialOffer = delivery.specialOffer;
            } else if (self.dish.user.delivery != nil) {
              viewController.specialOffer = self.dish.user.delivery.specialOffer;
            }
            [self.navigationController pushViewController:viewController animated:YES];
          }
        }
      }];
      
    }else{
      [[WDFoodMenuAPIController sharedInstance] addDish:dish image:image galleries:galleries completion:^(NSString *errorString, id responseObject) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        if(errorString){
          LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
          LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
          }];
          [alertController addAction:defaultAction];
          [self presentAlertController:alertController animated:YES completion:nil];
        }else{
          if(responseObject) {
            self.dishID = [[[responseObject valueForKey:@"data"] valueForKey:@"id"] integerValue];
            WDBecomeSellerStepThreeTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepThreeTableViewController"];
            viewController.dishID = self.dishID;
            viewController.isFromSlideMenu = self.isFromSlideMenu;
            viewController.isUpdate = self.isUpdate;
            viewController.currencyID = [self getCurrencyID:self.pickerCurrency.text];
            [self.navigationController pushViewController:viewController animated:YES];
          }
        }
      }];
    }
  }
}

- (void)loadData {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getDishDetail:self.dishID completionBlock:^(NSString *errorString, id responseObject) {
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      }else{
        if(responseObject) {
          NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"dish"];
          NSDictionary *dataGalleries = [dataDish valueForKey:@"galleries"];
          
          self.dish = [[WDDish alloc] init];
          [self.dish  setDishObject:dataDish];
          [self setData:self.dish];
          
          [self.dishURLImages removeAllObjects];
          
          NSString *stringURL = [NSString stringWithFormat:@"%@%@",kImageDomain,self.dish.dishImage];
          WDGallery *gallery = [[WDGallery alloc] init];
          gallery.galleryID = 0;
          gallery.galleryImage = [NSURL URLWithString:stringURL];
          
          [self.dishURLImages addObject:gallery];
          for (NSDictionary *object in dataGalleries) {
            WDGallery *objectGallery = [[WDGallery alloc] init];
            [objectGallery setGalleryObject:object];
            [self.dishURLImages addObject:objectGallery];
          }
          
          if(self.dishURLImages.count>0){
            [self downloadImages];
          }else{
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            dispatch_async(dispatch_get_main_queue(), ^{
              [self.tableView reloadData];
            });
          }
        }
      }
    }];
  }
}

- (void) downloadImages {
  [self.dishImages removeAllObjects];
  for (int i=0; i<self.dishURLImages.count; i++) {
    //create tasks and add all task to array
    SKTask *task=[SKTask taskWithBlock:^(id result, BlockTaskCompletion completion) {
      WDGallery *gallery = [self.dishURLImages objectAtIndex:i];
      NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",gallery.galleryImage]];
      NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
      AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
      manager.responseSerializer = [AFHTTPResponseSerializer serializer];
      
      NSURLRequest *request = [NSURLRequest requestWithURL:URL];
      
      NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
          NSLog(@"Error: %@", error);
        } else {
          WDGallery *galleryChild = [[WDGallery alloc] init];
          galleryChild.galleryID = gallery.galleryID;
          galleryChild.image =  [UIImage imageWithData:responseObject];
          [self.dishImages addObject:galleryChild];
        }
        completion(nil);
      }];
      [dataTask resume];
    }];
    [_arrTasks addObject:task];
  }
  
  [SKTaskManager parallerOperations:_arrTasks completion:^{
    if(self.dishImages.count>0){
      WDGallery *gallery = [self.dishImages lastObject];
      [self.ivDish setImage:gallery.image];
    }
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.tableView reloadData];
    });
  }];
}

- (void)deleteImageDish:(WDGallery*)gallery isDelete:(BOOL)isDelete{
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] deleteImageDish:gallery.galleryID completion:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          if(isDelete){
            [self.dishImages removeObject:gallery];
            [self updateLayout];
          }else{
            ((WDGallery*)[self.dishImages objectAtIndex:0]).galleryID = 0;
          }
        }
      }
    }];
  }
}
@end
