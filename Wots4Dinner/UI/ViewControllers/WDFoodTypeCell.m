//
//  WDFoodTypeCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDFoodTypeCell.h"

@implementation WDFoodTypeCell
- (void)awakeFromNib {
    [super awakeFromNib];
    self.clipsToBounds = YES;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.contentView.translatesAutoresizingMaskIntoConstraints = YES;
    self.ivFoodType.clipsToBounds = NO;
    
    self.lbFoodType.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.lbFoodType.layer.shadowRadius = 2.0f;
    self.lbFoodType.layer.shadowOpacity = .9;
    self.lbFoodType.layer.shadowOffset = CGSizeZero;
    self.lbFoodType.layer.masksToBounds = NO;
    
//    self.lbDishCount.layer.shadowColor = [[UIColor blackColor] CGColor];
//    self.lbDishCount.layer.shadowRadius = 2.0f;
//    self.lbDishCount.layer.shadowOpacity = .9;
//    self.lbDishCount.layer.shadowOffset = CGSizeZero;
//    self.lbDishCount.layer.masksToBounds = NO;
    
    self.lbViewDishCount.layer.cornerRadius = 10;
    self.lbViewDishCount.layer.borderWidth = 0.0f;
   self.lbViewDishCount.clipsToBounds = YES;

}

- (void)setImageOffset:(CGPoint)imageOffset {
    // Store padding value
    _imageOffset = imageOffset;
    
    // Grow image view
    CGRect frame = self.ivFoodType.bounds;
    CGRect offsetFrame = CGRectOffset(frame, _imageOffset.x, _imageOffset.y);
    self.ivFoodType.frame = offsetFrame;
}

@end
