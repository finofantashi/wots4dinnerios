//
//  WDSearchTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MARKRangeSlider.h"

@interface WDSearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet MARKRangeSlider *sliderDistance;
@property (weak, nonatomic) IBOutlet UILabel *lbPriceMax;
@property (weak, nonatomic) IBOutlet UILabel *lbPriceMin;
@property (weak, nonatomic) IBOutlet UILabel *lbDistance;
@property (weak, nonatomic) IBOutlet MARKRangeSlider *sliderPrice;

@end
