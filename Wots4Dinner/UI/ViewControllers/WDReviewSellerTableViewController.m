//
//  WDReviewSellerTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/27/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDReviewSellerTableViewController.h"
#import "WDUserProfileViewController.h"
#import "WDSellerProfileViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDReviewAPIController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "WDUserTemp.h"
#import "WDSourceConfig.h"
#import "WDReviewTableViewCell.h"
#import "WDReview.h"

@interface WDReviewSellerTableViewController () <WDReviewTableViewCellDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray *listReview;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;
@end

@implementation WDReviewSellerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.listReview = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    [self loadData:self.currentPage];
}

# pragma mark - Cell Setup

- (void)clickedAvatar:(WDUserTemp *)user {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    if(user.roleID == 2){
        WDSellerProfileViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDSellerProfileViewController"];
        viewController.user = user;
        [self.navigationController pushViewController:viewController animated:YES];
    }else{
        WDUserProfileViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDUserProfileViewController"];
        viewController.user = user;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)setUpCell:(WDReviewTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

# pragma mark - UITableViewControllerDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.currentPage == self.totalPages
        || self.totalItems == self.listReview.count) {
        return self.listReview.count;
    }
    return self.listReview.count + 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentPage != self.totalPages && indexPath.row == [self.listReview count] - 1 ) {
        [self loadData:++self.currentPage];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDReviewTableViewCell";
    UITableViewCell *cell;
    if (indexPath.row == [self.listReview count]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:100];
        [activityIndicator startAnimating];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        WDReview *review = self.listReview[indexPath.row];
        ((WDReviewTableViewCell*)cell).user = review.user;
        //Avatar
        ((WDReviewTableViewCell*)cell).ivAvatar.layer.cornerRadius = 70 / 2;
        ((WDReviewTableViewCell*)cell).ivAvatar.layer.masksToBounds = YES;
        ((WDReviewTableViewCell*)cell).ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
        ((WDReviewTableViewCell*)cell).ivAvatar.layer.borderWidth = 2.0f;
        [((WDReviewTableViewCell*)cell).ivAvatar setNeedsDisplay];
        
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,review.user.avatarURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        [((WDReviewTableViewCell*)cell).ivAvatar sd_setImageWithURL:[NSURL URLWithString:stringURL] placeholderImage:[UIImage imageNamed:@"empty_avatar"]];
        if(review.user.firstName&&review.user.lastName){
            ((WDReviewTableViewCell*)cell).lbName.text = [NSString stringWithFormat:@"%@ %@",review.user.firstName,review.user.lastName];
        }
        
        ((WDReviewTableViewCell*)cell).lbLocation.text = review.user.living;
        
        ((WDReviewTableViewCell*)cell).lbDescription.text = review.review;
        ((WDReviewTableViewCell*)cell).viewRatingSeller.value = review.user.rating;
        ((WDReviewTableViewCell*)cell).viewRatingForThisSeller.value = review.rating;
        if(review.user.roleID != 2){
            [((WDReviewTableViewCell*)cell).viewRatingSeller setHidden:YES];
        }else{
            [((WDReviewTableViewCell*)cell).viewRatingSeller setHidden:NO];
        }
        
        //Date Time
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:review.updatedAt==nil?@"":review.updatedAt];
        NSString *stringDate = [NSDateFormatter localizedStringFromDate:dateFromString
                                                              dateStyle:NSDateFormatterMediumStyle
                                                              timeStyle:NSDateFormatterNoStyle];
        if(!stringDate){
            stringDate = @"";
        }
        ((WDReviewTableViewCell*)cell).lbDate.text = [NSString stringWithFormat:@"%@",stringDate];
        ((WDReviewTableViewCell*)cell).delegate = self;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != [self.listReview count]) {
        WDReview *review = self.listReview[indexPath.row];
        float sizeHeight = 120 + [self sizeOfMultiLineLabel:review.review];
        return sizeHeight;
    }
    return 44;
}

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:14];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.view.frame.size.width-16;
    CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{
                                                                 NSFontAttributeName : aLabelFont
                                                                 }
                                                       context:nil].size;
    return labelSize.height;
}

- (void)loadData:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDReviewAPIController sharedInstance] getListSellerReview:self.sellerID page:(long)page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDReview *review = [[WDReview alloc] init];
                        [review setReviewObject:object isSeller:YES];
                        [self.listReview addObject:review];
                    };
                    
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==1){
                        self.title = [NSString stringWithFormat:@"%ld Review", self.totalItems];
                    }else{
                        self.title = [NSString stringWithFormat:@"%ld Reviews", self.totalItems];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

@end
