//
//  PayPalMethodTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "PayPalMethodTableViewController.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDCurrency.h"
#import "NSString+IsValidEmail.h"
#import "WDBecomeSellerStepOneTableViewController.h"
#import "WDOrderAPIController.h"
#import "WDPayoutMethodTableViewController.h"

@interface PayPalMethodTableViewController ()

@end

@implementation PayPalMethodTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnFinish.layer.cornerRadius = self.btnFinish.frame.size.height/2; // this value vary as per your desire
    self.btnFinish.layer.borderWidth = 1.0f;
    self.btnFinish.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
    self.btnFinish.clipsToBounds = YES;
    [self getListCurrency];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickPaypal:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.tvPaypal addGestureRecognizer:tapGestureRecognizer];
    self.tvPaypal.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setData {
    if(!self.payoutMethod.currency&&![self.payoutMethod.currency isEqualToString:@""]){
        self.pickerCurrency.text = self.payoutMethod.currency;
    }
    if(!self.payoutMethod.account&&![self.payoutMethod.account isEqualToString:@""]&&[self.payoutMethod.account isValidEmail]){
        self.tfEmail.text = self.payoutMethod.account;
    }
}

- (void)setupPickerCurrency {
    self.picker = [[DownPicker alloc] initWithTextField:self.pickerCurrency withData:self.currenciessData];
    [self.pickerCurrency setFont:[UIFont fontWithName:@"Poppins-Medium" size:14]];
    [self.pickerCurrency setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:1.0]];
    [self.picker setPlaceholder:@"Tap to choose a currency"];
}

-(void)clickPaypal:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://www.paypal.com"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (BOOL)validate {
    if([[self.tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Email is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    if(![[self.tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isValidEmail]){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Email is invalid." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    
    //    if([[self.pickerCurrency.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    //        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Currency is required." preferredStyle:LEAlertControllerStyleAlert];
    //        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
    //            // handle default button action
    //        }];
    //        [alertController addAction:defaultAction];
    //        [self presentAlertController:alertController animated:YES completion:nil];
    //        return NO;
    //    }
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==4)
        return 0; //set the hidden cell's height to 0
    
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark - API

- (void) addPayout:(WDPayoutMethod *)payout {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] savePayoutMethod:payout completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                for (UIViewController *aViewController in allViewControllers) {
                    if ([aViewController isKindOfClass:[WDPayoutMethodTableViewController class]]) {
                        [self.navigationController popToViewController:aViewController animated:YES];
                        break;
                    }
                }
            }
        }];
    }
}

- (void) getListCurrency {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getCurrencyByCountry:self.payoutMethod.countryID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    self.currencies = [[NSMutableArray alloc] init];
                    self.currenciessData = [[NSMutableArray alloc] init];
                    
                    NSDictionary *dataCategories = [responseObject valueForKey:@"data"];
                    for (NSDictionary *object in dataCategories) {
                        WDCurrency *currency = [[WDCurrency alloc] init];
                        [currency setCurrencyObject:object];
                        [self.currencies addObject:currency];
                        [self.currenciessData addObject:currency.code];
                    };
                    [self setupPickerCurrency];
                    [self setData];
                    [self.tableView reloadData];
                }
                
            }
        }];
    }
}

#pragma mark - IBAction

- (IBAction)clickedFinish:(id)sender {
    if([self validate]){
        self.payoutMethod.currency = self.pickerCurrency.text;
        self.payoutMethod.account = [self.tfEmail.text stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        if(self.isAddPayout){
            [self addPayout:self.payoutMethod];
        }else{
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:self.payoutMethod forKey:@"PayoutMethod"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"addPayoutMethod" object:nil userInfo:dictionary];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[WDBecomeSellerStepOneTableViewController class]]) {
                    [self.navigationController popToViewController:aViewController animated:YES];
                    break;
                }
            }
        }
    }
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
