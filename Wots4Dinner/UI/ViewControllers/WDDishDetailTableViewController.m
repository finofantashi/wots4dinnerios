//
//  WDDishDetailViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/25/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDDishDetailTableViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDFoodType.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDUserTemp.h"
#import "WDDelivery.h"
#import <QuartzCore/QuartzCore.h>
#import "WDSourceConfig.h"
#import "UIColor+Style.h"
#import "WDDishDetailMoreViewController.h"
#import "WDSellerProfileViewController.h"
#import "WDSeller.h"
#import "WDDishOrderTableViewController.h"
#import "WDReviewDishViewController.h"
#import "WDDishViewController.h"
#import "WDOrderAPIController.h"
#import "URBNActivityViewController.h"
#import "WDMapViewController.h"
#import "WDReceiver.h"
#import "WDContentMessageViewController.h"
#import "WDExploreDishViewController.h"
#import "WDUserDefaultsManager.h"
#import "NSString+StringAdditions.h"
#import "WDSourceConfig.h"
#import <HandyFrame/UIView+LayoutMethods.h>
#import <CTVideoView+OperationButtons.h>
#import <CTVideoView+Time.h>

@interface WDDishDetailTableViewController() <CTVideoViewTimeDelegate>

@end

@implementation WDDishDetailTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.estimatedRowHeight = 500;
  self.tableView.rowHeight = UITableViewAutomaticDimension;
  _datasource = [[NSMutableArray alloc] init];
  [self loadData];
  
  //self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
  self.btnOrderNow.layer.cornerRadius = self.btnOrderNow.frame.size.height/2; // this value vary as per your desire
  self.btnOrderNow.layer.borderWidth = 1.0f;
  self.btnOrderNow.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
  self.btnOrderNow.clipsToBounds = YES;
  
  UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedAvatar:)];
  singleTap.numberOfTapsRequired = 1;
  self.ivAvatarSeller.userInteractionEnabled = YES;
  [self.ivAvatarSeller addGestureRecognizer:singleTap];
  self.title = @"Dish Detail";
  
  [self.slideView setDelay:3]; // Delay between transitions
  [self.slideView setTransitionDuration:1]; // Transition duration
  [self.slideView setTransitionType:KASlideShowTransitionFade]; // Choose a transition type
  [self.slideView setImagesContentMode:UIViewContentModeScaleAspectFill]; // Choose a content mode for images to display
  [self setupActions];
  self.configuration = [[self class] defaultConfiguration];
  if (self.topToolbarView) {
    [self.configuration.viewsToHideOnIdle addObject:self.topToolbarView];
  }
  if (self.bottomToolbarView) {
    [self.configuration.viewsToHideOnIdle addObject:self.bottomToolbarView];
  }
  [self.viewVideo setShouldObservePlayTime:YES withTimeGapToObserve:10.0f];
  self.viewVideo.timeDelegate = self;
  
}

+ (DZVideoPlayerViewControllerConfiguration *)defaultConfiguration {
  DZVideoPlayerViewControllerConfiguration *configuration = [[DZVideoPlayerViewControllerConfiguration alloc] init];
  configuration.viewsToHideOnIdle = [NSMutableArray new];
  configuration.delayBeforeHidingViewsOnIdle = 3.0;
  configuration.isShowFullscreenExpandAndShrinkButtonsEnabled = YES;
  configuration.isHideControlsOnIdleEnabled = YES;
  configuration.isBackgroundPlaybackEnabled = YES;
  return configuration;
}

- (void)setupActions {
  UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleControls)];
  [self.viewVideo addGestureRecognizer:tapGR];
  
  [self.playButton addTarget:self action:@selector(play) forControlEvents:UIControlEventTouchUpInside];
  [self.pauseButton addTarget:self action:@selector(pause) forControlEvents:UIControlEventTouchUpInside];
  
  [self.progressIndicator addTarget:self action:@selector(seek:) forControlEvents:UIControlEventValueChanged];
  [self.progressIndicator addTarget:self action:@selector(startSeeking:) forControlEvents:UIControlEventTouchDown];
  [self.progressIndicator addTarget:self action:@selector(endSeeking:) forControlEvents:UIControlEventTouchUpInside|UIControlEventTouchUpOutside];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  if (self.viewVideo != nil) {
    [self.viewVideo play];
  }
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  if (self.viewVideo != nil) {
    [self.viewVideo pause];
  }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if(indexPath.row==4||indexPath.row==6||indexPath.row==8){
    return 1;
  }
  
  if (indexPath.row==10) {
    if(self.dish.user.videoURL != nil || self.ads.url != nil) {
      return 56;
    }
    return 0;
  }
  
  if (indexPath.row==2) {
    return 8;
  }
  
  if (indexPath.row==11) {
    if(self.dish.user.videoURL != nil || self.ads.url != nil) {
      return 200;
    }
    return 0;
  }
  
  return UITableViewAutomaticDimension;
}

- (void) setData:(WDDish*)dish {
  //Dish detail
  CLLocationDistance distance = [self getDistanceMetresBetweenLocationCoordinates:self.dish.user.seller.lat lng:self.dish.user.seller.lng];
  NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
  distance = distance / 1000;
  [numberFormatter setLocale:[NSLocale currentLocale]];
  [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
  [numberFormatter setMaximumFractionDigits:1];
  NSString *location = [NSString stringWithFormat:@"%@ km",[numberFormatter stringFromNumber:[NSNumber numberWithDouble:distance]]];
  self.lbAddress.text = location;
  
  NSString *imageURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish.dishImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
  [self.ivDish sd_setImageWithURL:[NSURL URLWithString:imageURL]
                 placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
  self.nameDish.text = dish.dishName;
  if(dish.quantity > 0){
    self.lbQuantityLeft.text = [NSString stringWithFormat:@"%ld",dish.quantity];
  }else{
    self.lbTitleQtyLeft.text = @"Sold Out";
    [self.btnOrderNow setEnabled:NO];
    [self.btnOrderNow setTitleColor:[UIColor colorFromHexString:@"cccccc" withAlpha:1.0f] forState:UIControlStateNormal];
    self.btnOrderNow.layer.borderColor = [UIColor colorFromHexString:@"cccccc" withAlpha:1.0f].CGColor;
  }
  self.lbPrice.text = [[NSString string] formatCurrency:dish.currencyCode symbol:dish.currencySymbol price:dish.price];
  
  //Description
  self.lbDescription.text = dish.dishDescription;
  //Seller
  self.ivAvatarSeller.layer.cornerRadius = self.ivAvatarSeller.frame.size.width / 2;
  self.ivAvatarSeller.layer.masksToBounds = YES;
  self.ivAvatarSeller.layer.borderColor = [UIColor grayColor].CGColor;
  self.ivAvatarSeller.layer.borderWidth = 1.0f;
  [self.ivAvatarSeller setNeedsDisplay];
  
  NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish.user.avatarURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
  [self.ivAvatarSeller sd_setImageWithURL:[NSURL URLWithString:stringURL]
                         placeholderImage:[UIImage imageNamed:@"empty_avatar"]];
  self.lbNameSeller.text = [NSString stringWithFormat:@"%@",dish.user.seller.kitchenName];
  
  if(dish.user.hasFavorite>0){
    [self.btnFavoriteSeller setImage:[UIImage imageNamed:@"ic_has_favorite_seller_small"] forState:UIControlStateNormal];
  }else{
    [self.btnFavoriteSeller setImage:[UIImage imageNamed:@"ic_favorite_seller_small"] forState:UIControlStateNormal];
  }
  
  if(dish.hasWishList>0){
    [self.btnFavoriteDish setImage:[UIImage imageNamed:@"btn_has_wish_list"] forState:UIControlStateNormal];
  }else{
    [self.btnFavoriteDish setImage:[UIImage imageNamed:@"btn_wish_list"] forState:UIControlStateNormal];
  }
  
  self.viewRating.value = dish.user.rating;
  self.lbReviews.text = [NSString stringWithFormat:@"%ld",dish.user.countReviews];
  if(dish.user.seller.category){
    self.lbCategory.text = dish.user.seller.category;
  }
  if(self.dish.user.hasOrder>0){
    [self.btnMessage setHidden:NO];
  }else{
    [self.btnMessage setHidden:YES];
    
  }
  
  if(self.dish.user.seller.certified>0){
    [self.viewCertified setHidden:NO];
  }else{
    [self.viewCertified setHidden:YES];
  }
  
  if (self.ads != nil && self.ads.url != nil) {
    NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain, self.ads.url] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    self.viewVideo.videoUrl = [NSURL URLWithString:stringURL];
    [self play];
    self.viewVideo.userInteractionEnabled = NO;
    [self hideControls];
  } else if (self.dish.user.videoURL != nil) {
    NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain, self.dish.user.videoURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    self.viewVideo.videoUrl = [NSURL URLWithString:stringURL];
    [self play];
  }
}

#pragma mark - API
- (void) addCartAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    
    [[WDOrderAPIController sharedInstance] addCart:[NSNumber numberWithInteger:self.dishID] quantity:[NSNumber numberWithInteger:1] completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if([[responseObject valueForKey:@"success"] integerValue]!=0){
          LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Success" message:[NSString stringWithFormat:@"Added successfully!"] preferredStyle:LEAlertControllerStyleAlert];
          LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
          }];
          [alertController addAction:defaultAction];
          [self presentAlertController:alertController animated:YES completion:nil];
        }else{
          LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:[responseObject valueForKey:@"message"] preferredStyle:LEAlertControllerStyleAlert];
          LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
          }];
          [alertController addAction:defaultAction];
          [self presentAlertController:alertController animated:YES completion:nil];
        }
      }
    }];
  }
}

- (void)loadData {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getDishDetail:self.dishID completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"dish"];
          NSArray *dataAds = [[responseObject valueForKey:@"data"] valueForKey:@"ads"];
          NSDictionary *dataGalleries = [dataDish valueForKey:@"galleries"];
          WDADS *ads = [[WDADS alloc] init];
          if (dataAds.count > 0) {
            [ads setAdsObject:dataAds.firstObject];
            self.ads = ads;
          }
          
          self.dish = [[WDDish alloc] init];
          [self.dish  setDishObject:dataDish];
          [self setData:self.dish];
          
          [self.datasource removeAllObjects];
          NSString *stringURL = [NSString stringWithFormat:@"%@%@",kImageDomain,self.dish.dishImage];
          [self.datasource addObject:[NSURL URLWithString:stringURL]];
          for (NSDictionary *object in dataGalleries) {
            NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,[object valueForKey:@"image"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            [self.datasource addObject:[NSURL URLWithString:stringURL]];
          }
          [self.slideView reloadData];
          [self.slideView start];
          dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
          });
        }
      }
    }];
  }
}

- (void)favoriteSeller:(NSInteger) sellerId {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] favoriteSeller:sellerId completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self loadData];
        }
      }
    }];
  }
}

- (void)deleteFavoriteSeller:(NSInteger) favoriteSellerID {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] deleteFavoriteSeller:favoriteSellerID completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self loadData];
        }
      }
    }];
  }
}

- (void)wishListDish:(NSInteger) dishId {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] wishListsDish:dishId completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self loadData];
        }
      }
    }];
  }
}

- (void)deleteWishListDish:(NSInteger) wishListId {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] deleteWishListDish:wishListId completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self loadData];
        }
      }
    }];
  }
}

#pragma mark - IBAction
- (void)play {
  if(self.viewVideo != nil ) {
    [self.viewVideo play];
  }
  [self startIdleCountdown];
  [self syncUI];
}

- (void)pause {
  if( self.viewVideo != nil ) {
    [self.viewVideo pause];
  }
  [self stopIdleCountdown];
  [self syncUI];
}

- (void)seek:(UISlider *)slider {
  if( self.viewVideo.playerItem != nil ) {
    int timescale = self.viewVideo.playerItem.asset.duration.timescale;
    if( timescale > 0 ) {
      float time = slider.value * (self.viewVideo.playerItem.asset.duration.value / timescale);
      if(self.viewVideo.player != nil ) {
        [self.viewVideo.player seekToTime:CMTimeMakeWithSeconds(time, timescale)];
      }
    }
  }
}

- (void)seekToTime:(NSTimeInterval)newPlaybackTime {
  if( self.viewVideo.playerItem != nil ) {
    int timescale = 1;
    if(
       self.viewVideo.playerItem != nil
       && [self.viewVideo.playerItem asset] != nil
       )
    {
      timescale = self.viewVideo.playerItem.asset.duration.timescale;
    }
    if(self.viewVideo.player != nil ) {
      [self.viewVideo.player seekToTime:CMTimeMakeWithSeconds(newPlaybackTime, timescale)];
    }
  }
}

- (void)startSeeking:(id)sender {
  [self stopIdleCountdown];
  self.isSeeking = YES;
}

- (void)endSeeking:(id)sender {
  [self startIdleCountdown];
  self.isSeeking = NO;
}

- (void)updateProgressIndicator:(id)sender {
  CGFloat duration = CMTimeGetSeconds(self.viewVideo.playerItem.asset.duration);
  
  if (duration == 0 || isnan(duration)) {
    // Video is a live stream
    self.progressIndicator.hidden = YES;
    [self.currentTimeLabel setText:nil];
    [self.remainingTimeLabel setText:nil];
  }
  else {
    self.progressIndicator.hidden = NO;
    
    CGFloat current;
    if ( self.isSeeking ) {
      current = self.progressIndicator.value * duration;
    }
    else if(self.viewVideo.player != nil ) {
      // Otherwise, use the actual video position
      current = CMTimeGetSeconds(self.viewVideo.player.currentTime);
    }
    else {
      current = self.progressIndicator.value * duration;
    }
    
    CGFloat left = duration - current;
    
    [self.progressIndicator setValue:(current / duration)];
    [self.progressIndicator setSecondaryValue:([self availableDuration] / duration)];
    
    // Set time labels
    
    NSString *currentTimeString = current > 0 ? [self stringFromTimeInterval:current] : @"00:00";
    NSString *remainingTimeString = left > 0 ? [self stringFromTimeInterval:left] : @"00:00";
    
    [self.currentTimeLabel setText:currentTimeString];
    [self.remainingTimeLabel setText:[NSString stringWithFormat:@"-%@", remainingTimeString]];
    
  }
}

- (IBAction)clickedMap:(id)sender {
  if(self.dish.user.seller.lat!=0&&self.dish.user.seller.lng!=0){
    WDMapViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDMapViewController"];
    viewController.kitchen = self.dish.user.seller.kitchenName;
    viewController.location = self.dish.user.seller.location;
    viewController.lat = self.dish.user.seller.lat;
    viewController.lng = self.dish.user.seller.lng;
    [self.navigationController pushViewController:viewController animated:YES];
  }
}

- (IBAction)clickedBack:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedFavoriteDish:(id)sender {
  if(self.dish.hasWishList>0){
    [self deleteWishListDish:self.dish.hasWishList];
  }else{
    [self wishListDish:self.dishID];
  }
}

- (IBAction)clickedMoreInfo:(id)sender {
  WDDishDetailMoreViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishDetailMoreViewController"];
  viewController.dish = self.dish;
  [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedReviews:(id)sender {
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
  WDReviewDishViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDReviewDishViewController"];
  viewController.dishID = self.dishID;
  viewController.numOrders = self.dish.numOrders;
  [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedLike:(id)sender {
  //    WDDishViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishViewController"];
  //    viewController.foodTypeID = [self.dish.foodTypeID integerValue];
  //    viewController.isFrom = 3;
  //    [self.navigationController pushViewController:viewController animated:YES];
  WDExploreDishViewController *todayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDExploreDishViewController"];
  todayViewController.sellerID = self.dish.user.userID;
  todayViewController.isToday = YES;
  todayViewController.isFromDishDetail = YES;
  [self.navigationController pushViewController:todayViewController animated:YES];
}

- (IBAction)clickedOrder:(id)sender {
  WDDishOrderTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishOrderTableViewController"];
  viewController.dishID = self.dishID;
  [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedFavoriteSeller:(id)sender {
  if(self.dish.user.hasFavorite>0){
    [self deleteFavoriteSeller:self.dish.user.hasFavorite];
  }else{
    [self favoriteSeller:self.dish.user.userID];
  }
}

- (IBAction)clickedAddCart:(id)sender {
  [self addCartAPI];
}

- (IBAction)clickedShare:(id)sender {
  
  NSString *contentShare = [NSString stringWithFormat:@"%@dishes/%ld",[WDSourceConfig config].webURL, self.dish.dishID];
  
  NSURL *url = [NSURL URLWithString:contentShare];
  
  URBNActivityViewController *urbnActivityController = [[URBNActivityViewController alloc] initWithDefaultBody:@"" url:url canOpenInSafari:YES];
  
  [urbnActivityController setIncludedActivityTypes:@[UIActivityTypeCopyToPasteboard, UIActivityTypeMail, UIActivityTypeMessage, UIActivityTypePostToFacebook, UIActivityTypePostToTwitter, kURBNActivityTypePinterest, UIActivityTypeAddToReadingList]];
  [self presentViewController:urbnActivityController animated:YES completion:nil];
}

- (IBAction)clickedSendMessage:(id)sender {
  NSMutableArray *selectedReceivers = [NSMutableArray array];
  WDReceiver *receiver = [[WDReceiver alloc] init];
  [receiver setReceiverID:self.dish.user.userID];
  [receiver setRoleID:self.dish.user.roleID ];
  [receiver setFirstName:self.dish.user.firstName];
  [receiver setLastName:self.dish.user.lastName];
  [receiver setKitchenName:self.dish.user.seller.kitchenName];
  [receiver setAvatar:self.dish.user.avatarURL];
  [selectedReceivers addObject:receiver];
  
  //WDContentMessageViewController *viewController = [WDContentMessageViewController messagesViewController];
  WDContentMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDContentMessageViewController"];
  viewController.listReceiver = selectedReceivers;
  viewController.isNewMessage = YES;
  [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedAvatar:(id)sender {
  if(self.dish.user){
    WDSellerProfileViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDSellerProfileViewController"];
    viewController.user = self.dish.user;
    [self.navigationController pushViewController:viewController animated:YES];
  }
}

- (int)sizeOfMultiLineLabel: (NSString *)text{
  //Label text
  NSString *aLabelTextString = text;
  
  //Label font
  UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:14];
  
  //Width of the Label
  CGFloat aLabelSizeWidth = self.view.frame.size.width-40;
  CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{
                                                    NSFontAttributeName : aLabelFont
                                                  }
                                                     context:nil].size;
  
  return labelSize.height;
}

-(CGFloat)sizeOfLocation: (NSString *)text {
  //Label text
  NSString *aLabelTextString = text;
  
  //Label font
  UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:14];
  
  //Width of the Label
  CGFloat aLabelSizeWidth = self.view.frame.size.width-46;
  CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{
                                                    NSFontAttributeName : aLabelFont
                                                  }
                                                     context:nil].size;
  return ceil(labelSize.height / aLabelFont.lineHeight);
}

- (CLLocationDistance) getDistanceMetresBetweenLocationCoordinates:(double)lat
                                                               lng:(double)lng  {
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  
  CLLocation* location1 = [[CLLocation alloc]
                           initWithLatitude: [[current valueForKey:@"kLat"] doubleValue]
                           longitude: [[current valueForKey:@"kLng"] doubleValue]];
  
  CLLocation* location2 = [[CLLocation alloc]
                           initWithLatitude: lat
                           longitude: lng];
  return [location1 distanceFromLocation: location2];
}


#pragma mark - KASlideShow datasource

- (NSObject *)slideShow:(KASlideShow *)slideShow objectAtIndex:(NSUInteger)index {
  return _datasource[index];
}

- (NSUInteger)slideShowImagesNumber:(KASlideShow *)slideShow {
  return _datasource.count;
}

#pragma mark - KASlideShow delegate

- (void) slideShowWillShowNext:(KASlideShow *)slideShow {
  NSLog(@"slideShowWillShowNext, index : %@",@(slideShow.currentIndex));
}

- (void)startIdleCountdown {
  if (self.idleTimer) {
    [self.idleTimer invalidate];
  }
  if (self.configuration.isHideControlsOnIdleEnabled) {
    self.idleTimer = [NSTimer scheduledTimerWithTimeInterval:self.configuration.delayBeforeHidingViewsOnIdle
                                                      target:self selector:@selector(hideControls)
                                                    userInfo:nil repeats:NO];
  }
}

- (void)stopIdleCountdown {
  if (self.idleTimer) {
    [self.idleTimer invalidate];
  }
}

- (void)hideControls {
  [self hideControlsWithAnimationDuration:0.3f];
}
- (void)hideControlsWithAnimationDuration:(NSTimeInterval)animationDuration {
  NSArray *views = self.configuration.viewsToHideOnIdle;
  if( animationDuration <= 0 )
  {
    for (UIView *view in views) {
      view.alpha = 0.0;
    }
  }
  else
  {
    [UIView animateWithDuration:0.3f animations:^{
      for (UIView *view in views) {
        view.alpha = 0.0;
      }
    }];
  }
  self.isControlsHidden = YES;
}

- (void)showControls {
  [self showControlsWithAnimationDuration:0.3f];
}
- (void)showControlsWithAnimationDuration:(NSTimeInterval)animationDuration {
  NSArray *views = self.configuration.viewsToHideOnIdle;
  if( animationDuration <= 0 )
  {
    for (UIView *view in views) {
      view.alpha = 1.0;
    }
  }
  else
  {
    [UIView animateWithDuration:animationDuration animations:^{
      for (UIView *view in views) {
        view.alpha = 1.0;
      }
    }];
  }
  self.isControlsHidden = NO;
}

- (void)toggleControls {
  if (self.isControlsHidden) {
    [self showControls];
  }
  else {
    [self hideControls];
  }
  [self stopIdleCountdown];
}

- (void)syncUI {
  if (self.viewVideo != nil && self.viewVideo.isPlaying) {
    self.playButton.hidden = YES;
    self.playButton.enabled = NO;
    
    self.pauseButton.hidden = NO;
    self.pauseButton.enabled = YES;
  }
  else {
    self.playButton.hidden = NO;
    self.playButton.enabled = YES;
    
    self.pauseButton.hidden = YES;
    self.pauseButton.enabled = NO;
  }
}

#pragma mark - Helpers

- (NSString *)stringFromTimeInterval:(NSTimeInterval)time {
  NSString *string = [NSString stringWithFormat:@"%02li:%02li:%02li",
                      lround(floor(time / 3600.)) % 100,
                      lround(floor(time / 60.)) % 60,
                      lround(floor(time)) % 60];
  
  NSString *extraZeroes = @"00:";
  
  if ([string hasPrefix:extraZeroes]) {
    string = [string substringFromIndex:extraZeroes.length];
  }
  
  return string;
}

- (void)videoView:(CTVideoView *)videoView didPlayToSecond:(CGFloat)second {
  CGFloat duration = CMTimeGetSeconds(self.viewVideo.playerItem.asset.duration);
  if ([self.viewVideo.videoUrl.absoluteString containsString:@"ads"] && second >= duration && self.dish.user.videoURL != nil) {
    NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain, self.dish.user.videoURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [self.viewVideo stopWithReleaseVideo:YES];
    self.viewVideo.videoUrl = [NSURL URLWithString:stringURL];
    [self play];
    self.viewVideo.userInteractionEnabled = YES;
  } else {
    [self updateProgressIndicator:nil];
    [self syncUI];
  }
}


@end
