//
//  WDMainViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMainViewController.h"
#import "UIColor+Style.h"
#import "WDMessageAPIController.h"
#import "WDOrderAPIController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "NSDictionary+SafeValues.h"
#import "WDUser.h"
#import "WDSourceConfig.h"
#import "WDUserDefaultsManager.h"
@interface WDMainViewController ()

@end

@implementation WDMainViewController

static WDMainViewController *singletonInstance;

+ (WDMainViewController *)sharedInstance {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    if([[WDUserDefaultsManager valueForKey:kLoginBecomeSeller] isEqualToNumber:[NSNumber numberWithBool:YES]]){
      UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"WDHomeNavigation"];
      singletonInstance.rootViewController = navigationController;
    }else{
      UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"WDSearchAroundNavigation"];
      singletonInstance = [storyboard instantiateViewControllerWithIdentifier:@"WDMainViewController"];
      singletonInstance.rootViewController = navigationController;
    }
    [singletonInstance getUnreadMessagesAPI];
    [singletonInstance setupWithPresentationStyle];
  });
  return singletonInstance;
}

- (id)init {
  self = [super init];
  if (self) {
  }
  return self;
}

- (void)setupWithPresentationStyle  {
  [self setLeftViewEnabledWithWidth:250.f
                  presentationStyle:LGSideMenuPresentationStyleSlideAbove
               alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
  
  self.leftViewStatusBarStyle = UIStatusBarStyleDarkContent;
  self.leftViewAlwaysVisibleOptions = LGSideMenuAlwaysVisibleOnNone;
  self.leftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftViewController"];
  self.leftViewBackgroundColor = [UIColor whiteColor];
  self.leftViewController.tableView.backgroundColor = [UIColor clearColor];
  self.leftViewController.tintColor = [UIColor colorFromHexString:@"464646" withAlpha:1.0];
  [self.leftViewController.tableView reloadData];
}

- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size {
  [super leftViewWillLayoutSubviewsWithSize:size];
  self.leftViewController.tableView.frame = CGRectMake(0.f , 20.f, size.width, size.height-20.f);
}

- (void)rightViewWillLayoutSubviewsWithSize:(CGSize)size {
  [super rightViewWillLayoutSubviewsWithSize:size];
  
}

- (void)changeHomeSharedInstance {
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
  UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"WDHomeNavigation"];
  [WDMainViewController sharedInstance].rootViewController = navigationController;
  UIWindow *window = UIApplication.sharedApplication.delegate.window;
  window.rootViewController = [WDMainViewController sharedInstance];
  [UIView transitionWithView:window
                     duration:0.3
                      options:UIViewAnimationOptionTransitionCrossDissolve
                   animations:nil
                   completion:nil];
}

- (void)getUnreadMessagesAPI {
  if([WDUser sharedInstance].roleID==2&&[WDUser sharedInstance].isSellerMode){
    [self.leftViewController setSellerMode];
    [self getUnreadOrderAPI];
  }else{
    [self.leftViewController setUserMode];
  }
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    
  }else{
    //Call Api
    [[WDMessageAPIController sharedInstance]unreadMessages:^(NSString *errorString, id responseObject) {
      if(errorString){
        
      }else{
        if([[[responseObject valueForKey:@"data"] safeNumberForKey:@"unread_messages"] integerValue]!=0){
          self.leftViewController.countUnreadMessages = [[[responseObject valueForKey:@"data"] safeNumberForKey:@"unread_messages"] integerValue];
        }else{
          self.leftViewController.countUnreadMessages = 0;
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:self.leftViewController.countUnreadMessages] forKey:kCountNotificationMessage];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSNumber *countMessage = [[NSUserDefaults standardUserDefaults] valueForKey:kCountNotificationMessage];
        NSNumber *countOrder = [[NSUserDefaults standardUserDefaults] valueForKey:kCountNotificationOrder];
        
        if([countMessage isKindOfClass:[NSNull class]]){
          countMessage = [NSNumber numberWithInt:0];
        }
        
        if([countOrder isKindOfClass:[NSNull class]]){
          countOrder = [NSNumber numberWithInt:0];
        }
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[countMessage intValue]+[countOrder intValue]];
        
        [self.leftViewController.tableView reloadData];
      }
    }];
  }
}

- (void)getUnreadOrderAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    
  }else{
    //Call Api
    [[WDOrderAPIController sharedInstance]unreadOrders:^(NSString *errorString, id responseObject) {
      if(errorString){
        
      }else{
        if([[[responseObject valueForKey:@"data"] safeNumberForKey:@"unread_orders"] integerValue]!=0){
          self.leftViewController.countUnreadOrder = [[[responseObject valueForKey:@"data"] safeNumberForKey:@"unread_orders"] integerValue];
        }else{
          self.leftViewController.countUnreadOrder = 0;
        }
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:self.leftViewController.countUnreadOrder] forKey:kCountNotificationOrder];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSNumber *countMessage = [[NSUserDefaults standardUserDefaults] valueForKey:kCountNotificationMessage];
        NSNumber *countOrder = [[NSUserDefaults standardUserDefaults] valueForKey:kCountNotificationOrder];
        
        if([countMessage isKindOfClass:[NSNull class]]){
          countMessage = [NSNumber numberWithInt:0];
        }
        
        if([countOrder isKindOfClass:[NSNull class]]){
          countOrder = [NSNumber numberWithInt:0];
        }
        
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:[countMessage intValue]+[countOrder intValue]];
        [self.leftViewController.tableView reloadData];
      }
    }];
  }
}

@end
