//
//  WDReviewTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/27/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDUserTemp.h"
#import "HCSStarRatingView.h"

@protocol WDReviewTableViewCellDelegate <NSObject>
@optional
- (void)clickedAvatar:(WDUserTemp *)user;
@end

@interface WDReviewTableViewCell : UITableViewCell

@property (strong, nonatomic) WDUserTemp *user;
@property (nonatomic, weak) id <WDReviewTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbLocation;
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewRatingSeller;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewRatingForThisSeller;

- (IBAction)clickedAvatar:(id)sender;

@end
