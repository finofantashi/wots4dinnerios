//
//  WDPaymentMethodTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WDCard;

@protocol WDPaymentMethodTableViewCellDelegate <NSObject>
@optional
- (void)clickedCheck:(WDCard*)card;
- (void)clickedDelete:(WDCard*)card;
@end

@interface WDPaymentMethodTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbCardNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbExpiryDate;
@property (weak, nonatomic) IBOutlet UIButton *btnChecked;
@property (weak, nonatomic) IBOutlet UIImageView *ivCard;

- (IBAction)clickedDelete:(id)sender;
- (IBAction)clickedCheck:(id)sender;

@property (strong, nonatomic) WDCard *card;
@property (nonatomic, weak) id <WDPaymentMethodTableViewCellDelegate> delegate;

@end
