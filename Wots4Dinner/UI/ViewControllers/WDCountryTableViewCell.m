//
//  WDCountryTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDCountryTableViewCell.h"

@implementation WDCountryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
