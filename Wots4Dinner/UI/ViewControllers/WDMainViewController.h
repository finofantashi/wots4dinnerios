//
//  WDMainViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "LGSideMenuController.h"
#import "LeftViewController.h"

@interface WDMainViewController : LGSideMenuController

+ (WDMainViewController *)sharedInstance;
- (void)getUnreadMessagesAPI;
- (void)setupWithPresentationStyle;
- (void)changeHomeSharedInstance;
@end
