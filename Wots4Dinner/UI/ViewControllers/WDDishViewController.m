//
//  WDDishViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//


#import "WDDishViewController.h"
#import "WDDishTableViewCell.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDFoodType.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDUserTemp.h"
#import "WDDishDetailTableViewController.h"
#import "WDSourceConfig.h"
#import "WDDishSearch.h"
#import <CoreLocation/CoreLocation.h>
#import "WDUserDefaultsManager.h"
#import "NSString+StringAdditions.h"
#import "WDMainViewController.h"

@interface WDDishViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray *listDish;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;

@end

@implementation WDDishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.listDish = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    if(self.isFrom==1){
        [self searchDishAPI:self.lat lng:self.lng distance:self.distance foodTypeIDs:self.foodTypeIDs priceFrom:self.priceFrom priceTo:self.priceTo page:self.currentPage];
        self.title = @"Search results";
    }else if(self.isFrom==2){
        [self loadData:self.currentPage];
        self.title = self.foodTypeName;
    }else if(self.isFrom==3){
        [self youMayAlsoLikeAPI];
        self.title = @"You May Also Like";
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(self.isFrom==4){
        [self popularOrdersAPI];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.isFrom==4){
        return self.listDish.count;
    }
    if (self.currentPage == self.totalPages
        || self.totalItems == self.listDish.count) {
        return self.listDish.count;
    }
    return self.listDish.count + 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isFrom==4){
        return;
    }
    if (self.currentPage != self.totalPages && indexPath.row == [self.listDish count] - 1 ) {
        if(self.isFrom==1){
            [self searchDishAPI:self.lat lng:self.lng distance:self.distance foodTypeIDs:self.foodTypeIDs priceFrom:self.priceFrom priceTo:self.priceTo page:++self.currentPage];
        }else if(self.isFrom==2){
            [self loadData:++self.currentPage];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDDishTableViewCell";
    UITableViewCell *cell;
    if (indexPath.row == [self.listDish count]&&(self.isFrom!=3||self.isFrom!=4)) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:100];
        [activityIndicator startAnimating];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        WDDishSearch *dish = self.listDish[indexPath.row];
        
        CLLocationDistance distance = [self getDistanceMetresBetweenLocationCoordinates:dish.lat lng:dish.lng];
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        distance = distance / 1000;
        [numberFormatter setLocale:[NSLocale currentLocale]];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setMaximumFractionDigits:1];
        
        ((WDDishTableViewCell*)cell).lbDistance.text = [NSString stringWithFormat:@"%@ km",[numberFormatter stringFromNumber:[NSNumber numberWithDouble:distance]]];
        ((WDDishTableViewCell*)cell).viewDistance.layer.cornerRadius = 10;
        ((WDDishTableViewCell*)cell).viewDistance.layer.borderWidth = 0.0f;
        ((WDDishTableViewCell*)cell).viewDistance.clipsToBounds = YES;
        
        ((WDDishTableViewCell*)cell).ratingView.value = dish.ratingValue;
        //get image name and assign
        NSString *imageURL = [[NSString stringWithFormat:@"%@%@", kImageDomain,dish.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        [((WDDishTableViewCell*)cell).ivDish sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                               placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
        ((WDDishTableViewCell*)cell).lbNameDish.text = dish.dishName;
        ((WDDishTableViewCell*)cell).lbDescription.text = [NSString stringWithFormat:@"Sold by %@",dish.kitchenName];
        
        ((WDDishTableViewCell*)cell).lbPrice.text = [[NSString string] formatCurrency:dish.currencyCode symbol:dish.currencySymbol price:dish.price];
        
        [ ((WDDishTableViewCell*)cell) cellOnTableView:self.tableView didScrollOnView:self.view];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isFrom==1){
        WDDishSearch *dish = self.listDish[indexPath.row];
        WDDishDetailTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishDetailTableViewController"];
        viewController.dishID = dish.dishID;
        [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    }else{
        WDDish *dish = self.listDish[indexPath.row];
        WDDishDetailTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishDetailTableViewController"];
        viewController.dishID = dish.dishID;
        [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Get visible cells on table view.
    NSArray *visibleCells = [self.tableView visibleCells];
    
    for (UITableViewCell *cell in visibleCells) {
        if([cell isKindOfClass:[WDDishTableViewCell class]] ){
            [(WDDishTableViewCell*)cell cellOnTableView:self.tableView didScrollOnView:self.view];
        }
    }
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadData:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getDishByFoodType:[[self.foodTypeIDs objectAtIndex:0] integerValue] page:(long)page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDDishSearch *dish = [[WDDishSearch alloc] init];
                        [dish setDishObject:object];
                        [self.listDish addObject:dish];
                    };
                    
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Sorry no dishes available in this food type.\nPlease choose another food type." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

- (void) searchDishAPI:(double)lat
                   lng:(double)lng
              distance:(long)distance
           foodTypeIDs:(NSMutableArray*)foodTypeIDs
             priceFrom:(long)priceFrom
               priceTo:(long)priceTo
                  page:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] searchDish:lat lng:lng distance:distance foodTypeIDs:foodTypeIDs priceFrom:priceFrom priceTo:priceTo page:page completionBlock:^(NSString *errorString, id responseObject) {
            
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDDishSearch *dish = [[WDDishSearch alloc] init];
                        [dish setDishObject:object];
                        [self.listDish addObject:dish];
                    };
                    
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Sorry no dishes available in this food type.\nPlease choose another food type." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)youMayAlsoLikeAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getYouMayAlsoLike:[[self.foodTypeIDs objectAtIndex:0] integerValue] completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"dishes"];
                    for (NSDictionary *object in dataDish) {
                        WDDishSearch *dish = [[WDDishSearch alloc] init];
                        [dish setDishObject:object];
                        [self.listDish addObject:dish];
                    };
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

- (void)popularOrdersAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getPopularOrders:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    [self.listDish removeAllObjects];
                    NSDictionary *dataDish = [responseObject valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDDishSearch *dish = [[WDDishSearch alloc] init];
                        [dish setDishObject:object];
                        [self.listDish addObject:dish];
                    };
                    if(self.listDish.count==0){
                        self.viewNoData.hidden = NO;
                    }else{
                        self.viewNoData.hidden = YES;
                    }
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

- (CLLocationDistance) getDistanceMetresBetweenLocationCoordinates:(double)lat
                                                               lng:(double)lng  {
    NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
    
    CLLocation* location1 = [[CLLocation alloc]
                             initWithLatitude: [[current valueForKey:@"kLat"] doubleValue]
                             longitude: [[current valueForKey:@"kLng"] doubleValue]];
    
    CLLocation* location2 = [[CLLocation alloc]
                             initWithLatitude: lat
                             longitude: lng];
    return [location1 distanceFromLocation: location2];
}

@end
