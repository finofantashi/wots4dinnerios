//
//  WDMessageTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/5/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMessageTableViewCell.h"

@implementation WDMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
