//
//  CategoryView.h
//  MIResizableTableViewDemo
//
//  Created by Mario on 17/01/16.
//  Copyright © 2016 Mario. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDMyOrderView : UIView

+ (WDMyOrderView *)getViewWithNumberOrder:(NSString *)numberOrder
                                     date:(NSString* )date
                             nameCustomer:(NSString* )nameCustomer
                             shippingInfo:(NSString* )shippingInfo
                                   isRead:(BOOL)isRead;


@end
