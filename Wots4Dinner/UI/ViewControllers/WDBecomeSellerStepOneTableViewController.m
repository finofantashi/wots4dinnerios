//
//  WDStepOneTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/12/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBecomeSellerStepOneTableViewController.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDCategory.h"
#import "WDSeller.h"
#import "NSString+IsValidEmail.h"
#import "WDUser.h"
#import "WDMainViewController.h"
#import "AddPayoutMethodStep2TableViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@interface WDBecomeSellerStepOneTableViewController () <PlaceSearchTextFieldDelegate, GMSMapViewDelegate, UITextFieldDelegate>{
  CLLocationCoordinate2D searchtLocation;
  UITapGestureRecognizer *tap;
  WDSeller* mSeller;
  
}

@end

@implementation WDBecomeSellerStepOneTableViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = false;
  self.tfLocation.delegate = self;
  self.tfLocation.placeSearchDelegate                 = self;
  self.tfLocation.strApiKey                           = @"AIzaSyAZDt39UV2JIP_zlfaGOWNSy3CaVA7dX-0";
  self.tfLocation.superViewOfList                     = self.view;  // View, on which Autocompletion list should be appeared.
  self.tfLocation.autoCompleteShouldHideOnSelection   = YES;
  self.tfLocation.maximumNumberOfAutoCompleteRows     = 5;
  
  self.btnNext.layer.cornerRadius = self.btnNext.frame.size.height/2; // this value vary as per your desire
  self.btnNext.layer.borderWidth = 1.0f;
  self.btnNext.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
  self.btnNext.clipsToBounds = YES;
  [self getListCategory];
  
  tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
  [self.view addGestureRecognizer:tap];
  
  if(self.isBasic){
    self.title = @"Basic info";
    [self.btnNext setTitle:@"Update" forState:UIControlStateNormal];
  } else {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Different City’s States and Countries have different guidelines for selling home cooked food. Please check with your local government to make sure that you know the guidelines in your area. Let’s Start Selling!!" preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
    }];
    
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(addPayoutNotification:)
                                               name:@"addPayoutMethod"
                                             object:nil];
}

-(void)viewDidAppear:(BOOL)animated{
  
  //Optional Properties
  self.tfLocation.autoCompleteRegularFontName =  @"HelveticaNeue-Bold";
  self.tfLocation.autoCompleteBoldFontName = @"HelveticaNeue";
  self.tfLocation.autoCompleteTableCornerRadius=0.0;
  self.tfLocation.autoCompleteRowHeight= 35;
  self.tfLocation.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
  self.tfLocation.autoCompleteFontSize= 14;
  self.tfLocation.autoCompleteTableBorderWidth=1.0;
  self.tfLocation.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=YES;
  self.tfLocation.autoCompleteShouldHideOnSelection=YES;
  self.tfLocation.autoCompleteShouldHideClosingKeyboard=YES;
  self.tfLocation.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
  self.tfLocation.autoCompleteTableFrame = CGRectMake(self.tfLocation.frame.origin.x, self.tfLocation.frame.origin.y +  self.tfLocation.frame.size.height+74, self.tfLocation.frame.size.width, 200.0);
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = true;
}

#pragma mark - Private

- (void)addPayoutNotification:(NSNotification *)data {
  WDPayoutMethod *payoutMethod = [[data userInfo] valueForKey:@"PayoutMethod"];
  self.payoutMethod = payoutMethod;
  if([payoutMethod.method isEqualToString:@"PayPal"]){
    [self.btnAddPayout setTitle:[NSString stringWithFormat:@"PayPal - %@", payoutMethod.account] forState:UIControlStateNormal];
  }else{
    if(payoutMethod.account.length>4){
      [self.btnAddPayout setTitle:[NSString stringWithFormat:@"Bank transfer - Account ****%@", [payoutMethod.account substringFromIndex:payoutMethod.account.length-4]] forState:UIControlStateNormal];
    }else{
      [self.btnAddPayout setTitle:[NSString stringWithFormat:@"Bank transfer - Account ****%@", payoutMethod.account ] forState:UIControlStateNormal];
    }
  }
  [self.tfLocation becomeFirstResponder];
  
}

- (void)setupCategory {
  // bind yourTextField to DownPicker
  self.picker = [[DownPicker alloc] initWithTextField:self.pickerCategory withData:self.categoriesData];
  [self.pickerCategory setFont:[UIFont fontWithName:@"Poppins-Medium" size:14]];
  [self.pickerCategory setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:1.0]];
  [self.picker setPlaceholder:@"Tap to choose category"];
  
}

- (NSInteger)getcategoryID:(NSString*)categoryName {
  for (WDCategory *object in self.categories) {
    if([object.categoryName isEqualToString:categoryName]){
      return object.categoryID;
    }
  }
  return 0;
}

- (NSString *)getcategoryName:(NSInteger)categoryID {
  for (WDCategory *object in self.categories) {
    if(object.categoryID == categoryID){
      return object.categoryName;
    }
  }
  return @"";
}

- (void)dismissKeyboard {
  [self.tfKitchenName resignFirstResponder];
  [self.tvDescription resignFirstResponder];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if((self.isBasic&&indexPath.row==0)||(self.isBasic&&indexPath.row==8)){
    return 0;
  }else{
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
  }
}

- (void)setData {
  CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(mSeller.lat, mSeller.lng);
  
  self.tfKitchenName.text = mSeller.kitchenName;
  self.pickerCategory.text = [self getcategoryName:mSeller.categoryID];
  self.tfLocation.text = mSeller.location;
  
  searchtLocation = coordinate;
  [self.mapView clear];
  [self.mapView animateToLocation:searchtLocation];
  [self.mapView animateToZoom:14];
  GMSMarker *marker = [GMSMarker markerWithPosition:searchtLocation];
  marker.appearAnimation = YES;
  marker.map = self.mapView;
  self.tvDescription.text = mSeller.sellerDescription;
}

#pragma mark - Place search Textfield Delegates

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict{
  [self.view endEditing:YES];
  searchtLocation = responseDict.coordinate;
  [self.mapView animateToLocation:searchtLocation];
  [self.mapView animateToZoom:14];
  [self.mapView clear];
  GMSMarker *marker = [GMSMarker markerWithPosition:searchtLocation];
  marker.appearAnimation = YES;
  marker.map = self.mapView;
  [self.tfLocation resignFirstResponder];
}

-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField{
  [self.view removeGestureRecognizer:tap];
}

-(void)placeSearchWillHideResult:(MVPlaceSearchTextField
                                  *)textField{
  [self.view addGestureRecognizer:tap];
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index{
  if(index%2==0){
    cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
  }else{
    cell.contentView.backgroundColor = [UIColor whiteColor];
  }
}

#pragma mark - IBAction

- (IBAction)clickedPayout:(id)sender {
  AddPayoutMethodStep2TableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPayoutMethodStep2TableViewController"];
  if(self.payoutMethod){
    viewController.payoutMethod = self.payoutMethod;
  }
  [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedNext:(id)sender {
  if([self validate]){
    WDSeller *seller = [[WDSeller alloc] init];
    seller.categoryID = [self getcategoryID:self.pickerCategory.text];
    seller.kitchenName = [self.tfKitchenName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    seller.lat = searchtLocation.latitude;
    seller.lng = searchtLocation.longitude;
    seller.sellerDescription = [self.tvDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    seller.location = [self.tfLocation.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    seller.payoutMethod = self.payoutMethod;
    [self registerSeller:seller];
  }
}

- (IBAction)clickedBack:(id)sender {
  if([WDUser sharedInstance].isSellerMode&&[WDUser sharedInstance].roleID==2&&!self.isBasic){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMyMenu"];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
  }else{
    [self.navigationController popViewControllerAnimated:YES];
  }
}

-(void)categorySelected:(id)dp {
  // do what you want
}

- (BOOL)validate {
  if([[self.tfKitchenName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Kitchen name is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.pickerCategory.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Category is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if(!self.payoutMethod&&!self.isBasic) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Payout method is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tfLocation.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Location is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if(searchtLocation.latitude==0||searchtLocation.longitude==0){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Location is invalid." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tvDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Description is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  return YES;
}

#pragma mark - API

- (void) getListCategory {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getCategories:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
          [self loadData];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self loadData];
          self.categories = [[NSMutableArray alloc] init];
          self.categoriesData = [[NSMutableArray alloc] init];
          
          NSDictionary *dataCategories = [responseObject valueForKey:@"data"];
          for (NSDictionary *object in dataCategories) {
            WDCategory *category = [[WDCategory alloc] init];
            [category setCategoryObject:object];
            [self.categories addObject:category];
            [self.categoriesData addObject:category.categoryName];
          };
          [self setupCategory];
          [self.tableView reloadData];
        }
        
      }
    }];
  }
}

- (void) registerSeller:(WDSeller*) seller {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    
    //Call Api
    if(self.sellerID!=0){
      [[WDFoodMenuAPIController sharedInstance] updateSeller:seller completionBlock:^(NSString *errorString, id responseObject) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        if(errorString){
          LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
          LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
          }];
          [alertController addAction:defaultAction];
          [self presentAlertController:alertController animated:YES completion:nil];
        }else{
          if(responseObject) {
            if(self.isBasic){
              [self.navigationController popViewControllerAnimated:YES];
            }else{
              self.sellerID = [[[responseObject valueForKey:@"data"] valueForKey:@"id"] integerValue];
              UITableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepTwoTableViewController"];
              [self.navigationController pushViewController:viewController animated:YES];
            }
          }
        }
      }];
      
    }else{
      [[WDFoodMenuAPIController sharedInstance] registerSeller:seller completionBlock:^(NSString *errorString, id responseObject) {
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        if(errorString){
          LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
          LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
          }];
          [alertController addAction:defaultAction];
          [self presentAlertController:alertController animated:YES completion:nil];
        }else{
          if(responseObject) {
            self.sellerID = [[[responseObject valueForKey:@"data"] valueForKey:@"id"] integerValue];
            [[WDUser sharedInstance] setSellerMode:YES];
            [[WDUser sharedInstance] setRoleID:2];
            
            UITableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepTwoTableViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
          }
        }
      }];
    }
  }
}

- (void)loadData {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getSeller:^(NSString *errorString, id responseObject) {
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      }else{
        if(responseObject) {
          NSDictionary *dataSeller = [responseObject valueForKey:@"data"];
          mSeller = [[WDSeller alloc] init];
          [mSeller setSellerObject:dataSeller];
          self.sellerID = [mSeller.sellerID integerValue];
          [self setData];
          [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
          dispatch_async(dispatch_get_main_queue(), ^{
            //[self.tableView reloadData];
          });
        }
      }
    }];
  }
}

@end
