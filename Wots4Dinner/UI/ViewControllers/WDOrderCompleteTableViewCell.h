//
//  WDOrderCompleteTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/12/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDOrderCompleteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbNameDish;
@property (weak, nonatomic) IBOutlet UILabel *lbPriceQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lbTotalPrice;

@end
