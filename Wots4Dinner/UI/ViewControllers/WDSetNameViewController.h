//
//  WDSetNameViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDSetNameViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

@property (weak, nonatomic) IBOutlet UIButton *btnSetName;
- (IBAction)touchDown:(id)sender;

- (IBAction)clickedSetName:(id)sender;

@end
