//
//  WDCheckOutViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDCheckOutViewController.h"
#import "MBProgressHUD.h"
#import "WDUser.h"
#import "WDSourceConfig.h"
#import "WDOrderCompleteViewController.h"

@interface WDCheckOutViewController ()<UIWebViewDelegate>

@end

@implementation WDCheckOutViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *token = [[WDUser sharedInstance] getToken];

    NSString *urlAddress = [[WDSourceConfig config].apiKey stringByAppendingString:[NSString stringWithFormat:@"/checkout?token=%@",token]];
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    self.webView.delegate = self;
    self.webView.scrollView.bounces = NO;
    [self.webView loadRequest:requestObj];
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    self.title = @"Payment";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)webView:(UIWebView *)wv shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    // Determine if we want the system to handle it.
    NSURL *url = request.URL;
    if (![url.scheme isEqual:@"http"] && ![url.scheme isEqual:@"https"]) {
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
            return NO;
        }
    }
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    if([[webView.request.URL.absoluteString lastPathComponent] rangeOfString:@"cancel-checkout"].location != NSNotFound){
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        for (UIViewController *aViewController in allViewControllers) {
            if ([NSStringFromClass([aViewController class]) isEqualToString:@"WDCartViewController"]) {
                [self.navigationController popToViewController:aViewController animated:YES];
                break;
            }
        }
    }
    
    if ([[webView.request.URL.absoluteString lastPathComponent] rangeOfString:@"complete-checkout?order"].location != NSNotFound) {
        NSArray *pairComponents = [[webView.request.URL.absoluteString lastPathComponent] componentsSeparatedByString:@"="];
        NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
        
        WDOrderCompleteViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDOrderCompleteViewController"];
        viewController.orderNumber = value;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

@end
