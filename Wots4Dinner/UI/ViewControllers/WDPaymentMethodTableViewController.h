//
//  WDPaymentMethodTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDPaymentMethodTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnAdd;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedAdd:(id)sender;

@end
