//
//  WDPayoutMethodTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDPayoutMethodTableViewController.h"
#import "WDPayoutMethodTableViewCell.h"
#import "WDPayout.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "WDOrderAPIController.h"
#import "WDMainViewController.h"
#import "AddPayoutMethodStep2TableViewController.h"

@interface WDPayoutMethodTableViewController ()<WDPayoutMethodTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *listPayout;
@property (nonatomic, assign) BOOL isFirst;

@end

@implementation WDPayoutMethodTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.listPayout = [[NSMutableArray alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [self getPayouts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listPayout.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDPayoutMethodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WDPayoutMethodTableViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    WDPayout *payout = [self.listPayout objectAtIndex:indexPath.row];
    cell.payout = payout;
    cell.lbMethod.text = [NSString stringWithFormat:@"Method: %@", payout.method];
    if([payout.method isEqualToString:@"PayPal"]){
        cell.lbEmail.text = [NSString stringWithFormat:@"Email: %@", payout.account];
    }else{
        if(payout.account.length>4){
            cell.lbEmail.text = [NSString stringWithFormat:@"Account: ****%@", [payout.account substringFromIndex:payout.account.length-4]];
        }else{
            cell.lbEmail.text = [NSString stringWithFormat:@"Account: %@", payout.account];
        }
    }
    if(payout.payoutDefault==1){
        [cell.btnChecked setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
    }else{
        [cell.btnChecked setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
    }
    
    return cell;
}

#pragma mark - API

- (void) getPayouts {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] getPayouts:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                if(responseObject) {
                    [self.listPayout removeAllObjects];
                    NSDictionary *dataCard = [responseObject valueForKey:@"data"];
                    for (NSDictionary *dict in dataCard) {
                        WDPayout *payout = [[WDPayout alloc] init];
                        [payout setPayoutObject:dict];
                        [self.listPayout addObject:payout];
                    }
                    if(self.listPayout.count==0&&!self.isFirst){
                        self.isFirst = YES;
                        [self clickedAdd:nil];
                    }
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

- (void) updatePayout:(WDPayout *)payout {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] updatePayout:payout.payoutID isDefault:@"1" completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                [self getPayouts];
            }
        }];
    }
}

- (void) deletePayout:(WDPayout *)payout {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] deletePayout:payout.payoutID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:[responseObject valueForKey:@"message"] preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    [self getPayouts];
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }
        }];
    }
}

#pragma mark - IBAction

- (void)clickedCheck:(WDPayout *)payout {
    if(payout.payoutDefault!=1){
        [self updatePayout:payout];
    }
}

- (void)clickedDelete:(WDPayout *)payout {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Are you sure you want to delete?" preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        [self deletePayout:payout];
    }];
    LEAlertAction *cancelAction = [LEAlertAction actionWithTitle:@"Cancel" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
}

- (IBAction)clickedBack:(id)sender {
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
        [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
}

- (IBAction)clickedAdd:(id)sender {
    AddPayoutMethodStep2TableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPayoutMethodStep2TableViewController"];
    viewController.isAddPayout = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
