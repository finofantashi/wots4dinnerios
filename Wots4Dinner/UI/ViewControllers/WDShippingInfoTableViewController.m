//
//  WDShippingInfoTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDShippingInfoTableViewController.h"
#import "SBPickerSelector.h"
#import "WDShipping.h"
#import "UIColor+Style.h"
#import "LEAlertController.h"
#import "WDSourceConfig.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDOrderAPIController.h"
#import "Reachability.h"
#import "NSDictionary+SafeValues.h"
#import "UITextView+Placeholder.h"
#import "WDSelectPaymentTableViewController.h"
#import "WDCart.h"
#import "WDDish.h"
#import "WDDelivery.h"
#import "NSString+StringAdditions.h"
#import "WDUserTemp.h"

@interface WDShippingInfoTableViewController () <SBPickerSelectorDelegate>
@property (strong, nonatomic) SBPickerSelector *pickerTimeFrom;
@property (strong, nonatomic) SBPickerSelector *pickerTimeTo;
@property (strong, nonatomic) NSDate *datePickupFrom;
@property (strong, nonatomic) NSDate *datePickupTo;
@property (assign, nonatomic) BOOL isDelivery;
@property (assign, nonatomic) BOOL isHasDelivery;
@property (assign, nonatomic) BOOL isSaveInfo;

@end

@implementation WDShippingInfoTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.estimatedRowHeight = 500;
  self.tableView.rowHeight = UITableViewAutomaticDimension;
  [self loadData];
  
  self.btnCheckout.layer.cornerRadius = self.btnCheckout.frame.size.height/2; // this value vary as per your desire
  self.btnCheckout.layer.borderWidth = 1.0f;
  self.btnCheckout.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
  self.btnCheckout.clipsToBounds = YES;
  
  [self setPaymentDelivery:NO];
  self.title = @"More info";
  self.lbCouponCode.text = self.strCoupon;
  self.cartTotal.text = self.strCartTotal;
  float delivery = 0;
  NSMutableArray *listUser = [[NSMutableArray alloc] init];
  for (WDCart *cart in self.listCart) {
    if (![listUser containsObject: [NSNumber numberWithInteger:cart.userID]]) {
      [listUser addObject:[NSNumber numberWithInteger:cart.userID]];
      if(cart.dish.user.delivery){
        WDDelivery *object = cart.dish.user.delivery;
        if(object.currencyID != 0){
          delivery += object.fee;
        }
      }
    }
  }
  self.lbDelivery.text = [[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:delivery];
  self.orderTotal.text = [[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:self.totalPrice + delivery];
}

#pragma mark - Private

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if(!self.isDeliveryShip && indexPath.row==2) {
    return 0;
  }
  
  if(!self.isDelivery){
    if(indexPath.row==3||indexPath.row==4||indexPath.row==5||
       indexPath.row==6||indexPath.row==7||indexPath.row==8||
       indexPath.row==9||indexPath.row==10||indexPath.row==11||indexPath.row==12){
      return 0;
    }
  }
  return UITableViewAutomaticDimension;
}

- (void) setData:(WDShipping*)ship {
  self.tfName.text = ship.name;
  self.tfPhone.text = ship.phone;
  self.tfShipping.text = ship.address;
  
  //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  //    [formatter setDateFormat:@"HH:mm:ss"];
  //    self.datePickupFrom = [formatter dateFromString:self.dish.pickupTimeFrom];
  //    self.datePickupTo = [formatter dateFromString:self.dish.pickupTimeTo];
  //
  //    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
  //    [timeFormat setDateFormat:@"hh:mm a"];
  //    self.lbPickupTimeFrom.text = [timeFormat stringFromDate:self.datePickupFrom];
  //    self.lbPickupTimeTo.text = [timeFormat stringFromDate:self.datePickupTo];
  
}

-(void)setPaymentDelivery:(BOOL)isChecked {
  if(isChecked){
    if(!self.isHasDelivery) {
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No home delivery service available" preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
    }else{
      self.isDelivery = YES;
      [self.btnPickup setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
      [self.btnPayDelivery setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
    }
  }else{
    self.isDelivery = NO;
    [self.btnPickup setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
    [self.btnPayDelivery setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
  }
  [self.tableView reloadData];
}

-(void)setSave:(BOOL)isChecked {
  if(isChecked){
    self.isSaveInfo = YES;
    [self.btnSave setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
  }else{
    self.isSaveInfo = NO;
    [self.btnSave setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
  }
  [self.tableView reloadData];
}

- (BOOL)validate {
  if(self.isDelivery){
    if([[self.tfName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Name is required." preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
      return NO;
    }
    
    if([[self.tfPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Address is required." preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
      return NO;
    }
    
    if([[self.tfShipping.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Address is required." preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
      return NO;
    }
    
  }
  return YES;
}

#pragma mark - IBAction
- (IBAction)clickedApply:(id)sender {
}

- (IBAction)clickedBack:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedPickupTimeFrom:(id)sender {
  _pickerTimeFrom = [SBPickerSelector picker];
  _pickerTimeFrom.delegate = self;
  _pickerTimeFrom.doneButtonTitle = @"Done";
  _pickerTimeFrom.cancelButtonTitle = @"Cancel";
  _pickerTimeFrom.pickerType = SBPickerSelectorTypeDate;
  _pickerTimeFrom.datePickerType = SBPickerSelectorDateTypeOnlyHour;
  if(self.datePickupFrom){
    _pickerTimeTo.defaultDate = self.datePickupFrom;
  }
  [_pickerTimeFrom showPickerOver:self];
}

- (IBAction)clickedPickupTimeTo:(id)sender {
  _pickerTimeTo= [SBPickerSelector picker];
  _pickerTimeTo.delegate = self;
  _pickerTimeTo.doneButtonTitle = @"Done";
  _pickerTimeTo.cancelButtonTitle = @"Cancel";
  _pickerTimeTo.pickerType = SBPickerSelectorTypeDate;
  _pickerTimeTo.datePickerType = SBPickerSelectorDateTypeOnlyHour;
  if(self.datePickupTo){
    _pickerTimeTo.defaultDate = self.datePickupTo;
  }
  [_pickerTimeTo showPickerOver:self];
}

- (IBAction)clickCheckout:(id)sender {
  if([self validate]){
    if(self.isHasDelivery && self.isDelivery) {
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"\"Leave at door\" delivery is required" message:@"To help everyone stay safe at this time, no-contact deliveries are required in your area. Your order will be conveniently left at your door." preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self saveShipping];
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
    } else {
      [self saveShipping];
    }
  }
}

- (IBAction)clickedSave:(id)sender {
  if(self.isSaveInfo){
    [self setSave:NO];
  }else{
    [self setSave:YES];
  }
}

- (IBAction)clickedPickup:(id)sender {
  [self setPaymentDelivery:NO];
}

- (IBAction)clickedPayDelivery:(id)sender {
  [self setPaymentDelivery:YES];
}

- (void)loadData {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDOrderAPIController sharedInstance] getShipping:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        
      }else{
        if(responseObject) {
          NSDictionary *dataShip = [responseObject valueForKey:@"data"];
          if(![dataShip isKindOfClass:[NSNull class]]&&[dataShip safeNumberForKey:@"id"]>0){
            self.isHasDelivery = YES;
            self.ship = [[WDShipping alloc] init];
            [self.ship  setShippingObject:dataShip];
            [self setData:self.ship];
            [self setSave:YES];
            
          }else {
            self.isHasDelivery = NO;
            
          }
          dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
          });
        }
      }
    }];
  }
}

- (void) saveShipping {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    
    WDShipping *ship = [[WDShipping alloc] init];
    ship.name = self.tfName.text;
    ship.phone = self.tfPhone.text;
    ship.address = self.tfShipping.text;
    
    NSInteger isDelivery = 0;
    NSInteger saveInfo = 0;
    if(self.isDelivery){
      isDelivery = 1;
    }
    if(self.isSaveInfo){
      saveInfo = 1;
    }
    
    //Call Api
    [[WDOrderAPIController sharedInstance] saveShipping:ship saveInfo:saveInfo delivery:isDelivery completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if(responseObject) {
          WDSelectPaymentTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDSelectPaymentTableViewController"];
          viewController.listCart = self.listCart;
          [self.navigationController pushViewController:viewController animated:YES];
        }
      }
    }];
  }
}

@end
