//
//  WDMessageTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/5/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDMessageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonRead;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonDelete;

- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedEdit:(id)sender;
- (IBAction)clickedRead:(id)sender;
- (IBAction)clickedDelete:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

@end
