//
//  ExploreMenuViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "ExploreMenuViewController.h"
#import <PureLayout/PureLayout.h>
#import "WDExploreDishViewController.h"
#import "UIColor+Style.h"

@interface ExploreMenuViewController () <MSSPageViewControllerDelegate, MSSPageViewControllerDataSource>{
    MSSTabBarView *_tabBarView; // creating the tab bar in code - (self.tabBarView is weak)
}
@end

@implementation ExploreMenuViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Explore the menu";
    // create tab bar
    _tabBarView = [MSSTabBarView new];
    self.tabBarView = _tabBarView;
    
    [self.view addSubview:self.tabBarView];
    [self.tabBarView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(64.0f, 0.0f, 0.0f, 0.0f) excludingEdge:ALEdgeBottom];
    [self.tabBarView autoSetDimension:ALDimensionHeight toSize:44.0f];
    self.tabBarView.sizingStyle = MSSTabSizingStyleDistributed;
    [self createTabBar];

}

-(void)createTabBar {
    // set up tab bar
    self.tabBarView = _tabBarView;
    self.tabBarView.delegate = self;
    self.tabBarView.dataSource = self;
    
    [self.tabBarView setBackgroundColor:[UIColor whiteColor]];
    self.tabBarView.tabAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"Poppins-Medium" size:12],
                                      NSForegroundColorAttributeName : [UIColor colorFromHexString:@"959595" withAlpha:1.0],MSSTabTitleAlpha: @(0.8f),MSSTabTransitionAlphaEffectEnabled:@(YES)};
    
    self.tabBarView.selectedTabAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"Poppins-Medium" size:12],
                                              NSForegroundColorAttributeName : [UIColor colorFromHexString:@"e7663f" withAlpha:1.0],MSSTabIndicatorLineHeight:@(2.0f)};
    self.tabBarView.indicatorAttributes = @{NSForegroundColorAttributeName : [UIColor colorFromHexString:@"e7663f" withAlpha:1.0]};
    [self.tabBarContainerView mss_addExpandingSubview:self.tabBarView];
    [self.pageContainerView mss_addExpandingSubview:self.pageViewController.view];
    self.tabBarView.delegate = self;
    self.tabBarView.dataSource = self;
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
}

#pragma mark - MSSPageViewControllerDataSource

- (NSArray<UIViewController *> *)viewControllersForPageViewController:(MSSPageViewController *)pageViewController {
    WDExploreDishViewController *todayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDExploreDishViewController"];
    todayViewController.sellerID = self.sellerID;
    todayViewController.isToday = YES;
    
    WDExploreDishViewController *allViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDExploreDishViewController"];
    allViewController.sellerID = self.sellerID;
    
    return @[todayViewController, allViewController];
}

- (void)tabBarView:(MSSTabBarView *)tabBarView
       populateTab:(MSSTabBarCollectionViewCell *)tab
           atIndex:(NSInteger)index {
    if(index==0){
        tab.title = @"Today's Menu";
    }else if(index==1){
        tab.title = @"All";
    }
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
