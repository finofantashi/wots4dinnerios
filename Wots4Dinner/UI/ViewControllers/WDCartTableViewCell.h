//
//  WDCartTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDCart.h"
#import "WDDish.h"

@class WDCartTableViewCell;
@protocol WDCartTableViewCellDelegate <NSObject>
@optional
- (void)clickedPlus:(WDCartTableViewCell*)cartTableViewCell cart:(WDCart*)cart;
- (void)clickedMinus:(WDCartTableViewCell*)cartTableViewCell cart:(WDCart*)cart;
- (void)clickedDelete:(NSInteger)cartID;
- (void)clickedEditNote:(WDCart*)cart note:(NSString*)note;

@end

@interface WDCartTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbNote;
@property (weak, nonatomic) IBOutlet UIImageView *ivDish;
@property (weak, nonatomic) IBOutlet UILabel *lbPriceDish;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantity;
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;
@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantityLeft;
@property (weak, nonatomic) IBOutlet UILabel *lbNameDish;
@property (weak, nonatomic) IBOutlet UILabel *lbTotalPrice;
@property (strong, nonatomic) WDCart *cart;
@property (nonatomic, weak) id <WDCartTableViewCellDelegate> delegate;

- (void)updateTotalPricePlus;
- (void)updateTotalPriceMinus;
- (void)enableButtonPlus:(BOOL)isEnable;
- (void)enableButtonMinus:(BOOL)isEnable;

- (IBAction)clickedMinus:(id)sender;
- (IBAction)clickedPlus:(id)sender;
- (IBAction)clickedDelete:(id)sender;
- (IBAction)clickedEditNote:(id)sender;

@end
