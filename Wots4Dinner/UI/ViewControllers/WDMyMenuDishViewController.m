//
//  WDMyMenuDishViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/21/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMyMenuDishViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDFoodMenuAPIController.h"
#import "WDDishSearch.h"
#import "WDMyMenuTableViewCell.h"
#import "WDDishDetailTableViewController.h"
#import "WDSourceConfig.h"
#import "WDBecomeSellerStepTwoTableViewController.h"
#import "NSString+StringAdditions.h"
#import "WDMainViewController.h"

@interface WDMyMenuDishViewController () <UIScrollViewDelegate, WDMyMenuTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *listDish;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;
@end

@implementation WDMyMenuDishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.listDish = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    if(self.isToday){
        [self loadTodayData:self.currentPage];
        
    }else{
        [self loadAllData:self.currentPage];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.currentPage == self.totalPages
        || self.totalItems == self.listDish.count) {
        return self.listDish.count;
    }
    return self.listDish.count + 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentPage != self.totalPages && indexPath.row == [self.listDish count] - 1 ) {
        if(self.isToday){
            [self loadTodayData:++self.currentPage];
            
        }else{
            [self loadAllData:++self.currentPage];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDMyMenuTableViewCell";
    UITableViewCell *cell;
    if (indexPath.row == [self.listDish count]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:100];
        [activityIndicator startAnimating];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(indexPath.row <= [self.listDish count]){
            WDDishSearch *dish = self.listDish[indexPath.row];
            ((WDMyMenuTableViewCell*)cell).myMenuDelegate = self;
            ((WDMyMenuTableViewCell*)cell).dishId = dish.dishID;
            ((WDMyMenuTableViewCell*)cell).dish = dish;

            //get image name and assign
            NSString *imageURL = [[NSString stringWithFormat:@"%@%@", kImageDomain,dish.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            [((WDMyMenuTableViewCell*)cell).ivDish sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                                     placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
            ((WDMyMenuTableViewCell*)cell).lbNameDish.text = dish.dishName;
            ((WDMyMenuTableViewCell*)cell).lbFoodType.text = dish.foodTypeName;
            if([dish.dishStatus isEqualToString:@"enabled"]){
                [((WDMyMenuTableViewCell*)cell).btnActive setImage:[UIImage imageNamed:@"ic_invisible"] forState:UIControlStateNormal];
            }else{
                [((WDMyMenuTableViewCell*)cell).btnActive setImage:[UIImage imageNamed:@"ic_visible"] forState:UIControlStateNormal];
            }
            
            ((WDMyMenuTableViewCell*)cell).lbPrice.text = [[NSString string] formatCurrency:dish.currencyCode symbol:dish.currencySymbol price:dish.price];
            [ ((WDMyMenuTableViewCell*)cell) cellOnTableView:self.tableView didScrollOnView:self.view];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    WDDishSearch *dish = self.listDish[indexPath.row];
    WDDishDetailTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDDishDetailTableViewController"];
    viewController.dishID = dish.dishID;
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Get visible cells on table view.
    NSArray *visibleCells = [self.tableView visibleCells];
    
    for (UITableViewCell *cell in visibleCells) {
        if([cell isKindOfClass:[WDMyMenuTableViewCell class]] ){
            [(WDMyMenuTableViewCell*)cell cellOnTableView:self.tableView didScrollOnView:self.view];
        }
    }
}

#pragma mark - Table view data source

- (void)clickedEdit:(NSInteger)dishId {
    WDBecomeSellerStepTwoTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepTwoTableViewController"];
    viewController.isFromSlideMenu = YES;
    viewController.isUpdate = YES;
    viewController.dishID = dishId;
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
}

- (void)clickedActived:(WDDishSearch *)dish {
    NSString *message = @"";
    if([dish.dishStatus isEqualToString:@"disabled"]){
        message = @"Are you sure you want to activate this dish?";
    }else{
        message = @"Are you sure you want to deactivate this dish?";
    }
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:message preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *cancel = [LEAlertAction actionWithTitle:@"Cancel" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
    }];
    LEAlertAction *ok = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        [self changeStatus:dish];
    }];
    [alertController addAction:cancel];
    [alertController addAction:ok];
    [self presentAlertController:alertController animated:YES completion:nil];
}

- (void)clickedDelete:(WDDishSearch *)dish{
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Are you sure you want to delete?" preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *cancel = [LEAlertAction actionWithTitle:@"Cancel" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
    }];
    LEAlertAction *ok = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        [self deleteDish:dish];
    }];
    [alertController addAction:cancel];
    [alertController addAction:ok];
    [self presentAlertController:alertController animated:YES completion:nil];
}

#pragma mark - API

- (void)loadTodayData:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //[MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getMyMenuToday:page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDDishSearch *dish = [[WDDishSearch alloc] init];
                        [dish setDishObject:object];
                        [self.listDish addObject:dish];
                    };
                    
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No Dish." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)loadAllData:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //[MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getMyMenuAll:page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDDishSearch *dish = [[WDDishSearch alloc] init];
                        [dish setDishObject:object];
                        [self.listDish addObject:dish];
                    };
                    
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No Dish." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)changeStatus:(WDDishSearch*)dish {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        NSString *status;
        if([dish.dishStatus isEqualToString:@"disabled"]){
            status = @"enabled";
        }else{
            status = @"disabled";
        }
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] changeStatusDish:dish.dishID status:status
                                                   completionBlock:^(NSString *errorString, id responseObject) {
                                                       [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
                                                       if(errorString){
                                                           LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                                                           LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                                                               // handle default button action
                                                           }];
                                                           [alertController addAction:defaultAction];
                                                           [self presentAlertController:alertController animated:YES completion:nil];
                                                       }else{
                                                           if(responseObject) {
                                                               dish.dishStatus = status;
                                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                                   [self.tableView reloadData];
                                                               });
                                                           }
                                                       }
                                                   }];
    }
}

- (void)deleteDish:(WDDishSearch*)dish {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] deleteDish:dish.dishID completion:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    [self.listDish removeAllObjects];
                    self.currentPage = 0;
                    self.totalPages = 0;
                    self.totalItems = 0;
                    if(self.isToday){
                        [self loadTodayData:self.currentPage];
                        
                    }else{
                        [self loadAllData:self.currentPage];
                    }
                }
            }
        }];
    }
}
@end
