//
//  WDDishDetailMoreViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/25/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDDishDetailMoreViewController.h"
#import "UIColor+Style.h"
#import "WDDelivery.h"
#import "NSString+StringAdditions.h"
#import "WDCurrency.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "LEAlertController.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDUserTemp.h"

@interface WDDishDetailMoreViewController () {
  BOOL isHaveDelivery;
  BOOL isHaveRecommended;
}
@end

@implementation WDDishDetailMoreViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = @"More Info";
  [self getCurrenciesAPI];
  
}

#pragma mark Private

- (NSString*)pickupTimeFormat:(NSString*) stringDate{
  if(!stringDate){
    return @"";
  }
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
  [dateFormatter setDateFormat:@"HH:mm:ss"];
  NSDate *date = [dateFormatter dateFromString:stringDate];
  
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"hh:mm"];
  return [formatter stringFromDate:date];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if(indexPath.row==2||indexPath.row==4||indexPath.row==8||indexPath.row==10){
    if(indexPath.row==8&&!isHaveRecommended){
      return 0;
    }
    if(indexPath.row==10&&!isHaveDelivery){
      return 0;
    }
    return 1;
  }else if(indexPath.row==5){
    NSArray* days = [self.dish.repeat componentsSeparatedByString: @","];
    if(days.count==7||days.count==0||
       (days.count==2&&[self.dish.repeat rangeOfString:@"Saturday"].location != NSNotFound&&[self.dish.repeat rangeOfString:@"Sunday"].location != NSNotFound)||
       (days.count==5&&[self.dish.repeat rangeOfString:@"Saturday"].location == NSNotFound&&[self.dish.repeat rangeOfString:@"Sunday"].location == NSNotFound)){
      return 56;
    }else {
      return 110;
    }
  }else if(indexPath.row==7){
    float height = 28;
    if(self.dish.deliveries&&self.dish.deliveries.count>0){
      WDDelivery *object  = [self.dish.deliveries objectAtIndex:0];
      if(object.recommendation.length>0){
        height += 28;
        isHaveRecommended = YES;
      }
    }
    if(height==28){
      return 0;
    }
    return height;
  }else if(indexPath.row==9){
    float height = 28;
    if(self.dish.deliveries&&self.dish.deliveries.count>0){
      WDDelivery *object  = [self.dish.deliveries objectAtIndex:0];
      if(object.specialOffer.length>0){
        height += 28;
        isHaveDelivery = YES;
      }
    }
    if(height==28){
      return 0;
    }
    return height;
  }else if(indexPath.row==11){
    return 170;
  }
  
  return 56;
}

- (void) setData:(WDDish*)dish {
  //Pick time
  self.lbPickupTime.text = [NSString stringWithFormat:@"%@ - %@",[self pickupTimeFormat: dish.pickupTimeFrom],[self pickupTimeFormat: dish.pickupTimeTo]];
  //Serving Date
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  NSDate *dateFromString = [[NSDate alloc] init];
  dateFromString = [dateFormatter dateFromString:dish.servingDate];
  NSString *stringDate = [NSDateFormatter localizedStringFromDate:dateFromString
                                                        dateStyle:NSDateFormatterMediumStyle
                                                        timeStyle:NSDateFormatterNoStyle];
  if(!stringDate){
    stringDate = @"";
  }
  
  self.lbServingDate.text = [NSString stringWithFormat:@"%@",stringDate];
  
  NSArray* days = [dish.repeat componentsSeparatedByString: @","];
  if(days.count==7){
    self.lbRepeat.text = [NSString stringWithFormat:@"Everyday"];
  }else if(days.count==2&&[dish.repeat rangeOfString:@"Saturday"].location != NSNotFound&&[dish.repeat rangeOfString:@"Sunday"].location != NSNotFound){
    self.lbRepeat.text = [NSString stringWithFormat:@"Weekends"];
  }else if(days.count==5&&[dish.repeat rangeOfString:@"Saturday"].location == NSNotFound&&[dish.repeat rangeOfString:@"Sunday"].location == NSNotFound){
    self.lbRepeat.text = [NSString stringWithFormat:@"Weekdays"];
  }else if(days.count>0){
    self.lbRepeat.text = [NSString stringWithFormat:@"%@",dish.repeat];
  }
  
  if(dish.deliveries&&dish.deliveries.count>0){
    WDDelivery *object  = [self.dish.deliveries objectAtIndex:0];
    if(object.specialOffer.length>0){
      self.lbSpecialOffer.text = object.specialOffer;
    }
    if(dish.user.delivery.fee > 0){
      NSDictionary *localeInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @"USD", NSLocaleCurrencyCode,
                                  [[NSLocale preferredLanguages] objectAtIndex:0], NSLocaleLanguageCode,
                                  nil];
      NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:
                          [NSLocale localeIdentifierFromComponents:localeInfo]];
      NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
      [fmt setNumberStyle:NSNumberFormatterCurrencyStyle];
      [fmt setLocale:locale];
      [fmt setCurrencySymbol:@""];
      
      NSString *content = [NSString stringWithFormat:@"I can deliver for %@ %@ in a %@ radius", [fmt stringFromNumber:[NSNumber numberWithFloat:dish.user.delivery.fee]], [self getCurrencyName:dish.user.delivery.currencyID], dish.user.delivery.radius];
      NSMutableString *mutableString = [[NSMutableString alloc] initWithString:content];
      
      NSString *text1 = [NSString stringWithFormat:@"%@ %@", [fmt stringFromNumber:[NSNumber numberWithFloat:dish.user.delivery.fee]], [self getCurrencyName:dish.user.delivery.currencyID]];
      NSRange range = [mutableString rangeOfString:text1 options:NSCaseInsensitiveSearch];
      NSRange range1 = [mutableString rangeOfString:dish.user.delivery.radius options:NSCaseInsensitiveSearch];
      
      NSMutableAttributedString *attr =  [[NSMutableAttributedString alloc] initWithString:content];
      if (range.location != NSNotFound) {
        [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0] range:range];
      }
      if (range1.location != NSNotFound) {
        [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0] range:range1];
      }
      self.lbDeliveryService.attributedText = attr;
      
    } else if (dish.user.delivery.isDelivery != 1) {
      self.lbDeliveryService.text = @"N/A";
    } else {
      NSMutableAttributedString *attr =  [[NSMutableAttributedString alloc] initWithString:@"I can deliver for Free"];
      [attr addAttribute:NSForegroundColorAttributeName value:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0] range:NSMakeRange(18, 4)];
      self.lbDeliveryService.attributedText = attr;
    }
  }
  
}

- (NSString*)getCurrencyName:(NSInteger)currencyID {
  for (WDCurrency *object in self.currencies) {
    if(object.currencyID == currencyID){
      return object.code;
    }
  }
  return @"";
}
#pragma mark IBAction

- (IBAction)clickedBack:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (void) getCurrenciesAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getCurrencies:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          self.currencies = [[NSMutableArray alloc] init];
          NSDictionary *dataCurrencies = [responseObject valueForKey:@"data"];
          for (NSDictionary *object in dataCurrencies) {
            WDCurrency *currency = [[WDCurrency alloc] init];
            [currency setCurrencyObject:object];
            [self.currencies addObject:currency];
          };
          [self setData:self.dish];
        }
      }
    }];
  }
}
@end
