//
//  WDBlogCommentViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDUserTemp.h"

@protocol WDBlogCommentTableViewCellDelegate <NSObject>
@optional
- (void)clickedAvatar:(WDUserTemp *)user;
@end

@interface WDBlogCommentTableViewCell : UITableViewCell

@property (strong, nonatomic) WDUserTemp *user;
@property (nonatomic, weak) id <WDBlogCommentTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbLocation;
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;

- (IBAction)clickedAvatar:(id)sender;
@end
