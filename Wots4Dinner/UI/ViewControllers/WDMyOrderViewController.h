//
//  WDMyOrderViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIResizableTableView.h"

@interface WDMyOrderViewController : UIViewController
@property (weak, nonatomic) IBOutlet MIResizableTableView *tableView;
@property (nonatomic, assign) BOOL isToday;
@property (weak, nonatomic) IBOutlet UITextField *tfSearchView;
- (IBAction)clickedSearch:(id)sender;

@end
