//
//  WDSearchAroundViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/20/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSearchAroundViewController.h"
#import "WDMainViewController.h"
#import <LEAlertController/LEAlertController.h>
#import "PBBlueDotView.h"
#import "WDUserDefaultsManager.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDDishSearch.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDSourceConfig.h"
#import "WDDishDetailTableViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@import GooglePlaces;

@interface WDSearchAroundViewController () <PlaceSearchTextFieldDelegate, GMSMapViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate> {
  // Declare a GMSMarker instance at the class level.
  GMSMarker *infoMarker;
  BOOL isHaveCurrentLocation;
  GMSPlacesClient *_placesClient;
  CLLocation *currentLocation;
  CLLocationCoordinate2D searchtLocation;
  BOOL isTextChange;
  PBBlueDotView *blueDotView;
  BOOL isClickedSearch;
}
@end

@implementation WDSearchAroundViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = false;
  
  _placesClient = [GMSPlacesClient sharedClient];
  [self.viewMap addObserver:self forKeyPath:@"myLocation" options:0 context:nil];
  self.viewMap.myLocationEnabled = YES;
  self.viewMap.settings.myLocationButton = YES;
  self.viewMap.settings.indoorPicker = NO;
  self.viewMap.delegate = self;
  
  self.tfLocation.delegate = self;
  
  self.tfLocation.placeSearchDelegate                 = self;
  self.tfLocation.strApiKey                           = @"AIzaSyAZDt39UV2JIP_zlfaGOWNSy3CaVA7dX-0";
  self.tfLocation.superViewOfList                     = self.view;  // View, on which Autocompletion list should be appeared.
  self.tfLocation.autoCompleteShouldHideOnSelection   = YES;
  self.tfLocation.maximumNumberOfAutoCompleteRows     = 5;
  blueDotView = [[PBBlueDotView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width)];
  _placesClient = [GMSPlacesClient sharedClient];
  [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
    if (error != nil) {
      NSLog(@"Pick Place error %@", [error localizedDescription]);
      return;
    }
    if (placeLikelihoodList != nil) {
      GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
      if (place != nil) {
        self.tfLocation.text = place.formattedAddress;
      }else{
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your location." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }
    }
  }];
  isClickedSearch = NO;
  
  self.viewSearch.layer.cornerRadius = 10; // this value vary as per your desire
  self.viewSearch.clipsToBounds = YES;
  
  self.btnSearchAround.layer.cornerRadius = self.btnSearchAround.frame.size.height/2; // this value vary as per your desire
  self.btnSearchAround.clipsToBounds = YES;
  
  self.viewPopular1.layer.cornerRadius = 6;
  self.viewPopular1.clipsToBounds = YES;
  self.viewPopular1.layer.borderColor=[[UIColor whiteColor] CGColor];
  self.viewPopular1.layer.borderWidth = 1;
  
  self.viewPopular2.layer.cornerRadius = 10;
  self.viewPopular2.clipsToBounds = YES;
  self.viewPopular2.layer.borderColor=[[UIColor whiteColor] CGColor];
  self.viewPopular2.layer.borderWidth = 1;
  
  self.viewPopular3.layer.cornerRadius = 10;
  self.viewPopular3.clipsToBounds = YES;
  self.viewPopular3.layer.borderColor=[[UIColor whiteColor] CGColor];
  self.viewPopular3.layer.borderWidth = 1;
  
  //    self.btSkip.titleLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
  //    self.btSkip.titleLabel.layer.shadowRadius = 2.0f;
  //    self.btSkip.titleLabel.layer.shadowOpacity = .9;
  //    self.btSkip.titleLabel.layer.shadowOffset = CGSizeZero;
  //    self.btSkip.titleLabel.layer.masksToBounds = NO;
  
  if ([CLLocationManager locationServicesEnabled]){
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
      UIAlertController *alertController = [UIAlertController
                                            alertControllerWithTitle:@"App Permission Denied"
                                            message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                            preferredStyle:UIAlertControllerStyleAlert];
      UIAlertAction *cancelAction = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleCancel
                                     handler:nil];
      [alertController addAction:cancelAction];
      [self presentViewController:alertController animated:YES completion:nil];
    }
  }
}

- (void)viewWillAppear:(BOOL)animated {
  [self.viewMap addObserver:self forKeyPath:@"myLocation" options:0 context:nil];
}

-(void)viewDidAppear:(BOOL)animated{
  
  //Optional Properties
  self.tfLocation.autoCompleteRegularFontName =  @"HelveticaNeue-Bold";
  self.tfLocation.autoCompleteBoldFontName = @"HelveticaNeue";
  self.tfLocation.autoCompleteTableCornerRadius=0.0;
  self.tfLocation.autoCompleteRowHeight=35;
  self.tfLocation.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
  self.tfLocation.autoCompleteFontSize=14;
  self.tfLocation.autoCompleteTableBorderWidth=1.0;
  self.tfLocation.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=YES;
  self.tfLocation.autoCompleteShouldHideOnSelection=YES;
  self.tfLocation.autoCompleteShouldHideClosingKeyboard=YES;
  self.tfLocation.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
  self.tfLocation.autoCompleteTableFrame = CGRectMake(self.viewContent.frame.origin.x, self.viewContent.frame.size.height+74, self.viewContent.frame.size.width, 200.0);
}

- (void)viewDidDisappear:(BOOL)animated {
  [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = true;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)dealloc {
  [self.viewMap removeObserver:self forKeyPath:@"myLocation"];
}

#pragma mark - Place search Textfield Delegates

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict{
  [self.view endEditing:YES];
  searchtLocation = responseDict.coordinate;
  [self.viewMap animateToLocation:searchtLocation];
  [self.viewMap animateToZoom:14];
  [self.viewMap clear];
  GMSMarker *marker = [GMSMarker markerWithPosition:searchtLocation];
  marker.appearAnimation = YES;
  marker.map = self.viewMap;
  marker.iconView = blueDotView;
  [marker setGroundAnchor:CGPointMake(0.5f, 0.5f)];
  [marker setInfoWindowAnchor:CGPointMake(0.5f, 0.5f)];
}

-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField{
  
}

-(void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField{
  
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index{
  if(index%2==0){
    cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
  }else{
    cell.contentView.backgroundColor = [UIColor whiteColor];
  }
}

#pragma mark - UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  isTextChange = YES;
  searchtLocation = CLLocationCoordinate2DMake(0, 0);
  return YES;
}

#pragma mark - Map

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
  if([keyPath isEqualToString:@"myLocation"]) {
    if(!isHaveCurrentLocation){
      CLLocation *location = [object myLocation];
      currentLocation = location;
      [self.viewMap clear];
      GMSMarker *marker = [GMSMarker markerWithPosition:currentLocation.coordinate];
      marker.appearAnimation = YES;
      marker.map = self.viewMap;
      marker.iconView = blueDotView;
      [marker setGroundAnchor:CGPointMake(0.5f, 0.5f)];
      [marker setInfoWindowAnchor:CGPointMake(0.5f, 0.5f)];
      CLLocationCoordinate2D target =
      CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
      [self.viewMap removeObserver:self forKeyPath:@"myLocation"];
      [self.viewMap animateToLocation:target];
      [self.viewMap animateToZoom:14];
      if(location!=nil) {
        [self getAdrressFromLatLong:location.coordinate.latitude lon:location.coordinate.longitude];
      }
      isHaveCurrentLocation = YES;
      isTextChange = NO;
      NSDictionary *current = @{@"kLng" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude] doubleValue]],
                                @"kLat" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude] doubleValue]]};
      [WDUserDefaultsManager setValue:current forKey:@"kCurrentLocation"];
      [self popularOrdersAPI];
      [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
        if (error != nil) {
          NSLog(@"Pick Place error %@", [error localizedDescription]);
          return;
        }
        if (placeLikelihoodList != nil) {
          GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
          if (place != nil) {
            self.tfLocation.text = place.formattedAddress;
          }else{
            LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your location." preferredStyle:LEAlertControllerStyleAlert];
            LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
              // handle default button action
            }];
            [alertController addAction:defaultAction];
            [self presentAlertController:alertController animated:YES completion:nil];
          }
        }
      }];
    }
  }
}

- (CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
  double latitude = 0, longitude = 0;
  NSString *esc_addr =  [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
  NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
  NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
  if (result) {
    NSScanner *scanner = [NSScanner scannerWithString:result];
    if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
      [scanner scanDouble:&latitude];
      if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
        [scanner scanDouble:&longitude];
      }
    }
  }
  CLLocationCoordinate2D center;
  center.latitude=latitude;
  center.longitude = longitude;
  NSLog(@"View Controller get Location Logitute : %f",center.latitude);
  NSLog(@"View Controller get Location Latitute : %f",center.longitude);
  return center;
}

- (void)getAdrressFromLatLong : (CGFloat)lat lon:(CGFloat)lon{
  NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&amp;sensor=false",lat,lon];
  
  NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]]];
  
  NSURLResponse *response = nil;
  NSError *requestError = nil;
  NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&requestError];
  NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
  
  if ([[result valueForKey:@"status"] isEqualToString:@"OK"] ) {
    NSArray *resultsArray = [result valueForKey:@"results"];
    NSString *address = nil;
    
    if ([resultsArray count]>0) {
      address = [[resultsArray objectAtIndex:0] valueForKey:@"formatted_address"];
      self.tfLocation.text = address;
    }
    // use the address variable to access the ADDRESS :)
  } else {
    
  }
}

#pragma mark - IBAction

- (IBAction)clickedSearchAround:(id)sender {
  if([self.tfLocation.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length!=0&&!isClickedSearch){
    if(isTextChange&&searchtLocation.latitude!=0){
      isClickedSearch = YES;
      [blueDotView setEnableAnimation:YES];
      [self.viewMap animateToLocation:searchtLocation];
      [self.viewMap animateToZoom:14];
      double delayInSeconds = 5.0;
      dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
      dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSDictionary *current = @{@"kLng" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",searchtLocation.longitude] doubleValue]],
                                  @"kLat" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",searchtLocation.latitude] doubleValue]]};
        [WDUserDefaultsManager setValue:current forKey:@"kCurrentLocation"];
        [self popView];
      });
      
    }else if(!isTextChange&&currentLocation.coordinate.latitude!=0){
      [blueDotView setEnableAnimation:YES];
      isClickedSearch = YES;
      [self.viewMap animateToLocation:currentLocation.coordinate];
      [self.viewMap animateToZoom:14];
      double delayInSeconds = 5.0;
      dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
      dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSDictionary *current = @{@"kLng" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude] doubleValue]],
                                  @"kLat" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude] doubleValue]]};
        [WDUserDefaultsManager setValue:current forKey:@"kCurrentLocation"];
        [self popView];
      });
    }else{
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your location." preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
    }
  }else if(!isClickedSearch){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please input your location." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }
}

- (IBAction)clickedSkip:(id)sender {
  if(currentLocation.coordinate.latitude!=0){
    NSDictionary *current = @{@"kLng" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude] doubleValue]],
                              @"kLat" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude] doubleValue]]};
    [WDUserDefaultsManager setValue:current forKey:@"kCurrentLocation"];
    [self popView];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SkipSearch" object:self];
  }else{
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please enable current your location." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }
  
}

- (IBAction)clickedPopular:(id)sender {
  self.tag = [sender tag];
  if([self.tfLocation.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length!=0&&!isClickedSearch){
    if(isTextChange&&searchtLocation.latitude!=0){
      NSDictionary *current = @{@"kLng" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",searchtLocation.longitude] doubleValue]],
                                @"kLat" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",searchtLocation.latitude] doubleValue]]};
      [WDUserDefaultsManager setValue:current forKey:@"kCurrentLocation"];
      [[WDMainViewController sharedInstance] changeHomeSharedInstance];
      double delayInSeconds = 0.5;
      dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
      dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSInteger dishID = 0;
        if(self.tag == 1 && self.listDish.count>=1){
          WDDishSearch *dish = self.listDish[0];
          dishID = dish.dishID;
        }else if (self.tag == 2 && self.listDish.count>=2) {
          WDDishSearch *dish = self.listDish[1];
          dishID = dish.dishID;
        }else if(self.tag == 3 && self.listDish.count>=3){
          WDDishSearch *dish = self.listDish[2];
          dishID = dish.dishID;
        }
        WDDishDetailTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishDetailTableViewController"];
        viewController.dishID = dishID;
        [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
      });
      
    }else if(!isTextChange&&currentLocation.coordinate.latitude!=0){
      NSDictionary *current = @{@"kLng" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude] doubleValue]],
                                @"kLat" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude] doubleValue]]};
      [WDUserDefaultsManager setValue:current forKey:@"kCurrentLocation"];
      [[WDMainViewController sharedInstance] changeHomeSharedInstance];
      double delayInSeconds = 0.5;
      dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
      dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSInteger dishID = 0;
        if(self.tag == 1 && self.listDish.count>=1){
          WDDishSearch *dish = self.listDish[0];
          dishID = dish.dishID;
        }else if (self.tag == 2 && self.listDish.count>=2) {
          WDDishSearch *dish = self.listDish[1];
          dishID = dish.dishID;
        }else if(self.tag == 3 && self.listDish.count>=3){
          WDDishSearch *dish = self.listDish[2];
          dishID = dish.dishID;
        }
        WDDishDetailTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishDetailTableViewController"];
        viewController.dishID = dishID;
        [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
      });
      
    }else{
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your location." preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
    }
  }else if(!isClickedSearch){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please input your location." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }
}

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
  [self.tfLocation resignFirstResponder];
}

-(BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView {
  [self getAdrressFromLatLong:mapView.myLocation.coordinate.latitude lon:mapView.myLocation.coordinate.longitude];
  [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
    if (error != nil) {
      NSLog(@"Pick Place error %@", [error localizedDescription]);
      return;
    }
    if (placeLikelihoodList != nil) {
      GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
      if (place != nil) {
        self.tfLocation.text = place.formattedAddress;
      }else{
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your location." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }
    }
  }];
  
  isTextChange = NO;
  currentLocation = mapView.myLocation;
  [self.viewMap clear];
  GMSMarker *marker = [GMSMarker markerWithPosition:currentLocation.coordinate];
  marker.appearAnimation = YES;
  marker.map = self.viewMap;
  marker.iconView = blueDotView;
  [marker setGroundAnchor:CGPointMake(0.5f, 0.5f)];
  [marker setInfoWindowAnchor:CGPointMake(0.5f, 0.5f)];
  
  [self.viewMap animateToLocation:currentLocation.coordinate];
  [self.viewMap animateToZoom:14];
  
  return YES;
}

// Attach an info window to the POI using the GMSMarker.
- (void)mapView:(GMSMapView *)mapView
didTapPOIWithPlaceID:(NSString *)placeID
           name:(NSString *)name
       location:(CLLocationCoordinate2D)location {
  infoMarker = [GMSMarker markerWithPosition:location];
  infoMarker.snippet = placeID;
  infoMarker.title = name;
  infoMarker.opacity = 0;
  CGPoint pos = infoMarker.infoWindowAnchor;
  pos.y = 1;
  infoMarker.infoWindowAnchor = pos;
  infoMarker.map = mapView;
  mapView.selectedMarker = infoMarker;
}

- (void)popularOrdersAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getPopularOrders:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          self.listDish = [[NSMutableArray alloc] init];
          NSDictionary *dataDish = [responseObject valueForKey:@"data"];
          int count = 0;
          for (NSDictionary *object in dataDish) {
            count ++;
            WDDishSearch *dish = [[WDDishSearch alloc] init];
            [dish setDishObject:object];
            [self.listDish addObject:dish];
            if(count>=3){
              break;
            }
          };
          if(self.listDish.count==0){
            self.viewPopular.hidden = YES;
          }else{
            self.viewMap.padding = UIEdgeInsetsMake(0, 0, 110, 0);
            self.viewPopular.hidden = NO;
            
            if(self.listDish.count==1){
              self.lbPopular1.hidden = NO;
              self.lbPopular2.hidden = YES;
              self.lbPopular3.hidden = YES;
              
              self.viewPopular1.hidden = NO;
              self.viewPopular2.hidden = YES;
              self.viewPopular3.hidden = YES;
              self.constraintLeft.constant = 16;
              self.constraintRight.constant =16;
              self.constraintLbLeft.constant = 16;
              
              WDDishSearch *dish = [self.listDish objectAtIndex:0];
              NSString *imageURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
              [self.imageViewPopular1 sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                        placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
              self.lbPopular1.text = dish.dishName;
              NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.lbPopular1.text];
              NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
              paragraphStyle.lineSpacing = 40;
              [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.lbPopular1.text.length)];
            } else if(self.listDish.count==2){
              self.lbPopular1.hidden = NO;
              self.lbPopular2.hidden = NO;
              self.lbPopular3.hidden = YES;
              
              self.viewPopular1.hidden = NO;
              self.viewPopular2.hidden = NO;
              self.viewPopular3.hidden = YES;
              self.constraintLeft.constant = 74.5;
              self.constraintRight.constant = 74.5;
              self.constraintLbLeft.constant = 74.5;
              
              WDDishSearch *dish = [self.listDish objectAtIndex:0];
              WDDishSearch *dish1 = [self.listDish objectAtIndex:1];
              
              NSString *imageURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
              NSString *imageURL1 = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish1.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
              
              [self.imageViewPopular1 sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                        placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
              self.lbPopular1.text = dish.dishName;
              
              [self.imageViewPopular2 sd_setImageWithURL:[NSURL URLWithString:imageURL1]
                                        placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
              self.lbPopular2.text = dish1.dishName;
              NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.lbPopular1.text];
              NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
              paragraphStyle.lineSpacing = 40;
              [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.lbPopular1.text.length)];
              
              NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc] initWithString:self.lbPopular2.text];
              NSMutableParagraphStyle *paragraphStyle2 = [[NSMutableParagraphStyle alloc] init];
              paragraphStyle.lineSpacing = 40;
              [attributedString2 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle2 range:NSMakeRange(0, self.lbPopular2.text.length)];
              
            }else{
              self.lbPopular1.hidden = NO;
              self.lbPopular2.hidden = NO;
              self.lbPopular3.hidden = NO;
              
              self.viewPopular1.hidden = NO;
              self.viewPopular2.hidden = NO;
              self.viewPopular3.hidden = NO;
              
              self.constraintLeft.constant = 16;
              self.constraintRight.constant =16;
              self.constraintLbLeft.constant = 16;
              
              WDDishSearch *dish = [self.listDish objectAtIndex:0];
              WDDishSearch *dish1 = [self.listDish objectAtIndex:1];
              WDDishSearch *dish2 = [self.listDish objectAtIndex:2];
              
              NSString *imageURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
              NSString *imageURL1 = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish1.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
              NSString *imageURL2 = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish2.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
              
              [self.imageViewPopular1 sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                        placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
              self.lbPopular1.text = dish.dishName;
              
              [self.imageViewPopular2 sd_setImageWithURL:[NSURL URLWithString:imageURL1]
                                        placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
              self.lbPopular2.text = dish1.dishName;
              
              [self.imageViewPopular3 sd_setImageWithURL:[NSURL URLWithString:imageURL2]
                                        placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
              self.lbPopular3.text = dish2.dishName;
              NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.lbPopular1.text];
              NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
              paragraphStyle.lineHeightMultiple = 0.8;
              [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.lbPopular1.text.length)];
              self.lbPopular1.attributedText = attributedString;
              
              NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc] initWithString:self.lbPopular2.text];
              NSMutableParagraphStyle *paragraphStyle2 = [[NSMutableParagraphStyle alloc] init];
              paragraphStyle2.lineHeightMultiple = 0.8;
              [attributedString2 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle2 range:NSMakeRange(0, self.lbPopular2.text.length)];
              self.lbPopular2.attributedText = attributedString2;
              
              NSMutableAttributedString *attributedString3 = [[NSMutableAttributedString alloc] initWithString:self.lbPopular3.text];
              NSMutableParagraphStyle *paragraphStyle3 = [[NSMutableParagraphStyle alloc] init];
              paragraphStyle3.lineHeightMultiple = 0.8;
              [attributedString3 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle3 range:NSMakeRange(0, self.lbPopular3.text.length)];
              self.lbPopular3.attributedText = attributedString3;
            }
          }
        } else {
          self.viewPopular.hidden = YES;
        }
      }
    }];
  }
}

- (void) popView {
  [[WDMainViewController sharedInstance] changeHomeSharedInstance];
}
@end
