//
//  WDBlogPostViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDBlog.h"

@interface WDBlogPostViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *tfComment;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIView *viewReviews;
@property (weak, nonatomic) IBOutlet UILabel *lbReviews;
@property (nonatomic, strong) WDBlog *blog;
@property (nonatomic, assign) BOOL isWatchReviews;
- (IBAction)textChanged:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstrant;

- (IBAction)clickedSend:(id)sender;
@end
