//
//  WDProfileViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDProfileViewController.h"
#import "UIColor+Style.h"
#import "WDUser.h"
#import "TOCropViewController.h"
#import <Reachability/Reachability.h>
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDUserAPIController.h"
#import "FCFileManager.h"
#import "UIImage+ResizeMagick.h"
#import "UIImageView+AFNetworking.h"
#import "WDSourceConfig.h"
#import "WDMainViewController.h"
#import "WDWishListTableViewController.h"
#import "WDFavoriteSellerTableViewController.h"
@import BonMot;
@interface WDProfileViewController () <UINavigationControllerDelegate,UIImagePickerControllerDelegate, TOCropViewControllerDelegate>

@property (nonatomic, strong) UIImage *image;           // The image we'll be cropping

@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (nonatomic, assign) CGRect croppedFrame;
@property (nonatomic, assign) NSInteger angle;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
@property (nonatomic, strong) UIPopoverController *activityPopoverController;
#pragma clang diagnostic pop

- (void)showCropViewController;

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController;

@end

@implementation WDProfileViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLayout];
    [self setData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setData];
}

#pragma mark Private

- (void) setUpLayout {
    self.ivAvatar.layer.cornerRadius = 120 / 2;
    self.ivAvatar.layer.masksToBounds = YES;
    self.ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
    self.ivAvatar.layer.borderWidth = 2.0f;
    [self.ivAvatar setNeedsDisplay];
    self.title = @"Profile";
}

- (void)setData {
    if(![[[WDUser sharedInstance] getAvatarURL] isEqualToString:@""]){
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,[[WDUser sharedInstance] getAvatarURL]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:stringURL]];
        [self.ivAvatar setImageWithURLRequest:urlRequest placeholderImage:[UIImage imageNamed:@"empty_avatar"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            self.ivAvatar.image = image;
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            self.ivAvatar.image = [UIImage imageNamed:@"empty_avatar"];
        }];
    }

    self.lbName.text = [[WDUser sharedInstance] getFullName];
    NSString *sinceday = [[WDUser sharedInstance] getSinceday];
    if(![sinceday isEqualToString:@""]){
        self.lbSinceday.text = [NSString stringWithFormat:@"Member since %@",[[WDUser sharedInstance] stringWithSinceDate:sinceday]];
    }
    BONChain *space = BONChain.new.string(@" ");
    BONChain *chain = BONChain.new;
    [chain appendLink:BONChain.new.image([UIImage imageNamed:@"ic_location"]).baselineOffset(-4.0)];
    [chain appendLink:BONChain.new.string([[WDUser sharedInstance] getLiving]) separatorTextable: space];
    NSAttributedString *string = chain.attributedString;
    self.lbLocation.attributedText = string;
    
    if([[WDUser sharedInstance] getAbout].length>0){
        self.tvAbout.text = [[WDUser sharedInstance] getAbout];
    }else{
        self.tvAbout.text = @"About me";
    }

}

#pragma mark - Image Picker Delegate -

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:image];
    cropController.delegate = self;
    cropController.toolbarPosition = TOCropViewControllerToolbarPositionBottom;
    self.image = image;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    self.croppedFrame = cropRect;
    self.angle = angle;
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    self.croppedFrame = cropRect;
    self.angle = angle;
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)updateImageViewWithImage:(UIImage *)image fromCropViewController:(TOCropViewController *)cropViewController {
    [self uploadAvatar:image];
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Bar Button Items -
- (void)showCropViewController
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Take Photo"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              self.croppingStyle = TOCropViewCroppingStyleCircular;
                                                              
                                                              UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
                                                              standardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                              standardPicker.allowsEditing = NO;
                                                              standardPicker.delegate = self;
                                                              [self presentViewController:standardPicker animated:YES completion:nil];
                                                          }];
    
    UIAlertAction *profileAction = [UIAlertAction actionWithTitle:@"Choose Photo"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              self.croppingStyle = TOCropViewCroppingStyleCircular;
                                                              
                                                              UIImagePickerController *profilePicker = [[UIImagePickerController alloc] init];
                                                              profilePicker.modalPresentationStyle = UIModalPresentationPopover;
                                                              profilePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                              profilePicker.allowsEditing = NO;
                                                              profilePicker.delegate = self;
                                                              profilePicker.preferredContentSize = CGSizeMake(512,512);
                                                              [self presentViewController:profilePicker animated:YES completion:nil];
                                                          }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       
                                                   }];
    
    [alertController addAction:defaultAction];
    [alertController addAction:profileAction];
    [alertController addAction:cancel];
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)clickedAvatar:(id)sender {
    [self showCropViewController];
}

- (IBAction)clickedEdit:(id)sender {
     UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDEditProfileViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedBack:(id)sender {
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
        [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
}

- (IBAction)clickedWishList:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
    WDWishListTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDWishListTableViewController"];
    viewController.isMyProfile = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedSeller:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
    WDFavoriteSellerTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDFavoriteSellerTableViewController"];
    viewController.isSeller = YES;
    viewController.isMyProfile = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark API

- (void) uploadAvatar:(UIImage *)image{
    if(!image){
        return;
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyddMMHHmm"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
        NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
        
        NSError *error;
        NSData *imageData = UIImagePNGRepresentation([image resizedImageWithMaximumSize:CGSizeMake(720, 720)]);
        NSString *urlString = [NSString stringWithFormat:@"%@/%@.png",[FCFileManager pathForDocumentsDirectory],stringFromDate];
        [FCFileManager writeFileAtPath:urlString content:imageData error:&error];
        if(error){
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:error.description preferredStyle:LEAlertControllerStyleAlert];
            LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                // handle default button action
            }];
            [alertController addAction:defaultAction];
            [self presentAlertController:alertController animated:YES completion:nil];
            return;
        }
        //Call Api
        [[WDUserAPIController sharedInstance] uploadAvatar:[NSURL URLWithString:urlString] nameFile:[NSString stringWithFormat:@"%@.png",stringFromDate] mimeType:@"" completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                self.ivAvatar.image = image;
                [FCFileManager removeItemAtPath:urlString error:nil];

            }
        }];
    }
}

@end
