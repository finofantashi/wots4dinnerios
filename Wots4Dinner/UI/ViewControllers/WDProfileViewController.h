//
//  WDProfileViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UIButton *btnPicker;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbSinceday;
@property (weak, nonatomic) IBOutlet UILabel *lbLocation;
@property (weak, nonatomic) IBOutlet UITextView *tvAbout;
@property (weak, nonatomic) IBOutlet UIView *viewAvatar;
- (IBAction)clickedAvatar:(id)sender;
- (IBAction)clickedEdit:(id)sender;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedWishList:(id)sender;
- (IBAction)clickedSeller:(id)sender;

@end
