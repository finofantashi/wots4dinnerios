//
//  WDEditProfileViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/28/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDEditProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTop;

@end
