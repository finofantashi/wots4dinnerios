//
//  WDUserProfile.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSellerProfileViewController.h"
#import "WDUser.h"
#import "WDUserTemp.h"
#import "UIImage+ResizeMagick.h"
#import "UIImageView+AFNetworking.h"
#import "ExploreMenuViewController.h"
#import "WDSourceConfig.h"
#import "WDReviewSellerTableViewController.h"
#import "WDUserAPIController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDReviewAPIController.h"
#import "WDWishListTableViewController.h"
#import "WDFavoriteSellerTableViewController.h"
#import "WDSeller.h"
#import "WDContentMessageViewController.h"
#import "WDReceiver.h"
@import BonMot;

@implementation WDSellerProfileViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  [self setUpLayout];
  //[self setData:self.user];
  [self getUserAPI];
  self.tvAbout.textContainerInset = UIEdgeInsetsMake(14, 14, 14, 14);
  
  [self.viewRateThisSeller addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                 initWithTarget:self
                                 action:@selector(dismissKeyboard)];
  [self.view addGestureRecognizer:tap];
  
}

#pragma mark Private

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
  if(textView==self.tfComment){
    NSUInteger oldLength = [textView.text length];
    NSUInteger replacementLength = [text length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [text rangeOfString: @"\n"].location != NSNotFound;
    return newLength <= 350 || returnKey;
  }
  return YES;
}

- (void) setUpLayout {
  self.ivAvatar.layer.cornerRadius = 120 / 2;
  self.ivAvatar.layer.masksToBounds = YES;
  self.ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
  self.ivAvatar.layer.borderWidth = 2.0f;
  [self.ivAvatar setNeedsDisplay];
  self.title = @"Profile";
}

- (void)dismissKeyboard {
  [self.tfComment resignFirstResponder];
}

- (CGFloat)sizeOfMultiLineLabel: (NSString *)text{
  //Label text
  NSString *aLabelTextString = text;
  
  //Label font
  UIFont *aLabelFont = [UIFont fontWithName:@"Poppins-Light" size:14];
  
  //Width of the Label
  CGFloat aLabelSizeWidth = self.view.frame.size.width - 40;
  CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{
                                                    NSFontAttributeName : aLabelFont
                                                  }
                                                     context:nil].size;
  return labelSize.height;
}

- (void)setData: (WDUserTemp*)user{
  if(![user.avatarURL isEqualToString:@""]){
    NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain, user.avatarURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:stringURL]];
    [self.ivAvatar setImageWithURLRequest:urlRequest placeholderImage:[UIImage imageNamed:@"empty_avatar"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
      self.ivAvatar.image = image;
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
      self.ivAvatar.image = [UIImage imageNamed:@"empty_avatar"];
    }];
  }
  if(user.firstName&&user.lastName){
    self.lbName.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
  }
  
  NSString *sinceday = user.sinceday;
  if(![sinceday isEqualToString:@""]){
    self.lbSinceDay.text = [NSString stringWithFormat:@"Member since %@",[[WDUser sharedInstance] stringWithSinceDate:sinceday]];
  }
  
  BONChain *space = BONChain.new.string(@" ");
  BONChain *chain = BONChain.new;
  [chain appendLink:BONChain.new.image([UIImage imageNamed:@"ic_location"]).baselineOffset(-4.0)];
  [chain appendLink:BONChain.new.string(user.living) separatorTextable: space];
  NSAttributedString *string = chain.attributedString;
  self.lbLocation.attributedText = string;
  //self.lbLocation.text = user.living;
  if(user.about.length>0){
    self.tvAbout.text = user.about;
  }else{
    self.tvAbout.text = @"About me";
  }
  
  if(user.hasRating==0&&user.numberOrders>0){
    self.constraintHeight.constant = 765 + [self sizeOfMultiLineLabel:user.about];
    [self.viewRating setHidden:NO];
    [self.viewRatingShowHiden setHidden:NO];
    
  }else{
    self.constraintHeight.constant = (765-177) + [self sizeOfMultiLineLabel:user.about];
    [self.viewRating setHidden:YES];
    [self.viewRatingShowHiden setHidden:YES];
  }
  
  self.lbCategory.text = user.seller.category;
  
  if(user.seller.certified>0){
    [self.viewCertified setHidden:NO];
    self.heightCertified.constant = 20;
  }else{
    [self.viewCertified setHidden:YES];
    self.heightCertified.constant = 0;
  }
}

#pragma mark - IBAction
- (IBAction)clickedSend:(id)sender {
  [self postCommentAPI];
}

- (IBAction)clickedWishList:(id)sender {
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
  WDWishListTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDWishListTableViewController"];
  if(self.user.userID==[WDUser sharedInstance].userID){
    viewController.isMyProfile = YES;
  }else{
    viewController.userID = self.user.userID;
    viewController.isMyProfile = NO;
  }
  [self.navigationController pushViewController:viewController animated:YES];
  
}

- (IBAction)clickedFavoriteSeller:(id)sender {
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
  WDFavoriteSellerTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDFavoriteSellerTableViewController"];
  viewController.isSeller = YES;
  if(self.user.userID==[WDUser sharedInstance].userID){
    viewController.isMyProfile = YES;
  }else{
    viewController.userID = self.user.userID;
    viewController.isMyProfile = NO;
  }
  [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedBack:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedSendMessage:(id)sender {
  if(self.user.numberOrders>0){
    NSMutableArray *selectedReceivers = [NSMutableArray array];
    WDReceiver *receiver = [[WDReceiver alloc] init];
    [receiver setReceiverID:self.user.userID];
    [receiver setRoleID:self.user.roleID];
    [receiver setFirstName:self.user.firstName];
    [receiver setLastName:self.user.lastName];
    [receiver setKitchenName:self.user.seller.kitchenName];
    [receiver setAvatar:self.user.avatarURL];
    [selectedReceivers addObject:receiver];
    
    //WDContentMessageViewController *viewController = [WDContentMessageViewController messagesViewController];
    WDContentMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDContentMessageViewController"];

    viewController.listReceiver = selectedReceivers;
    viewController.isNewMessage = YES;
    [self.navigationController pushViewController:viewController animated:YES];
  }
}

- (IBAction)clickedExplore:(id)sender {
  ExploreMenuViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExploreMenuViewController"];
  viewController.sellerID = self.user.userID;
  [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedAllReviews:(id)sender {
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
  WDReviewSellerTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDReviewSellerTableViewController"];
  viewController.sellerID = self.user.userID;
  [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)didChangeValue:(id)sender {
  if(self.viewRateThisSeller.value!=0){
    self.btnSend.enabled = YES;
    self.btnSend.alpha = 1.0f;
  }else{
    self.btnSend.enabled = NO;
    self.btnSend.alpha = 0.5f;
  }
}

#pragma mark - API

- (void) getUserAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDUserAPIController sharedInstance] getUserInformationByID:self.user.userID completion:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        NSDictionary *data = [responseObject valueForKey:@"data"];
        WDUserTemp *user = [[WDUserTemp alloc] init];
        [user setUserObject:data];
        self.user = user;
        [self setData:self.user];
      }
    }];
  }
}

- (void) postCommentAPI {
  [self dismissKeyboard];
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDReviewAPIController sharedInstance] sellerReview:[NSNumber numberWithInteger:self.user.userID] content:[self.tfComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] rating:self.viewRateThisSeller.value completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if([[responseObject valueForKey:@"success"] isEqualToNumber:[NSNumber numberWithBool:YES]]){
          self.tfComment.text = @"";
          [self getUserAPI];
        }else{
          LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:[responseObject valueForKey:@"message"] preferredStyle:LEAlertControllerStyleAlert];
          LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
          }];
          [alertController addAction:defaultAction];
          [self presentAlertController:alertController animated:YES completion:nil];
        }
      }
    }];
  }
}


@end
