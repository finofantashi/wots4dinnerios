//
//  WDReviewSellerTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/27/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDReviewSellerTableViewController : UITableViewController
@property (nonatomic, assign) NSInteger sellerID;

@end
