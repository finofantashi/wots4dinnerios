//
//  WDFavoriteSellerTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDFavoriteSellerTableViewController : UITableViewController
@property (nonatomic, assign) BOOL isSeller;
@property (nonatomic, assign) BOOL isMyProfile;
@property (nonatomic, assign) NSInteger userID;

@end
