//
//  WDPayoutMethodTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WDPayout;

@protocol WDPayoutMethodTableViewCellDelegate <NSObject>
@optional
- (void)clickedCheck:(WDPayout*)payout;
- (void)clickedDelete:(WDPayout*)payout;
@end

@interface WDPayoutMethodTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbMethod;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;

@property (weak, nonatomic) IBOutlet UIButton *btnChecked;

- (IBAction)clickedDelete:(id)sender;
- (IBAction)clickedCheck:(id)sender;

@property (strong, nonatomic) WDPayout *payout;
@property (nonatomic, weak) id <WDPayoutMethodTableViewCellDelegate> delegate;
@end
