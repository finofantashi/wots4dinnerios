//
//  WDOrderHistoryView.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/17/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDOrderHistoryView : UIView
+ (WDOrderHistoryView *)getViewWithNumberOrder:(NSString *)numberOrder
                                     date:(NSString* )date;
@end
