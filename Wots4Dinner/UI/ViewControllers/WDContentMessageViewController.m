//
//  WDContentMessageViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDContentMessageViewController.h"
#import <STPopup/STPopup.h>
#import "WDPeopleViewController.h"
#import "WDReceiver.h"
#import "WDSourceConfig.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDMessageAPIController.h"
#import "WDUser.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface WDContentMessageViewController ()<STPopupControllerTransitioning>
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;
@end

@implementation WDContentMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    if(self.listReceiver.count>0){
        if(self.listReceiver.count>1){
            self.title = [NSString stringWithFormat:@"%@ + %ld", ((WDReceiver*)[self.listReceiver objectAtIndex:0]).fullName, self.listReceiver.count-1];
            [self.btnGroup setEnabled:YES];
            
        }else{
            self.title = [NSString stringWithFormat:@"%@", ((WDReceiver*)[self.listReceiver objectAtIndex:0]).fullName];
            [self.btnGroup setEnabled:NO];
            [self.btnGroup setImage:nil];
        }
    }
    //self.collectionView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
    if ([self.collectionView respondsToSelector:@selector(setPrefetchingEnabled:)]) {
        self.collectionView.prefetchingEnabled = false;
    }
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
    self.collectionView.collectionViewLayout.springinessEnabled = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setupUI {
    self.messageData = [[WDMessageData alloc] init];
    
    if(self.isNewMessage&&self.listReceiver.count>1){
        self.showLoadEarlierMessagesHeader = NO;
    }else{
        self.currentPage = 0;
        self.totalPages = 0;
        self.totalItems = 0;
        if(self.listReceiver.count==1){
            [self loadAllDataWithUser:self.currentPage];
        }else{
            [self loadAllData:self.currentPage];
        }
        
        /**
         *  Set up message accessory button delegate and configuration
         */
        //self.collectionView.delegate = self;
        self.showLoadEarlierMessagesHeader = YES;
    }
}

#pragma mark - IBAction

- (IBAction)clickedPeople:(id)sender {
    [STPopupNavigationBar appearance].barTintColor = [UIColor colorWithRed:0.20 green:0.60 blue:0.86 alpha:1.0];
    [STPopupNavigationBar appearance].tintColor = [UIColor whiteColor];
    [STPopupNavigationBar appearance].barStyle = UIBarStyleDefault;
    [STPopupNavigationBar appearance].titleTextAttributes = @{ NSFontAttributeName: [UIFont fontWithName:@"Poppins" size:18],
                                                               NSForegroundColorAttributeName: [UIColor whiteColor] };
    
    [[UIBarButtonItem appearanceWhenContainedIn:[STPopupNavigationBar class], nil] setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Poppins" size:17] } forState:UIControlStateNormal];
    WDPeopleViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WDPeopleViewController"];
    vc.listReceiver = self.listReceiver;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:vc];
    popupController.style = STPopupStyleBottomSheet;
    [popupController setHidesCloseButton:YES];
    [popupController.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:vc action:@selector(backgroundViewDidTap)]];
    [popupController presentInViewController:self];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date {
    
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:senderDisplayName
                                                          date:date
                                                          text:text];
    [self sendMessageAPI:self.groupID message:message];
}

#pragma mark - JSQMessages CollectionView DataSource

- (NSString *)senderId {
    return [NSString stringWithFormat:@"%ld",[[WDUser sharedInstance] getUserID]];
}

- (NSString *)senderDisplayName {
    return [[WDUser sharedInstance] getFullName];
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.messageData.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath {
    [self.messageData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.messageData.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.messageData.outgoingBubbleImageData;
    }
    
    return self.messageData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //JSQMessage *message = [self.messageData.messages objectAtIndex:indexPath.item];
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    //    if (indexPath.item % 3 == 0) {
    //        JSQMessage *message = [self.messageData.messages objectAtIndex:indexPath.item];
    //        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    //    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessage *message = [self.messageData.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messageData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *message = [self.messageData.messages objectAtIndex:indexPath.item];
    if([message.senderId isEqualToString:self.senderId]){
        [[JSQMessagesTimestampFormatter sharedFormatter] setAlignment:NO];
    }else{
        [[JSQMessagesTimestampFormatter sharedFormatter] setAlignment:YES];
    }
    return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.messageData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.messageData.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor whiteColor];
        }
        else {
            cell.textView.textColor = [UIColor blackColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    //cell.accessoryButton.hidden = ![self shouldShowAccessoryButtonForMessage:msg];
    // Avatar
    NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,[self.messageData.avatars objectForKey:msg.senderId]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:stringURL] placeholderImage:[UIImage imageNamed:@"empty_avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if(image){
            // Avatar styling
            cell.avatarImageView.image = [JSQMessagesAvatarImageFactory circularAvatarImage:image withDiameter:48];
        }else{
            cell.avatarImageView.image = [JSQMessagesAvatarImageFactory circularAvatarImage:[UIImage imageNamed:@"empty_avatar"] withDiameter:48];
        }
    }];
    
    return cell;
}

- (BOOL)shouldShowAccessoryButtonForMessage:(id<JSQMessageData>)message {
    return NO;
}

#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender {
    return YES;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    //    if (indexPath.item % 3 == 0) {
    //        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    //    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.messageData.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]||self.listReceiver.count<=1) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messageData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath {
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender {
    if (self.currentPage != self.totalPages) {
        if(self.listReceiver.count==1){
            [self loadAllDataWithUser:++self.currentPage];
        }else{
            [self loadAllData:++self.currentPage];
        }
    }
}

#pragma mark - STPopupControllerTransitioning

- (NSTimeInterval)popupControllerTransitionDuration:(STPopupControllerTransitioningContext *)context {
    return context.action == STPopupControllerTransitioningActionPresent ? 0.5 : 0.35;
}

- (void)popupControllerAnimateTransition:(STPopupControllerTransitioningContext *)context completion:(void (^)())completion {
    UIView *containerView = context.containerView;
    if (context.action == STPopupControllerTransitioningActionPresent) {
        containerView.transform = CGAffineTransformMakeTranslation(containerView.superview.bounds.size.width - containerView.frame.origin.x, 0);
        
        [UIView animateWithDuration:[self popupControllerTransitionDuration:context] delay:0 usingSpringWithDamping:1 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            context.containerView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            completion();
        }];
    }
    else {
        [UIView animateWithDuration:[self popupControllerTransitionDuration:context] delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            containerView.transform = CGAffineTransformMakeTranslation(- 2 * (containerView.superview.bounds.size.width - containerView.frame.origin.x), 0);
        } completion:^(BOOL finished) {
            containerView.transform = CGAffineTransformIdentity;
            completion();
        }];
    }
}

#pragma mark - API

- (void) sendMessageAPI:(long)groupID message:(JSQMessage*)message {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        NSMutableArray *listReceiver = [[NSMutableArray alloc] init];
        for (WDReceiver *object in self.listReceiver) {
            [listReceiver addObject:[NSNumber numberWithInteger:object.receiverID]];
        }
        [[WDMessageAPIController sharedInstance]sendMessage:groupID content:message.text receivers:listReceiver  completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                if([responseObject valueForKey:@"success"]){
                    [JSQSystemSoundPlayer jsq_playMessageSentSound];
                    [self.messageData.messages addObject:message];
                    [self finishSendingMessageAnimated:YES];
                }
            }
        }];
    }
}

- (void)loadAllData:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        UIWindow * window = [[UIApplication sharedApplication] keyWindow];
        
        [MBProgressHUD showHUDAddedTo:window animated:YES];
        //Call Api
        [[WDMessageAPIController sharedInstance] getListMessageDetail:self.recordID page:page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:window animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataMessage = [[[responseObject valueForKey:@"data"] valueForKey:@"messages"] valueForKey:@"data"];
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                    
                    for (NSDictionary *object in dataMessage) {
                        NSString *senderID = [NSString stringWithFormat:@"%@",[[object valueForKey:@"sender_info"] valueForKey:@"id"]];
                        NSString *senderName = [NSString stringWithFormat:@"%@ %@",[[object valueForKey:@"sender_info"] valueForKey:@"first_name"],[[object valueForKey:@"sender_info"] valueForKey:@"last_name"]];
                        NSString *content = [NSString stringWithFormat:@"%@",[object valueForKey:@"content"]];
                        NSString *avatar = [NSString stringWithFormat:@"%@",[[object valueForKey:@"sender_info"] valueForKey:@"avatar"]];
                        
                        NSDate *dateFromString = [[NSDate alloc] init];
                        dateFromString = [dateFormatter dateFromString:[object valueForKey:@"created_at"]];
                        
                        JSQMessage *message = [[JSQMessage alloc]
                                               initWithSenderId:senderID
                                               senderDisplayName:senderName
                                               date:dateFromString
                                               text:content];
                        [self.messageData.messages insertObject:message atIndex:0];
                        [self.messageData.avatars setObject:avatar==nil?@"":avatar forKey:senderID];
                    };
                    
                    //                    if([dataReceivers isKindOfClass:[NSNull class]]) {
                    //
                    //                    }else{
                    //
                    //                    }
                    
                    
                    self.currentPage = [[[[responseObject valueForKey:@"data"] valueForKey:@"messages"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[[responseObject valueForKey:@"data"] valueForKey:@"messages"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[[responseObject valueForKey:@"data"] valueForKey:@"messages"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No records found." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.collectionView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)loadAllDataWithUser:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        UIWindow * window = [[UIApplication sharedApplication] keyWindow];
        
        [MBProgressHUD showHUDAddedTo:window animated:YES];
        //Call Api
        [[WDMessageAPIController sharedInstance] showWithUser:((WDReceiver*)[self.listReceiver objectAtIndex:0]).receiverID page:page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:window animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataMessage = [[[responseObject valueForKey:@"data"] valueForKey:@"messages"] valueForKey:@"data"];
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                    
                    for (NSDictionary *object in dataMessage) {
                        NSString *senderID = [NSString stringWithFormat:@"%@",[[object valueForKey:@"sender_info"] valueForKey:@"id"]];
                        NSString *senderName = [NSString stringWithFormat:@"%@ %@",[[object valueForKey:@"sender_info"] valueForKey:@"first_name"],[[object valueForKey:@"sender_info"] valueForKey:@"last_name"]];
                        NSString *content = [NSString stringWithFormat:@"%@",[object valueForKey:@"content"]];
                        NSString *avatar = [NSString stringWithFormat:@"%@",[[object valueForKey:@"sender_info"] valueForKey:@"avatar"]];
                        
                        NSDate *dateFromString = [[NSDate alloc] init];
                        dateFromString = [dateFormatter dateFromString:[object valueForKey:@"created_at"]];
                        
                        JSQMessage *message = [[JSQMessage alloc]
                                               initWithSenderId:senderID
                                               senderDisplayName:senderName
                                               date:dateFromString
                                               text:content];
                        [self.messageData.messages insertObject:message atIndex:0];
                        [self.messageData.avatars setObject:avatar==nil?@"":avatar forKey:senderID];
                    };
                    
                    self.currentPage = [[[[responseObject valueForKey:@"data"] valueForKey:@"messages"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[[responseObject valueForKey:@"data"] valueForKey:@"messages"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[[responseObject valueForKey:@"data"] valueForKey:@"messages"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No records found." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.collectionView reloadData];
                    });
                }
            }
        }];
    }
}
@end
