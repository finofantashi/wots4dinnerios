//
//  WDPayoutMethodTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDPayoutMethodTableViewCell.h"

@implementation WDPayoutMethodTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickedCheck:(id)sender {
    if([self.delegate respondsToSelector:@selector(clickedCheck:)]) {
        [self.delegate clickedCheck:self.payout];
    }
}

- (IBAction)clickedDelete:(id)sender {
    if([self.delegate respondsToSelector:@selector(clickedDelete:)]) {
        [self.delegate clickedDelete:self.payout];
    }
}

@end
