//
//  WDFoodTypeCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#define IMAGE_OFFSET_SPEED 25

@interface WDFoodTypeCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivFoodType;
@property (weak, nonatomic) IBOutlet UILabel *lbFoodType;
@property (weak, nonatomic) IBOutlet UIView *viewBox;
@property (weak, nonatomic) IBOutlet UILabel *lbDishCount;
@property (weak, nonatomic) IBOutlet UIView *lbViewDishCount;

/*
 Image will always animate according to the imageOffset provided. Higher the value means higher offset for the image
 */
@property (nonatomic, assign, readwrite) CGPoint imageOffset;

@end
