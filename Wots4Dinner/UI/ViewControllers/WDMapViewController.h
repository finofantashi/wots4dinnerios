//
//  WDMapViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;

@interface WDMapViewController : UIViewController
@property (nonatomic, strong) NSString *kitchen;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, assign) double lat;
@property (nonatomic, assign) double lng;
- (IBAction)clickedBack:(id)sender;

@end
