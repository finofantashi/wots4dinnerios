//
//  WDPromotionalVideo.m
//  MealsAround
//
//  Created by Giau Le on 8/22/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDCertifiedTableVC.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <FCFileManager/FCFileManager.h>
#import "UIImage+ResizeMagick.h"
#import "WDAppFeedbackAPIController.h"
#import "WDMainViewController.h"

@interface WDCertifiedTableVC ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate, UIDocumentPickerDelegate>
@property (nonatomic, strong) UIPopoverController *activityPopoverController;
@property (nonatomic, strong) UIImage *imageFile;
@property (nonatomic, assign) Boolean isScreenDish;           // The image we'll be cropping
@property (nonatomic, strong) NSURL *fileURL;

@end

@implementation WDCertifiedTableVC
- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.title = @"Certified Seller";
  UITapGestureRecognizer *singleTapFile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedUploadFile:)];
  singleTapFile.numberOfTapsRequired = 1;
  singleTapFile.numberOfTouchesRequired = 1;
  
  
  [self.ivUploadFile addGestureRecognizer:singleTapFile];
  [self.ivUploadFile setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


- (void)viewDidLayoutSubviews {
  [self drawDashedBorderAroundView:self.btnUploadFile];
}

- (void)drawDashedBorderAroundView:(UIView *)v {
  //border definitions
  CGFloat cornerRadius = 10;
  CGFloat borderWidth = 2;
  NSInteger dashPattern1 = 8;
  NSInteger dashPattern2 = 8;
  UIColor *lineColor = [UIColor colorFromHexString:@"cccccc" withAlpha:1.0];
  
  //drawing
  CGRect frame = v.bounds;
  
  CAShapeLayer *_shapeLayer = [CAShapeLayer layer];
  
  //creating a path
  CGMutablePathRef path = CGPathCreateMutable();
  
  //drawing a border around a view
  CGPathMoveToPoint(path, NULL, 0, frame.size.height - cornerRadius);
  CGPathAddLineToPoint(path, NULL, 0, cornerRadius);
  CGPathAddArc(path, NULL, cornerRadius, cornerRadius, cornerRadius, M_PI, -M_PI_2, NO);
  CGPathAddLineToPoint(path, NULL, frame.size.width - cornerRadius, 0);
  CGPathAddArc(path, NULL, frame.size.width - cornerRadius, cornerRadius, cornerRadius, -M_PI_2, 0, NO);
  CGPathAddLineToPoint(path, NULL, frame.size.width, frame.size.height - cornerRadius);
  CGPathAddArc(path, NULL, frame.size.width - cornerRadius, frame.size.height - cornerRadius, cornerRadius, 0, M_PI_2, NO);
  CGPathAddLineToPoint(path, NULL, cornerRadius, frame.size.height);
  CGPathAddArc(path, NULL, cornerRadius, frame.size.height - cornerRadius, cornerRadius, M_PI_2, M_PI, NO);
  
  //path is set as the _shapeLayer object's path
  _shapeLayer.path = path;
  CGPathRelease(path);
  
  _shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
  _shapeLayer.frame = frame;
  _shapeLayer.masksToBounds = NO;
  [_shapeLayer setValue:[NSNumber numberWithBool:NO] forKey:@"isCircle"];
  _shapeLayer.fillColor = [[UIColor clearColor] CGColor];
  _shapeLayer.strokeColor = [lineColor CGColor];
  _shapeLayer.lineWidth = borderWidth;
  _shapeLayer.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:dashPattern1], [NSNumber numberWithInt:dashPattern2], nil];
  _shapeLayer.lineCap = kCALineCapRound;
  
  //_shapeLayer is added as a sublayer of the view, the border is visible
  [v.layer addSublayer:_shapeLayer];
  v.layer.cornerRadius = cornerRadius;
}

- (BOOL)validate {
  if(!self.fileURL && !self.imageFile) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"File upload is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  return YES;
}

#pragma mark - Image Picker Delegate -

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
  self.imageFile = image;
  [self.ivUploadFile setImage:image];
  [self.ivUploadFile setHidden:NO];
  [self.btnUploadFile setHidden:YES];
  [picker dismissViewControllerAnimated:YES completion:^{
  }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Bar Button Items -

- (IBAction)clickedSubmit:(id)sender {
  if([self validate]) {
    [self sendAppFeedbackAPI];
  }
}


- (IBAction)clickedBack:(id)sender {
  [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
    [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
  }];
}

- (void) chooseImage {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
  
  UIAlertAction *profileAction = [UIAlertAction actionWithTitle:@"Choose Photo"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
    UIImagePickerController *profilePicker = [[UIImagePickerController alloc] init];
    profilePicker.modalPresentationStyle = UIModalPresentationPopover;
    profilePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    profilePicker.allowsEditing = NO;
    profilePicker.delegate = self;
    profilePicker.preferredContentSize = CGSizeMake(512,512);
    [self presentViewController:profilePicker animated:YES completion:nil];
  }];
  
  UIAlertAction *pdfAction = [UIAlertAction actionWithTitle:@"Choose PDF"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
    NSArray *documentTypes = @[(NSString*)kUTTypePDF,(NSString*)kUTTypePNG,(NSString*)kUTTypeJPEG];
    UIDocumentPickerViewController *controller = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeImport];
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];
  }];
  
  UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleCancel
                                                 handler:^(UIAlertAction *action) {
    
  }];
  
  [alertController addAction:profileAction];
  [alertController addAction:pdfAction];
  [alertController addAction:cancel];
  [alertController setModalPresentationStyle:UIModalPresentationPopover];
  [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)clickedUploadFile:(id)sender {
  [self chooseImage];
}

- (void) sendAppFeedbackAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    
    [MBProgressHUD showHUDAddedTo:window animated:YES];
    NSString *urlStringFile = @"";
    NSString *stringFromDateFile = @"";
    
    if (self.fileURL != nil && [self.fileURL.pathExtension containsString:@"pdf"]) {
      urlStringFile = [self.fileURL absoluteString];
    } else {
      NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
      [formatter setDateFormat:@"yyyyddMMHHmm"];
      [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
      stringFromDateFile = [formatter stringFromDate:[NSDate date]];
      
      NSError *error;
      NSData *imageData = UIImagePNGRepresentation([self.imageFile resizedImageWithMaximumSize:CGSizeMake(720, 720)]);
      urlStringFile = [NSString stringWithFormat:@"%@/%@.png",[FCFileManager pathForDocumentsDirectory],stringFromDateFile];
      [FCFileManager writeFileAtPath:urlStringFile content:imageData error:&error];
      if(error){
        [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:error.description preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return;
      }
    }
    
    //Call Api
    
    [[WDAppFeedbackAPIController sharedInstance] sendCertified:[NSURL URLWithString:urlStringFile]
                                                          note:[self.tfNotes.text stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet]
                                                    completion:^(NSString *errorString, id responseObject){
      [MBProgressHUD hideHUDForView:window animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if(![urlStringFile isEqualToString:@""]){
          [FCFileManager removeItemAtPath:urlStringFile error:nil];
        }
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Your information has been successfully submitted. We will review your request very soon." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }
    }];
  }
}

#pragma mark - UIDocumentPickerDelegate

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray <NSURL *>*)urls {
  self.fileURL = urls[0];
  if ([self.fileURL.absoluteString containsString:@"pdf"]) {
    [self.ivUploadFile setImage:[UIImage imageNamed:@"ic_pdf"]];
  } else {
    self.imageFile = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.fileURL]];
    [self.ivUploadFile setImage:self.imageFile];
  }
  [self.ivUploadFile setHidden:NO];
  [self.btnUploadFile setHidden:YES];
}

- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
}

@end
