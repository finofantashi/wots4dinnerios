//
//  WDPaymentDetailTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/1/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKTextField.h"

@interface WDPaymentDetailTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITableViewCell *cellCheck;
@property (weak, nonatomic) IBOutlet PKTextField *tfCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfFirstName;
@property (weak, nonatomic) IBOutlet UITextField *tfLastName;
@property (weak, nonatomic) IBOutlet UITextField *tfExpiryDate;
@property (weak, nonatomic) IBOutlet UITextField *tfCCV;
@property (weak, nonatomic) IBOutlet UIImageView *ivCardNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UIButton *btnOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (nonatomic, strong) NSMutableArray *listCart;
@property (nonatomic, assign) BOOL isFromAddCard;

- (IBAction)clickedSave:(id)sender;
- (IBAction)clickedOrder:(id)sender;

@end
