//
//  WDMyOrderViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMyOrderViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDFoodMenuAPIController.h"
#import "WDMyOrder.h"
#import "WDMyOrderItem.h"
#import "WDMyOrderItemTableViewCell.h"
#import "WDMyOrderView.h"

@interface WDMyOrderViewController ()<MIResizableTableViewDataSource, MIResizableTableViewDelegate, WDMyOrderItemTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *listMyOrder;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;

@end

@implementation WDMyOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView configureWithDelegate:self andDataSource:self];
    self.view.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
    
    [self.tableView registerNib:[WDMyOrderItemTableViewCell cellNib] forCellReuseIdentifier:[WDMyOrderItemTableViewCell cellIdentifier]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.listMyOrder = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    if(self.isToday){
        [self loadTodayData:self.currentPage search:self.tfSearchView.text];
        
    }else{
        [self loadAllData:self.currentPage search:self.tfSearchView.text];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MIResizableTableViewDataSource

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:14];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.view.frame.size.width-42;
    CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{
                                                                 NSFontAttributeName : aLabelFont
                                                                 }
                                                       context:nil].size;
    return labelSize.height;
}

- (NSInteger)numberOfSectionsInResizableTableView:(MIResizableTableView *)resizableTableView {
    return self.listMyOrder.count;
}

- (NSInteger)resizableTableView:(MIResizableTableView *)resizableTableView numberOfRowsInSection:(NSInteger)section {
    WDMyOrder *order = [self.listMyOrder objectAtIndex:section];
    if(order.listOrderItem.count>0){
        return order.listOrderItem.count + 1;
    }
    return order.listOrderItem.count;
}

- (UIView *)resizableTableView:(MIResizableTableView *)resizableTableView viewForHeaderInSection:(NSInteger)section {
    
    WDMyOrder *order = [self.listMyOrder objectAtIndex:section];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [dateFormatter setTimeZone:inputTimeZone];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *createDate = [dateFormatter dateFromString:order.createAt];

    NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:outputTimeZone];
    dateFormatter.dateFormat = @"MM/dd/yy HH:mm";
    NSString *strCreateDate = [dateFormatter stringFromDate:createDate];
    NSString *shippingInfo = @"";
    if(order.shippingPhone.length!=0){
        shippingInfo = [NSString stringWithFormat:@"%@ - %@ - %@",order.shippingName,order.shippingPhone,order.shippingAddress];
    }
    return [WDMyOrderView getViewWithNumberOrder:order.code
                                            date:strCreateDate
                                    nameCustomer:[NSString stringWithFormat:@"%@ %@",order.firstName, order.lastName]
                                    shippingInfo:shippingInfo
                                          isRead:order.reateAt.length>0?true:false];
}

- (UITableViewCell *)resizableTableView:(MIResizableTableView *)resizableTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDMyOrderItemTableViewCell *cell = [resizableTableView dequeueReusableCellWithIdentifier:[WDMyOrderItemTableViewCell cellIdentifier] forIndexPath:indexPath];
    cell.myOrderItemDelegate = self;
    float totalPrice = 0;

    
    if(indexPath.row==((WDMyOrder*)[self.listMyOrder objectAtIndex:indexPath.section]).listOrderItem.count){
      float fee = 0;
        for (WDMyOrderItem *object in ((WDMyOrder*)[self.listMyOrder objectAtIndex:indexPath.section]).listOrderItem) {
            totalPrice = totalPrice + object.totalPrice;
            fee = object.deliveryFee;
        }
        totalPrice += fee;
        [cell configureWithTotalPrice:totalPrice fee:fee currencyCode:self.currencyCode
                       currencySymbol:self.currencySymbol];
    }else{
        WDMyOrderItem *item = [((WDMyOrder*)[self.listMyOrder objectAtIndex:indexPath.section]).listOrderItem objectAtIndex:indexPath.row];
        self.currencyCode = item.currencyCode;
        self.currencySymbol = item.currencySymbol;
        
        [cell configureWithItem:item
                   currencyCode:self.currencyCode
                 currencySymbol:self.currencySymbol];
    }
    
    return cell;
}

#pragma mark - MIResizableTableViewDelegate

- (void)resizableTableView:(MIResizableTableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if (self.currentPage != self.totalPages && section == [self.listMyOrder count] - 1) {
        if(self.isToday){
            [self loadTodayData:++self.currentPage search:self.tfSearchView.text];
            
        }else{
            [self loadAllData:++self.currentPage search:self.tfSearchView.text];
        }
    }
}

- (CGFloat)resizableTableView:(MIResizableTableView *)resizableTableView heightForHeaderInSection:(NSInteger)section {
    WDMyOrder *order = ((WDMyOrder*)[self.listMyOrder objectAtIndex:section]);
    NSString *shippingInfo = @"";
    if(order.shippingPhone.length!=0){
        shippingInfo = [NSString stringWithFormat:@"%@ - %@ - %@",order.shippingName,order.shippingPhone,order.shippingAddress];
        float sizeHeight = 94 + [self sizeOfMultiLineLabel:shippingInfo];
        return sizeHeight;
    }
    return 132;
}

- (CGFloat)resizableTableView:(MIResizableTableView *)resizableTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==((WDMyOrder*)[self.listMyOrder objectAtIndex:indexPath.section]).listOrderItem.count){
        return 60;
    }
    WDMyOrderItem *item = [((WDMyOrder*)[self.listMyOrder objectAtIndex:indexPath.section]).listOrderItem objectAtIndex:indexPath.row];
    if(item.note.length>0){
        float sizeHeight;
        if([item.status isEqualToString:@"pending"]){
            sizeHeight = 80 + [self sizeOfMultiLineLabel:[NSString stringWithFormat:@"Note: %@",item.note]];
        }else{
            sizeHeight = 60 + [self sizeOfMultiLineLabel:[NSString stringWithFormat:@"Note: %@",item.note]];
        }
        return sizeHeight;
    }
    if([item.status isEqualToString:@"pending"]){
        return 78;
    }
    return 64;
}

- (UITableViewRowAnimation)resizableTableViewInsertRowAnimation {
    return UITableViewRowAnimationFade;
}

- (UITableViewRowAnimation)resizableTableViewDeleteRowAnimation {
    return UITableViewRowAnimationFade;
}

- (BOOL)resizableTableViewSectionShouldExpandSection:(NSInteger)section {
    if(((WDMyOrder*)[self.listMyOrder objectAtIndex:section]).listOrderItem.count==0){
        [self getListItemWithOrderId:((WDMyOrder*)[self.listMyOrder objectAtIndex:section]).orderID index:section];
    }
    return YES;
}

- (void)resizableTableView:(MIResizableTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - IBAction

- (void)clickedConfirm:(WDMyOrderItem *)dish {
    [self confirmWithDish:dish];
}

-(void)clickedDeclined:(WDMyOrderItem *)dish {
    [self declineWithDish:dish];
}

#pragma mark - API

- (void)loadTodayData:(NSInteger)page
               search:(NSString*)search{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getMyOrderToday:search page:page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDMyOrder *order = [[WDMyOrder alloc] init];
                        [order setMyOrderObject:object];
                        order.listOrderItem = [[NSMutableArray alloc] init];
                        [self.listMyOrder addObject:order];
                    };
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No Order." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)loadAllData:(NSInteger)page
             search:(NSString*)search{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getMyOrderAll:search page:page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDMyOrder *order = [[WDMyOrder alloc] init];
                        [order setMyOrderObject:object];
                        order.listOrderItem = [[NSMutableArray alloc] init];
                        [self.listMyOrder addObject:order];
                    };
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No Order." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)getListItemWithOrderId:(NSInteger)orderID
                         index:(NSInteger)index{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getMyOrderItem:orderID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    [((WDMyOrder*)[self.listMyOrder objectAtIndex:index]).listOrderItem removeAllObjects];
                    NSDictionary *dataOrderItem = [responseObject valueForKey:@"data"];
                    for (NSDictionary *object in dataOrderItem) {
                        WDMyOrderItem *orderItem = [[WDMyOrderItem alloc] init];
                        [orderItem setMyOrderItemObject:object];
                        [((WDMyOrder*)[self.listMyOrder objectAtIndex:index]).listOrderItem addObject:orderItem];
                    };
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                        [self.tableView expand:YES rowsInSection:index];
                    });
                }
            }
        }];
    }
}

- (IBAction)clickedSearch:(id)sender {
    [self.tfSearchView resignFirstResponder];
    self.listMyOrder = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    if(self.isToday){
        [self loadTodayData:self.currentPage search:self.tfSearchView.text];
        
    }else{
        [self loadAllData:self.currentPage search:self.tfSearchView.text];
    }
}

- (void)confirmWithDish:(WDMyOrderItem *)dish{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] confirmOrderItem:dish.dishID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    dish.status = @"confirmed";
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)declineWithDish:(WDMyOrderItem *)dish{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] declineOrderItem:dish.dishID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    dish.status = @"declined";
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

@end
