//
//  WDSettingTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSettingTableViewController.h"

@interface WDSettingTableViewController ()

@end

@implementation WDSettingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Setting";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
