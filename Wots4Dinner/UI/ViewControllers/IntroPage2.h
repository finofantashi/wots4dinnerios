//
//  IntroPage.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroPage2 : UIView
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (nonatomic, strong) CATransition* slideTransition;
@property (nonatomic, strong) NSMutableArray *imagesArrays;
@property (nonatomic, assign) NSInteger indexSlide;

@end
