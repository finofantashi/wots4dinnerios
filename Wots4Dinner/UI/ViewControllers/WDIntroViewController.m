//
//  WDIntroViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDIntroViewController.h"
#import "EAIntroView.h"
#import "BIZCircularTransitionHandler.h"
#import <QuartzCore/QuartzCore.h>

@interface WDIntroViewController ()<EAIntroDelegate>
@property (nonatomic, strong) EAIntroView *intro;
@property (nonatomic, strong) BIZCircularTransitionHandler *circularTransitionHandler;
@end

@implementation WDIntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showIntroWithCustomViewFromNib];
    self.circularTransitionHandler = [[BIZCircularTransitionHandler alloc] init];
}

- (void)showIntroWithCustomViewFromNib {
    EAIntroPage *page = [EAIntroPage pageWithCustomViewFromNibNamed:@"IntroPage"];
    EAIntroPage *page1 = [EAIntroPage pageWithCustomViewFromNibNamed:@"IntroPage1"];
    EAIntroPage *page3 = [EAIntroPage pageWithCustomViewFromNibNamed:@"IntroPage2"];

    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page,page1,page3]];
    [intro setDelegate:self];
    
    
    [intro showInView:self.view animateDuration:0.3];
    _intro = intro;
}

#pragma mark - EAIntroView delegate

- (void)introDidFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped {
    CGPoint centerOfButton = [self.viewCenter.superview convertPoint:self.viewCenter.center toView:nil];
    UINavigationController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDNavigationFirstLaucher"];
    viewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.circularTransitionHandler transitionWithDestinationViewController:viewController initialTransitionPoint:centerOfButton];
    [self presentViewController:viewController animated:YES completion:^{
    }];

}

@end
