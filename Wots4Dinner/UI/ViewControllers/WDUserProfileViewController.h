//
//  WDUserProfile.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDUserTemp.h"

@interface WDUserProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbSinceDay;
@property (weak, nonatomic) IBOutlet UILabel *lbLocation;
@property (weak, nonatomic) IBOutlet UITextView *tvAbout;
@property (strong, nonatomic) WDUserTemp *user;
- (IBAction)clickedWishList:(id)sender;
- (IBAction)clickedFavorite:(id)sender;

@end
