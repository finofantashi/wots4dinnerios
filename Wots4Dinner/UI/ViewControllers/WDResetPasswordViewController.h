//
//  WDResetPasswordViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDResetPasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *tfNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *tfConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnResetPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnShowHideNewPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnShowHideConfirmPassword;
@property (assign, nonatomic) BOOL isFromSignUp;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *userName;

- (IBAction)touchDown:(id)sender;

- (IBAction)clickedResetPassword:(id)sender;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedShowHideNewPassword:(id)sender;
- (IBAction)clickedShowHideConfirmPassword:(id)sender;

@end
