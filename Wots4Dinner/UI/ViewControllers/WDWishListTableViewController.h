//
//  WishListTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/25/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDWishListTableViewController : UITableViewController
@property (nonatomic, assign) BOOL isMyProfile;
@property (nonatomic, assign) NSInteger userID;

@end
