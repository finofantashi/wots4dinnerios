//
//  WDPaymentMethodTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDPaymentMethodTableViewCell.h"

@implementation WDPaymentMethodTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickedCheck:(id)sender {
    if([self.delegate respondsToSelector:@selector(clickedCheck:)]) {
        [self.delegate clickedCheck:self.card];
    }
}
- (IBAction)clickedDelete:(id)sender {
    if([self.delegate respondsToSelector:@selector(clickedDelete:)]) {
        [self.delegate clickedDelete:self.card];
    }
}
@end
