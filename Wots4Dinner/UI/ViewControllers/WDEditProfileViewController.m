//
//  WDEditProfileViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/28/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDEditProfileViewController.h"
#import "WDUser.h"
#import "WDUserTemp.h"
#import "WDEditProfileTableViewCell.h"
#import "SBPickerSelector.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDUserAPIController.h"
#import "WDGetResetCodeViewController.h"
#import "NSString+IsValidEmail.h"

@interface WDEditProfileViewController () <SBPickerSelectorDelegate>
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) WDUserTemp *user;

@end

@implementation WDEditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Edit Profile";
    self.user = [[WDUserTemp alloc] init];
    [self.user setUser:[WDUser sharedInstance]];
    self.birthday = [[WDUser sharedInstance] getBirthday];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.user setUser:[WDUser sharedInstance]];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self.navigationController setNavigationBarHidden:NO];
}

- (void) showPicker:(id)sender{
    //*********************
    //setup here your picker
    //*********************
    [self.tableView endEditing:YES];
    SBPickerSelector *picker = [SBPickerSelector picker];
    picker.delegate = self;
    picker.doneButtonTitle = @"Done";
    picker.cancelButtonTitle = @"Cancel";
    picker.pickerType = SBPickerSelectorTypeDate;
    picker.datePickerType = SBPickerSelectorDateTypeOnlyDay;
    [picker showPickerOver:self];
    
    //    CGPoint point = [self.view convertPoint:[sender frame].origin fromView:[sender superview]];
    //    CGRect frame = [sender frame];
    //    frame.origin = point;
    //    [picker showPickerIpadFromRect:frame inView:self.view];
    
}

- (BOOL)invalidate {
    [self saveUser];
    if([self.user.firstName length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"First Name is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    if([self.user.lastName length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Last Name is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    if([self.user.email length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Email is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    if(![self.user.email isValidEmail]) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Email is invalid." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (void) saveUser {
    self.user.firstName = [NSString stringWithFormat:@"%@",((WDEditProfileTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]).tfFirstName.text];
    
    self.user.lastName =[NSString stringWithFormat:@"%@",((WDEditProfileTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]]).tfLastName.text];
    
    self.user.about = [NSString stringWithFormat:@"%@",((WDEditProfileTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]]).tvAbout.text];
    
    UISegmentedControl *segment = ((WDEditProfileTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]]).segmentGender;
    self.user.gender = [NSString stringWithFormat:@"%@",[[segment titleForSegmentAtIndex:segment.selectedSegmentIndex] lowercaseString]];
    
    self.user.birthday = self.birthday;
    
    self.user.email = [NSString stringWithFormat:@"%@",((WDEditProfileTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]]).tfEmail.text];
    
    self.user.living =[NSString stringWithFormat:@"%@",((WDEditProfileTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:1]]).tfLocation.text];
}

#pragma mark - UITableView DataSOurce / Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableViewInner cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDEditProfileTableViewCell *cell;
    
    if(indexPath.section==0){
        if(indexPath.row == 0){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellFirstName"];
            cell.tfFirstName.text = self.user.firstName;
        }else if(indexPath.row == 2){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellLastName"];
            cell.tfLastName.text = self.user.lastName;
            
        }else if(indexPath.row == 4){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellEmail"];
            cell.tfEmail.text = self.user.email;
            
        }else if(indexPath.row == 6){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellPhone"];
            cell.lbPhone.text = [NSString stringWithFormat:@"%@ %@",self.user.phonePrefix,self.user.phoneNumber];
            
            [cell.btnPhone addTarget:self action:@selector(clickedPhone:) forControlEvents:UIControlEventTouchUpInside];

        }else if(indexPath.row == 8){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellPassword"];
            [cell.btnPassword addTarget:self action:@selector(clickedPassword:) forControlEvents:UIControlEventTouchUpInside];
        }else{
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellLine"];
        }
    }else{
        if(indexPath.row == 0){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellAbout"];
            cell.tvAbout.text = self.user.about;
        }else if(indexPath.row == 2){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellGender"];
            if([self.user.gender isEqualToString:@"male"]){
                [cell.segmentGender setSelectedSegmentIndex:0];
                
            }else if([self.user.gender isEqualToString:@"female"]){
                [cell.segmentGender setSelectedSegmentIndex:1];
                
            }else {
                [cell.segmentGender setSelectedSegmentIndex:2];
            }
        }else if(indexPath.row == 4){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellbirthday"];
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPicker:)];
            tapGestureRecognizer.numberOfTapsRequired = 1;
            [cell.lbBirthday addGestureRecognizer:tapGestureRecognizer];
            cell.lbBirthday.userInteractionEnabled = YES;
            if(![self.birthday isEqualToString:@""]){
                cell.lbBirthday.text = [[WDUser sharedInstance] stringWithDate:self.birthday];
            }
        }else if(indexPath.row == 6){
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellLocation"];
            cell.tfLocation.text = self.user.living;
            
        }else{
            cell = [tableViewInner dequeueReusableCellWithIdentifier:@"cellLine"];
        }
    }
    
    return cell;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    WDEditProfileTableViewCell *cell;
    if(section==0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"cellHeaderAccount"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"cellHeaderOptional"];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section==1&&indexPath.row==0){
        return 130;
    }else if(indexPath.row==1||indexPath.row==3||indexPath.row==5||indexPath.row==7){
        return 1;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

#pragma mark - SBPickerSelectorDelegate

-(void) pickerSelector:(SBPickerSelector *)selector selectedValue:(NSString *)value index:(NSInteger)idx{
}

-(void) pickerSelector:(SBPickerSelector *)selector dateSelected:(NSDate *)date{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    self.birthday = [dateFormat stringFromDate:date];
    [self.tableView reloadData];
}

-(void) pickerSelector:(SBPickerSelector *)selector cancelPicker:(BOOL)cancel{
    
}

-(void) pickerSelector:(SBPickerSelector *)selector intermediatelySelectedValue:(id)value atIndex:(NSInteger)idx{
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedSave:(id)sender {
    if([self invalidate]){
        [self updateUserAPI];
    }
}

- (IBAction)clickedPhone:(id)sender {
    WDGetResetCodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDGetResetCodeViewController"];
    viewController.isChangePhoneNumber = YES;
    [self.navigationController pushViewController:viewController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    //self.constraintTop.constant =  -40;
}

- (IBAction)clickedPassword:(id)sender {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDChangePasswordViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
    [self.navigationController setNavigationBarHidden:YES];
    //self.constraintTop.constant =  -40;
}

#pragma mark API

- (void) updateUserAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //Call API
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        [[WDUserAPIController sharedInstance] updateUser:self.user completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                [[WDUser sharedInstance] setUser:self.user];
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"You have updated your profile successfully!" preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }
        }];
    }
}

@end
