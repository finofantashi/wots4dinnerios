//
//  WDMyMenuTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/21/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDDishSearch.h"

@protocol WDMyMenuTableViewCellDelegate <NSObject>

- (void)clickedEdit:(NSInteger) dishId;
- (void)clickedActived:(WDDishSearch*) dish;
- (void)clickedDelete:(WDDishSearch*) dish;

@end

@interface WDMyMenuTableViewCell : UITableViewCell
@property (nonatomic, strong, readwrite) IBOutlet UIImageView *ivDish;
@property (weak, nonatomic) IBOutlet UILabel *lbNameDish;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbFoodType;
@property (weak, nonatomic) IBOutlet UIButton *btnActive;

@property (assign, nonatomic) NSInteger dishId;
@property (assign, nonatomic) WDDishSearch *dish;
@property (assign, nonatomic) id <WDMyMenuTableViewCellDelegate> myMenuDelegate;

- (IBAction)clickedEdit:(id)sender;
- (IBAction)clickedActivate:(id)sender;
- (IBAction)clickedDelete:(id)sender;
- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view;
@end
