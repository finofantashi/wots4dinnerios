//
//  WDFavoriteSellerTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDFavoriteSellerTableViewController.h"
#import "WDFavoriteSellerTableViewCell.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "WDFoodMenuAPIController.h"
#import "WDFavoriteSellers.h"
#import "WDSourceConfig.h"
#import "WDSellerProfileViewController.h"
#import "WDUserProfileViewController.h"

@interface WDFavoriteSellerTableViewController ()<WDFavoriteSellerTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *listSeller;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;
@end

@implementation WDFavoriteSellerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listSeller = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    if(self.isSeller){
        self.title = @"Favourite Seller";
        [self loadDataFavoriteSeller:++self.currentPage isFavoriteSellerOf:NO];
        
    }else{
        self.title = @"Favourite Of";
        [self loadDataFavoriteSeller:++self.currentPage isFavoriteSellerOf:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.currentPage == self.totalPages
        || self.totalItems == self.listSeller.count) {
        return self.listSeller.count;
    }
    return self.listSeller.count + 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentPage != self.totalPages && indexPath.row == [self.listSeller count] - 1 ) {
        if(self.isSeller){
            [self loadDataFavoriteSeller:++self.currentPage isFavoriteSellerOf:NO];
            
        }else{
            [self loadDataFavoriteSeller:++self.currentPage isFavoriteSellerOf:YES];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDFavoriteSellerTableViewCell";
    UITableViewCell *cell;
    
    if (indexPath.row == [self.listSeller count]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:100];
        [activityIndicator startAnimating];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        WDFavoriteSellers *favoriteSeller = self.listSeller[indexPath.row];
        
        ((WDFavoriteSellerTableViewCell*)cell).favoriteSeller = favoriteSeller;
        ((WDFavoriteSellerTableViewCell*)cell).favoriteSellerDelegate = self;
        
        //Avatar
        ((WDFavoriteSellerTableViewCell*)cell).ivAvatar.layer.cornerRadius = 70 / 2;
        ((WDFavoriteSellerTableViewCell*)cell).ivAvatar.layer.masksToBounds = YES;
        ((WDFavoriteSellerTableViewCell*)cell).ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
        ((WDFavoriteSellerTableViewCell*)cell).ivAvatar.layer.borderWidth = 2.0f;
        [((WDFavoriteSellerTableViewCell*)cell).ivAvatar setNeedsDisplay];
        
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,favoriteSeller.userSeller.avatarURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        [((WDFavoriteSellerTableViewCell*)cell).ivAvatar sd_setImageWithURL:[NSURL URLWithString:stringURL] placeholderImage:[UIImage imageNamed:@"empty_avatar"]];
        //Name
        if(favoriteSeller.userSeller.firstName&&favoriteSeller.userSeller.lastName){
            ((WDFavoriteSellerTableViewCell*)cell).lbName.text = [NSString stringWithFormat:@"%@ %@",favoriteSeller.userSeller.firstName,favoriteSeller.userSeller.lastName];
        }
        
        //Address
        ((WDFavoriteSellerTableViewCell*)cell).lbAddress.text = favoriteSeller.userSeller.living;
        
        //Rating
        ((WDFavoriteSellerTableViewCell*)cell).viewRating.value = favoriteSeller.userSeller.rating;
        
        //Configure right buttons
        MGSwipeButton *removeButton = [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"ic_remove.png"] backgroundColor:[UIColor colorFromHexString:@"898989" withAlpha:1.0] callback:^BOOL(MGSwipeTableCell *sender) {
            if([sender isKindOfClass:[WDFavoriteSellerTableViewCell class]]){
                [self deleteFavoriteSeller:((WDFavoriteSellerTableViewCell*)cell).favoriteSeller];
            }
            return YES;
        }];
        MGSwipeButton *messageButton = [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"ic_send_message.png"] backgroundColor:[UIColor colorFromHexString:@"4ca8f8" withAlpha:1.0] callback:^BOOL(MGSwipeTableCell *sender) {
            return YES;
        }];
        
        if(self.isMyProfile){
            if(self.isSeller){
                ((WDFavoriteSellerTableViewCell*)cell).rightButtons = @[removeButton,messageButton];
            }else{
                ((WDFavoriteSellerTableViewCell*)cell).rightButtons = @[messageButton];
            }
            
            ((WDFavoriteSellerTableViewCell*)cell).rightSwipeSettings.transition = MGSwipeTransitionDrag;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

#pragma mark - IBAction

- (void)clickedAvatar:(WDUserTemp *)user {
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if(user.roleID == 2){
        WDSellerProfileViewController *viewController = [main instantiateViewControllerWithIdentifier:@"WDSellerProfileViewController"];
        viewController.user = user;
        [self.navigationController pushViewController:viewController animated:YES];
    }else{
        WDUserProfileViewController *viewController = [main instantiateViewControllerWithIdentifier:@"WDUserProfileViewController"];
        viewController.user = user;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - API

- (void)loadDataFavoriteSeller:(NSInteger)page isFavoriteSellerOf:(BOOL)isFavoriteSellerOf{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getFavoriteSellers:page
                                                              userID:self.userID
                                                  isFavoriteSellerOf:isFavoriteSellerOf
                                                     completionBlock:^(NSString *errorString, id responseObject) {
                                                         [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
                                                         if(errorString){
                                                             LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                                                             LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                                                                 // handle default button action
                                                             }];
                                                             [alertController addAction:defaultAction];
                                                             [self presentAlertController:alertController animated:YES completion:nil];
                                                         }else{
                                                             if(responseObject) {
                                                                 NSDictionary *dataFavorite = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                                                                 for (NSDictionary *object in dataFavorite) {
                                                                     WDFavoriteSellers *comment = [[WDFavoriteSellers alloc] init];
                                                                     [comment setFavoriteSellersObject:object];
                                                                     [self.listSeller addObject:comment];
                                                                 };
                                                                 
                                                                 self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                                                                 self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                                                                 self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                                                                 if(self.totalItems==0){
                                                                     LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Hello" message:@"You have no favourites yet." preferredStyle:LEAlertControllerStyleAlert];
                                                                     LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                                                                         // handle default button action
                                                                     }];
                                                                     [alertController addAction:defaultAction];
                                                                     [self presentAlertController:alertController animated:YES completion:nil];
                                                                     
                                                                 }
                                                                 dispatch_async(dispatch_get_main_queue(), ^{
                                                                     [self.tableView reloadData];
                                                                 });
                                                             }
                                                         }
                                                     }];
    }
}

- (void)deleteFavoriteSeller:(WDFavoriteSellers*) favoriteSeller {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] deleteFavoriteSeller:favoriteSeller.favoriteSellerID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Success" message:@"You have removed a seller from your list successfully!" preferredStyle:LEAlertControllerStyleAlert];
                    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                        // handle default button action
                    }];
                    [alertController addAction:defaultAction];
                    [self presentAlertController:alertController animated:YES completion:nil];
                    [self.listSeller removeObject:favoriteSeller];
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

@end
