//
//  WDBecomeSellerStepThreeTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDownPicker.h"
#import "WDCurrency.h"
#import "WDDelivery.h"

@interface WDBecomeSellerStepThreeTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITextField *tfHomeDelivery;
@property (weak, nonatomic) IBOutlet UIButton *btnFinish;
@property (nonatomic, assign)  NSInteger dishID;
@property (nonatomic, assign)  BOOL isFromSlideMenu;
@property (nonatomic, assign)  BOOL isUpdate;
@property (nonatomic, assign)  BOOL isHomeDeliveryService;
@property (weak, nonatomic) IBOutlet UITextField *tfPrice;
@property (weak, nonatomic) IBOutlet UITextField *pickerCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (nonatomic) DownPicker *picker1;
@property (nonatomic, strong) NSMutableArray* currenciesData;
@property (nonatomic, strong) NSMutableArray* currencies;
@property (weak, nonatomic) IBOutlet UITextField *tfRadius;
@property (nonatomic, assign)  NSInteger currencyID;
@property (nonatomic, strong)  NSString *specialOffer;
@property (weak, nonatomic) IBOutlet UIButton *btnFree;
@property (weak, nonatomic) IBOutlet UIButton *btnNoDelivery;
@property (weak, nonatomic) IBOutlet UILabel *lbFree;

- (IBAction)clickedFinish:(id)sender;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedTerm:(id)sender;
- (IBAction)clickedFree:(id)sender;
- (IBAction)clickedDelivery:(id)sender;

@end
