//
//  InviteFriendViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDInviteFriendViewController.h"
#import "UIColor+Style.h"
#import "KBContactsSelectionViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <URBNShareKit/URBNShareKit.h>
#import "WDUser.h"
#import "WDSourceConfig.h"
@import APAddressBook;

@interface WDInviteFriendViewController ()<KBContactsSelectionViewControllerDelegate>
@property (weak) KBContactsSelectionViewController* presentedCSVC;

@end

@implementation WDInviteFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.btnShare.clipsToBounds = YES;
    self.btnShare.layer.cornerRadius = self.btnShare.frame.size.height/2;//half of the width
    self.btnShare.layer.borderColor=[UIColor colorFromHexString:@"ffffff" withAlpha:1.0].CGColor;
    
    self.btnInviteContact.clipsToBounds = YES;
    self.btnInviteContact.layer.cornerRadius = self.btnInviteContact.frame.size.height/2;//half of the width
    self.btnInviteContact.layer.borderColor=[UIColor colorFromHexString:@"ffffff" withAlpha:1.0].CGColor;
    self.title = @"Invite Friends";
    self.lbTitle.text = [NSString stringWithFormat:@"Join MealsAround with %@",[WDUser sharedInstance].getFullName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedInvite:(id)sender {
    __block KBContactsSelectionViewController *vc = [KBContactsSelectionViewController contactsSelectionViewControllerWithConfiguration:^(KBContactsSelectionConfiguration *configuration) {
        configuration.tintColor = [UIColor colorFromHexString:@"F78B62" withAlpha:1.0];
        configuration.mode = KBContactsSelectionModeMessages;
        configuration.title = @"Invite Contacts";
        configuration.selectButtonTitle = @"Invites";
        
        configuration.messageBody = [NSString stringWithFormat:@"%@ invited you to join MealsAround. Sign up and explore our service at https://mealsaround.com",[WDUser sharedInstance].getFullName];
//        configuration.customSelectButtonHandler = ^(NSArray * contacts) {
//            NSLog(@"%@", contacts);
//        };
        
        configuration.searchByKeywords = YES;
        
    }];
    
    
    [vc setDelegate:self];
    [self presentViewController:vc animated:YES completion:nil];
    self.presentedCSVC = vc;
    
    __block UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 24)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Select people you want to email";
    
    vc.additionalInfoView = label;

}

- (IBAction)clickedShare:(id)sender {
    NSString *userName = [NSString stringWithFormat:@"%@%@",[WDUser sharedInstance].phonePrefix,[WDUser sharedInstance].phoneNumber];
    NSString *contentShare = [userName stringByAppendingString:@" invited you to join MealsAround. Sign up and explore our services."];
    NSURL *url = [NSURL URLWithString:[WDSourceConfig config].apiKey];
    
    URBNActivityViewController *urbnActivityController = [[URBNActivityViewController alloc] initWithDefaultBody:contentShare url:[NSURL URLWithString:[NSString stringWithFormat:@"%@://%@",[url scheme], [url host]]] canOpenInSafari:YES];
    
    [urbnActivityController setIncludedActivityTypes:@[UIActivityTypeCopyToPasteboard, UIActivityTypeMail, UIActivityTypeMessage, UIActivityTypePostToFacebook, UIActivityTypePostToTwitter, kURBNActivityTypePinterest, UIActivityTypeAddToReadingList]];
    [self presentViewController:urbnActivityController animated:YES completion:nil];

}

#pragma mark - KBContactsSelectionViewControllerDelegate
- (void) contactsSelection:(KBContactsSelectionViewController*)selection didSelectContact:(APContact *)contact {
    
    __block UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 36)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [NSString stringWithFormat:@"%@ Selected", @(self.presentedCSVC.selectedContacts.count)];
    
    self.presentedCSVC.additionalInfoView = label;
    
    NSLog(@"%@", self.presentedCSVC.selectedContacts);
}

- (void) contactsSelection:(KBContactsSelectionViewController*)selection didRemoveContact:(APContact *)contact {
    
    __block UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 36)];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [NSString stringWithFormat:@"%@ Selected", @(self.presentedCSVC.selectedContacts.count)];
    
    self.presentedCSVC.additionalInfoView = label;
    
    NSLog(@"%@", self.presentedCSVC.selectedContacts);
}

- (void)contactsSelectionWillLoadContacts:(KBContactsSelectionViewController *)csvc {
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
}

- (void)contactsSelectionDidLoadContacts:(KBContactsSelectionViewController *)csvc {
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
}


@end
