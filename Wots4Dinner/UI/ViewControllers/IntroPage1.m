//
//  IntroPage.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "IntroPage1.h"

@implementation IntroPage1

- (void)awakeFromNib {
    self.imagesArrays = [[NSMutableArray alloc]init];
    [self.imagesArrays addObject:[UIImage imageNamed:@"item1_choose_intro"]];
    [self.imagesArrays addObject:[UIImage imageNamed:@"item_choose_intro"]];
    [self.imagesArrays addObject:[UIImage imageNamed:@"item2_choose_intro"]];
    
    self.slideTransition = [CATransition animation];
    self.slideTransition.duration = 0.5;
    self.slideTransition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    self.slideTransition.type = kCATransitionFade;
    self.slideTransition.delegate = self;
    NSTimer * timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(slideShow) userInfo:nil repeats:YES];
    [timer fire];

}

- (void)slideShow {
    self.slideTransition.subtype =kCATransitionFromLeft; // or kCATransitionFromRight
    [self.itemImageView.layer addAnimation:self.slideTransition forKey:nil];
    if (self.indexSlide < self.imagesArrays.count-1) {
        self.indexSlide++;
        
    }else{
        self.indexSlide=0;
    }
    self.itemImageView.image =[self.imagesArrays objectAtIndex:self.indexSlide];
}

@end
