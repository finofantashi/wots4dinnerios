//
//  WDTodayMenuViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDTodayMenuViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *parallaxCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *lbSearch;
@property (weak, nonatomic) IBOutlet UIImageView *ivSearch;
@property (atomic, assign) BOOL isSkip;

@end
