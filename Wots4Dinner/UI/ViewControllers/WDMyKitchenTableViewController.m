//
//  WDMyKitchenTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/14/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDMyKitchenTableViewController.h"
#import "WDBecomeSellerStepOneTableViewController.h"
#import "WDBecomeSellerStepThreeTableViewController.h"
#import "WDMainViewController.h"

@interface WDMyKitchenTableViewController ()

@end

@implementation WDMyKitchenTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - IBAction
- (IBAction)clickedBack:(id)sender {
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
        [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
}

- (IBAction)clickedBasic:(id)sender {
    WDBecomeSellerStepOneTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepOneTableViewController"];
    viewController.isBasic = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedHomeDelivery:(id)sender {
    WDBecomeSellerStepThreeTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepThreeTableViewController"];
    viewController.isHomeDeliveryService = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
