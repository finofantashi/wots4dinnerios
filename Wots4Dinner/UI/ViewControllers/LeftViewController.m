
#import "LeftViewController.h"
#import "LeftViewCell.h"
#import "Constants.h"
#import "WDMainViewController.h"
#import "WDUser.h"
#import "UIImageView+AFNetworking.h"
#import "LEAlertController.h"
#import "WDSourceConfig.h"
#import "WDCartViewController.h"
#import "WDBecomeSellerStepTwoTableViewController.h"
#import "WDMyMenu.h"
#import "WDUserAPIController.h"

@interface LeftViewController ()

@property (strong, nonatomic) NSArray *titlesArray;
@property (strong, nonatomic) NSArray *iconsArray;

@end

@implementation LeftViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  if([WDUser sharedInstance].roleID==2&&[WDUser sharedInstance].isSellerMode){
    [self setSellerMode];
  }else{
    [self setUserMode];
  }
  self.tableView.contentInset = UIEdgeInsetsMake(44.f, 0.f, 44.f, 0.f);
}

- (void) setUserMode {
  _titlesArray = @[@"",
                   @"",
                   @"Home",
                   @"Become a Seller",
                   @"Search",
                   @"Message",
                   @"Food Cart",
                   @"Order History",
                   @"Profile",
                   @"Other",
                   @"Log Out"];
  
  _iconsArray = @[@"",
                  @"",
                  @"ic_home",
                  @"ic_profile",
                  @"ic_search_nav",
                  @"ic_message",
                  @"ic_cart",
                  @"ic_history",
                  @"ic_profile",
                  @"ic_other",
                  @"ic_logout"];
}

- (void) setSellerMode {
  _titlesArray = @[@"",
                   @"",
                   @"Switch to user",
                   @"Add dish",
                   @"My menu",
                   @"My orders",
                   @"Certified Seller",
                   @"Kitchen Video",
                   @"Message",
                   @"My kitchen",
                   @"Payout Method",
                   @"Log Out"];
  
  _iconsArray = @[@"",
                  @"",
                  @"ic_switch",
                  @"ic_add_dish",
                  @"ic_my_menu",
                  @"ic_my_order",
                  @"ic_menu_cer",
                  @"ic_video_library",
                  @"ic_message",
                  @"ic_my_kitchen",
                  @"ic_payout",
                  @"ic_logout"];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return _titlesArray.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  LeftViewCell *cell;
  if(indexPath.row==0){
    if(![[[WDUser sharedInstance] getAvatarURL] isEqualToString:@""]){
      NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,[[WDUser sharedInstance] getAvatarURL]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
      NSURL *url = [NSURL URLWithString:stringURL];
      NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
          UIImage *image = [UIImage imageWithData:data];
          dispatch_async(dispatch_get_main_queue(), ^{
            LeftViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
            if (updateCell){
              if (image) {
                updateCell.ivAvatar.image = image;
              }else{
                updateCell.ivAvatar.image = [UIImage imageNamed:@"empty_avatar"];
                
              }
            }
          });
          
        }
      }];
      [task resume];
      
    }
    cell = [tableView dequeueReusableCellWithIdentifier:@"leftViewCellAvatar"];
    cell.lbName.text = [[WDUser sharedInstance] getFullName];
    NSString *sinceday = [[WDUser sharedInstance] getSinceday];
    if(![sinceday isEqualToString:@""]){
      cell.lbSince.text = [NSString stringWithFormat:@"Member since %@",[[WDUser sharedInstance] stringWithSinceDate:sinceday]];
    }
  }else{
    cell = [tableView dequeueReusableCellWithIdentifier:@"leftViewCellItem"];
    cell.lbTitle.text = _titlesArray[indexPath.row];
    cell.ivIcon.image = [UIImage imageNamed:_iconsArray[indexPath.row]];
    
    if([WDUser sharedInstance].roleID==2&&[WDUser sharedInstance].isSellerMode){
      if(indexPath.row==8&&self.countUnreadMessages>0){
        cell.lbTitle.text = [cell.lbTitle.text stringByAppendingString:[NSString stringWithFormat:@" (%ld)",_countUnreadMessages]];
      }
      if(indexPath.row==5&&self.countUnreadOrder>0){
        cell.lbTitle.text = [cell.lbTitle.text stringByAppendingString:[NSString stringWithFormat:@" (%ld)",_countUnreadOrder]];
      }
    }else{
      if(indexPath.row==3&&[WDUser sharedInstance].roleID==2){
        cell.lbTitle.text = @"Switch to seller";
        cell.ivIcon.image = [UIImage imageNamed:@"ic_switch"];
      }
      if(indexPath.row==5&&self.countUnreadMessages>0){
        cell.lbTitle.text = [cell.lbTitle.text stringByAppendingString:[NSString stringWithFormat:@" (%ld)",_countUnreadMessages]];
      }
    }
    
    cell.separatorView.hidden = !(indexPath.row != 0 && indexPath.row != 1 && indexPath.row != _titlesArray.count-1);
    cell.userInteractionEnabled = (indexPath.row != 1);
    
    cell.tintColor = _tintColor;
  }
  
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if(indexPath.row == 0){
    return 150;
  }else if (indexPath.row == 1) {
    return 0.f;
  }
  return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  if([WDUser sharedInstance].roleID==2&&[WDUser sharedInstance].isSellerMode){
    [self didSelectRowAtIndexPathWithSellerMode:indexPath];
  }else{
    [self didSelectRowAtIndexPathWithUserMode:indexPath];
  }
  [[WDMainViewController sharedInstance] hideLeftViewAnimated:YES completionHandler:nil];
}

- (void) didSelectRowAtIndexPathWithSellerMode:(NSIndexPath *)indexPath {
  if(indexPath.row==0){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDProfileViewController"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==2) {
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController popToRootViewControllerAnimated:YES];
    [[WDUser sharedInstance] setSellerMode:NO];
    [self setUserMode];
    [self.tableView reloadData];
  }else if(indexPath.row==3){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    WDBecomeSellerStepTwoTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepTwoTableViewController"];
    viewController.isFromSlideMenu = YES;
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
  } else if(indexPath.row==4){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMyMenu"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==5){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    WDMyMenu *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMyMenu"];
    viewController.isMyOrder = YES;
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  } else if(indexPath.row==6){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDCertifiedTableVC"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  } else if(indexPath.row==7){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"KitchenVideoVC"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  } else if(indexPath.row==8){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMessageViewController"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==9){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMyKitchenTableViewController"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==10){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDPayoutMethodTableViewController"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==11) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Are you sure you want to logout?" preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      [[WDUserAPIController sharedInstance] logout];
      [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
      [[WDUser sharedInstance] deleteAllData];
      [self firstLaunch];
    }];
    
    LEAlertAction *cancelAction = [LEAlertAction actionWithTitle:@"Cancel" style:LEAlertActionStyleCancel handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }
}

- (void) didSelectRowAtIndexPathWithUserMode:(NSIndexPath *)indexPath {
  if(indexPath.row==2) {
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController popToRootViewControllerAnimated:YES];
  }else if(indexPath.row==3){
    if([WDUser sharedInstance].roleID == 2){
      [[WDUser sharedInstance] setSellerMode:YES];
      UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
      UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMyMenu"];
      viewController.title = _titlesArray[indexPath.row];
      //Remove
      NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
      NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
      if(newControllers.count>1){
        for (int i = 1; i < newControllers.count; i++) {
          [newControllers removeObjectAtIndex:i];
        }
      }
      [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
      
      [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
      [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
      [self setSellerMode];
      [self.tableView reloadData];
      
    }else{
      UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
      UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepOneTableViewController"];
      viewController.title = _titlesArray[indexPath.row];
      //Remove
      NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
      NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
      if(newControllers.count>1){
        for (int i = 1; i < newControllers.count; i++) {
          [newControllers removeObjectAtIndex:i];
        }
      }
      [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
      
      [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
      [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    }
  }else if(indexPath.row==4){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDSearchViewController"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==5){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Message" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMessageViewController"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==6){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    WDCartViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDCartViewController"];
    viewController.isFromSlideMenu = YES;
    
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==7){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDOrderHistoryViewController"];
    
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==0||indexPath.row==8){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDProfileViewController"];
    viewController.title = _titlesArray[indexPath.row];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==9){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
    UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDOtherTableViewController"];
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
      for (int i = 1; i < newControllers.count; i++) {
        [newControllers removeObjectAtIndex:i];
      }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
  }else if(indexPath.row==10){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Are you sure you want to logout?" preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      //            NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: [WDMainViewController sharedInstance].navigationController.viewControllers];
      //            if(navigationArray.count){
      //
      //            }
      [[WDUserAPIController sharedInstance] logout];
      [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
      [[WDUser sharedInstance] deleteAllData];
      [self firstLaunch];
    }];
    
    LEAlertAction *cancelAction = [LEAlertAction actionWithTitle:@"Cancel" style:LEAlertActionStyleCancel handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }
}

- (void) firstLaunch {
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
  UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"WDNavigationFirstLaucher"];
  UIWindow *window = UIApplication.sharedApplication.delegate.window;
  window.rootViewController = navigationController;
  [UIView transitionWithView:window
                    duration:0.3
                     options:UIViewAnimationOptionTransitionCrossDissolve
                  animations:nil
                  completion:nil];
}
@end
