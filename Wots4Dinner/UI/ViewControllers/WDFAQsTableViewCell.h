//
//  WDFAQsTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDFAQsTableViewCell : UITableViewCell <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lbQuestion;
@property (weak, nonatomic) IBOutlet UIView *viewLine;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
