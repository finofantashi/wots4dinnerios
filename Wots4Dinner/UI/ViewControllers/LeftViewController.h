

#import <UIKit/UIKit.h>

@interface LeftViewController : UITableViewController

@property (strong, nonatomic) UIColor *tintColor;
@property (assign, nonatomic) NSInteger countUnreadMessages;
@property (assign, nonatomic) NSInteger countUnreadOrder;

- (void) setSellerMode;
- (void) setUserMode;
@end
