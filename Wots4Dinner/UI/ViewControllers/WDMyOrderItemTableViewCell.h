//
//  CustomTableViewCell.h
//  MIResizableTableViewDemo
//
//  Created by Mario on 17/01/16.
//  Copyright © 2016 Mario. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDMyOrderItemTableViewCell.h"
#import "WDMyOrderItem.h"

@protocol WDMyOrderItemTableViewCellDelegate <NSObject>

- (void)clickedConfirm:(WDMyOrderItem *)dish;
- (void)clickedDeclined:(WDMyOrderItem *)dish;

@end

@interface WDMyOrderItemTableViewCell : UITableViewCell
@property (assign, nonatomic) id <WDMyOrderItemTableViewCellDelegate> myOrderItemDelegate;

+ (UINib *)cellNib;
+ (NSString *)cellIdentifier;

- (void)configureWithItem:(WDMyOrderItem *)dish
             currencyCode:(NSString *)currencyCode
           currencySymbol:(NSString *)currencySymbol;

- (void)configureWithTotalPrice:(float)totalPrice
                            fee:(float)fee
                   currencyCode:(NSString *)currencyCode
                 currencySymbol:(NSString *)currencySymbol;
@end
