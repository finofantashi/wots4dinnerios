//
//  ExploreMenuViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSSTabbedPageViewController.h"

@interface ExploreMenuViewController : MSSTabbedPageViewController
@property (nonatomic, assign) NSInteger sellerID;
@property (weak, nonatomic) IBOutlet UIView *tabBarContainerView;
@property (weak, nonatomic) IBOutlet UIView *pageContainerView;
@end

