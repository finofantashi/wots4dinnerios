//
//  WDDishViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDDishViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *foodTypeIDs;
@property (nonatomic, assign) NSInteger isFrom; //1 Search, 2 Today, 3 YouMayAlsoLike, 4 Popular Orders
@property (nonatomic, strong) NSString *foodTypeName;

//Search
@property (nonatomic, assign) double  lat;
@property (nonatomic, assign) double  lng;
@property (nonatomic, assign) long  distance;
@property (nonatomic, assign) long priceFrom;
@property (nonatomic, assign) long priceTo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintTableView;
@property (weak, nonatomic) IBOutlet UIView *viewNoData;

@end
