//
//  KitchenVideoVC.h
//  MealsAround
//
//  Created by Macbook on 5/24/20.
//  Copyright © 2020 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KitchenVideoVC : UITableViewController
@property (weak, nonatomic) IBOutlet UIButton *btnUploadFile;
@property (weak, nonatomic) IBOutlet UIImageView *ivUploadFile;

- (IBAction)clickedUploadFile:(id)sender;

@end
