//
//  BankMethodTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDPayoutMethod.h"

@interface BankMethodTableViewController : UITableViewController
@property (nonatomic, strong) WDPayoutMethod* payoutMethod;
@property (nonatomic, assign) BOOL isAddPayout;

@property (weak, nonatomic) IBOutlet UITextField *tfSwift;
@property (weak, nonatomic) IBOutlet UITextField *tfBankName;
@property (weak, nonatomic) IBOutlet UITextField *tfBankAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfAccountNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfAccountName;
@property (weak, nonatomic) IBOutlet UIButton *btnFinish;
- (IBAction)clickedFinish:(id)sender;

@end
