//
//  WDOurBlogViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//


#import "WDOurBlogViewController.h"
#import "WDOurBlogTableViewCell.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDBlogAPIController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDBlog.h"
#import "WDBlogPostViewController.h"
#import "WDSourceConfig.h"
#import "WDMainViewController.h"

@interface WDOurBlogViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray *listBlog;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;

@end

@implementation WDOurBlogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.listBlog = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    [self loadData:self.currentPage];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.currentPage == self.totalPages
        || self.totalItems == self.listBlog.count) {
        return self.listBlog.count;
    }
    return self.listBlog.count + 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentPage != self.totalPages && indexPath.row == [self.listBlog count] - 1 ) {
        [self loadData:++self.currentPage];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDOurBlogTableViewCell";
    UITableViewCell *cell;
    if (indexPath.row == [self.listBlog count]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:100];
        [activityIndicator startAnimating];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        WDBlog *blog = self.listBlog[indexPath.row];
        //get image name and assign
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,blog.image] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];

        [((WDOurBlogTableViewCell*)cell).ivOurBlog sd_setImageWithURL:[NSURL URLWithString:stringURL]
                                                     placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
        ((WDOurBlogTableViewCell*)cell).lbName.text = blog.title;
        //Date Time
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:blog.createdAt];
        NSString *stringDate = [NSDateFormatter localizedStringFromDate:dateFromString
                                                              dateStyle:NSDateFormatterMediumStyle
                                                              timeStyle:NSDateFormatterNoStyle];
        if(!stringDate){
            stringDate = @"";
        }
        ((WDOurBlogTableViewCell*)cell).lbDate.text = [NSString stringWithFormat:@"%@",stringDate];
        
        //[ ((WDOurBlogTableViewCell*)cell) cellOnTableView:self.tableView didScrollOnView:self.view];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WDBlog *blog = self.listBlog[indexPath.row];

    WDBlogPostViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBlogPostViewController"];
    viewController.blog = blog;
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // Get visible cells on table view.
    NSArray *visibleCells = [self.tableView visibleCells];
    
    for (UITableViewCell *cell in visibleCells) {
        if([cell isKindOfClass:[WDOurBlogTableViewCell class]] ){
            [(WDOurBlogTableViewCell*)cell cellOnTableView:self.tableView didScrollOnView:self.view];
        }
    }
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadData:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDBlogAPIController sharedInstance] getBlogs:page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDBlog *blog = [[WDBlog alloc] init];
                        [blog setBlogObject:object];
                        [self.listBlog addObject:blog];
                    };
                    
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No Blog." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

@end
