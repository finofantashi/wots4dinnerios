//
//  WDFavoriteSellerTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "HCSStarRatingView.h"
#import "WDFavoriteSellers.h"

@protocol WDFavoriteSellerTableViewCellDelegate <NSObject>

- (void)clickedAvatar:(WDUserTemp *)user;

@end

@interface WDFavoriteSellerTableViewCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbAddress;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewRating;
@property (nonatomic, strong) WDFavoriteSellers *favoriteSeller;
@property (assign, nonatomic) id <WDFavoriteSellerTableViewCellDelegate> favoriteSellerDelegate;

@end
