//
//  WDDishDetailMoreViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/25/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDDish.h"

@interface WDDishDetailMoreViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *lbPickupTime;
@property (weak, nonatomic) IBOutlet UILabel *lbServingDate;
@property (weak, nonatomic) IBOutlet UILabel *lbRepeat;
@property (weak, nonatomic) IBOutlet UILabel *lbDeliveryService;
@property (nonatomic, strong) WDDish *dish;
@property (weak, nonatomic) IBOutlet UILabel *lbSpecialOffer;
@property (nonatomic, strong) NSMutableArray* currencies;

@end
