//
//  WDGetResetCodeViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDGetResetCodeViewController.h"
#import "CountryListDataSource.h"
#import "WDCountryTableViewCell.h"
#import "WDVerifyResetCodeViewController.h"
#import "MBProgressHUD.h"
#import "LEAlertController.h"
#import "Reachability.h"
#import "WDUserAPIController.h"
#import "NSString+IsValidEmail.h"
#import "WDUserDefaultsManager.h"
#import "WDSourceConfig.h"
#import "UIColor+Style.h"

@interface WDGetResetCodeViewController () <UITextFieldDelegate,UIGestureRecognizerDelegate>
@property (strong, nonatomic) NSArray *dataRows;
@property (strong, nonatomic) NSMutableArray *searchArray;
@property (strong, nonatomic) NSString *searchTextString;
@property (strong, nonatomic) NSString *countryCode;

@end

@implementation WDGetResetCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [WDUserDefaultsManager setValue:[NSNumber numberWithBool:YES] forKey:kSessionSignUp];
    [self setupSearch];
    [self setupDismissKeyboard];
    if(self.isFromSignUp){
        [self.btnGetResetCode setTitle:@"Get verification code" forState:UIControlStateNormal];
    }
    self.btnGetResetCode.layer.cornerRadius = self.btnGetResetCode.frame.size.height/2; // this value vary as per your desire
    self.btnGetResetCode.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setupDismissKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.delegate = self;
    
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [self.tfPhoneNumber resignFirstResponder];
    [self showHideViewSearch:NO];
}

- (void)setupCountryList {
    self.tableView.layer.cornerRadius=5;
    CountryListDataSource *dataSource = [[CountryListDataSource alloc] init];
    _dataRows = [dataSource countries];
    [_tableView reloadData];
}

- (void)enableButtonGetCode {
    if(![self.lbCountry isEqual:@"Your country"]&&![self.lbAreaCode isEqual:@"Area code"]&&self.tfPhoneNumber.text.length!=0){
        self.btnGetResetCode.enabled = YES;
        self.btnGetResetCode.alpha = 1.0f;
    }else{
        self.btnGetResetCode.enabled = NO;
        self.btnGetResetCode.alpha = 0.5f;
    }
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"WDCountryTableViewCell";
    
    WDCountryTableViewCell *cell = (WDCountryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryCode]];
    
    cell.ivFlag.image = [UIImage imageNamed:imagePath];
    cell.lbCountry.text = [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryName];
    cell.lbCode.text = [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryCallingCode];
    
    return cell;
}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.lbCountry.text = [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryName];
    self.lbAreaCode.text = [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryCallingCode];
    self.countryCode =  [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryCode];
    [self showHideViewSearch:NO];
    [self enableButtonGetCode];
}

#pragma mark - Search methods

- (void) setupSearch{
    self.tfSearch.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_search"]];
    
    //set the selector to the text field in order to change its value when edited
    [self.tfSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //here you set up the methods to search array and reloading the tableview
    [self setupCountryList];
    [self updateSearchArray];
}

//update seach method where the textfield acts as seach bar
-(void)updateSearchArray {
    if (self.searchTextString.length != 0) {
        self.searchArray = [NSMutableArray array];
        for (NSDictionary* item in self.dataRows) {
            if ([[[item objectForKey:@"name"] lowercaseString] rangeOfString:[self.searchTextString lowercaseString]].location != NSNotFound) {
                [self.searchArray addObject:item];
            }
        }
    } else {
        self.searchArray = [[NSMutableArray alloc] initWithArray:self.dataRows];
    }
    
    [self.tableView reloadData];
}

- (IBAction)textFieldDidChange:(id)sender {
    self.searchTextString = self.tfSearch.text;
    [self updateSearchArray];
}

- (void) showHideViewSearch:(BOOL)isShow {
    if(isShow){
        self.tableView.hidden = NO;
        self.viewSearch.hidden = NO;
        self.lbCountry.hidden = YES;
    }else{
        self.lbCountry.hidden = NO;
        self.tableView.hidden = YES;
        self.viewSearch.hidden = YES;
        [self.tfSearch setText:@""];
        [self textFieldDidChange:nil];
    }
}

- (IBAction)textFieldDidChaned:(id)sender {
    [self enableButtonGetCode];
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.tableView]) {
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }else if([touch.view isDescendantOfView:self.lbCountry.superview]){
        if(self.tableView.hidden == NO){
            return NO;
        }
        [self clickedCountry:nil];
        return NO;
    }
    
    return YES;
}

#pragma mark IBAction

- (IBAction)touchDown:(id)sender {
    [self.btnGetResetCode setBackgroundColor:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0]];
}

- (IBAction)clickedBack:(id)sender {
    [WDUserDefaultsManager setValue:[NSNumber numberWithBool:NO] forKey:kSessionSignUp];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedCountry:(id)sender {
    [self.tfPhoneNumber resignFirstResponder];
    
    if (self.tableView.hidden == YES) {
        [self showHideViewSearch:YES];
    }else{
        [self showHideViewSearch:NO];
    }
}

- (IBAction)clickedGetResetCode:(id)sender {
    [self.btnGetResetCode setBackgroundColor:[UIColor colorFromHexString:@"e7663f" withAlpha:1.0]];

    if(self.isChangePhoneNumber){
        [self changePhoneAPI];
    }else if(self.isFromSignUp){
        [self signupAPI];
    }else{
        [self forgotPassAPI];
    }
}

#pragma mark API

- (void) signupAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //Call API
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        NSString *phoneNumber = [self.tfPhoneNumber.text trim];
        if(phoneNumber.length>0&&[phoneNumber characterAtIndex:0] == '0'){
            phoneNumber = [phoneNumber stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }
        [[WDUserAPIController sharedInstance] signup:phoneNumber password:@"" phonePrefix:self.countryCode completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                WDVerifyResetCodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDVerifyResetCodeViewController"];
                viewController.phoneNumber = [self.tfPhoneNumber.text trim];
                viewController.prefixPhone = self.lbAreaCode.text;
                viewController.isFromSignUp = self.isFromSignUp;
                viewController.countryCode = self.countryCode;

                [self.navigationController pushViewController:viewController animated:YES];
            }
        }];
    }
}

- (void) forgotPassAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //Call API
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        NSString *phoneNumber = [self.tfPhoneNumber.text trim];
        if(phoneNumber.length>0&&[phoneNumber characterAtIndex:0] == '0'){
            phoneNumber = [phoneNumber stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }
        [[WDUserAPIController sharedInstance] forgotPassword:phoneNumber phonePrefix:self.lbAreaCode.text completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                WDVerifyResetCodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDVerifyResetCodeViewController"];
                viewController.phoneNumber = [self.tfPhoneNumber.text trim];
                viewController.prefixPhone = self.lbAreaCode.text;
                [self.navigationController pushViewController:viewController animated:YES];
            }
        }];
    }
}

- (void) changePhoneAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //Call API
        NSString *phoneNumber = [self.tfPhoneNumber.text trim];
        if(phoneNumber.length>0&&[phoneNumber characterAtIndex:0] == '0'){
            phoneNumber = [phoneNumber stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        [[WDUserAPIController sharedInstance] changePhoneNumber:phoneNumber phonePrefix:self.countryCode completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                WDVerifyResetCodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDVerifyResetCodeViewController"];
                viewController.phoneNumber = [self.tfPhoneNumber.text trim];
                viewController.prefixPhone = self.lbAreaCode.text;
                viewController.isChangePhone = YES;
                viewController.countryCode = self.countryCode;
                [self.navigationController pushViewController:viewController animated:YES];
            }
        }];
    }
}

@end
