//
//  WDPaymentDetailTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/1/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDPaymentDetailTableViewController.h"
#import "UIColor+Style.h"
#import "PKCardNumber.h"
#import "PKCardExpiry.h"
#import "PKCardCVC.h"
#import <CardIO.h>
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "WDOrderAPIController.h"
#import "WDCard.h"
#import "WDOrderCompleteViewController.h"

@interface WDPaymentDetailTableViewController () <UITextFieldDelegate, CardIOPaymentViewControllerDelegate>
@property (nonatomic, assign) BOOL isValidState;
@property (assign, nonatomic) BOOL isSaveInfo;
@property (strong, nonatomic) WDCard *cardDefault;
@property (strong, nonatomic) NSString *expiryDate;
@property (strong, nonatomic) NSString *orderNumber;

@end

@implementation WDPaymentDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![CardIOUtilities canReadCardWithCamera]) {
        self.btnScan.enabled = NO;
        self.btnScan.hidden = YES;
    }
    [CardIOUtilities preloadCardIO];
    self.btnOrder.layer.cornerRadius = self.btnOrder.frame.size.height/2; // this value vary as per your desire
    self.btnOrder.layer.borderWidth = 1.0f;
    self.btnOrder.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
    self.btnOrder.clipsToBounds = YES;
    self.cardDefault = [[WDCard alloc] init];
    if(self.isFromAddCard){
        self.title = @"Payment Method";
    }else{
        self.title = @"Payment Details";
        [self getCards];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - textField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)replacementString {
    if ([textField isEqual:self.tfCardNumber]) {
        return [self cardNumberFieldShouldChangeCharactersInRange:range replacementString:replacementString];
    }
    else if ([textField isEqual:self.tfExpiryDate]) {
        return [self cardExpiryShouldChangeCharactersInRange:range replacementString:replacementString];
    }
    else if ([textField isEqual:self.tfCCV]) {
        return [self cardCVCShouldChangeCharactersInRange:range replacementString:replacementString];
    }
    
    return YES;
}

- (BOOL)cardNumberFieldShouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)replacementString {
    NSString *resultString = [self.tfCardNumber.text stringByReplacingCharactersInRange:range withString:replacementString];
    resultString = [PKTextField textByRemovingUselessSpacesFromString:resultString];
    PKCardNumber *cardNumber = [PKCardNumber cardNumberWithString:resultString];
    
    if (![cardNumber isPartiallyValid]) {
        return NO;
    }
    
    if (replacementString.length > 0) {
        self.tfCardNumber.text = [cardNumber formattedStringWithTrail];
    }
    else {
        self.tfCardNumber.text = [cardNumber formattedString];
    }
    
    [self setPlaceholderToCardType];
    
    if ([cardNumber isValid]) {
        [self textFieldIsValid:self.tfCardNumber];
        [self.tfFirstName becomeFirstResponder];
    } else if ([cardNumber isValidLength] && ![cardNumber isValidLuhn]) {
        [self textFieldIsInvalid:self.tfCardNumber withErrors:YES];
    } else if (![cardNumber isValidLength]) {
        [self textFieldIsInvalid:self.tfCardNumber withErrors:NO];
    }
    
    return NO;
}

- (BOOL)cardExpiryShouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)replacementString {
    NSString *resultString = [self.tfExpiryDate.text stringByReplacingCharactersInRange:range withString:replacementString];
    resultString = [PKTextField textByRemovingUselessSpacesFromString:resultString];
    PKCardExpiry *cardExpiry = [PKCardExpiry cardExpiryWithString:resultString];
    
    if (![cardExpiry isPartiallyValid]) {
        return NO;
    }
    
    // Only support shorthand year
    if ([cardExpiry formattedString].length > 5) return NO;
    
    if (replacementString.length > 0) {
        self.tfExpiryDate.text = [cardExpiry formattedStringWithTrail];
    } else {
        self.tfExpiryDate.text = [cardExpiry formattedString];
    }
    
    if ([cardExpiry isValid]) {
        [self textFieldIsValid:self.tfExpiryDate];
        [self.tfCCV becomeFirstResponder];
    } else if ([cardExpiry isValidLength] && ![cardExpiry isValidDate]) {
        [self textFieldIsInvalid:self.tfExpiryDate withErrors:YES];
    } else if (![cardExpiry isValidLength]) {
        [self textFieldIsInvalid:self.tfExpiryDate withErrors:NO];
    }
    
    return NO;
}


- (BOOL)cardCVCShouldChangeCharactersInRange: (NSRange)range replacementString:(NSString *)replacementString {
    NSString *resultString = [self.tfCCV.text stringByReplacingCharactersInRange:range withString:replacementString];
    resultString = [PKTextField textByRemovingUselessSpacesFromString:resultString];
    PKCardCVC *cardCVC = [PKCardCVC cardCVCWithString:resultString];
    PKCardType cardType = [[PKCardNumber cardNumberWithString:self.tfCardNumber.text] cardType];
    
    // Restrict length
    if (![cardCVC isPartiallyValidWithType:cardType]) {
        return NO;
    }
    
    // Strip non-digits
    self.tfCCV.text = [cardCVC string];
    
    if ([cardCVC isValidWithType:cardType]) {
        [self textFieldIsValid:self.tfCCV];
    } else {
        [self textFieldIsInvalid:self.tfCCV withErrors:NO];
    }
    
    return NO;
}

#pragma mark - Private

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0||indexPath.row==2||indexPath.row==4||indexPath.row==6||indexPath.row==8){
        return 54;
    }else if(indexPath.row==10){
        return 68;
    }else if(indexPath.row==9){
        if(self.cardDefault.cardID!=0){
            return 0;
        }
        return 54;
    }else{
        return 1;
    }
}

- (void)setPlaceholderToCardType {
    PKCardNumber *cardNumber = [PKCardNumber cardNumberWithString:self.tfCardNumber.text];
    PKCardType cardType      = [cardNumber cardType];
    NSString *cardTypeName   = @"placeholder";
    
    switch (cardType) {
        case PKCardTypeAmex:
            cardTypeName = @"amex";
            break;
        case PKCardTypeDinersClub:
            cardTypeName = @"diners";
            break;
        case PKCardTypeDiscover:
            cardTypeName = @"discover";
            break;
        case PKCardTypeJCB:
            cardTypeName = @"jcb";
            break;
        case PKCardTypeMasterCard:
            cardTypeName = @"mastercard";
            break;
        case PKCardTypeVisa:
            cardTypeName = @"visa";
            break;
        default:
            break;
    }
    //cardTypeName = [NSString stringWithFormat:@"%@-outline", cardTypeName];
    [self setPlaceholderViewImage:[UIImage imageNamed:cardTypeName]];
}

- (void)setPlaceholderViewImage:(UIImage *)image {
    if (![self.ivCardNumber.image isEqual:image]) {
        [UIView animateWithDuration:0.25 delay:0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.ivCardNumber.image = image;
                         } completion:^(BOOL finished) {
                             
                         }];
    }
}

-(void)setSave:(BOOL)isChecked {
    if(isChecked){
        self.isSaveInfo = YES;
        [self.btnSave setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
    }else{
        self.isSaveInfo = NO;
        [self.btnSave setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
    }
    [self.tableView reloadData];
}

- (void)setData {
    if(self.cardDefault.cardID!=0){
        self.btnScan.hidden = YES;
        self.tfCardNumber.enabled = NO;
        // Use the card info...
//        for (int i=0; i<self.cardDefault.cardNumber.length; i++) {
//            NSRange range = NSMakeRange(self.tfCardNumber.text.length, 0);
//            [self textField:self.tfCardNumber shouldChangeCharactersInRange:range replacementString:[self.cardDefault.cardNumber substringWithRange:NSMakeRange(i, 1)]];
//        }
        self.tfCardNumber.text = [NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[self cardNumber:self.cardDefault.cardNumber].lastGroup];

        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/yy"];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setYear:self.cardDefault.expireYear];
        [comps setMonth:self.cardDefault.expireMonth];
        NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
        NSString *expiryDate = [formatter stringFromDate:date];
        self.tfExpiryDate.text = expiryDate;
        self.expiryDate = expiryDate;
        self.tfFirstName.text = self.cardDefault.firstName;
        self.tfLastName.text = self.cardDefault.lastName;

    }
}

#pragma mark - Accessors

- (PKCardNumber *)cardNumber:(NSString*)cardNumber  {
    return [PKCardNumber cardNumberWithString:cardNumber];
}

- (PKCardExpiry *)cardExpiry {
    return [PKCardExpiry cardExpiryWithString:self.tfExpiryDate.text];
}

- (PKCardCVC *)cardCVC {
    return [PKCardCVC cardCVCWithString:self.tfCCV.text];
}

#pragma mark - Validations

- (BOOL)isValid {
    return [[self cardNumber:self.tfCardNumber.text] isValid] && [self.cardExpiry isValid] &&
    [self.cardCVC isValidWithType:[self cardNumber:self.tfCardNumber.text].cardType];
}

- (BOOL)isValidPlaceOrder {
    NSString *strCardNumber = @"";
    if(self.cardDefault.cardNumber==0){
        strCardNumber  = self.tfCardNumber.text;
    }else{
        strCardNumber = self.cardDefault.cardNumber;
    }
    if([[self.tfCardNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Card Number is required!" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfCardNumber becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }else if(![[self cardNumber:strCardNumber] isValid]){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Card Number is invalid!" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfCardNumber becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }else if([[self.tfFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"First Name is required!" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfFirstName becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }else if([[self.tfLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Last Name is required!" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfFirstName becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else if([[self.tfExpiryDate.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Expiry Date is required!" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfExpiryDate becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }else if(![self.cardExpiry isValid]){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Expiry Date is invalid!" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfExpiryDate becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
        
    }else if([[self.tfCCV.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"CCV/CVV is required!" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfCCV becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }else if(![self.cardCVC isValidWithType:[self cardNumber:self.tfCardNumber.text].cardType]){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"CCV/CVV is invalid!" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfCCV becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
        
    }
    return YES;
}

- (void)checkValid {
    if ([self isValid]) {
        self.isValidState = YES;
        
        
    } else if (![self isValid] && self.isValidState) {
        self.isValidState = NO;
        
        
    }
}

- (void)textFieldIsInvalid:(UITextField *)textField withErrors:(BOOL)errors {
    if (errors) {
        textField.textColor = [UIColor redColor];
    } else {
        textField.textColor = [UIColor colorFromHexString:@"464646" withAlpha:1.0];
    }
    
    [self checkValid];
}

- (void)textFieldIsValid:(UITextField *)textField {
    textField.textColor = [UIColor colorFromHexString:@"464646" withAlpha:1.0];
    [self checkValid];
}

#pragma mark - CardIOPaymentViewControllerDelegate

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    NSLog(@"User canceled payment info");
    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    // The full card number is available as info.cardNumber, but don't log that!
    NSString *expiryYear = [[NSString stringWithFormat:@"%ld",info.expiryYear] substringFromIndex:3];
    // Use the card info...
    for (int i=0; i<info.cardNumber.length; i++) {
        NSRange range = NSMakeRange(self.tfCardNumber.text.length, 0);
        [self textField:self.tfCardNumber shouldChangeCharactersInRange:range replacementString:[info.cardNumber substringWithRange:NSMakeRange(i, 1)]];
    }
    self.tfExpiryDate.text = [NSString stringWithFormat:@"%ld/%@",info.expiryMonth, expiryYear];
    self.tfCCV.text = info.cvv;
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - IBAction

- (IBAction)clickedScand:(id)sender {
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self scanningEnabled:YES];
    [self presentViewController:scanViewController animated:YES completion:nil];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedSave:(id)sender {
    if(self.isSaveInfo){
        [self setSave:NO];
    }else{
        [self setSave:YES];
    }
}

- (IBAction)clickedOrder:(id)sender {
    if([self isValidPlaceOrder]){
        if(self.isFromAddCard){
            WDCard *card = [[WDCard alloc] init];
            [card setCardNumber:[self.tfCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
            [card setFirstName:self.tfFirstName.text];
            [card setLastName:self.tfLastName.text];
            [card setExpireMonth:[[self.tfExpiryDate.text substringWithRange:NSMakeRange(0, 2)] integerValue]];
            [card setExpireYear:[[self.tfExpiryDate.text substringFromIndex:3] integerValue]];
            [self saveCard:card];
        }else{
            [self checkoutCreditCard];
        }
    }
}

- (void) getCards {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] getCards:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                if(responseObject) {
                    self.cardDefault = [[WDCard alloc] init];
                    NSMutableArray *listCard = [[NSMutableArray alloc] init];
                    NSDictionary *dataCard = [responseObject valueForKey:@"data"];
                    for (NSDictionary *dict in dataCard) {
                        WDCard *card = [[WDCard alloc] init];
                        [card setCardObject:dict];
                        if(card.cardDefault==1){
                            self.cardDefault = card;
                            break;
                        }
                        [listCard addObject:card];
                    }
                    if(listCard.count>0&&self.cardDefault.cardID==0){
                        self.cardDefault = [listCard objectAtIndex:0];
                    }
                    [self setData];
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

- (void) updateCard {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] updateCard:self.cardDefault isDefault:@"1" completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            
            WDOrderCompleteViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDOrderCompleteViewController"];
            viewController.orderNumber = self.orderNumber;
            [self.navigationController pushViewController:viewController animated:YES];
        }];
    }
}

- (void) saveCard:(WDCard*)card {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        NSString *cardDefault = @"1";
        if(self.isFromAddCard){
            cardDefault = self.isSaveInfo==NO?@"0":@"1";
        }
        
        //Call Api
        [[WDOrderAPIController sharedInstance] saveCard:card isDefault:cardDefault completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(self.isFromAddCard){
                [self clickedBack:nil];
            }else{
                WDOrderCompleteViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDOrderCompleteViewController"];
                viewController.orderNumber = self.orderNumber;
                [self.navigationController pushViewController:viewController animated:YES];
            }
        }];
    }
}

- (void) checkoutCreditCard {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        NSString *strCardNumber = @"";
        if(self.cardDefault.cardNumber==0){
            strCardNumber  = self.tfCardNumber.text;
        }else{
            strCardNumber = self.cardDefault.cardNumber;
        }
        
        WDCard *card = [[WDCard alloc] init];
        [card setCardNumber:[strCardNumber stringByReplacingOccurrencesOfString:@" " withString:@""]];
        [card setFirstName:self.tfFirstName.text];
        [card setLastName:self.tfLastName.text];
        [card setExpireMonth:[[self.tfExpiryDate.text substringWithRange:NSMakeRange(0, 2)] integerValue]];
        [card setExpireYear:[[NSString stringWithFormat:@"20%@",[self.tfExpiryDate.text substringFromIndex:3]] integerValue]];

        
        if([self cardNumber:strCardNumber].cardType==PKCardTypeVisa){
            [card setCardType:@"visa"];
        }else if([self cardNumber:strCardNumber].cardType==PKCardTypeMasterCard){
            [card setCardType:@"mastercard"];
        }else if([self cardNumber:strCardNumber].cardType==PKCardTypeAmex){
            [card setCardType:@"amex"];
        }else if([self cardNumber:strCardNumber].cardType==PKCardTypeDiscover){
            [card setCardType:@"discover"];
        }else if([self cardNumber:strCardNumber].cardType==PKCardTypeJCB){
            [card setCardType:@"jcb"];
        }else if([self cardNumber:strCardNumber].cardType==PKCardTypeDinersClub){
            [card setCardType:@"diners_club_international"];
        }
        
        //Call Api
        [[WDOrderAPIController sharedInstance] checkOutCreditCard:card cvv:self.tfCCV.text completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                if([[responseObject valueForKey:@"success"] boolValue]) {
                    self.orderNumber = [[responseObject valueForKey:@"data"] valueForKey:@"order_number"];
                    if(self.isSaveInfo&&self.cardDefault.cardID==0){
                        WDCard *card = [[WDCard alloc] init];
                        [card setCardNumber:[self.tfCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
                        [card setFirstName:self.tfFirstName.text];
                        [card setLastName:self.tfLastName.text];
                        [card setExpireMonth:[[self.tfExpiryDate.text substringWithRange:NSMakeRange(0, 2)] integerValue]];
                        [card setExpireYear:[[self.tfExpiryDate.text substringFromIndex:3] integerValue]];
                        [self saveCard:card];
                        
                    }else if(self.isSaveInfo&&
                             (![self.tfCardNumber.text isEqualToString:self.cardDefault.cardNumber]||
                              ![self.tfFirstName.text isEqualToString:self.cardDefault.firstName]||
                              ![self.tfLastName.text isEqualToString:self.cardDefault.lastName]||
                              ![self.tfExpiryDate.text isEqualToString:self.expiryDate])){
                                 [self.cardDefault setCardNumber:[self.tfCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""]];
                                 [self.cardDefault setFirstName:self.tfFirstName.text];
                                 [self.cardDefault setLastName:self.tfLastName.text];
                                 [self.cardDefault setExpireMonth:[[self.tfExpiryDate.text substringWithRange:NSMakeRange(0, 2)] integerValue]];
                                 [card setExpireYear:[[self.tfExpiryDate.text substringFromIndex:3] integerValue]];
                                 [self updateCard];
                             }else {
                                 WDOrderCompleteViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDOrderCompleteViewController"];
                                 viewController.orderNumber = self.orderNumber;
                                 [self.navigationController pushViewController:viewController animated:YES];
                             }
                }else {
                    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:[responseObject valueForKey:@"message"] preferredStyle:LEAlertControllerStyleAlert];
                    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                        // handle default button action
                    }];
                    [alertController addAction:defaultAction];
                    [self presentAlertController:alertController animated:YES completion:nil];                }
            }
        }];
    }
}

@end
