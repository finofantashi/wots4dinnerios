//
//  WDDishOrderTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDDish.h"

@interface WDDishOrderTableViewController : UITableViewController
@property (nonatomic, assign) NSInteger dishID;

@property (weak, nonatomic) IBOutlet UIImageView *ivDish;
@property (weak, nonatomic) IBOutlet UILabel *nameDish;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantityLeft;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantity;
@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnPlus;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckOut;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCart;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleQtyLeft;


@property (nonatomic, strong) WDDish *dish;

- (IBAction)clickedMinus:(id)sender;
- (IBAction)clickedPlus:(id)sender;
- (IBAction)clickedCheckOut:(id)sender;
- (IBAction)clickedAddCart:(id)sender;

@end
