//
//  WDEditProfileTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/28/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDEditProfileTableViewCell.h"
#import "NSString+IsValidEmail.h"

@implementation WDEditProfileTableViewCell

#pragma mark - Private

- (void)setupDismissKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.superview addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    if(self.tfFirstName){
        [self.tfFirstName resignFirstResponder];
    }
    if(self.tfLastName){
        [self.tfLastName resignFirstResponder];
    }
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger oldLength = [[textField.text trim] length];
    NSUInteger replacementLength = [[string trim] length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    if(textField==self.tfFirstName||textField==self.tfLastName){
        return newLength <= 30 || returnKey;
    }else if(textField==self.tfEmail||textField==self.tfLocation){
        return newLength <= 255 || returnKey;
    }
    return YES;
}

- (IBAction)textFieldChange:(id)sender {
    //[self enableButtonSetName];
}

@end
