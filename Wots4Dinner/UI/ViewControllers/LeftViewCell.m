
#import "LeftViewCell.h"
#import <QuartzCore/QuartzCore.h>

@interface LeftViewCell ()

@end

@implementation LeftViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    // -----

    self.backgroundColor = [UIColor clearColor];

    self.lbTitle.font = [UIFont fontWithName:@"Poppins-Light" size:16];
    if(self.ivAvatar){
        self.ivAvatar.layer.cornerRadius = 80 / 2;
        self.ivAvatar.layer.masksToBounds = YES;
        self.ivAvatar.layer.borderColor = [UIColor colorWithWhite:0.9f
                                                            alpha:1.0f].CGColor;
        self.ivAvatar.layer.borderWidth = 2.0f;
        [self.ivAvatar setNeedsDisplay];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];

   self.lbTitle.textColor = _tintColor;
    //_separatorView.backgroundColor = [_tintColor colorWithAlphaComponent:0.4];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted)
        self.lbTitle.textColor = [UIColor colorWithRed:0.f green:0.5 blue:1.f alpha:1.f];
    else
        self.lbTitle.textColor = _tintColor;
}

@end
