//
//  WDOurBlogTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//


#import "WDOurBlogTableViewCell.h"

@implementation WDOurBlogTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view {
    CGRect rectInSuperview = [tableView convertRect:self.frame toView:view];
    
    float distanceFromCenter = CGRectGetHeight(view.frame)/2 - CGRectGetMinY(rectInSuperview);
    float difference = CGRectGetHeight(self.ivOurBlog.frame) - CGRectGetHeight(self.frame);
    float move = (distanceFromCenter / CGRectGetHeight(view.frame)) * difference;
    
    CGRect imageRect = self.ivOurBlog.frame;
    imageRect.origin.y = -(difference/2)+move;
    self.ivOurBlog.frame = imageRect;
}

@end
