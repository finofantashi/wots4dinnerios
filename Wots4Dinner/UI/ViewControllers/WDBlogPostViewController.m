//
//  WDBlogPostViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBlogPostViewController.h"
#import "WDBlogPostTableViewCell.h"
#import "WDBlogCommentTableViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDSourceConfig.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDBlogAPIController.h"
#import "NSString+HTML.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define iOS7_0 @"7.0"
#define kOFFSET_FOR_KEYBOARD 250.0

@implementation WDBlogPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    self.title = @"Our Blog";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
  self.navigationController.navigationBar.hidden = false;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getBlogDetailAPI];
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)textChanged:(id)sender {
    [self enableButton];
}

- (IBAction)clickedSend:(id)sender {
    [self postCommentAPI];
}

#pragma mark - Private

-(void) setupUI {
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.viewReviews addGestureRecognizer:singleFingerTap];
    
    self.lbReviews.text = [NSString stringWithFormat:@"%ld",(long)self.blog.numComments];
  self.tableView.estimatedRowHeight = 44;
}

- (void)dismissKeyboard {
    [self.tfComment resignFirstResponder];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    WDBlogCommentTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDBlogCommentTableViewController"];
    viewController.blogID = self.blog.blogID;
    [self.navigationController pushViewController:viewController animated:NO];
    self.isWatchReviews = YES;
    [self dismissKeyboard];
}

- (void)enableButton {
    if(self.tfComment.text.length!=0&&self.tfComment.text.length!=0){
        self.btnSend.enabled = YES;
        self.btnSend.alpha = 1.0f;
    }else{
        self.btnSend.enabled = NO;
        self.btnSend.alpha = 0.5f;
    }
}

#pragma mark - UITextField Delegate methods

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField==self.tfComment){
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        return newLength <= 350 || returnKey;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)sender {
    if ([sender isEqual:self.tfComment]) {
        
    }
}

# pragma mark - UITableViewControllerDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDBlogPostTableViewCell *cell;
    if(!self.blog){
        self.blog = [[WDBlog alloc] init];
    }
    if(indexPath.row==0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDBlogPostTableViewCellTitle" forIndexPath:indexPath];
        if(self.blog.title){
            cell.lbTitle.text = self.blog.title;
        }
    }else if(indexPath.row==1){
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDBlogPostTableViewCellDate" forIndexPath:indexPath];
        
        //Date Time
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:self.blog.createdAt==nil?@"":self.blog.createdAt];
        NSString *stringDate = [NSDateFormatter localizedStringFromDate:dateFromString
                                                              dateStyle:NSDateFormatterMediumStyle
                                                              timeStyle:NSDateFormatterNoStyle];
        if(!stringDate){
            stringDate = @"";
        }
        cell.lbDate.text = [NSString stringWithFormat:@"%@",stringDate];
    }else if(indexPath.row==2){
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDBlogPostTableViewCellImage" forIndexPath:indexPath];
        //get image name and assign
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,self.blog.image] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        [cell.ivBlog sd_setImageWithURL:[NSURL URLWithString:stringURL] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDBlogPostTableViewCellDescription" forIndexPath:indexPath];
        if(self.blog.content){
            NSString *contentBlog = [[[self.blog.content stringByStrippingTags] stringByRemovingNewLinesAndWhitespace] stringByDecodingHTMLEntities];
            cell.lbDescription.text = contentBlog;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(indexPath.row==0){
//        return 72;
//    }else if(indexPath.row==1){
//        return 44;
//    }else if(indexPath.row==2){
//        return 225;
//    }else{
//        return [self sizeOfMultiLineLabel:self.blog.content==nil?@"":self.blog.content];
//    }
    return UITableViewAutomaticDimension;
}

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
    
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = [UIFont fontWithName:@"Poppins-Light" size:14];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.view.frame.size.width-40;
    CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{
                                                                 NSFontAttributeName : aLabelFont
                                                                 }
                                                       context:nil].size;
    return labelSize.height;
}

#pragma mark API

- (void) postCommentAPI {
    [self dismissKeyboard];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        
        [[WDBlogAPIController sharedInstance] postComment:[NSNumber numberWithInteger:self.blog.blogID] content:[self.tfComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                if([responseObject valueForKey:@"success"]){
                    [self getBlogDetailAPI];
                }
            }
        }];
    }
}

- (void)getBlogDetailAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDBlogAPIController sharedInstance] getBlogDetail:self.blog.blogID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataBlog= [responseObject valueForKey:@"data"];
                    WDBlog *blog = [[WDBlog alloc] init];
                    [blog setBlogObject:dataBlog];
                    self.blog = blog;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                        self.tfComment.text = @"";
                        self.lbReviews.text = [NSString stringWithFormat:@"%ld",(long)self.blog.numComments];
                    });
                }
            }
        }];
    }
}


@end
