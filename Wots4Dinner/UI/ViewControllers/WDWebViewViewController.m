//
//  WDWebViewViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDWebViewViewController.h"

@interface WDWebViewViewController ()<UIWebViewDelegate>

@end

@implementation WDWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.scrollView.bounces = NO;
    if(self.fromURL==0){
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"terms" ofType:@"html"]isDirectory:NO]]];
        self.title = @"Terms & Conditions";
    }else if(self.fromURL==1){
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"]isDirectory:NO]]];
        self.title = @"About Us";
    }else if(self.fromURL==2){
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"refund-policy" ofType:@"html"]isDirectory:NO]]];
        self.title = @"Refund Policy";
    }else{
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"seller-policy" ofType:@"html"]isDirectory:NO]]];
        self.title = @"Seller Policy";
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickedBack:(id)sender {
    if(self.isFromFirstLaucher){
        [self.navigationController setNavigationBarHidden:YES];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
