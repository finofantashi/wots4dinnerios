//
//  WDCartViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDCartViewController.h"
#import "WDCartTableViewCell.h"
#import "WDCart.h"
#import "WDSourceConfig.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDDish.h"
#import "WDOrderAPIController.h"
#import "UIColor+Style.h"
#import "WDShippingInfoTableViewController.h"
#import "WDMainViewController.h"
#import "WDSelectPaymentTableViewController.h"
#import <STPopup/STPopup.h>
#import "WDPopupRatingViewController.h"
#import "WDReviewAPIController.h"
#import "NSString+StringAdditions.h"
#import "WDCoupon.h"
#import "WDOrderCompleteViewController.h"
#import "WDDelivery.h"

@interface WDCartViewController () <UITableViewDelegate, UITableViewDataSource, WDCartTableViewCellDelegate, UITextFieldDelegate, WDPopupRatingViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray *listCart;
@property (nonatomic, strong) NSMutableArray *listSeller;
@property (nonatomic, strong) NSString *oldNote;
@property (nonatomic, strong) UIAlertAction *actionSave;
@property (nonatomic, assign) float totalPrice;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;

@end

@implementation WDCartViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  //self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
  self.btnNext.layer.cornerRadius = self.btnNext.frame.size.height/2; // this value vary as per your desire
  self.btnNext.layer.borderWidth = 1.0f;
  self.btnNext.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
  self.btnNext.clipsToBounds = YES;
  
  self.title = @"Your Food Cart";
  if(self.isFromSlideMenu){
    [self.btnBackOrHome setImage:[UIImage imageNamed:@"ic_menu"]];
  }
  self.listSeller = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.listCart = [[NSMutableArray alloc] init];
  [self loadData];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

#pragma mark - Private

- (void)updateTotalPrice {
  float price = 0;
  float fee = 0;
  float couponValue = 0;
  
  NSString *couponType = @"";
  NSString *couponCode = @"";
  self.currencySymbol = @"USD";
  for (WDCart *object in self.listCart) {
    self.currencyCode = object.dish.currencyCode;
    self.currencySymbol = object.dish.currencySymbol;
    price = price+(object.dish.price*object.quantity);
    fee = object.dish.fee;
    if(object.coupon){
      couponType = object.coupon.type;
      couponCode = object.coupon.code;
      couponValue = object.coupon.value;
    }
  }
  self.lbFee.text = [NSString stringWithFormat:@"%@",[[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:fee]];
  self.lbTotal.text = [[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:price];
  self.totalPrice =  price+fee;
  self.lbOrderTotal.text = [[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:self.totalPrice];
  
  if(couponType.length>0){
    self.tfCoupon.text = couponCode;
    self.tfCoupon.enabled = NO;
    [self.btnCoupon setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.btnCoupon setTitleColor:[UIColor colorFromHexString:@"da6a68" withAlpha:1.0f] forState:UIControlStateNormal];
    
    if([couponType isEqualToString:@"percentage"]){
      float coupon = couponValue/100*price;
      self.lbCoupon.text = [NSString stringWithFormat:@"-%@",[[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:coupon]];
      self.totalPrice = (price-coupon)+fee;
      
      self.lbOrderTotal.text = [[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:self.totalPrice];
      
    }else{
      self.lbCoupon.text = [NSString stringWithFormat:@"-%@",[[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:couponValue]];
      self.totalPrice = (price-couponValue)+fee;
      
      self.lbOrderTotal.text = [[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:self.totalPrice];
    }
    
  }else{
    self.tfCoupon.text = @"";
    self.tfCoupon.enabled = YES;
    [self.btnCoupon setTitle:@"Apply" forState:UIControlStateNormal];
    [self.btnCoupon setTitleColor:[UIColor colorFromHexString:@"9DB72C" withAlpha:1.0f] forState:UIControlStateNormal];
    self.lbCoupon.text = [[NSString string] formatCurrency:self.currencyCode symbol:self.currencySymbol price:0];
  }
}

- (void)showPopupRating {
  if(self.listSeller.count>0){
    [STPopupNavigationBar appearance].barTintColor = [UIColor colorWithRed:0.20 green:0.60 blue:0.86 alpha:1.0];
    [STPopupNavigationBar appearance].tintColor = [UIColor whiteColor];
    [STPopupNavigationBar appearance].barStyle = UIBarStyleDefault;
    [STPopupNavigationBar appearance].titleTextAttributes = @{ NSFontAttributeName: [UIFont fontWithName:@"Poppins" size:18],
                                                               NSForegroundColorAttributeName: [UIColor whiteColor] };
    
    [[UIBarButtonItem appearanceWhenContainedIn:[STPopupNavigationBar class], nil] setTitleTextAttributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Poppins" size:17] } forState:UIControlStateNormal];
    WDPopupRatingViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WDPopupRatingViewController"];
    vc.user = [self.listSeller objectAtIndex:0];
    vc.delegate = self;
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:vc];
    popupController.style = STPopupStyleFormSheet;
    [popupController setHidesCloseButton:YES];
    [popupController presentInViewController:self];
  }else{
    WDShippingInfoTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDShippingInfoTableViewController"];
    viewController.strCartTotal = self.lbTotal.text;
    viewController.strCoupon = self.lbCoupon.text;
    viewController.totalPrice = self.totalPrice;
    viewController.listCart = self.listCart;
    viewController.currencyCode = self.currencyCode;
    viewController.currencySymbol = self.currencySymbol;
    viewController.isDeliveryShip = ((WDDelivery*)((WDCart*)[self.listCart objectAtIndex:0]).dish.user.delivery).isDelivery == 1 ? YES:NO;
    [self.navigationController pushViewController:viewController animated:YES];
  }
}

- (void)dismissPopup {
  if(self.listSeller.count>0){
    [self.listSeller removeObjectAtIndex:0];
  }
  [self showPopupRating];
}

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
  
  //Label text
  NSString *aLabelTextString = text;
  
  //Label font
  UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:17];
  
  //Width of the Label
  CGFloat aLabelSizeWidth = self.view.frame.size.width-22;
  CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{
                                                    NSFontAttributeName : aLabelFont
                                                  }
                                                     context:nil].size;
  return labelSize.height;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.listCart.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"WDCartTableViewCell";
  
  WDCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  WDCart *cart = self.listCart[indexPath.row];
  cell.delegate = self;
  cell.cart = cart;
  //get image name and assign
  NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,cart.dish.dishImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
  
  [cell.ivDish sd_setImageWithURL:[NSURL URLWithString:stringURL]
                 placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
  cell.lbNameDish.text = cart.dish.dishName;
  
  if((long)cart.dish.quantity>0){
    cell.lbQuantityLeft.text = [NSString stringWithFormat:@"Qty left: %ld", (long)cart.dish.quantity - cart.quantity];
  }else{
    [cell.lbQuantityLeft setTextColor:[UIColor colorFromHexString:@"f26065" withAlpha:1.0]];
    cell.lbQuantityLeft.text = @"Sold Out";
  }
  
  if(cart.note.length>0){
    cell.lbNote.text = cart.note;
    [cell.lbNote setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:1.0]];
  }else{
    cell.lbNote.text = @"Any food allegies, your taste, your preferred pickup or delivery time within the Sellers set Times.";
    [cell.lbNote setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:0.5]];
    
  }
  
  if((long)cart.dish.quantity-cart.quantity<0){
    [self updateCartAPI:cart quantity:[NSNumber numberWithInteger:cart.dish.quantity]];
  }else{
    cell.lbQuantity.text  = [NSString stringWithFormat:@"%ld", cart.quantity];
  }
  
  cell.lbPriceDish.text = [[NSString string] formatCurrencySymbol:cart.dish.currencyCode symbol:cart.dish.currencySymbol price:cart.dish.price];
  
  cell.lbTotalPrice.text = [[NSString string] formatCurrencySymbol:cart.dish.currencyCode symbol:cart.dish.currencySymbol price:cart.dish.price*[cell.lbQuantity.text integerValue]];
  
  if([cell.lbQuantity.text integerValue]>=1&&[cell.lbQuantity.text integerValue] < cart.dish.quantity){
    [cell enableButtonPlus:YES];
  }else {
    [cell enableButtonPlus:NO];
  }
  
  if([cell.lbQuantity.text integerValue] <= 1){
    [cell enableButtonMinus:NO];
  }else{
    [cell enableButtonMinus:YES];
  }
  
  [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
  
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  WDCart *cart = self.listCart[indexPath.row];
  if(cart.note.length>0){
    return 164+[self sizeOfMultiLineLabel:cart.note];
  }
  return 242;
}

#pragma mark - IBAction
- (IBAction)clickedBack:(id)sender {
  if(self.isFromSlideMenu){
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
      [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
  }else{
    [self.navigationController popViewControllerAnimated:YES];
  }
}

- (IBAction)clickedCoupon:(id)sender {
  if([self.btnCoupon.titleLabel.text isEqualToString:@"Cancel"]){
    [self cancelCouponAPI];
  }else if(self.tfCoupon.text.length>0){
    [self applyCouponAPI:self.tfCoupon.text];
  }
}

- (void)clickedPlus:(WDCartTableViewCell *)cartTableViewCell cart:(WDCart *)cart {
  [self updateCartAPI:cartTableViewCell cart:cart isPlus:1];
}

- (void)clickedMinus:(WDCartTableViewCell *)cartTableViewCell cart:(WDCart *)cart {
  [self updateCartAPI:cartTableViewCell cart:cart isPlus:0];
}

- (void)clickedDelete:(NSInteger)cartID {
  [self deleteCartAPI:cartID];
}

- (IBAction)clickedNext:(id)sender {
  //WDOrderCompleteViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDOrderCompleteViewController"];
  //viewController.orderNumber = @"MA1498618817";
  //[self.navigationController pushViewController:viewController animated:YES];
  [self getListSellerNeedToReview];
}

- (void)clickedEditNote:(WDCart *)cart note:(NSString *)note {
  self.oldNote = note;
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                           message:@"Enter your note"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
  [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
    [textField setPlaceholder:@"Any food allegies, your taste, our preferred pickup or delivery time within the Sellers set Times."];
    textField.delegate = self;
  }];
  
  [self presentViewController:alertController animated:YES completion:nil];
  self.actionSave = [UIAlertAction actionWithTitle:@"Save"
                                             style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
    // access text from text field
    NSString *text = ((UITextField *)[alertController.textFields objectAtIndex:0]).text;
    if(![text isEqualToString:self.oldNote]){
      [self updateCartAPI:cart note:text];
    }
    
  }];
  self.actionSave.enabled = NO;
  
  UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                         style:UIAlertActionStyleDefault
                                                       handler:nil];
  [alertController addAction:actionCancel];
  [alertController addAction:self.actionSave];
  
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  self.actionSave.enabled = YES;
  return YES;
}

#pragma mark - API

- (void)loadData {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDOrderAPIController sharedInstance] getCarts:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self.listCart removeAllObjects];
          for (NSDictionary *dict in [responseObject valueForKey:@"data"]) {
            WDCart *cart = [[WDCart alloc] init];
            [cart setCartObject:dict];
            [self.listCart addObject:cart];
          }
          
          if(self.listCart.count>0){
            [self.btnNext setEnabled:YES];
            [self.btnNext setTitleColor:[UIColor colorFromHexString:@"f77e51" withAlpha:1.0f] forState:UIControlStateNormal];
            self.btnNext.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
            [self.btnCoupon setEnabled:YES];
          }else{
            [self.btnNext setEnabled:NO];
            [self.btnNext setTitleColor:[UIColor colorFromHexString:@"cccccc" withAlpha:1.0f] forState:UIControlStateNormal];
            self.btnNext.layer.borderColor = [UIColor colorFromHexString:@"cccccc" withAlpha:1.0f].CGColor;
            [self.btnCoupon setEnabled:NO];
          }
          [self updateTotalPrice];
          [self.tableView reloadData];
        }
      }
    }];
  }
}

- (void) updateCartAPI:(WDCartTableViewCell *)cartTableViewCell cart:(WDCart*)cart isPlus:(NSInteger)isPlus {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    NSNumber *quantity;
    if(isPlus==1){
      quantity = [NSNumber numberWithInteger:cart.quantity+1];
    }else{
      quantity = [NSNumber numberWithInteger:cart.quantity-1];
    }
    
    //Call Api
    [[WDOrderAPIController sharedInstance] updateCart:[NSNumber numberWithInteger:cart.dishID] cartID:[NSNumber numberWithInteger:cart.cartID] quantity:quantity completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if(responseObject) {
          NSDictionary *dataCart = [responseObject valueForKey:@"data"];
          WDCart *cartData = [[WDCart alloc] init];
          [cartData setCartObject:dataCart];
          
          if(self.listCart.count>0){
            for (int i = 0; i <self.listCart.count; i++) {
              if(((WDCart*)[self.listCart objectAtIndex:i]).cartID==cartData.cartID){
                [((WDCart*)[self.listCart objectAtIndex:i]) setQuantity:cartData.quantity];
                [((WDCart*)[self.listCart objectAtIndex:i]).dish setQuantity:cartData.dish.quantity];
              }
            }
          }else{
            LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"No dishes found." preferredStyle:LEAlertControllerStyleAlert];
            LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
              // handle default button action
            }];
            [alertController addAction:defaultAction];
            [self presentAlertController:alertController animated:YES completion:nil];
          }
          
          if(isPlus==1){
            [cartTableViewCell updateTotalPricePlus];
          }else{
            [cartTableViewCell updateTotalPriceMinus];
          }
          [self updateTotalPrice];
        }
      }
    }];
  }
}

- (void) updateCartAPI:(WDCart*)cart quantity:(NSNumber*)quantity{
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    
    //Call Api
    [[WDOrderAPIController sharedInstance] updateCart:[NSNumber numberWithInteger:cart.dishID] cartID:[NSNumber numberWithInteger:cart.cartID] quantity:quantity completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if(responseObject) {
          [self loadData];
        }
      }
    }];
  }
}

- (void) updateCartAPI:(WDCart*)cart note:(NSString*)note{
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    
    //Call Api
    [[WDOrderAPIController sharedInstance] updateCart:[NSNumber numberWithInteger:cart.dishID] cartID:[NSNumber numberWithInteger:cart.cartID] note:note completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if(responseObject) {
          [self loadData];
        }
      }
    }];
  }
}

- (void) deleteCartAPI:(NSInteger)cartID {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDOrderAPIController sharedInstance] deleteCart:[NSNumber numberWithInteger:cartID] completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        [self loadData];
      }
    }];
  }
}

- (void)getListSellerNeedToReview {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDReviewAPIController sharedInstance] getListNeedToReview:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self.listSeller removeAllObjects];
          NSDictionary *dataSeller = [responseObject valueForKey:@"data"];
          for (NSDictionary *data in dataSeller) {
            WDUserTemp *user = [[WDUserTemp alloc] init];
            [user setUserObject:data];
            [self.listSeller addObject:user];
          };
          [self showPopupRating];
        }
      }
    }];
  }
}

- (void) applyCouponAPI:(NSString*)code {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDOrderAPIController sharedInstance] applyCoupon:code completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        [self loadData];
      }
    }];
  }
}

- (void) cancelCouponAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDOrderAPIController sharedInstance] cancelCoupon:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        [self loadData];
      }
    }];
  }
}

@end
