//
//  WDResetPasswordViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDResetPasswordViewController.h"
#import "WDSetNameViewController.h"
#import "LEAlertController.h"
#import "NSString+IsValidEmail.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "WDUserAPIController.h"
#import "WDUser.h"
#import "WDSignInViewController.h"
#import "UIColor+Style.h"

@interface WDResetPasswordViewController ()<UITextFieldDelegate>

@end

@implementation WDResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupDismissKeyboard];
    if(self.isFromSignUp){
        [self.btnResetPassword setTitle:@"Set password" forState:UIControlStateNormal];
        self.tfNewPassword.placeholder = @"Password";
    }
    self.btnResetPassword.layer.cornerRadius = self.btnResetPassword.frame.size.height/2; // this value vary as per your desire
    self.btnResetPassword.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setupDismissKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [self.tfNewPassword resignFirstResponder];
    [self.tfConfirmPassword resignFirstResponder];
    
}

- (void)enableButtonPassword {
    if(self.tfNewPassword.text.length!=0&&self.tfConfirmPassword.text.length!=0){
        self.btnResetPassword.enabled = YES;
        self.btnResetPassword.alpha = 1.0f;
    }else{
        self.btnResetPassword.enabled = NO;
        self.btnResetPassword.alpha = 0.5f;
    }
}

- (void)toggleTextFieldSecureEntry: (UITextField*) textField btnShowHide:(UIButton*) btnShowHide{
    BOOL isFirstResponder = textField.isFirstResponder; //store whether textfield is firstResponder
    
    if (isFirstResponder) [textField resignFirstResponder]; //resign first responder if needed, so that setting the attribute to YES works
    textField.secureTextEntry = !textField.secureTextEntry; //change the secureText attribute to opposite
    if (isFirstResponder) [textField becomeFirstResponder]; //give the field focus again, if it was first responder initially
    if(textField.secureTextEntry){
        [btnShowHide setImage:[UIImage imageNamed:@"ic_show"] forState:UIControlStateNormal];
    }else{
        [btnShowHide setImage:[UIImage imageNamed:@"ic_hiden"] forState:UIControlStateNormal];
        
    }
}

- (BOOL)invalidate {
    if([self.tfNewPassword.text length] < 6) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Password must include at least six characters. Please try again." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfNewPassword becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    } else if(![self.tfNewPassword.text isEqualToString:self.tfConfirmPassword.text]) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Passwords don’t match. Please try again." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            [self.tfConfirmPassword becomeFirstResponder];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (IBAction)textFieldDidChaned:(id)sender {
    [self enableButtonPassword];
}

#pragma mark IBAction

- (IBAction)clickedShowHideNewPassword:(id)sender {
    [self toggleTextFieldSecureEntry:self.tfNewPassword btnShowHide:sender];
}

- (IBAction)clickedShowHideConfirmPassword:(id)sender {
    [self toggleTextFieldSecureEntry:self.tfConfirmPassword btnShowHide:sender];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)touchDown:(id)sender {
    [self.btnResetPassword setBackgroundColor:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0]];
}

- (IBAction)clickedResetPassword:(id)sender {
    [self.btnResetPassword setBackgroundColor:[UIColor colorFromHexString:@"e7663f" withAlpha:1.0]];
    if([self invalidate]){
        if(self.isFromSignUp){
            [[WDUser sharedInstance] setPassword:self.tfNewPassword.text];
            WDSetNameViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDSetNameViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }else{
            [self resetPasswordAPI];
        }
    }
}

#pragma mark API

- (void) resetPasswordAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call API
        [[WDUserAPIController sharedInstance] resetPassword:self.code userName:self.userName password:self.tfNewPassword.text completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                WDSignInViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDSignInViewController"];
                [self.navigationController pushViewController:viewController animated:YES];
            }
        }];
    }
}

@end
