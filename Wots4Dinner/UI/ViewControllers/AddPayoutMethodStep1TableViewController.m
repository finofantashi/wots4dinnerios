//
//  AddPayoutMethodStep1TableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/2/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "AddPayoutMethodStep1TableViewController.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDCountry.h"
#import "AddPayoutMethodStep2TableViewController.h"

@interface AddPayoutMethodStep1TableViewController ()

@end

@implementation AddPayoutMethodStep1TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnNext.layer.cornerRadius = self.btnNext.frame.size.height/2; // this value vary as per your desire
    self.btnNext.layer.borderWidth = 1.0f;
    self.btnNext.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
    self.btnNext.clipsToBounds = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [self getListCountry];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissKeyboard {
    [self.tfAddress resignFirstResponder];
    [self.tfCity resignFirstResponder];
    [self.tfZip resignFirstResponder];
    [self.tfState resignFirstResponder];
    
}

- (void)setData {
    if(self.payoutMethod){
        self.pickerCountry.text = self.payoutMethod.countryName;
        self.tfAddress.text = self.payoutMethod.address;
        self.tfCity.text = self.payoutMethod.city;
        self.tfState.text = self.payoutMethod.state;
        self.tfZip.text = self.payoutMethod.zip;
    }

}

- (void)setupPickerCountry {
    self.picker = [[DownPicker alloc] initWithTextField:self.pickerCountry withData:self.countriesData];
    //[self.pickerCountry initWithData:self.countriesData];
    [self.pickerCountry setFont:[UIFont fontWithName:@"Poppins-Medium" size:14]];
    [self.pickerCountry setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:1.0]];
    [self.picker setPlaceholder:@"Tap to choose your country"];
}

- (NSInteger)getcountryID:(NSString*)countryName {
    for (WDCountry *object in self.countries) {
        if([object.countryName isEqualToString:countryName]){
            return object.countryID;
        }
    }
    return 0;
}

- (BOOL)validate {
    if([[self.pickerCountry.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Country is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    if([[self.tfAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Address is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    if([[self.tfCity.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"City is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    if([[self.tfZip.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Zip code/Postal code is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (IBAction)clickedNext:(id)sender {
    if([self validate]){
        if(!self.payoutMethod){
            self.payoutMethod =  [[WDPayoutMethod alloc] init];
        }
        self.payoutMethod.countryID = [self getcountryID:self.pickerCountry.text];
        self.payoutMethod.countryName = self.pickerCountry.text;
        self.payoutMethod.address = [self.tfAddress.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
        self.payoutMethod.city = [self.tfCity.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
        self.payoutMethod.state = [self.tfState.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
        self.payoutMethod.zip = [self.tfZip.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
        AddPayoutMethodStep2TableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddPayoutMethodStep2TableViewController"];
        viewController.payoutMethod = self.payoutMethod;
        viewController.isAddPayout = self.isAddPayout;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}
- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - API

- (void) getListCountry {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getCountryCodes:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    self.countries = [[NSMutableArray alloc] init];
                    self.countriesData = [[NSMutableArray alloc] init];
                    
                    NSDictionary *dataCategories = [responseObject valueForKey:@"data"];
                    for (NSDictionary *object in dataCategories) {
                        WDCountry *country = [[WDCountry alloc] init];
                        [country setCountryObject:object];
                        [self.countries addObject:country];
                        [self.countriesData addObject:country.countryName];
                    };
                    [self setupPickerCountry];
                    [self setData];
                    [self.tableView reloadData];
                }
                
            }
        }];
    }
}
@end
