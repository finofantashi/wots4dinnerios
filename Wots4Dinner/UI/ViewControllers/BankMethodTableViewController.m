//
//  BankMethodTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "BankMethodTableViewController.h"
#import "UIColor+Style.h"
#import "LEAlertController.h"
#import "WDBecomeSellerStepOneTableViewController.h"
#import "NSString+IsValidEmail.h"
#import "WDPayoutMethodTableViewController.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDOrderAPIController.h"

@interface BankMethodTableViewController ()

@end

@implementation BankMethodTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.btnFinish.layer.cornerRadius = self.btnFinish.frame.size.height/2; // this value vary as per your desire
    self.btnFinish.layer.borderWidth = 1.0f;
    self.btnFinish.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
    self.btnFinish.clipsToBounds = YES;
    [self setData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setData {
    if(!self.payoutMethod.bankSwift&&![self.payoutMethod.bankSwift isEqualToString:@""]){
        self.tfSwift.text = self.payoutMethod.bankSwift;
    }
    if(!self.payoutMethod.bankName&&![self.payoutMethod.bankName isEqualToString:@""]){
        self.tfBankName.text = self.payoutMethod.bankName;
    }
    if(!self.payoutMethod.bankAddress&&![self.payoutMethod.bankAddress isEqualToString:@""]){
        self.tfBankAddress.text = self.payoutMethod.bankAddress;
    }
    if(!self.payoutMethod.bankAccountName&&![self.payoutMethod.bankAccountName isEqualToString:@""]){
        self.tfAccountName.text = self.payoutMethod.bankAccountName;
    }
    if(!self.payoutMethod.account&&![self.payoutMethod.account isEqualToString:@""]&&![self.payoutMethod.account isValidEmail]){
        self.tfAccountNumber.text = self.payoutMethod.account;
    }
}

- (BOOL)validate {
//    if([[self.tfSwift.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
//        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"SWIFT/BIC is required." preferredStyle:LEAlertControllerStyleAlert];
//        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
//            // handle default button action
//        }];
//        [alertController addAction:defaultAction];
//        [self presentAlertController:alertController animated:YES completion:nil];
//        return NO;
//    }
    
    if([[self.tfBankName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Bank name is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
//    if([[self.tfBankAddress.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
//        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Bank address is required." preferredStyle:LEAlertControllerStyleAlert];
//        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
//            // handle default button action
//        }];
//        [alertController addAction:defaultAction];
//        [self presentAlertController:alertController animated:YES completion:nil];
//        return NO;
//    }
    
    if([[self.tfAccountName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Account number is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    
    if([[self.tfAccountName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Account name is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0||indexPath.row==1||indexPath.row==4||indexPath.row==5)
        return 0; //set the hidden cell's height to 0
    
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

#pragma mark - API

- (void) addPayout:(WDPayoutMethod *)payout {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] savePayoutMethod:payout completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                for (UIViewController *aViewController in allViewControllers) {
                    if ([aViewController isKindOfClass:[WDPayoutMethodTableViewController class]]) {
                        [self.navigationController popToViewController:aViewController animated:YES];
                        break;
                    }
                }
            }
        }];
    }
}

#pragma mark - IBAction

- (IBAction)clickedFinish:(id)sender {
    if([self validate]){
        self.payoutMethod.bankSwift = [self.tfSwift.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
        self.payoutMethod.bankName = [self.tfBankName.text stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceCharacterSet]];
        self.payoutMethod.bankAddress = [self.tfBankAddress.text stringByTrimmingCharactersInSet:
                                         [NSCharacterSet whitespaceCharacterSet]];
        self.payoutMethod.bankAccountName = [self.tfAccountName.text stringByTrimmingCharactersInSet:
                                             [NSCharacterSet whitespaceCharacterSet]];
        self.payoutMethod.account = [self.tfAccountNumber.text stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];

        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        if(self.isAddPayout){
            [self addPayout:self.payoutMethod];
        }else{
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:self.payoutMethod forKey:@"PayoutMethod"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"addPayoutMethod" object:nil userInfo:dictionary];
            for (UIViewController *aViewController in allViewControllers) {
                if ([aViewController isKindOfClass:[WDBecomeSellerStepOneTableViewController class]]) {
                    [self.navigationController popToViewController:aViewController animated:YES];
                    break;
                }
            }
        }
    }
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
