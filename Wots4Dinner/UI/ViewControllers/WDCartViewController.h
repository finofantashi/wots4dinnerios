//
//  WDCartViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDCartViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbTotal;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnBackOrHome;
@property (weak, nonatomic) IBOutlet UILabel *lbCoupon;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbFee;
@property (weak, nonatomic) IBOutlet UITextField *tfCoupon;
@property (weak, nonatomic) IBOutlet UIButton *btnCoupon;

@property (nonatomic, assign) BOOL isFromSlideMenu;

- (IBAction)clickedNext:(id)sender;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedCoupon:(id)sender;


@end
