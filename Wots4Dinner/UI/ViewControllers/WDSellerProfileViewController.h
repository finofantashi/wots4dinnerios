//
//  WDUserProfile.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCSStarRatingView/HCSStarRatingView.h>
#import "WDUserTemp.h"

@interface WDSellerProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbCategory;

@property (weak, nonatomic) IBOutlet UIButton *btnSendMessage;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbSinceDay;
@property (weak, nonatomic) IBOutlet UILabel *lbLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;
@property (strong, nonatomic) WDUserTemp *user;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextView *tvAbout;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewRateThisSeller;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewRateSeller;
@property (weak, nonatomic) IBOutlet UITextView *tfComment;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIView *viewRating;
@property (weak, nonatomic) IBOutlet UIView *viewRatingShowHiden;
@property (weak, nonatomic) IBOutlet UIStackView *viewCertified;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCertified;

- (IBAction)clickedSendMessage:(id)sender;
- (IBAction)clickedExplore:(id)sender;
- (IBAction)clickedAllReviews:(id)sender;
- (IBAction)clickedSend:(id)sender;
- (IBAction)clickedWishList:(id)sender;
- (IBAction)clickedFavoriteSeller:(id)sender;

@end
