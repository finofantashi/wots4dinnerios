//
//  WDFavoriteSellerTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDFavoriteSellerTableViewCell.h"
#import "WDSellerProfileViewController.h"
#import "WDUserProfileViewController.h"

@implementation WDFavoriteSellerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedAvatar:)];
    singleTap.numberOfTapsRequired = 1;
    self.ivAvatar.userInteractionEnabled = YES;
    [self.ivAvatar addGestureRecognizer:singleTap];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickedAvatar:(id)sender {
    if([self.favoriteSellerDelegate respondsToSelector:@selector(clickedAvatar:)]) {
        [self.favoriteSellerDelegate clickedAvatar:self.favoriteSeller.userSeller];
    }
}

@end
