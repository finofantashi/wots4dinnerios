//
//  WDOrderHistoryViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/17/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIResizableTableView.h"

@interface WDOrderHistoryViewController : UIViewController
@property (weak, nonatomic) IBOutlet MIResizableTableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *tfSearchView;
- (IBAction)clickedSearch:(id)sender;
@end
