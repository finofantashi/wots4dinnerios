//
//  Constants.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "Constants.h"

@implementation Constants

int const kStatusAdd = 0;
int const kStatusUpdate = 1;
int const kStatusDelete = 2;
int const kStatusTranscribed = 3;
int const kStatusDeleteNew = 4;


BOOL const IsFirstLaucher = YES;
NSString *const S3BucketName = @"leadcapture-cdn/leads";

@end
