//
//  WDExploreDishViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDExploreDishViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintTableView;
@property (nonatomic, assign) NSInteger sellerID;
@property (nonatomic, assign) BOOL isToday;
@property (nonatomic, assign) BOOL isFromDishDetail;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItemExplore;
- (IBAction)clickedBack:(id)sender;

@end
