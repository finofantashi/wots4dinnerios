//
//  WDMyKitchenTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/14/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDMyKitchenTableViewController : UITableViewController

- (IBAction)clickedBasic:(id)sender;
- (IBAction)clickedHomeDelivery:(id)sender;

@end
