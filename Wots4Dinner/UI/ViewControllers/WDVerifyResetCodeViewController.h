//
//  WDVerifyResetCodeViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDVerifyResetCodeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *tfResetCode;
@property (weak, nonatomic) IBOutlet UILabel *lbResetCode;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnResendCode;

@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *userName;

@property (nonatomic, strong) NSString *prefixPhone;
@property (nonatomic, strong) NSString *countryCode;

@property (assign, nonatomic) BOOL isFromSignUp;
@property (assign, nonatomic) BOOL isChangePhone;
@property (assign, nonatomic) BOOL isVerify;
- (IBAction)touchDown:(id)sender;

- (IBAction)clickdedVerify:(id)sender;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedResend:(id)sender;

@end
