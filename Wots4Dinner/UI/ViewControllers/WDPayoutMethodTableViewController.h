//
//  WDPayoutMethodTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDPayoutMethodTableViewController : UITableViewController
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedAdd:(id)sender;
@end
