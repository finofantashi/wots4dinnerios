//
//  WDMessageTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/5/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "GroupAvatarView.h"

@interface WDMessageTableViewCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet GroupAvatarView *avatarGroup;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbAddress;
@property (weak, nonatomic) IBOutlet UILabel *lbDateTime;
@property (nonatomic, assign) BOOL isGroup;
@property (nonatomic, assign) NSInteger removeID;

@end
