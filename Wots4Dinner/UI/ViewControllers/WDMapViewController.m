//
//  WDMapViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMapViewController.h"

@interface WDMapViewController ()

@end

@implementation WDMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Create a GMSCameraPosition that tells the map to display the
    // coordinate -33.86,151.20 at zoom level 6.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.lat
                                                            longitude:self.lng
                                                                 zoom:14];
    
    GMSMapView *mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.myLocationEnabled = YES;
    mapView.settings.compassButton = YES;
    mapView.settings.myLocationButton = YES;

    self.view = mapView;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(self.lat, self.lng);
    marker.title = self.kitchen;
    marker.snippet = self.location;
    marker.map = mapView;
    self.title = self.kitchen;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
