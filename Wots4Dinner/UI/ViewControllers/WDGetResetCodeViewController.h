//
//  WDGetResetCodeViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDGetResetCodeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbAreaCode;
@property (weak, nonatomic) IBOutlet UITextField *tfPhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnGetResetCode;
@property (assign, nonatomic) BOOL isFromSignUp;
@property (assign, nonatomic) BOOL isChangePhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIView *viewSearch;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;

- (IBAction)touchDown:(id)sender;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedCountry:(id)sender;
- (IBAction)clickedGetResetCode:(id)sender;

@end
