//
//  WDContentMessageViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDMessageData.h"

@interface WDContentMessageViewController : JSQMessagesViewController <JSQMessagesComposerTextViewPasteDelegate>
@property (strong, nonatomic) WDMessageData *messageData;
@property (nonatomic, strong) NSMutableArray *listReceiver;
@property (nonatomic, assign) NSInteger groupID;
@property (nonatomic, assign) NSInteger recordID;
@property (nonatomic, assign) BOOL isNewMessage;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnGroup;

@end
