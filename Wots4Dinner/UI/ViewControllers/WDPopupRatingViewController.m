//
//  WDPopupRatingViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDPopupRatingViewController.h"
#import "WDUserTemp.h"
#import "WDSeller.h"
#import "UIImage+ResizeMagick.h"
#import "UIImageView+AFNetworking.h"
#import "WDSourceConfig.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDReviewAPIController.h"
#import <STPopup/STPopup.h>
@import BonMot;

@interface WDPopupRatingViewController ()

@end

@implementation WDPopupRatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLayout];
    [self setData:self.user];
    self.tvDish.textContainerInset = UIEdgeInsetsMake(14, 14, 14, 14);
    
    [self.viewRateThisSeller addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    self.contentSizeInPopup = CGSizeMake(self.view.frame.size.width-40, self.view.frame.size.height-80);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Private

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if(textView==self.tfComment){
        NSUInteger oldLength = [textView.text length];
        NSUInteger replacementLength = [text length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [text rangeOfString: @"\n"].location != NSNotFound;
        return newLength <= 350 || returnKey;
    }
    return YES;
}

- (void) setUpLayout {
    self.ivAvatar.layer.cornerRadius = 120 / 2;
    self.ivAvatar.layer.masksToBounds = YES;
    self.ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
    self.ivAvatar.layer.borderWidth = 2.0f;
    [self.ivAvatar setNeedsDisplay];
    self.title = @"How was your last order?";
}

- (void)dismissKeyboard {
    [self.tfComment resignFirstResponder];
}

- (CGFloat)sizeOfMultiLineLabel: (NSString *)text{
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = [UIFont fontWithName:@"Poppins-Light" size:14];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.view.frame.size.width - 40;
    CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{
                                                                 NSFontAttributeName : aLabelFont
                                                                 }
                                                       context:nil].size;
    return labelSize.height;
}

- (void)setData: (WDUserTemp*)user{
    if(![user.avatarURL isEqualToString:@""]){
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain, user.avatarURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:stringURL]];
        [self.ivAvatar setImageWithURLRequest:urlRequest placeholderImage:[UIImage imageNamed:@"empty_avatar"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            self.ivAvatar.image = image;
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            self.ivAvatar.image = [UIImage imageNamed:@"empty_avatar"];
        }];
    }
    if(user.firstName&&user.lastName){
        self.lbName.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    }
    
    BONChain *space = BONChain.new.string(@" ");
    BONChain *chain = BONChain.new;
    [chain appendLink:BONChain.new.image([UIImage imageNamed:@"ic_location"]).baselineOffset(-4.0)];
    [chain appendLink:BONChain.new.string(user.living) separatorTextable: space];
    NSAttributedString *string = chain.attributedString;
    self.lbLocation.attributedText = string;
    //self.lbLocation.text = user.living;
    for (NSString *object in self.user.dishes) {
        self.tvDish.text = [self.tvDish.text stringByAppendingString:[NSString stringWithFormat:@"%@\n",object]];
    }
    self.constraintHeight.constant = 450 + [self sizeOfMultiLineLabel:self.tvDish.text];
    [self.viewRating setHidden:NO];
    [self.viewRatingShowHiden setHidden:NO];
    self.lbCategory.text = user.seller.category;
}

#pragma mark - IBAction
- (IBAction)clickedSend:(id)sender {
    [self postCommentAPI];
}

- (IBAction)didChangeValue:(id)sender {
    if(self.viewRateThisSeller.value!=0){
        self.btnSend.enabled = YES;
        self.btnSend.alpha = 1.0f;
    }else{
        self.btnSend.enabled = NO;
        self.btnSend.alpha = 0.5f;
    }
}

#pragma mark - API

- (void) postCommentAPI {
    [self dismissKeyboard];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDReviewAPIController sharedInstance] sellerReview:[NSNumber numberWithInteger:self.user.userID] content:[self.tfComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] rating:self.viewRateThisSeller.value completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            } else{
                if([[responseObject valueForKey:@"success"] isEqualToNumber:[NSNumber numberWithBool:YES]]){
                    [self.popupController dismissWithCompletion:^{
                        if([self.delegate respondsToSelector:@selector(dismissPopup)]) {
                            [self.delegate dismissPopup];
                        }
                    }];
                }else{
                    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:[responseObject valueForKey:@"message"] preferredStyle:LEAlertControllerStyleAlert];
                    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                        // handle default button action
                    }];
                    [alertController addAction:defaultAction];
                    [self presentAlertController:alertController animated:YES completion:nil];
                }
            }
        }];
    }
}

@end
