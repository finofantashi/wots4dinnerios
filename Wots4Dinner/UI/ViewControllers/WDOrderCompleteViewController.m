//
//  WDOrderCompleteViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/12/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDOrderCompleteViewController.h"
#import "WDOrderCompleteTableViewCell.h"
#import "WDCart.h"
#import "WDDish.h"
#import "WDMainViewController.h"
#import "NSString+StringAdditions.h"
#import "WDCoupon.h"
#import "WDOrderHistoryItemTableViewCell.h"
#import "WDMapViewController.h"
#import "WDOrderHistoryKitchen.h"
#import "WDOrderHistoryItem.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"

@interface WDOrderCompleteViewController ()<WDOrderHistoryItemTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *listKitchen;
@property (nonatomic, strong) NSMutableArray *kitchenIndex;

@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;
@property (nonatomic, strong) NSString *couponCode;
@property (nonatomic, strong) NSString *couponType;

@end

@implementation WDOrderCompleteViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  NSString *stringDate = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                        dateStyle:NSDateFormatterMediumStyle
                                                        timeStyle:NSDateFormatterNoStyle];
  self.lbDate.text = [NSString stringWithFormat:@"Date: %@",stringDate];
  self.lbOrderNumber.text = [NSString stringWithFormat:@"Order Number: %@",self.orderNumber];
  self.title = @"Order Complete";
  [self.tableView registerNib:[WDOrderHistoryItemTableViewCell cellNib] forCellReuseIdentifier:[WDOrderHistoryItemTableViewCell cellIdentifier]];
  self.listKitchen = [[NSMutableArray alloc] init];
  self.kitchenIndex = [[NSMutableArray alloc] init];
  
  [self loadDatawithOrderNum:self.orderNumber];
  
}

- (void)clickedMap:(double)lat lng:(double)lng kitchenName:(NSString *)kitchenName location:(NSString *)location {
  if(lat!=0&&lng!=0){
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    WDMapViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMapViewController"];
    viewController.kitchen = kitchenName;
    viewController.location = location;
    viewController.lat = lat;
    viewController.lng = lng;
    [self.navigationController pushViewController:viewController animated:YES];
  }
}

#pragma mark - Table view data source

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
  //Label text
  NSString *aLabelTextString = text;
  
  //Label font
  UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:14];
  
  //Width of the Label
  CGFloat aLabelSizeWidth = self.view.frame.size.width-42;
  CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{
                                                    NSFontAttributeName : aLabelFont
                                                  }
                                                     context:nil].size;
  return labelSize.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  if(self.listKitchen.count>0){
    long count = self.listKitchen.count;
    [self.kitchenIndex removeAllObjects];
    [self.kitchenIndex addObject:@0];
    for (int i = 0; i < self.listKitchen.count; i++) {
      WDOrderHistoryKitchen *historyKichen = [self.listKitchen objectAtIndex:i];
      count = count + historyKichen.listOrderItem.count;
      if(i != self.listKitchen.count-1){
        [self.kitchenIndex addObject:[NSNumber numberWithInteger:[(NSNumber*)[self.kitchenIndex lastObject] integerValue] + historyKichen.listOrderItem.count+1]];
      }
    }
    return count;
  }
  return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  WDOrderHistoryItemTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[WDOrderHistoryItemTableViewCell cellIdentifier] forIndexPath:indexPath];
  cell.orderHistoryItemDelegate = self;
  
  if([self.kitchenIndex containsObject:[NSNumber numberWithInteger:indexPath.row]]){
    NSInteger currentIndexChicken = [self.kitchenIndex indexOfObject:[NSNumber numberWithInteger:indexPath.row]];
    WDOrderHistoryKitchen *orderHistoryKitchen = [self.listKitchen objectAtIndex:currentIndexChicken];
    if(indexPath.row!=0){
      [cell configureWithKitchen:orderHistoryKitchen.kitchenName location:orderHistoryKitchen.location delivery:orderHistoryKitchen.delivery lng:orderHistoryKitchen.lng lat:orderHistoryKitchen.lat isMargin:YES];
    }else{
      [cell configureWithKitchen:orderHistoryKitchen.kitchenName location:orderHistoryKitchen.location delivery:orderHistoryKitchen.delivery lng:orderHistoryKitchen.lng lat:orderHistoryKitchen.lat isMargin:NO];
    }
  }else{
    int index=0;
    for (int i = 0; i < self.kitchenIndex.count; i++) {
      if([[self.kitchenIndex objectAtIndex:i] integerValue] < indexPath.row){
        index = i;
      }else{
        break;
      }
    }
    
    WDOrderHistoryItem  *item = [((WDOrderHistoryKitchen*)[self.listKitchen objectAtIndex:index]).listOrderItem objectAtIndex:indexPath.row - ([[self.kitchenIndex objectAtIndex:index] integerValue]+1)];
    self.currencyCode = item.currencyCode;
    self.currencySymbol = item.currencySymbol;
    
    [cell configureWithItem:item.dishName
                   quantity:item.quantity
                description:item.note
               currencyCode:self.currencyCode
             currencySymbol:self.currencySymbol
                      price:item.price
                   delivery:item.delivery
                 pickupFrom:item.pickupTimeFrom
                   pickupTo:item.pickupTimeTo
                homeService:item.specialOffer
                     status:item.status];
  }
  
  return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if([self.kitchenIndex containsObject:[NSNumber numberWithInteger:indexPath.row]]){
    int index=0;
    for (int i = 0; i < self.kitchenIndex.count; i++) {
      if([[self.kitchenIndex objectAtIndex:i] integerValue] < indexPath.row){
        index = i;
      }else{
        break;
      }
    }
    WDOrderHistoryKitchen *orderHistoryKitchen = [self.listKitchen objectAtIndex:index];
    if(orderHistoryKitchen.delivery==1){
      return 44;
    }
    return 68;
  }
  
  //if(indexPath.row == self.totalRow - 1){
  //    return 88;
  //}
  int index=0;
  for (int i = 0; i < self.kitchenIndex.count; i++) {
    if([[self.kitchenIndex objectAtIndex:i] integerValue] < indexPath.row){
      index = i;
    }else{
      break;
    }
  }
  
  WDOrderHistoryItem  *item = [((WDOrderHistoryKitchen*)[self.listKitchen objectAtIndex:index]).listOrderItem objectAtIndex:indexPath.row - ([[self.kitchenIndex objectAtIndex:index] integerValue]+1)];
  float size = 0;
  if(item.delivery==1){
    size = 74;
    if(item.specialOffer.length>0){
      size = size + [self sizeOfMultiLineLabel:[NSString stringWithFormat:@"Delivery service:%@",item.specialOffer]];
    }
  }else{
    size = 84;
  }
  
  if(item.note.length>0){
    float sizeHeight = size + [self sizeOfMultiLineLabel:[NSString stringWithFormat:@"Your notes: %@",item.note]];
    return sizeHeight;
  }
  return size;
}

- (void)updateTotalPrice {
  float totalPrice = 0;
  NSString *currencyCode = @"USD";
  NSString *currencySymbol;
  NSString *couponType = @"";
  NSString *couponCode = @"";
  float couponValue = 0;
  float fee = 0;
  float feeDelivery = 0;
  
  for (WDOrderHistoryKitchen *kitchen in self.listKitchen) {
    for (WDOrderHistoryItem *object in kitchen.listOrderItem) {
      currencyCode = object.currencyCode;
      currencySymbol = object.currencySymbol;
      couponType = object.type;
      couponCode = object.code;
      couponValue = object.value;
      fee = object.fee;
      feeDelivery = object.deliveryFee;
      totalPrice = totalPrice + object.totalPrice;
    }
  }
  
  self.lbFee.text = [NSString stringWithFormat:@"%@",[[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:fee]];
  self.lbDeliveryFee.text = [NSString stringWithFormat:@"%@",[[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:feeDelivery]];
  
  self.lbTotal.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:totalPrice];
  self.lbOrderTotal.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:totalPrice+feeDelivery];
  if(couponType.length>0){
    if([couponType isEqualToString:@"percentage"]){
      float coupon = couponValue/100*totalPrice;
      self.lbCoupon.text = [NSString stringWithFormat:@"-%@",[[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:coupon]];
      self.lbOrderTotal.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:(totalPrice-coupon)+fee+feeDelivery];
      
    }else{
      self.lbCoupon.text = [NSString stringWithFormat:@"-%@",[[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:couponValue]];
      self.lbOrderTotal.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:(totalPrice-couponValue)+fee+feeDelivery];
    }
  }else{
    self.lbCoupon.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:0];
  }
  
}

- (void)loadDatawithOrderNum:(NSString*)orderNum {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getOrderCompleteItem:orderNum completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self.listKitchen removeAllObjects];
          [self.kitchenIndex removeAllObjects];
          
          NSArray *kitchenNames = [[responseObject valueForKey:@"data"] valueForKeyPath:@"@distinctUnionOfObjects.kitchen_name"];
          for (NSString *kitchenName in kitchenNames) {
            WDOrderHistoryKitchen *result = [[WDOrderHistoryKitchen alloc] init];
            result.listOrderItem  = [[NSMutableArray alloc] init];
            result.kitchenName = kitchenName;
            NSArray *groupKitchens = [[responseObject valueForKey:@"data"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(kitchen_name == %@)", kitchenName]];
            for (int i = 0; i < groupKitchens.count; i++) {
              WDOrderHistoryItem *orderItem = [[WDOrderHistoryItem alloc] init];
              [orderItem setOrderHistoryItemObject:[groupKitchens objectAtIndex:i]];
              result.lat = orderItem.lat;
              result.lng = orderItem.lng;
              result.location = orderItem.location;
              result.delivery = orderItem.delivery;
              [result.listOrderItem addObject:orderItem];
            }
            [self.listKitchen addObject:result];
          }
          dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self updateTotalPrice];
          });
        }
      }
    }];
  }
}

- (IBAction)clickedBack:(id)sender {
  [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
  [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
    [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
  }];
}

@end
