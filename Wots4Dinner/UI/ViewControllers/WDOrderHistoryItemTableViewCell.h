//
//  WDOrderHistoryTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/17/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol WDOrderHistoryItemTableViewCellDelegate <NSObject>

- (void)clickedMap:(double)lat
               lng:(double)lng
       kitchenName:(NSString*)kitchenName
          location:(NSString*)location;

@end

@interface WDOrderHistoryItemTableViewCell : UITableViewCell
@property (assign, nonatomic) id <WDOrderHistoryItemTableViewCellDelegate> orderHistoryItemDelegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topKitchenConstraint;

+ (UINib *)cellNib;
+ (NSString *)cellIdentifier;

- (void)configureWithItem:(NSString *)dishName
                 quantity:(NSInteger)quantity
              description:(NSString *)description
             currencyCode:(NSString *)currencyCode
           currencySymbol:(NSString *)currencySymbol
                    price:(float)price
                 delivery:(NSInteger)delievey
               pickupFrom:(NSString *)pickupFrom
                 pickupTo:(NSString *)pickupTo
              homeService:(NSString *)homeService
                   status:(NSString *)status;

- (void)configureWithTotalPrice:(float)totalPrice
                   currencyCode:(NSString *)currencyCode
                 currencySymbol:(NSString *)currencySymbol
                     couponType:(NSString *)couponType
                     couponCode:(NSString *)couponCode
                    couponValue:(float) couponValue
                            fee:(float) fee
                    deliveryFee:(float)deliveryFee;

- (void)configureWithKitchen:(NSString *)kitchen
                    location:(NSString *)location
                    delivery:(NSInteger)delievey
                         lng:(double)lng
                         lat:(double)lat
 isMargin:(BOOL)isMargin;
@end
