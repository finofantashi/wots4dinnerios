//
//  WDHomeViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/16/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDHomeViewController.h"
#import "WDUser.h"
#import <PureLayout/PureLayout.h>
#import "UIColor+Style.h"
#import "WDMainViewController.h"
#import "WDUser.h"
#import "WDDishViewController.h"
#import "WDUserDefaultsManager.h"
#import "WDSourceConfig.h"

@interface WDHomeViewController () <MSSPageViewControllerDelegate, MSSPageViewControllerDataSource>
@property (weak, nonatomic) IBOutlet UIView *tabBarContainerView;
@property (weak, nonatomic) IBOutlet UIView *pageContainerView;
@end

@implementation WDHomeViewController

#pragma mark - Init

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    _style = [TabControllerStyle styleWithName:@"Default"
                                      tabStyle:MSSTabStyleText
                                   sizingStyle:MSSTabSizingStyleDistributed
                                  numberOfTabs:3];
  }
  return self;
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  [self createTabBar];
  [self.navigationController.navigationBar setTitleTextAttributes:
   @{NSForegroundColorAttributeName:[UIColor colorFromHexString:@"FF6D37" withAlpha:1.0],
     NSFontAttributeName:[UIFont fontWithName:@"Poppins-Medium" size:17]}];
  self.navigationController.navigationBar.alpha = 1.0;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.title = [NSString stringWithFormat:@"Hi, %@",[[WDUser sharedInstance] getFullName]];
}

- (void)viewDidAppear:(BOOL)animated {
  if (!self.isFirst) {
    self.isFirst = true;
    if([[WDUserDefaultsManager valueForKey:kLoginBecomeSeller] isEqualToNumber:[NSNumber numberWithBool:YES]]){
      [WDUserDefaultsManager setValue:[NSNumber numberWithBool:NO] forKey:kLoginBecomeSeller];
      [self showBecomeSeller];
    }
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark Private

-(void)createTabBar {
  [self.tabBarView setTransitionStyle:self.style.transitionStyle];
  self.tabBarView.tabStyle = self.style.tabStyle;
  self.tabBarView.sizingStyle = self.style.sizingStyle;
  if([[[UIDevice currentDevice] systemVersion] floatValue] < 10.0){
    [self.tabBarView setBackgroundColor:[UIColor clearColor]];
  }else{
    [self.tabBarView setBackgroundColor:[UIColor colorFromHexString:@"f7f7f7" withAlpha:1.0 ]];
  }
  self.tabBarView.tabAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"Poppins-Medium" size:12],
                                    NSForegroundColorAttributeName : [UIColor colorFromHexString:@"898989" withAlpha:1.0],MSSTabTitleAlpha: @(0.8f)};
  
  self.tabBarView.selectedTabAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"Poppins-Medium" size:12],
                                            NSForegroundColorAttributeName : [UIColor colorFromHexString:@"FF6D37" withAlpha:1.0]};
  
  self.tabBarView.indicatorAttributes = @{NSForegroundColorAttributeName : [UIColor colorFromHexString:@"FF6D37" withAlpha:1.0]};
  [self.tabBarContainerView mss_addExpandingSubview:self.tabBarView];
  [self.pageContainerView mss_addExpandingSubview:self.pageViewController.view];
  self.tabBarView.delegate = self;
  self.tabBarView.dataSource = self;
  self.pageViewController.dataSource = self;
  self.pageViewController.delegate = self;
}

#pragma mark - MSSPageViewControllerDataSource
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
  [super traitCollectionDidChange:previousTraitCollection];
  if ((self.traitCollection.verticalSizeClass != previousTraitCollection.verticalSizeClass)
      || (self.traitCollection.horizontalSizeClass != previousTraitCollection.horizontalSizeClass)) {
    self.tabBarView.axis = self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular ? UILayoutConstraintAxisVertical : UILayoutConstraintAxisHorizontal;
  }
}

- (NSArray<UIViewController *> *)viewControllersForPageViewController:(MSSPageViewController *)pageViewController {
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
  
  WDDishViewController *popularViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishViewController"];
  popularViewController.isFrom = 4;
  
  return @[[storyboard instantiateViewControllerWithIdentifier:@"WDTodayMenuViewController"],
           popularViewController,
           [storyboard instantiateViewControllerWithIdentifier:@"WDOurBlogViewController"]];
}

- (void)tabBarView:(MSSTabBarView *)tabBarView
       populateTab:(MSSTabBarCollectionViewCell *)tab
           atIndex:(NSInteger)index {
  if(index==0){
    tab.title = @"Today's Menu";
  }else if(index==1){
    tab.title = @"Popular";
  }else{
    tab.title = @"Our Blog";
  }
}

- (void)showMenu {
  double delayInSeconds = 0.5;
  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    [self clickedMenu:nil];
  });
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark IBAction

- (IBAction)clickedBack:(id)sender {
  [[WDUser sharedInstance] deleteAllData];
  [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)clickedMenu:(id)sender {
  [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
  [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
    [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
  }];
}

- (IBAction)clickedSearch:(id)sender {
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
  UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDSearchViewController"];
  viewController.title = @"Search";
  //Remove
  NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
  NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
  if(newControllers.count>1){
    for (int i = 1; i < newControllers.count; i++) {
      [newControllers removeObjectAtIndex:i];
    }
  }
  [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
  [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
}

- (void)showBecomeSeller {
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
  UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepOneTableViewController"];
  viewController.title = @"Become Seller";
  //Remove
  NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
  NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
  if(newControllers.count>1){
    for (int i = 1; i < newControllers.count; i++) {
      [newControllers removeObjectAtIndex:i];
    }
  }
  [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
  
  [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
  [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
}

@end
