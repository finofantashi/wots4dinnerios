//
//  WDPaymentMethodTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDPaymentMethodTableViewController.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "WDOrderAPIController.h"
#import "WDCard.h"
#import "WDOrderCompleteViewController.h"
#import "WDPaymentMethodTableViewCell.h"
#import "PKCardNumber.h"
#import "WDPaymentDetailTableViewController.h"

@interface WDPaymentMethodTableViewController ()<WDPaymentMethodTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *listCard;

@end

@implementation WDPaymentMethodTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listCard = [[NSMutableArray alloc] init];
}

- (void)viewDidAppear:(BOOL)animated {
    [self getCards];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listCard.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDPaymentMethodTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WDPaymentMethodTableViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    WDCard *card = [self.listCard objectAtIndex:indexPath.row];
    cell.card = card;
    cell.lbCardNumber.text = [NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@",[self cardNumber:card.cardNumber].lastGroup];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/yy"];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:card.expireYear];
    [comps setMonth:card.expireMonth];
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSString *expiryDate = [formatter stringFromDate:date];
    cell.lbExpiryDate.text = expiryDate;
    if(card.cardDefault==1){
        [cell.btnChecked setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
    }else{
        [cell.btnChecked setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
    }
    [cell.ivCard setImage:[UIImage imageNamed:[self getCardType:[self cardNumber:card.cardNumber]]]];

    return cell;
}

#pragma mark - Accessors

- (PKCardNumber *)cardNumber:(NSString*)cardNumber {
    return [PKCardNumber cardNumberWithString:cardNumber];
}

- (NSString*)getCardType:(PKCardNumber*)cardNumber {
    PKCardType cardType      = [cardNumber cardType];
    NSString *cardTypeName   = @"placeholder";
    
    switch (cardType) {
        case PKCardTypeAmex:
            cardTypeName = @"amex";
            break;
        case PKCardTypeDinersClub:
            cardTypeName = @"diners";
            break;
        case PKCardTypeDiscover:
            cardTypeName = @"discover";
            break;
        case PKCardTypeJCB:
            cardTypeName = @"jcb";
            break;
        case PKCardTypeMasterCard:
            cardTypeName = @"mastercard";
            break;
        case PKCardTypeVisa:
            cardTypeName = @"visa";
            break;
        default:
            break;
    }
    return cardTypeName;
}


#pragma mark - API

- (void) getCards {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] getCards:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                if(responseObject) {
                    [self.listCard removeAllObjects];
                    NSDictionary *dataCard = [responseObject valueForKey:@"data"];
                    for (NSDictionary *dict in dataCard) {
                        WDCard *card = [[WDCard alloc] init];
                        [card setCardObject:dict];
                        [self.listCard addObject:card];
                    }
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

- (void) updateCard:(WDCard *)card {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] updateCard:card isDefault:@"1" completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                [self getCards];
            }
        }];
    }
}

- (void) deleteCard:(WDCard *)card {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] deleteCard:[NSNumber numberWithInteger:card.cardID] completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:[responseObject valueForKey:@"message"] preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    [self getCards];
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }
        }];
    }
}

#pragma mark - IBAction

- (void)clickedCheck:(WDCard *)card {
    if(card.cardDefault!=1){
        [self updateCard:card];
    }
}

- (void)clickedDelete:(WDCard *)card {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Are you sure you want to delete?" preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        [self deleteCard:card];
    }];
    LEAlertAction *cancelAction = [LEAlertAction actionWithTitle:@"Cancel" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {

    }];
    [alertController addAction:cancelAction];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedAdd:(id)sender {
    WDPaymentDetailTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDPaymentDetailTableViewController"];
    viewController.isFromAddCard = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
