//
//  WDMessageTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/5/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMessageViewController.h"
#import "UIColor+Style.h"
#import "WDMessageTableViewCell.h"
#import "LGPlusButtonsView.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDMessageAPIController.h"
#import "WDMessage.h"
#import "WDSourceConfig.h"
#import "WDUser.h"
#import "NSDate+Utilities.h"
#import "WDContentMessageViewController.h"
#import "WDMainViewController.h"

@interface WDMessageViewController () <UIScrollViewDelegate, SDWebImageManagerDelegate>
@property (strong, nonatomic) LGPlusButtonsView *plusButtonsViewMain;
@property (nonatomic, strong) NSMutableArray *listMessage;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;
@property (nonatomic, assign) BOOL isReadAll;

@property (nonatomic, strong) NSMutableArray *conversationIDs;
@property (nonatomic, strong) NSMutableArray *groupIDs;
@property (nonatomic, strong) NSMutableArray *messageIDs;

@end

@implementation WDMessageViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.listMessage = [[NSMutableArray alloc] init];
  self.tableView.allowsMultipleSelectionDuringEditing = YES;
  self.isReadAll = YES;
  [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.currentPage = 0;
  self.totalPages = 0;
  self.totalItems = 0;
  self.listMessage = [[NSMutableArray alloc] init];
  [self loadAllData:self.currentPage];
  self.navigationController.navigationBar.translucent = false;
}
- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setupUI {
  _plusButtonsViewMain = [LGPlusButtonsView plusButtonsViewWithNumberOfButtons:1
                                                       firstButtonIsPlusButton:YES
                                                                 showAfterInit:YES
                                                                 actionHandler:^(LGPlusButtonsView *plusButtonView, NSString *title, NSString *description, NSUInteger index) {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDNewMessageViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
  }];
  
  _plusButtonsViewMain.observedScrollView = self.tableView;
  _plusButtonsViewMain.coverColor = [UIColor clearColor];
  _plusButtonsViewMain.position = LGPlusButtonsViewPositionBottomRight;
  _plusButtonsViewMain.plusButtonAnimationType = LGPlusButtonAnimationTypeNone;
  
  [_plusButtonsViewMain setButtonsImages:@[[UIImage imageNamed:@"ic_pencil"]]
                                forState:UIControlStateNormal
                          forOrientation:LGPlusButtonsViewOrientationAll];
  [_plusButtonsViewMain setButtonsAdjustsImageWhenHighlighted:NO];
  [_plusButtonsViewMain setButtonsBackgroundColor:[UIColor colorFromHexString:@"FF6D37" withAlpha:1.0] forState:UIControlStateNormal];
  [_plusButtonsViewMain setButtonsBackgroundColor:[UIColor colorFromHexString:@"F78B62" withAlpha:1.0] forState:UIControlStateHighlighted];
  [_plusButtonsViewMain setButtonsBackgroundColor:[UIColor colorFromHexString:@"F78B62" withAlpha:1.0] forState:UIControlStateHighlighted|UIControlStateSelected];
  
  [_plusButtonsViewMain setButtonsSize:CGSizeMake(44.f, 44.f) forOrientation:LGPlusButtonsViewOrientationAll];
  [_plusButtonsViewMain setButtonsLayerCornerRadius:44.f/2.f forOrientation:LGPlusButtonsViewOrientationAll];
  [_plusButtonsViewMain setButtonsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
  [_plusButtonsViewMain setButtonsLayerShadowOpacity:0.5];
  [_plusButtonsViewMain setButtonsLayerShadowRadius:3.f];
  [_plusButtonsViewMain setButtonsLayerShadowOffset:CGSizeMake(0.f, 2.f)];
  _plusButtonsViewMain.showHideOnScroll = NO;
  
  [self.view addSubview:_plusButtonsViewMain];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  if (self.currentPage == self.totalPages
      || self.totalItems == self.listMessage.count) {
    return self.listMessage.count;
  }
  return self.listMessage.count + 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
  if (self.currentPage != self.totalPages && indexPath.row == [self.listMessage count] - 1 ) {
    [self loadAllData:++self.currentPage];
  }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"WDMessageTableViewCell";
  UITableViewCell *cell;
  if (indexPath.row == [self.listMessage count]) {
    cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
    UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:100];
    [activityIndicator startAnimating];
  }else{
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //Configure right buttons
    MGSwipeButton *removeButton = [MGSwipeButton buttonWithTitle:@"" icon:[UIImage imageNamed:@"ic_remove.png"] backgroundColor:[UIColor colorFromHexString:@"898989" withAlpha:1.0] callback:^BOOL(MGSwipeTableCell *sender) {
      if([sender isKindOfClass:[WDMessageTableViewCell class]]){
        NSMutableArray *listConversation = [[NSMutableArray alloc] init];
        NSMutableArray *listGroup = [[NSMutableArray alloc] init];
        
        if(((WDMessageTableViewCell*)sender).isGroup){
          [listGroup addObject:[NSNumber numberWithInteger:((WDMessageTableViewCell*)sender).removeID]];
        }else{
          [listConversation addObject:[NSNumber numberWithInteger:((WDMessageTableViewCell*)sender).removeID]];
        }
        [self deleteMessageAPI:listConversation listGroup:listGroup];
      }
      return YES;
    }];
    ((WDMessageTableViewCell*)cell).rightButtons = @[removeButton];
    
    //get image name and assign
    ((WDMessageTableViewCell*)cell).ivAvatar.layer.cornerRadius = ((WDMessageTableViewCell*)cell).ivAvatar.frame.size.width / 2;
    ((WDMessageTableViewCell*)cell).ivAvatar.layer.masksToBounds = YES;
    [((WDMessageTableViewCell*)cell).ivAvatar setNeedsDisplay];
    
    WDMessage *message = self.listMessage[indexPath.row];
    NSLog(@"%@",message.contentMessage);
    ((WDMessageTableViewCell*)cell).lbAddress.text = message.contentMessage;
    
    NSString *avatar = @"";
    NSString *fullName = @"";
    
    if(message.senderID == [WDUser sharedInstance].userID&&message.groupID!=0){
      
      [((WDMessageTableViewCell*)cell).avatarGroup setHidden:YES];
      ((WDMessageTableViewCell*)cell).isGroup = YES;
      ((WDMessageTableViewCell*)cell).removeID = message.groupID;
      ((WDMessageTableViewCell*)cell).lbName.font =  [UIFont fontWithName:@"Poppins-Light" size:17];
      
      WDReceiver *receiver = [message.receiver objectAtIndex:0];
      avatar = message.avatar;
      fullName = [NSString stringWithFormat:@"%@ + %ld",receiver.fullName, message.receiver.count-1];
      [((WDMessageTableViewCell*)cell).ivAvatar setImage:[UIImage imageNamed:@"ic_group_avatar.png"]];
      
      //            NSMutableArray *listAvatar = [[NSMutableArray alloc] init];
      //            NSMutableArray *avatarImages = [[NSMutableArray alloc] init];
      //            int i = 0;
      //            for (WDReceiver *object in message.receiver) {
      //                if(i>=3){
      //                    break;
      //                }
      //                [listAvatar addObject:object.avatar];
      //                i = i+1;
      //
      //            }
      //
      //            if(message.receiver.count==2){
      //                [listAvatar addObject:message.avatar];
      //            }
      //
      //            for (NSString *link in listAvatar) {
      //                if(![link isEqualToString:@""]){
      //                    NSString *imageURL = [[NSString stringWithFormat:@"%@%@", kImageDomain, link] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
      //
      //                    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imageURL] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
      //
      //                    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
      //                        if(image){
      //                            [avatarImages addObject:image];
      //                            ((WDMessageTableViewCell*)cell).avatarGroup.avatars = avatarImages;
      //                        }else{
      //                            [avatarImages addObject:[UIImage imageNamed:@"empty_avatar"]];
      //                            ((WDMessageTableViewCell*)cell).avatarGroup.avatars = avatarImages;
      //                        }
      //                    }];
      //                }else{
      //                    [avatarImages addObject:[UIImage imageNamed:@"empty_avatar"]];
      //                    ((WDMessageTableViewCell*)cell).avatarGroup.avatars = avatarImages;
      //                }
      //            }
    }else {
      [((WDMessageTableViewCell*)cell).avatarGroup setHidden:YES];
      ((WDMessageTableViewCell*)cell).isGroup = NO;
      ((WDMessageTableViewCell*)cell).removeID = message.conversationID;
      
      if(message.senderID != [WDUser sharedInstance].userID){
        if([message.receiver count]>0){
          WDReceiver *receiver = [message.receiver objectAtIndex:0];
          if([receiver.receiverReadAt isEqualToString:@""]){
            ((WDMessageTableViewCell*)cell).lbName.font =  [UIFont fontWithName:@"Poppins-SemiBold" size:17];
            self.isReadAll = NO;
          }else{
            ((WDMessageTableViewCell*)cell).lbName.font =  [UIFont fontWithName:@"Poppins-Light" size:17];
          }
        }
        avatar = message.avatar;
        fullName = [NSString stringWithFormat:@"%@ %@",message.firstName, message.lastName];
      }else{
        ((WDMessageTableViewCell*)cell).lbName.font =  [UIFont fontWithName:@"Poppins-Light" size:17];
        if([message.receiver count]>0){
          WDReceiver *receiver = [message.receiver objectAtIndex:0];
          avatar = receiver.avatar;
          fullName = [NSString stringWithFormat:@"%@ %@",receiver.firstName, receiver.lastName];
          
        }
      }
      NSString *imageURL = [[NSString stringWithFormat:@"%@%@", kImageDomain, avatar] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
      
      [((WDMessageTableViewCell*)cell).ivAvatar sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                                  placeholderImage:[UIImage imageNamed:@"empty_avatar"]];
    }
    
    ((WDMessageTableViewCell*)cell).lbName.text = fullName;
    //Date Time
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:message.createdAt==nil?@"":message.createdAt];
    
    //Today
    if([dateFromString isToday]) {
      ((WDMessageTableViewCell*)cell).lbDateTime.text =  [dateFromString shortTimeString];
      
    }else if([dateFromString isYesterday]){
      ((WDMessageTableViewCell*)cell).lbDateTime.text =  [NSString stringWithFormat:@"Yesterday"];
      
    }else if([dateFromString isThisWeek]){
      [dateFormatter setDateFormat:@"EEEE"];
      ((WDMessageTableViewCell*)cell).lbDateTime.text = [dateFormatter stringFromDate:dateFromString];
    }else{
      ((WDMessageTableViewCell*)cell).lbDateTime.text = [dateFromString shortDateString];
    }
    
  }
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
  WDMessage *message = self.listMessage[indexPath.row];
  if(tableView.isEditing){
    if(message.senderID== [WDUser sharedInstance].userID&&message.groupID!=0){
      if([self.groupIDs containsObject:[NSNumber numberWithInteger:message.groupID]]){
        [self.groupIDs removeObject:[NSNumber numberWithInteger:message.groupID]];
      }else{
        [self.groupIDs addObject:[NSNumber numberWithInteger:message.groupID]];
      }
    }else{
      if([self.conversationIDs containsObject:[NSNumber numberWithInteger:message.conversationID]]){
        [self.conversationIDs removeObject:[NSNumber numberWithInteger:message.conversationID]];
      }else{
        [self.conversationIDs addObject:[NSNumber numberWithInteger:message.conversationID]];
      }
      //Message
      if([self.messageIDs containsObject:[NSNumber numberWithInteger:message.recordID]]){
        [self.messageIDs removeObject:[NSNumber numberWithInteger:message.recordID]];
      }else{
        if(message.senderID != [WDUser sharedInstance].userID){
          if([message.receiver count]>0){
            WDReceiver *receiver = [message.receiver objectAtIndex:0];
            if([receiver.receiverReadAt isEqualToString:@""]){
              [self.messageIDs addObject:[NSNumber numberWithInteger:message.recordID]];
            }
          }
        }
      }
    }
    
    if(self.groupIDs.count>0||self.conversationIDs.count>0){
      [self.barButtonDelete setEnabled:YES];
      
    }else{
      [self.barButtonDelete setEnabled:NO];
      
    }
    
    if(self.messageIDs.count>0){
      self.barButtonRead.title = @"Read";
      [self.barButtonRead setEnabled:YES];
      
    }else{
      self.barButtonRead.title = @"Read All";
      if(self.isReadAll){
        [self.barButtonRead setEnabled:NO];
      }else{
        [self.barButtonRead setEnabled:YES];
      }
    }
  }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  WDMessage *message = self.listMessage[indexPath.row];
  if(tableView.isEditing){
    if(message.senderID == [WDUser sharedInstance].userID&&message.groupID != 0){
      if([self.groupIDs containsObject:[NSNumber numberWithInteger:message.groupID]]){
        [self.groupIDs removeObject:[NSNumber numberWithInteger:message.groupID]];
      }else{
        [self.groupIDs addObject:[NSNumber numberWithInteger:message.groupID]];
      }
    }else{
      if([self.conversationIDs containsObject:[NSNumber numberWithInteger:message.conversationID]]){
        [self.conversationIDs removeObject:[NSNumber numberWithInteger:message.conversationID]];
      }else{
        [self.conversationIDs addObject:[NSNumber numberWithInteger:message.conversationID]];
      }
      //Message
      if([self.messageIDs containsObject:[NSNumber numberWithInteger:message.recordID]]){
        [self.messageIDs removeObject:[NSNumber numberWithInteger:message.recordID]];
      }else{
        if(message.senderID != [WDUser sharedInstance].userID){
          if([message.receiver count]>0){
            WDReceiver *receiver = [message.receiver objectAtIndex:0];
            if([receiver.receiverReadAt isEqualToString:@""]){
              [self.messageIDs addObject:[NSNumber numberWithInteger:message.recordID]];
            }
          }
        }
      }
    }
    
    if(self.groupIDs.count>0||self.conversationIDs.count>0){
      [self.barButtonDelete setEnabled:YES];
      
    }else{
      [self.barButtonDelete setEnabled:NO];
      
    }
    
    if(self.messageIDs.count>0){
      self.barButtonRead.title = @"Read";
      [self.barButtonRead setEnabled:YES];
      
    }else{
      self.barButtonRead.title = @"Read All";
      if(self.isReadAll){
        [self.barButtonRead setEnabled:NO];
      }else{
        [self.barButtonRead setEnabled:YES];
      }
    }
  }else{
    //WDContentMessageViewController *viewController = [WDContentMessageViewController messagesViewController];
    WDContentMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDContentMessageViewController"];
    viewController.recordID = message.recordID;
    
    if(message.senderID == [WDUser sharedInstance].userID&&message.groupID !=0){
      viewController.listReceiver = message.receiver;
      viewController.groupID = message.groupID;
    }else {
      if(message.senderID != [WDUser sharedInstance].userID){
        WDReceiver *receiver = [[WDReceiver alloc] init];
        [receiver setReceiverID:message.senderID];
        [receiver setReceiverReadAt:@""];
        [receiver setFirstName:message.firstName];
        [receiver setLastName:message.lastName];
        [receiver setKitchenName:message.kitchenName];
        [receiver setRoleID:message.roleID];
        [receiver setAvatar:message.avatar];
        
        viewController.listReceiver = [[NSMutableArray alloc] initWithObjects:receiver, nil];
      }else{
        viewController.listReceiver = message.receiver;
      }
    }
    [self.navigationController pushViewController:viewController animated:YES];
  }
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
  [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
  [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
    [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
  }];
}

- (IBAction)clickedEdit:(id)sender {
  [self.tableView setEditing:!self.tableView.isEditing animated:YES];
  if(self.tableView.isEditing){
    self.conversationIDs = [[NSMutableArray alloc] init];
    self.groupIDs = [[NSMutableArray alloc] init];
    self.messageIDs = [[NSMutableArray alloc] init];
    [self.barButtonDelete setEnabled:NO];
    if(self.isReadAll){
      [self.barButtonRead setEnabled:NO];
    }else{
      [self.barButtonRead setEnabled:YES];
    }
    
    self.plusButtonsViewMain.alpha = 0;
    [self.plusButtonsViewMain setButtonsEnabled:NO];
    [UIView animateWithDuration:0.4 animations:^{
      self.toolBar.alpha = 1;
    }];
  }else{
    self.plusButtonsViewMain.alpha = 1;
    [self.plusButtonsViewMain setButtonsEnabled:YES];
    [UIView animateWithDuration:0.4 animations:^{
      self.toolBar.alpha = 0;
      self.barButtonRead.title = @"Read All";
    }];
  }
}

- (IBAction)clickedRead:(id)sender {
  if([((UIBarButtonItem*)sender).title isEqualToString:@"Read"]){
    [self markMesagesAsReadAPI:self.messageIDs];
  }else{
    [self markMesagesAsReadAPI:[[NSMutableArray alloc] init]];
  }
}

- (IBAction)clickedDelete:(id)sender {
  [self deleteMessageAPI:self.conversationIDs listGroup:self.groupIDs];
}

#pragma mark - API

- (void)loadAllData:(NSInteger)page {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    
    [MBProgressHUD showHUDAddedTo:window animated:YES];
    //Call Api
    [[WDMessageAPIController sharedInstance] getListMessage:page completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:window animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          NSDictionary *dataMessage = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
          for (NSDictionary *object in dataMessage) {
            WDMessage *message = [[WDMessage alloc] init];
            [message setMessageObject:object];
            [self.listMessage addObject:message];
          };
          
          self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
          self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
          self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
          if(self.totalItems==0){
            LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No records found." preferredStyle:LEAlertControllerStyleAlert];
            LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
              // handle default button action
            }];
            [alertController addAction:defaultAction];
            [self presentAlertController:alertController animated:YES completion:nil];
            
          }
          dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
          });
        }
      }
    }];
  }
}

- (void) deleteMessageAPI:(NSMutableArray *)listConversation
                listGroup:(NSMutableArray *)listGroup {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    
    [MBProgressHUD showHUDAddedTo:window animated:YES];
    //Call Api
    [[WDMessageAPIController sharedInstance]deleteConversations:listConversation listGroup:listGroup completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:window animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if([responseObject valueForKey:@"success"]){
          if(self.tableView.isEditing){
            [self clickedEdit:nil];
          }
          self.currentPage = 0;
          self.totalPages = 0;
          self.totalItems = 0;
          self.listMessage = [[NSMutableArray alloc] init];
          [self loadAllData:self.currentPage];
        }
      }
    }];
  }
}

- (void) markMesagesAsReadAPI:(NSMutableArray *)listMessage {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    
    [MBProgressHUD showHUDAddedTo:window animated:YES];
    //Call Api
    [[WDMessageAPIController sharedInstance]markMesagesAsRead:listMessage  completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:window animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if([responseObject valueForKey:@"success"]){
          if(self.tableView.isEditing){
            [self clickedEdit:nil];
          }
          self.currentPage = 0;
          self.totalPages = 0;
          self.totalItems = 0;
          self.listMessage = [[NSMutableArray alloc] init];
          [self loadAllData:self.currentPage];
        }
      }
    }];
  }
}

@end
