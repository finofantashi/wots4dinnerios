//
//  WDAppFeedbackTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDAppFeedbackTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnScreenShot;
@property (weak, nonatomic) IBOutlet UILabel *lbDeviceModel;
@property (weak, nonatomic) IBOutlet UILabel *lbIOSVersion;
@property (weak, nonatomic) IBOutlet UILabel *lbAppVersion;
@property (weak, nonatomic) IBOutlet UILabel *lbAppBuild;
@property (weak, nonatomic) IBOutlet UIImageView *ivScreen;

@end
