//
//  WDSetNameViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSetNameViewController.h"
#import "NSString+IsValidEmail.h"
#import "MBProgressHUD.h"
#import "LEAlertController.h"
#import "Reachability.h"
#import "WDUserAPIController.h"
#import "WDUser.h"
#import "WDUserDefaultsManager.h"
#import "WDSourceConfig.h"
#import "WDMainViewController.h"
#import "UIColor+Style.h"

@interface WDSetNameViewController ()

@end

@implementation WDSetNameViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self setupDismissKeyboard];
  self.btnSetName.layer.cornerRadius = self.btnSetName.frame.size.height/2; // this value vary as per your desire
  self.btnSetName.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setupDismissKeyboard {
  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                 initWithTarget:self
                                 action:@selector(dismissKeyboard)];
  [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
  [self.tfFirstName resignFirstResponder];
  [self.tfLastName resignFirstResponder];
  [self.tfEmail resignFirstResponder];
}

- (void)enableButtonSetName {
  if([self.tfFirstName.text trim].length!=0&&[self.tfLastName.text trim].length!=0&&[self.tfEmail.text trim].length!=0 && [[self.tfEmail.text trim] isValidEmail]){
    self.btnSetName.enabled = YES;
    self.btnSetName.alpha = 1.0f;
  }else{
    self.btnSetName.enabled = NO;
    self.btnSetName.alpha = 0.5f;
  }
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  NSUInteger oldLength = [[textField.text trim] length];
  NSUInteger replacementLength = [[string trim] length];
  NSUInteger rangeLength = range.length;
  
  NSUInteger newLength = oldLength - rangeLength + replacementLength;
  
  BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
  return newLength <= 30 || returnKey;
}

- (IBAction)textFieldChange:(id)sender {
  [self enableButtonSetName];
}

#pragma mark IBAction

- (IBAction)clickedBack:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)touchDown:(id)sender {
  [self.btnSetName setBackgroundColor:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0]];
}

- (IBAction)clickedSetName:(id)sender {
  [self.btnSetName setBackgroundColor:[UIColor colorFromHexString:@"e7663f" withAlpha:1.0]];
  [self updateUserAPI];
}
#pragma mark API

- (void) updateUserAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    //Call API
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    [[WDUserAPIController sharedInstance] updateUser:[[WDUser sharedInstance] getPhoneNumber] phonePrefix:[[WDUser sharedInstance] getPhonePrefix]  password:[[WDUser sharedInstance] getPassword]  firstName:[self.tfFirstName.text trim]  lastName:[self.tfLastName.text trim] email:[self.tfEmail.text trim] completion:^(NSString *errorString) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        [[WDUser sharedInstance] setFirstName:[self.tfFirstName.text trim]];
        [[WDUser sharedInstance] setLastName:[self.tfLastName.text trim]];
        [[WDUser sharedInstance] setEmail:[self.tfEmail.text trim]];
        NSDate *todayDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *convertedDateString = [dateFormatter stringFromDate:todayDate];
        [[WDUser sharedInstance] setSinceDate:convertedDateString];
        [WDUserDefaultsManager setValue:[NSNumber numberWithBool:YES] forKey:kSessionSignIn];
        
        
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Thank you! You have completed registering with us. What do you want to do next?" preferredStyle:LEAlertControllerStyleAlert];
        
        LEAlertAction *cancelAction = [LEAlertAction actionWithTitle:@"Continue as user" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          UIWindow *window = UIApplication.sharedApplication.delegate.window;
          window.rootViewController = [WDMainViewController sharedInstance];
        }];
        
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"Become a selller" style:LEAlertActionStyleCancel handler:^(LEAlertAction *action) {
          [WDUserDefaultsManager setValue:[NSNumber numberWithBool:YES] forKey:kLoginBecomeSeller];
          UIWindow *window = UIApplication.sharedApplication.delegate.window;
          window.rootViewController = [WDMainViewController sharedInstance];
        }];
        
        [alertController addAction:defaultAction];
        [alertController addAction:cancelAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }
    }];
  }
}

@end
