//
//  WDMYMenu.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/21/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMyMenu.h"
#import "MBProgressHUD.h"
#import "WDTabMenuViewController.h"

@interface WDMyMenu ()

@end

@implementation WDMyMenu

- (void)viewDidLoad {
    [super viewDidLoad];
     WDTabMenuViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDTabMenuViewController"];
    viewController.isMyOrder = self.isMyOrder;
    [self.navigationController pushViewController:viewController animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
