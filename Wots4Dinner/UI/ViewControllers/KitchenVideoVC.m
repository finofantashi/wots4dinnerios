//
//  KitchenVideoVC.m
//  Meals Around
//
//  Created by Macbook on 5/24/20.
//  Copyright © 2020 Hung Hoang. All rights reserved.
//

#import "KitchenVideoVC.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <FCFileManager/FCFileManager.h>
#import "UIImage+ResizeMagick.h"
#import "WDAppFeedbackAPIController.h"
#import "WSSVideoCompression.h"
#import "WSSPermissionsManager.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import "WSSProgressHUD.h"
#import "WDMainViewController.h"

@interface KitchenVideoVC ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, strong) UIImage *imageFile;
@property (nonatomic, assign) Boolean isScreenDish;           // The image we'll be cropping
@property (nonatomic, strong) NSURL *fileURL;

@property (nonatomic, strong) UIImagePickerController *imagePickerController;
@property (nonatomic, strong) WSSVideoCompression *videoCompression;
@end

@implementation KitchenVideoVC
- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.title = @"Kitchen Video";
  UITapGestureRecognizer *singleTapFile = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedUploadFile:)];
  singleTapFile.numberOfTapsRequired = 1;
  singleTapFile.numberOfTouchesRequired = 1;
  
  
  [self.ivUploadFile addGestureRecognizer:singleTapFile];
  [self.ivUploadFile setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


- (void)viewDidLayoutSubviews {
  [self drawDashedBorderAroundView:self.btnUploadFile];
}

- (void)drawDashedBorderAroundView:(UIView *)v {
  //border definitions
  CGFloat cornerRadius = 10;
  CGFloat borderWidth = 2;
  NSInteger dashPattern1 = 8;
  NSInteger dashPattern2 = 8;
  UIColor *lineColor = [UIColor colorFromHexString:@"cccccc" withAlpha:1.0];
  
  //drawing
  CGRect frame = v.bounds;
  
  CAShapeLayer *_shapeLayer = [CAShapeLayer layer];
  
  //creating a path
  CGMutablePathRef path = CGPathCreateMutable();
  
  //drawing a border around a view
  CGPathMoveToPoint(path, NULL, 0, frame.size.height - cornerRadius);
  CGPathAddLineToPoint(path, NULL, 0, cornerRadius);
  CGPathAddArc(path, NULL, cornerRadius, cornerRadius, cornerRadius, M_PI, -M_PI_2, NO);
  CGPathAddLineToPoint(path, NULL, frame.size.width - cornerRadius, 0);
  CGPathAddArc(path, NULL, frame.size.width - cornerRadius, cornerRadius, cornerRadius, -M_PI_2, 0, NO);
  CGPathAddLineToPoint(path, NULL, frame.size.width, frame.size.height - cornerRadius);
  CGPathAddArc(path, NULL, frame.size.width - cornerRadius, frame.size.height - cornerRadius, cornerRadius, 0, M_PI_2, NO);
  CGPathAddLineToPoint(path, NULL, cornerRadius, frame.size.height);
  CGPathAddArc(path, NULL, cornerRadius, frame.size.height - cornerRadius, cornerRadius, M_PI_2, M_PI, NO);
  
  //path is set as the _shapeLayer object's path
  _shapeLayer.path = path;
  CGPathRelease(path);
  
  _shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
  _shapeLayer.frame = frame;
  _shapeLayer.masksToBounds = NO;
  [_shapeLayer setValue:[NSNumber numberWithBool:NO] forKey:@"isCircle"];
  _shapeLayer.fillColor = [[UIColor clearColor] CGColor];
  _shapeLayer.strokeColor = [lineColor CGColor];
  _shapeLayer.lineWidth = borderWidth;
  _shapeLayer.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:dashPattern1], [NSNumber numberWithInt:dashPattern2], nil];
  _shapeLayer.lineCap = kCALineCapRound;
  
  //_shapeLayer is added as a sublayer of the view, the border is visible
  [v.layer addSublayer:_shapeLayer];
  v.layer.cornerRadius = cornerRadius;
}

- (BOOL)validate {
  if(!self.fileURL) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"File upload is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  return YES;
}

#pragma mark - Bar Button Items -

- (IBAction)clickedSubmit:(id)sender {
  if([self validate]) {
    [self sendAppFeedbackAPI];
  }
}


- (IBAction)clickedBack:(id)sender {
  [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
      [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
  }];
}

- (void) chooseImage {
  UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
  
  UIAlertAction *profileAction = [UIAlertAction actionWithTitle:@"Choose Photo"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
    UIImagePickerController *profilePicker = [[UIImagePickerController alloc] init];
    profilePicker.modalPresentationStyle = UIModalPresentationPopover;
    profilePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    profilePicker.allowsEditing = NO;
    profilePicker.delegate = self;
    profilePicker.preferredContentSize = CGSizeMake(512,512);
    [self presentViewController:profilePicker animated:YES completion:nil];
  }];
  
  UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleCancel
                                                 handler:^(UIAlertAction *action) {
    
  }];
  
  [alertController addAction:profileAction];
  [alertController addAction:cancel];
  [alertController setModalPresentationStyle:UIModalPresentationPopover];
  [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)clickedUploadFile:(id)sender {
  [WSSPermissionsManager checkCameraPermissions:^(BOOL granted) {
    if (granted) {
      dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
          [self presentViewController:self.imagePickerController animated:YES completion:nil];
        }
      });
    }
  }];
}

- (void) sendAppFeedbackAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    
    [MBProgressHUD showHUDAddedTo:window animated:YES];
    NSString *urlStringFile =[self.fileURL absoluteString];
    
    //Call Api
    
    [[WDAppFeedbackAPIController sharedInstance] sendKitchenVideo:[NSURL URLWithString:urlStringFile]
                                                           completion:^(NSString *errorString, id responseObject){
      [MBProgressHUD hideHUDForView:window animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        
      }else{
        if(![urlStringFile isEqualToString:@""]){
          [FCFileManager removeItemAtPath:urlStringFile error:nil];
        }
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Video uploaded successfully." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }
    }];
  }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
  [picker dismissViewControllerAnimated:YES completion:nil];
  if ([info[UIImagePickerControllerMediaType] isEqualToString:(NSString *)kUTTypeMovie]) {
    NSURL *fileUrl = info[UIImagePickerControllerMediaURL];
    AVURLAsset *asset = [AVURLAsset assetWithURL:fileUrl];
    CMTime time = asset.duration;
    CGFloat seconds = CMTimeGetSeconds(time);
    if ((NSInteger)seconds > 60) {
      [WSSProgressHUD showMessageWithInWindow:@"The duration of the selected video cannot exceed 1 minutes"];
    } else {
      AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
      imageGenerator.appliesPreferredTrackTransform = YES;
      CGImageRef imgRef = [imageGenerator copyCGImageAtTime:CMTimeMake(0, 1) actualTime:NULL error:nil];
      UIImage* thumbnail = [[UIImage alloc] initWithCGImage:imgRef scale:UIViewContentModeScaleAspectFit orientation:UIImageOrientationUp];
      self.imageFile = thumbnail;
      [self compressedVideoWithFileUrl:fileUrl];
    }
  } else if ([info[UIImagePickerControllerMediaType] isEqualToString:(NSString *)kUTTypeImage]) {
    
  }
}
#pragma mark - private
- (void)compressedVideoWithFileUrl:(NSURL *)fileUrl {
  [WSSProgressHUD showProgressLoading:@"Compressing video..." inView:[WSSProgressHUD applicationWindow]];
  NSString *videoPath = [self getOutputFilePath];
  self.videoCompression.inputUrl = fileUrl;
  self.videoCompression.outputUrl = [NSURL fileURLWithPath:videoPath];
  [self.videoCompression startCompressionWithCompressionBlock:^(WSSVideoCompressionState state, NSError *error) {
    if (state == WSSVideoCompressionStateSuccess) {
      self.fileURL = [NSURL fileURLWithPath:videoPath];
      [self.ivUploadFile setImage:self.imageFile];
      [self.ivUploadFile setHidden:NO];
      [self.btnUploadFile setHidden:YES];
    } else {
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message: error.description preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
    }
    [WSSProgressHUD hideHUDWithView:[WSSProgressHUD applicationWindow]];
  }];
  self.videoCompression.compressionProgressBlock = ^(CGFloat progress) {
    NSLog(@"---progress----  %f",progress);
  };
}

- (NSString *)getOutputFilePath {
    NSString *tempPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"video"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:tempPath]) {
        [fileManager createDirectoryAtPath:tempPath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    NSString *filePath = [tempPath stringByAppendingPathComponent:[self getCurrentTimestampPath]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    return filePath;
}

- (NSString *)getCurrentTimestampPath {
    return [NSString stringWithFormat:@"atz%ld.mp4",(long)[self getCurrentTimestamp]];
}

- (NSInteger)getCurrentTimestamp {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval timestamp = [date timeIntervalSince1970];
    return (NSInteger)timestamp;
}

#pragma mark - getter

- (UIImagePickerController *)imagePickerController {
  if (!_imagePickerController) {
    _imagePickerController = [UIImagePickerController new];
    _imagePickerController.delegate = self;
    _imagePickerController.modalPresentationStyle = UIModalPresentationFullScreen;
    _imagePickerController.mediaTypes = @[(NSString *)kUTTypeMovie];
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
  }
  return _imagePickerController;
}

- (WSSVideoCompression *)videoCompression {
  if (!_videoCompression) {
    WSSVideoConfigurations videoConfigurations;
    videoConfigurations.fps = 30;
    videoConfigurations.videoResolution = WSSVideoResolutionPreset1920x1080;
    videoConfigurations.videoBitRate = WSSVideoBitRateHigh;
    WSSAudioConfigurations audioConfigurations;
    audioConfigurations.sampleRate = WSSAudioSampleRate44KHz;
    audioConfigurations.bitRate = WSSAudioBitRate128Kbps;
    audioConfigurations.numOfChannels = 2;
    audioConfigurations.frameSize = 16;
    _videoCompression = [[WSSVideoCompression alloc] initWithVideoConfigurations:videoConfigurations audioConfigurations:audioConfigurations];
  }
  return _videoCompression;
}
@end
