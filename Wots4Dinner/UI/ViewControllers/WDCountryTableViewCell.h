//
//  WDCountryTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDCountryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivFlag;
@property (weak, nonatomic) IBOutlet UILabel *lbCountry;
@property (weak, nonatomic) IBOutlet UILabel *lbCode;

@end
