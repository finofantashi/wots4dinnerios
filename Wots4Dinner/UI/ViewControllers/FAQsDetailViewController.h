//
//  FAQsDetailViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/27/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQsDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger faqID;
@property (weak, nonatomic) IBOutlet UILabel *lbQuestion;

@end
