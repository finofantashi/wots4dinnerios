//
//  PayPalMethodTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDownPicker.h"
#import "WDPayoutMethod.h"

@interface PayPalMethodTableViewController : UITableViewController
@property (nonatomic, assign) BOOL isAddPayout;

@property (nonatomic, strong) WDPayoutMethod* payoutMethod;
@property (nonatomic, strong) NSMutableArray* currencies;
@property (nonatomic, strong) NSMutableArray* currenciessData;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *pickerCurrency;
@property (weak, nonatomic) IBOutlet UIButton *btnFinish;
@property (weak, nonatomic) IBOutlet UILabel *tvPaypal;
@property (nonatomic) DownPicker *picker;
- (IBAction)clickedFinish:(id)sender;

@end
