//
//  FAQsViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "FAQsViewController.h"
#import "WDQuestion.h"
#import "WDFAQsTableViewCell.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFAQsAPIController.h"
#import "FAQsDetailViewController.h"

@interface FAQsViewController () <UIScrollViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) NSMutableArray *listQuestionBuyer;
@property (nonatomic, strong) NSMutableArray *listQuestionSeller;
@property (nonatomic, strong) NSString *textSearch;

@end

@implementation FAQsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listQuestionBuyer = [[NSMutableArray alloc] init];
    self.listQuestionSeller = [[NSMutableArray alloc] init];
    
    self.textSearch = @"";
    [self loadAllData];
    self.title = @"FAQs";
    
    self.viewSearch.clipsToBounds = YES;
    self.viewSearch.layer.cornerRadius = self.viewSearch.frame.size.height/2;//half of the width
    self.viewSearch.layer.borderColor=[UIColor redColor].CGColor;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.delegate = self;
    
    [self.view addGestureRecognizer:tap];
    self.tableView.allHeadersInitiallyCollapsed = YES;
    self.tableView.initiallyExpandedSection = 0;
    self.tableView.singleSelectionEnable = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissKeyboard {
    [self.tfSearch resignFirstResponder];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.tableView]) {
        return NO;
    }
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section==0){
        return [self.tableView headerWithTitle:@"BUYER'S FAQ" totalRows:self.listQuestionBuyer.count inSection:section];
    }
    return [self.tableView headerWithTitle:@"SELLER'S FAQ" totalRows:self.listQuestionSeller.count inSection:section];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section==0){
        return [self.tableView totalNumberOfRows:self.listQuestionBuyer.count inSection:section];
    }
    return [self.tableView totalNumberOfRows:self.listQuestionSeller.count inSection:section];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDFAQsTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.section == 0) {
        WDQuestion *question = self.listQuestionBuyer[indexPath.row];
        ((WDFAQsTableViewCell*)cell).lbQuestion.text = question.question;
    }else{
        WDQuestion *question = self.listQuestionSeller[indexPath.row];
        ((WDFAQsTableViewCell*)cell).lbQuestion.text = question.question;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WDQuestion *question;
    if (indexPath.section == 0) {
        question = self.listQuestionBuyer[indexPath.row];
        
    }else{
        question = self.listQuestionSeller[indexPath.row];
    }
    FAQsDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FAQsDetailViewController"];
    viewController.faqID = question.questionID;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDQuestion *question;
    if (indexPath.section == 0) {
        question = self.listQuestionBuyer[indexPath.row];
        
    }else{
        question = self.listQuestionSeller[indexPath.row];
    }
    float sizeHeight = 21 + [self sizeOfMultiLineLabel:question.question];
    return sizeHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return HEADER_VIEW_HEIGHT;
}

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:17];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.view.frame.size.width;
    CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{
                                                                 NSFontAttributeName : aLabelFont
                                                                 }
                                                       context:nil].size;
    return labelSize.height;
}

- (void)loadAllData {
    [self dismissKeyboard];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFAQsAPIController sharedInstance] getListQuestion:self.textSearch completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataSearch = [responseObject valueForKey:@"data"];
                    for (NSDictionary *object in dataSearch) {
                        WDQuestion *question = [[WDQuestion alloc] init];
                        [question setQuestionObject:object];
                        if([question.section isEqualToString:@"buyer"]){
                            [self.listQuestionBuyer addObject:question];
                        } else if([question.section isEqualToString:@"seller"]){
                            [self.listQuestionSeller addObject:question];
                        }
                    };
                    
                    if(self.listQuestionBuyer.count==0 && self.listQuestionSeller.count==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No matching results found." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self clickedSearch:nil];
    return YES;
}

- (IBAction)clickedSearch:(id)sender {
    self.textSearch = self.tfSearch.text;
    [self.listQuestionBuyer removeAllObjects];
    [self.listQuestionSeller removeAllObjects];
    [self loadAllData];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
