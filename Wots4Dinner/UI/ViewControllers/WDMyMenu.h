//
//  WDMYMenu.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/21/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDMyMenu : UIViewController
@property (nonatomic, assign) BOOL isMyOrder;

@end
