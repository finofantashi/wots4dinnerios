//
//  SignInViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/12/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSignInViewController.h"
#import "CountryListDataSource.h"
#import "WDCountryTableViewCell.h"
#import "WDGetResetCodeViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDUserAPIController.h"
#import "NSString+IsValidEmail.h"
#import "WDUser.h"
#import "WDMainViewController.h"
#import "WDSourceConfig.h"
#import "WDUserDefaultsManager.h"
#import "UIColor+Style.h"

@interface WDSignInViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) NSArray *dataRows;
@property (strong, nonatomic) NSMutableArray *searchArray;
@property (strong, nonatomic) NSString *searchTextString;

@end

@implementation WDSignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupDismissKeyboard];
    [self setupSearch];
    self.btnSignin.layer.cornerRadius = self.btnSignin.frame.size.height/2; // this value vary as per your desire
    self.btnSignin.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setupDismissKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.delegate = self;
    
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [self.tfPassword resignFirstResponder];
    [self.tfPhoneNumber resignFirstResponder];
    [self showHideViewSearch:NO];
}

- (void)setupCountryList {
    self.tableView.layer.cornerRadius=5;
    CountryListDataSource *dataSource = [[CountryListDataSource alloc] init];
    _dataRows = [dataSource countries];
    
    //    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //    dispatch_group_t group = dispatch_group_create();
    //
    //    for (int i=0; i <_dataRows.count; i++) {
    //        NSString *country = [[_dataRows objectAtIndex:i] valueForKey:kCountryName];
    //        NSString *code = [[_dataRows objectAtIndex:i] valueForKey:kCountryCode];
    //        NSString *phone_prefix = [[_dataRows objectAtIndex:i] valueForKey:kCountryCallingCode];
    //
    //        NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:country,code,phone_prefix, nil] forKeys:[NSArray arrayWithObjects:@"country",@"code",@"phone_prefix",nil]];
    //
    //        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    //        manager.securityPolicy.allowInvalidCertificates = YES;
    //        dispatch_group_enter(group);
    //        NSLog(@"%@",[[WDSourceConfig config].apiKey stringByAppendingString:@"countryCodes"]);
    //        NSLog(@"%@",parameters);
    //
    //        [manager POST:[[WDSourceConfig config].apiKey stringByAppendingString:@"/countryCodes"] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    //
    //        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    //            dispatch_group_leave(group);
    //        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    //            dispatch_group_leave(group);
    //
    //        }];
    //    }
    
    [_tableView reloadData];
}

- (void)enableSignin {
    if(![self.lbCountry isEqual:@"Your country"]&&![self.lbAreaCode isEqual:@"Area code"]&&self.tfPhoneNumber.text.length!=0&&self.tfPassword.text.length!=0){
        self.btnSignin.enabled = YES;
        self.btnSignin.alpha = 1.0f;
    }else{
        self.btnSignin.enabled = NO;
        self.btnSignin.alpha = 0.5f;
    }
}

- (void)toggleTextFieldSecureEntry: (UITextField*) textField {
    BOOL isFirstResponder = textField.isFirstResponder; //store whether textfield is firstResponder
    
    if (isFirstResponder) [textField resignFirstResponder]; //resign first responder if needed, so that setting the attribute to YES works
    textField.secureTextEntry = !textField.secureTextEntry; //change the secureText attribute to opposite
    if (isFirstResponder) [textField becomeFirstResponder]; //give the field focus again, if it was first responder initially
    if(textField.secureTextEntry){
        [self.btnShowHide setImage:[UIImage imageNamed:@"ic_show"] forState:UIControlStateNormal];
    }else{
        [self.btnShowHide setImage:[UIImage imageNamed:@"ic_hiden"] forState:UIControlStateNormal];
        
    }
}

- (BOOL)invalidate {
    if([self.lbAreaCode.text length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Your country is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    //    if([self.tfPassword.text length] < 6) {
    //        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Password must include at least six characters. Please try again." preferredStyle:LEAlertControllerStyleAlert];
    //        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
    //            // handle default button action
    //        }];
    //        [alertController addAction:defaultAction];
    //        [self presentAlertController:alertController animated:YES completion:nil];
    //        return NO;
    //    }
    return YES;
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"WDCountryTableViewCell";
    
    WDCountryTableViewCell *cell = (WDCountryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryCode]];
    
    cell.ivFlag.image = [UIImage imageNamed:imagePath];
    cell.lbCountry.text = [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryName];
    cell.lbCode.text = [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryCallingCode];
    
    return cell;
}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.lbCountry.text = [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryName];
    self.lbAreaCode.text = [[self.searchArray objectAtIndex:indexPath.row] valueForKey:kCountryCallingCode];
    [self showHideViewSearch:NO];
    [self enableSignin];
}

#pragma mark - Search methods

- (void) setupSearch{
    self.tfSearch.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_search"]];
    
    //set the selector to the text field in order to change its value when edited
    [self.tfSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //here you set up the methods to search array and reloading the tableview
    [self setupCountryList];
    [self updateSearchArray];
}

//update seach method where the textfield acts as seach bar
-(void)updateSearchArray {
    if (self.searchTextString.length != 0) {
        self.searchArray = [NSMutableArray array];
        for (NSDictionary* item in self.dataRows) {
            if ([[[item objectForKey:@"name"] lowercaseString] rangeOfString:[self.searchTextString lowercaseString]].location != NSNotFound) {
                [self.searchArray addObject:item];
            }
        }
    } else {
        self.searchArray = [[NSMutableArray alloc] initWithArray:self.dataRows];
    }
    
    [self.tableView reloadData];
}

- (IBAction)textFieldDidChange:(id)sender {
    self.searchTextString = self.tfSearch.text;
    [self updateSearchArray];
}

- (void) showHideViewSearch:(BOOL)isShow {
    if(isShow){
        self.tableView.hidden = NO;
        self.viewSearch.hidden = NO;
        self.lbCountry.hidden = YES;
    }else{
        self.lbCountry.hidden = NO;
        self.tableView.hidden = YES;
        self.viewSearch.hidden = YES;
        [self.tfSearch setText:@""];
        [self textFieldDidChange:nil];
    }
}

#pragma mark - UITextField Delegate methods

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField==self.tfPhoneNumber){
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        return newLength <= 15 || returnKey;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.tfPhoneNumber) {
        [self.tfPassword becomeFirstResponder];
    }
    
    if (textField == self.tfPassword) {
        [self.tfPassword resignFirstResponder];
    }
    
    return YES;
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:self.tableView]) {
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }else if([touch.view isDescendantOfView:self.lbCountry.superview]){
        if(self.tableView.hidden == NO){
            return NO;
        }
        [self clickedCountry:nil];
        return NO;
    }
    
    return YES;
}

#pragma mark IBAction

- (IBAction)touchDown:(id)sender {
    [self.btnSignin setBackgroundColor:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0]];
    
}

- (IBAction)clickedSignin:(id)sender {
    [self.btnSignin setBackgroundColor:[UIColor colorFromHexString:@"e7663f" withAlpha:1.0]];
    
    [self signinAPI];
}

- (IBAction)textFieldDidChaned:(id)sender {
    [self enableSignin];
}

- (IBAction)clickedShowHide:(id)sender {
    [self toggleTextFieldSecureEntry:self.tfPassword];
}

- (IBAction)clickedForgot:(id)sender {
    WDGetResetCodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDGetResetCodeViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)clickedCountry:(id)sender {
    [self.tfPassword resignFirstResponder];
    [self.tfPhoneNumber resignFirstResponder];
    
    if (self.tableView.hidden == YES) {
        [self showHideViewSearch:YES];
    }else{
        [self showHideViewSearch:NO];
    }
}

#pragma mark API

- (void) signinAPI {
    if([self invalidate]){
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        if(networkStatus == NotReachable){
            LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
            LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                // handle default button action
            }];
            [alertController addAction:defaultAction];
            [self presentAlertController:alertController animated:YES completion:nil];
        }else{
            [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
            //Call Api
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"+ "];
            NSString *phonePrefix = [[self.lbAreaCode.text componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
            NSString *phoneNumber = [[[self.tfPhoneNumber.text trim] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
            if(phoneNumber.length>0&&[phoneNumber characterAtIndex:0] == '0'){
                phoneNumber = [phoneNumber stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
            }
            NSString *userName = [NSString stringWithFormat:@"%@%@",phonePrefix,phoneNumber];
            
            [[WDUserAPIController sharedInstance] signin:userName password:self.tfPassword.text completion:^(NSString *errorString) {
                if(errorString){
                    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
                    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                        // handle default button action
                    }];
                    [alertController addAction:defaultAction];
                    [self presentAlertController:alertController animated:YES completion:nil];
                    
                }else{
                    [self getUserAPI];
                }
            }];
        }
    }
}

- (void) getUserAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //Call Api
        [[WDUserAPIController sharedInstance] getUserInformation:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                [WDUserDefaultsManager setValue:[NSNumber numberWithBool:YES] forKey:kSessionSignIn];
                [[WDUser sharedInstance] setPassword:self.tfPassword.text];
                UIWindow *window = UIApplication.sharedApplication.delegate.window;
                window.rootViewController = [WDMainViewController sharedInstance];
            }
        }];
    }
}

@end
