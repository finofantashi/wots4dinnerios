//
//  WDTodayMenuViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDTodayMenuViewController.h"
#import "WDFoodTypeCell.h"
#import "WDDishCell.h"
#import "WDDishViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDFoodType.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDUserTemp.h"
#import "WDDishDetailTableViewController.h"
#import "WDSourceConfig.h"
#import "PulsingHaloLayer.h"
#import "WDDishSearch.h"
#import "NSString+StringAdditions.h"
#import "WDMainViewController.h"

@interface WDTodayMenuViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate>
@property (nonatomic, weak) PulsingHaloLayer *halo;
@property (nonatomic, strong) NSMutableArray* foodTypes;
@property (nonatomic, strong) WDDishSearch* dish;
@property (nonatomic, assign) NSInteger total;

@end

@implementation WDTodayMenuViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.foodTypes = [[NSMutableArray alloc] init];
  self.dish = [[WDDishSearch alloc] init];
  PulsingHaloLayer *layer = [PulsingHaloLayer layer];
  self.halo = layer;
  [self.ivSearch.superview.layer insertSublayer:self.halo below:self.ivSearch.layer];
  
  //[self setupInitialValues];
  self.halo.haloLayerNumber = 5;
  self.halo.radius = 100;
  self.halo.animationDuration = 5;
  [self.halo setBackgroundColor:[UIColor orangeColor].CGColor];
  [self.halo start];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(skipSearch) name:@"SkipSearch" object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popular:) name:@"Popular" object:nil];
  
  
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  if(self.isSkip){
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showMenu" object:self];
    self.isSkip = NO;
  }else{
    [self getTodayMenuAPI];
  }
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  self.halo.position = self.ivSearch.center;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDatasource Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  if(self.foodTypes.count==0){
    self.total = 0;
    return 0;
  }
  
  if(self.foodTypes.count%2!=0&&(self.dish&&self.dish.dishName)){
    self.total = self.foodTypes.count + 2;
    return self.total;
  }
  
  if((self.dish&&self.dish.dishName)||self.foodTypes.count%2!=0){
    self.total = self.foodTypes.count + 1;
    return self.total;
  }
  
  self.total = self.foodTypes.count;
  return self.total;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell;
  if((self.foodTypes.count%2!=0&&indexPath.row<=self.foodTypes.count)||(self.foodTypes.count%2==0&&indexPath.row<self.foodTypes.count)){
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WDFoodTypeCell" forIndexPath:indexPath];
    if(indexPath.row==self.foodTypes.count&&self.foodTypes.count%2!=0){
      ((WDFoodTypeCell*)cell).viewBox.hidden = NO;
      ((WDFoodTypeCell*)cell).lbDishCount.hidden = YES;
    }else{
      ((WDFoodTypeCell*)cell).viewBox.hidden = YES;
      ((WDFoodTypeCell*)cell).lbDishCount.hidden = NO;
      WDFoodType *foodType = [self.foodTypes objectAtIndex:indexPath.row];
      //get image name and assign
      NSString *imageURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,foodType.foodTypeImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
      [((WDFoodTypeCell*)cell).ivFoodType sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                            placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
      ((WDFoodTypeCell*)cell).lbFoodType.text = foodType.foodTypeName;
      NSString *dishCount;
      if(foodType.dishCount==1){
        dishCount = [NSString stringWithFormat:@"1 dish"];
      }else {
        dishCount = [NSString stringWithFormat:@"%ld dishes", foodType.dishCount];
      }
      ((WDFoodTypeCell*)cell).lbDishCount.text = dishCount;
      //            //set offset accordingly
      //            CGFloat yOffset = ((self.parallaxCollectionView.contentOffset.y - cell.frame.origin.y) / collectionView.frame.size.width/2) * IMAGE_OFFSET_SPEED;
      //            ((WDFoodTypeCell*)cell).imageOffset = CGPointMake(0.0f, yOffset);
    }
  }else {
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WDDishCell" forIndexPath:indexPath];
    //get image name and assign
    NSString *imageURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,self.dish.dishThumb] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [((WDDishCell*)cell).ivDish sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                  placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
    ((WDDishCell*)cell).lbNameDish.text = self.dish.dishName;
    ((WDDishCell*)cell).lbDescription.text = [NSString stringWithFormat:@"Sold by %@",self.dish.kitchenName];
    
    ((WDDishCell*)cell).lbPrice.text = [[NSString string] formatCurrency:self.dish.currencyCode symbol:self.dish.currencySymbol price:self.dish.price];
  }
  
  return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  if(self.total == indexPath.row+1){
    if(self.dish&&self.dish.dishName){
      return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.width/2);
    }
  }
  return CGSizeMake((collectionView.frame.size.width/2)-2, (collectionView.frame.size.width/2)-2);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
  
  return 2.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
  return 4.0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
  
  if([[collectionView cellForItemAtIndexPath:indexPath] isKindOfClass:[WDFoodTypeCell class]]&&((WDFoodTypeCell*)cell).viewBox.hidden){
    WDFoodType *foodType = [self.foodTypes objectAtIndex:indexPath.row];
    if(foodType.dishCount>0){
      WDDishViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishViewController"];
      NSMutableArray *foodTypeIDs = [[NSMutableArray alloc] init];
      [foodTypeIDs addObject:[NSNumber numberWithInteger:foodType.foodTypeID]];
      viewController.foodTypeIDs = foodTypeIDs;
      viewController.isFrom = 2;
      viewController.foodTypeName = foodType.foodTypeName;
      [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    }else{
      LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Don't Panic!" message:@"We have not launched in your area just yet!\nWe will notify you as soon as we are ready to go in your town, city and country.\nMeanwhile get ready to Search! Choose! & Deliver!" preferredStyle:LEAlertControllerStyleAlert];
      LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        // handle default button action
      }];
      [alertController addAction:defaultAction];
      [self presentAlertController:alertController animated:YES completion:nil];
      
    }
  }else if([[collectionView cellForItemAtIndexPath:indexPath] isKindOfClass:[WDDishCell class]]){
    WDDishDetailTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishDetailTableViewController"];
    viewController.dishID = self.dish.dishID;
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
  }
}
#pragma mark - UIScrollViewdelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  //    for(UICollectionViewCell *view in self.parallaxCollectionView.visibleCells) {
  //        CGFloat yOffset = ((self.parallaxCollectionView.contentOffset.y - view.frame.origin.y) / view.frame.size.width/2) * IMAGE_OFFSET_SPEED;
  //
  //        if([view isKindOfClass:[WDFoodTypeCell class]]){
  //            ((WDFoodTypeCell*)view).imageOffset = CGPointMake(0.0f, yOffset);
  //
  //        }else if([view isKindOfClass:[WDDishCell class]]){
  //            ((WDDishCell*)view).imageOffset = CGPointMake(0.0f, yOffset);
  //        }
  //    }
}

- (void) getTodayMenuAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getTodayMenu:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self.foodTypes removeAllObjects];
          self.dish = [[WDDishSearch alloc] init];
          NSDictionary *dataFoodType = [[responseObject valueForKey:@"data"] valueForKey:@"food_types"];
          for (NSDictionary *object in dataFoodType) {
            WDFoodType *foodType = [[WDFoodType alloc] init];
            [foodType setFoodTypeObject:object];
            [self.foodTypes addObject:foodType];
          };
          
          NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"dishes"];
          for (NSDictionary *object in dataDish) {
            [self.dish setDishObject:object];
          };
          [self.parallaxCollectionView reloadData];
        }
      }
    }];
  }
}

- (void)skipSearch {
  self.isSkip = YES;
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)popular:(NSNotification *)notification{
  WDDishDetailTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishDetailTableViewController"];
  viewController.dishID = [notification.userInfo[@"dishID"] integerValue];
  [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
