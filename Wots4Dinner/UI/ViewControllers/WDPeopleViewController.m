//
//  WDPeopleViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDPeopleViewController.h"
#import "WDNewMessageTableViewCell.h"
#import <STPopup/STPopup.h>
#import "WDReceiver.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDSourceConfig.h"

@interface WDPeopleViewController ()

@end

@implementation WDPeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(!self.listReceiver){
        self.listReceiver = [[NSMutableArray alloc] init];

    }
    self.contentSizeInPopup = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height/2);
    self.navigationController.navigationBar.barTintColor = [UIColor orangeColor];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listReceiver.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDNewMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WDNewMessageTableViewCell" forIndexPath:indexPath];
     WDReceiver *receiver = self.listReceiver[indexPath.row];
    cell.lbName.text = [NSString stringWithFormat:@"%@ %@",receiver.firstName, receiver.lastName];

    //get image name and assign
    cell.ivAvatar.layer.cornerRadius = cell.ivAvatar.frame.size.width / 2;
    cell.ivAvatar.layer.masksToBounds = YES;
    cell.ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
    cell.ivAvatar.layer.borderWidth = 2.0f;
    [cell.ivAvatar setNeedsDisplay];
    
    NSString *imageURL = [[NSString stringWithFormat:@"%@%@", kImageDomain,receiver.avatar] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [cell.ivAvatar sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                                placeholderImage:[UIImage imageNamed:@"empty_avatar"]];
    return cell;
}

- (void)backgroundViewDidTap {
    [self.popupController dismissWithCompletion:nil];
}
@end
