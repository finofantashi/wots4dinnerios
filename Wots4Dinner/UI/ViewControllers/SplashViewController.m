//
//  SplashViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/29/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "SplashViewController.h"
#import "UIColor+Style.h"
#import "BIZCircularTransitionHandler.h"
#import "WDUserDefaultsManager.h"
#import "WDSourceConfig.h"
#import "WDMainViewController.h"
#import "WDUser.h"

@interface SplashViewController ()
@property (nonatomic, strong) BIZCircularTransitionHandler *circularTransitionHandler;
@property (nonatomic, assign) BOOL isLoad;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    //[self startAnimation];
    self.ivLogoSplash.alpha = 0.0f;
    [UIView animateWithDuration:2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.ivLogoSplash.alpha = 1.0f;
    } completion:^(BOOL finished){
        [UIView animateWithDuration:2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.ivLogoSplash.alpha = 1.0f;
        } completion:^(BOOL finished){
            _isLoad = YES;
            CGPoint centerOfButton = [self.viewCenter.superview convertPoint:self.viewCenter.center toView:nil];
            if(![WDUserDefaultsManager valueForKey:kFirstLaucher]){
                [WDUserDefaultsManager setValue:@YES forKey:kFirstLaucher];
                UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDIntroViewController"];
                viewController.modalPresentationStyle = UIModalPresentationFullScreen;
                [self.circularTransitionHandler transitionWithDestinationViewController:viewController initialTransitionPoint:centerOfButton];
                [self dismissViewControllerAnimated:NO completion:nil];
                [self presentViewController:viewController animated:YES completion:^{
                }];
                
            }else{
                UINavigationController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDNavigationFirstLaucher"];
                viewController.modalPresentationStyle = UIModalPresentationFullScreen;
                [self.circularTransitionHandler transitionWithDestinationViewController:viewController initialTransitionPoint:centerOfButton];
                [self dismissViewControllerAnimated:NO completion:nil];
                [self presentViewController:viewController animated:YES completion:^{
                 
                }];
            }
        }];
    }];
    
    self.circularTransitionHandler = [[BIZCircularTransitionHandler alloc] init];
}

- (void) startAnimation {
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.fromValue = @(0.0);
    scaleAnimation.toValue = @1;
    
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.fromValue = @(0.0);
    opacityAnimation.toValue = @1.0;
    
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    NSArray *animations = @[scaleAnimation, opacityAnimation];
    animationGroup.duration = 0.8;
    animationGroup.delegate = self;
    
    animationGroup.animations = animations;
    self.view.layer.position = self.view.center;
    [self.viewCircle.layer addAnimation:animationGroup forKey:@"pulse"];
    
    CGRect forkLeftFrame = _ivFork.frame;
    forkLeftFrame.origin.x = -500;
    _ivFork.frame = forkLeftFrame;
    
    CGRect spoonFrame = _ivSpoon.frame;
    spoonFrame.origin.x = 500;
    _ivSpoon.frame = spoonFrame;
    
    CGRect kniferFrame = _ivKnifer.frame;
    kniferFrame.origin.y = 500;
    _ivKnifer.frame = kniferFrame;
    
    [UIView animateWithDuration:1.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect basketTopFrame = _ivFork.frame;
        basketTopFrame.origin.x = 27;
        _ivFork.frame = basketTopFrame;
        self.constraintLeftFork.constant = 27;
    } completion:^(BOOL finished){
        
    }];
    [UIView animateWithDuration:1.5 delay:1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect basketTopFrame = _ivSpoon.frame;
        basketTopFrame.origin.x = 26;
        _ivSpoon.frame = basketTopFrame;
        self.constraintLeftSpoon.constant = 26;
        
    } completion:^(BOOL finished){
        _ivSpoon.image = [UIImage imageNamed:@"ic_spoon_color"];
    }];
    
    [UIView animateWithDuration:1.5 delay:2 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGRect basketTopFrame = _ivKnifer.frame;
        basketTopFrame.origin.y = 30;
        _ivKnifer.frame = basketTopFrame;
        self.constraintTopKnifer.constant = 30;
        self.constraintBottomKnifer.constant = 15;
        
    } completion:^(BOOL finished){
        _ivKnifer.image = [UIImage imageNamed:@"ic_knifer_color"];
    }];
    
    [self.hintAnimation stop];
    self.hintAnimation = [[MFLHintLabel alloc]
                          createHintAnimationForLabel:self.lbName
                          beginningAt:CGPointMake(40, 150)
                          displayingAt:CGPointMake(self.view.center.x/5, self.view.center.y+160) endingAt:CGPointMake(40, self.view.frame.size.height+200) inTargetView:self.view];
    
    [self.hintAnimation setAnimateOnType:kMFLAnimateOnImplode];
    [self.hintAnimation setAnimateOffType:kMFLAnimateOffNone];
    
    [self.hintAnimation setDuration:3];
    
    [self.hintAnimation setPhaseDelayTimeIn:0];
    [self.hintAnimation setPhaseDelayTimeOut:2];
    
    [self.hintAnimation setCharactersToMoveSimultaneouslyIn:self.hintAnimation.stringToDisplay.length/3];
    [self.hintAnimation setCharactersToMoveSimultaneouslyOut:4];
    
    [self.hintAnimation prepareToRun];
    [self.hintAnimation runWithCompletion:^{
        _isLoad = YES;
        CGPoint centerOfButton = [self.viewCenter.superview convertPoint:self.viewCenter.center toView:nil];
        if(![WDUserDefaultsManager valueForKey:kFirstLaucher]){
            [WDUserDefaultsManager setValue:@YES forKey:kFirstLaucher];
            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDIntroViewController"];
            [self.circularTransitionHandler transitionWithDestinationViewController:viewController initialTransitionPoint:centerOfButton];
            viewController.modalPresentationStyle = UIModalPresentationFullScreen;
            [self dismissViewControllerAnimated:NO completion:nil];
            [self presentViewController:viewController animated:YES completion:^{
            }];
            
        }else{
            UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDNavigationFirstLaucher"];
            viewController.modalPresentationStyle = UIModalPresentationFullScreen;
            [self.circularTransitionHandler transitionWithDestinationViewController:viewController initialTransitionPoint:centerOfButton];
            [self dismissViewControllerAnimated:NO completion:nil];
            [self presentViewController:viewController animated:YES completion:^{
            }];
        }
    }];
}

@end
