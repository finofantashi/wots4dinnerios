//
//  WDReviewTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/27/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDReviewTableViewCell.h"

@implementation WDReviewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickedAvatar:(id)sender {
    if([self.delegate respondsToSelector:@selector(clickedAvatar:)]) {
        [self.delegate clickedAvatar:self.user];
    }
}
@end
