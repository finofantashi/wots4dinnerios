//
//  WDPromotionalVideo.h
//  MealsAround
//
//  Created by Giau Le on 8/22/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDCertifiedTableVC : UITableViewController
@property (weak, nonatomic) IBOutlet UIButton *btnUploadFile;
@property (weak, nonatomic) IBOutlet UIImageView *ivUploadFile;
@property (weak, nonatomic) IBOutlet UITextField *tfNotes;

- (IBAction)clickedUploadFile:(id)sender;

@end
