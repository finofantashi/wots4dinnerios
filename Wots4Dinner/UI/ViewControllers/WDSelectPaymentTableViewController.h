//
//  WDSelectPaymentTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDSelectPaymentTableViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray *listCart;

- (IBAction)clickedCreditCard:(id)sender;
- (IBAction)clickedPaypal:(id)sender;

@end
