//
//  WDDishOrderTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDDishOrderTableViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDSourceConfig.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "LEAlertController.h"
#import "Reachability.h"
#import "UIColor+Style.h"
#import "WDOrderAPIController.h"
#import "WDCartViewController.h"
#import "WDMainViewController.h"
#import "NSString+StringAdditions.h"

@implementation WDDishOrderTableViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.estimatedRowHeight = 500;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self loadData];
    
    self.btnCheckOut.layer.cornerRadius = self.btnCheckOut.frame.size.height/2; // this value vary as per your desire
    self.btnCheckOut.layer.borderWidth = 1.0f;
    self.btnCheckOut.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
    self.btnCheckOut.clipsToBounds = YES;
    
    self.btnAddCart.layer.cornerRadius = self.btnAddCart.frame.size.height/2; // this value vary as per your desire
    self.btnAddCart.layer.borderWidth = 1.0f;
    self.btnAddCart.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
    self.btnAddCart.clipsToBounds = YES;
    self.title = @"Make Your Selection";
    self.btnPlus.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0){
        return 180;
    }else if(indexPath.row==3){
        return 68;
    }else if(indexPath.row==1){
        return 120;
    }
    return 54;
}

#pragma mark - Private

- (void)enableButtonPlus:(BOOL)isEnable {
    if(isEnable){
        [self.btnPlus setImage:[UIImage imageNamed:@"ic_plus_enable"] forState:UIControlStateNormal];
        self.btnPlus.userInteractionEnabled = YES;
    }else{
        [self.btnPlus setImage:[UIImage imageNamed:@"ic_plus_disable"] forState:UIControlStateNormal];
        self.btnPlus.userInteractionEnabled = NO;
    }
}

- (void)enableButtonMinus:(BOOL)isEnable {
    if(isEnable){
        [self.btnMinus setImage:[UIImage imageNamed:@"ic_minus_enable"] forState:UIControlStateNormal];
        self.btnMinus.userInteractionEnabled = YES;
    }else{
        [self.btnMinus setImage:[UIImage imageNamed:@"ic_minus_disable"] forState:UIControlStateNormal];
        self.btnMinus.userInteractionEnabled = NO;
    }
}

- (void) setData:(WDDish*)dish {
    //Dish detail
    NSString *imageURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,dish.dishImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [self.ivDish sd_setImageWithURL:[NSURL URLWithString:imageURL]
                   placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
    self.nameDish.text = dish.dishName;
    if(dish.quantity > 0){
        self.lbQuantityLeft.text = [NSString stringWithFormat:@"%ld", self.dish.quantity - [self.lbQuantity.text integerValue]];
    }else{
        self.lbTitleQtyLeft.text = @"Sold Out";
        [self.btnAddCart setEnabled:NO];
        [self.btnCheckOut setEnabled:NO];
        
        [self.btnAddCart setTitleColor:[UIColor colorFromHexString:@"cccccc" withAlpha:1.0f] forState:UIControlStateNormal];
        [self.btnCheckOut setTitleColor:[UIColor colorFromHexString:@"cccccc" withAlpha:1.0f] forState:UIControlStateNormal];

        self.btnAddCart.layer.borderColor = [UIColor colorFromHexString:@"cccccc" withAlpha:1.0f].CGColor;
        self.btnCheckOut.layer.borderColor = [UIColor colorFromHexString:@"cccccc" withAlpha:1.0f].CGColor;

    }
    
    self.lbPrice.text = [[NSString string] formatCurrency:dish.currencyCode symbol:dish.currencySymbol price:dish.price];

    self.lbTotalPrice.text = [[NSString string] formatCurrency:dish.currencyCode symbol:dish.currencySymbol price:dish.price*[self.lbQuantity.text integerValue]];
    
    if(dish.quantity>0){
        if([self.lbQuantity.text integerValue]>=1&&[self.lbQuantity.text integerValue] < dish.quantity){
            [self enableButtonPlus:YES];
        }else {
            [self enableButtonPlus:NO];
        }
        
        if([self.lbQuantity.text integerValue] <= 1){
            [self enableButtonMinus:NO];
        }else{
            [self enableButtonMinus:YES];
        }
    }else{
        self.lbQuantity.text = 0;
    }

}

- (void)loadData {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getDishDetail:self.dishID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"dish"];
                    self.dish = [[WDDish alloc] init];
                    [self.dish  setDishObject:dataDish];
                    [self setData:self.dish ];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
        [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
}

- (IBAction)clickedCheckOut:(id)sender {
    [self addCartAPI:YES];
}

- (IBAction)clickedAddCart:(id)sender {
    if([self.lbQuantity.text integerValue]>=1){
        [self addCartAPI:NO];
    }
}

- (void)clickedPlus:(id)sender {
    if([self.lbQuantity.text integerValue] < self.dish.quantity){
        self.lbQuantity.text = [NSString stringWithFormat:@"%ld", [self.lbQuantity.text integerValue] +1];
        
        self.lbTotalPrice.text = [[NSString string] formatCurrency:self.dish.currencyCode symbol:self.dish.currencySymbol price:self.dish.price*[self.lbQuantity.text integerValue]];
        self.lbQuantityLeft.text = [NSString stringWithFormat:@"%ld", self.dish.quantity - [self.lbQuantity.text integerValue]];
        if([self.lbQuantity.text integerValue] < self.dish.quantity){
            [self enableButtonPlus:YES];
        }else {
            [self enableButtonPlus:NO];
        }
        if([self.lbQuantity.text integerValue]>1){
            [self enableButtonMinus:YES];
        }
    }
}

- (void)clickedMinus:(id)sender {
    if([self.lbQuantity.text integerValue]>1){
        self.lbQuantity.text = [NSString stringWithFormat:@"%ld", [self.lbQuantity.text integerValue] -1];
        self.lbQuantityLeft.text = [NSString stringWithFormat:@"%ld", self.dish.quantity - [self.lbQuantity.text integerValue]];
        
        self.lbTotalPrice.text = [[NSString string] formatCurrency:self.dish.currencyCode symbol:self.dish.currencySymbol price:self.dish.price*[self.lbQuantity.text integerValue]];
        
        if([self.lbQuantity.text integerValue] <= 1){
            [self enableButtonMinus:NO];
        }
    }
}

#pragma mark API

- (void) addCartAPI:(BOOL)isCheckOut {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] addCart:[NSNumber numberWithInteger:self.dishID] quantity:[NSNumber numberWithInteger:[self.lbQuantity.text integerValue]] completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                if([[responseObject valueForKey:@"success"] integerValue]!=0){
                    if(isCheckOut){
                        WDCartViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDCartViewController"];                        [self.navigationController pushViewController:viewController animated:YES];
                        
                    }else{
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Success" message:[NSString stringWithFormat:@"Added successfully!"] preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                    }
                }else{
                    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:[responseObject valueForKey:@"message"] preferredStyle:LEAlertControllerStyleAlert];
                    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                        // handle default button action
                    }];
                    [alertController addAction:defaultAction];
                    [self presentAlertController:alertController animated:YES completion:nil];
                }
            }
        }];
    }
}


@end
