//
//  WDSearchViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSearchViewController.h"
#import "WDSearchTableViewCell.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "WDFoodType.h"
#import "AHTag.h"
#import "WDFoodTypeTagTableViewCell.h"
#import "WDDishViewController.h"
#import "WDMainViewController.h"

@implementation WDSearchViewController {
    GMSPlacesClient *_placesClient;
    CLLocationCoordinate2D coordinateLocation;
    long  distance;
    long priceFrom;
    long priceTo;
    long maxPrice;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    maxPrice = 200;
    [self getListFoodTypeAPI];
    self.btnSearch.layer.cornerRadius = self.btnSearch.frame.size.height/2; // this value vary as per your desire
    self.btnSearch.clipsToBounds = YES;
    //[self.locationManager startUpdatingLocation];
    
    _resultsViewController = [[GMSAutocompleteResultsViewController alloc] init];
    _resultsViewController.delegate = self;

    _searchController = [[UISearchController alloc]
                         initWithSearchResultsController:_resultsViewController];
    _searchController.searchBar.delegate = self;
    _searchController.searchBar.placeholder = @"Location";
    _searchController.searchResultsUpdater = _resultsViewController;
    //_searchController.hidesNavigationBarDuringPresentation = NO;
    //_searchController.searchBar.barTintColor = [UIColor colorFromHexString:@"F77E51" withAlpha:1.0f];
//    [[UITextField appearanceWhenContainedInInstancesOfClasses:
//      @[[UISearchBar class]]] setDefaultTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    
    //self.extendedLayoutIncludesOpaqueBars = YES;
    //self.edgesForExtendedLayout = UIRectEdgeTop;
    
    // When UISearchController presents the results view, present it in
    // this view controller, not one further up the chain.
    self.definesPresentationContext = YES;
    _placesClient = [GMSPlacesClient sharedClient];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void) setupUI {
   
}

- (void) getCurrentLocation {
    [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        if (placeLikelihoodList != nil) {
            GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
            if (place != nil) {
                _searchController.searchBar.text = place.formattedAddress;
                coordinateLocation = place.coordinate;
            }else{
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your location." preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }
        }
    }];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
}

// Handle the user's selection.
- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
 didAutocompleteWithPlace:(GMSPlace *)place {
    self.searchController.active = NO;
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    _searchController.searchBar.text = place.formattedAddress;
    coordinateLocation.latitude = place.coordinate.latitude;
    coordinateLocation.longitude = place.coordinate.longitude;

}

- (void)resultsController:(GMSAutocompleteResultsViewController *)resultsController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictionsForResultsController:
(GMSAutocompleteResultsViewController *)resultsController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictionsForResultsController:
(GMSAutocompleteResultsViewController *)resultsController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

// Location Manager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"%@", [locations lastObject]);
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusDenied:
            [self getCurrentLocation];
            break;
    }
}

- (void)getDataSearch {
    NSMutableArray *foodTypeIDs = [[NSMutableArray alloc] init];
    WDFoodTypeTagTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    for (AHTag *object in cell.dataSource) {
        if([object.enabled isEqualToNumber:[NSNumber numberWithBool:YES]]){
            [foodTypeIDs addObject:[NSNumber numberWithInteger:object.foodTypeID]];
        }
    }
    if(coordinateLocation.longitude == 0||coordinateLocation.latitude == 0){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your location." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else if(foodTypeIDs.count==0){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"No food type select." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        WDDishViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDDishViewController"];
        viewController.lat = coordinateLocation.latitude;
        viewController.lng = coordinateLocation.longitude;
        viewController.distance = distance;
        viewController.priceFrom = priceFrom;
        viewController.priceTo = priceTo;
        viewController.foodTypeIDs = foodTypeIDs;
        viewController.isFrom = 1;
        [self.navigationController pushViewController:viewController animated:YES];
    }

}

#pragma mark - IBAction

- (IBAction)sliderValueChanged:(MARKRangeSlider* )sender {
    WDSearchTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    cell.lbDistance.text = [NSString stringWithFormat:@"%dkm",(int)sender.rightValue];
    distance = (int)sender.rightValue;

}

- (IBAction)priceSliderChanged:(MARKRangeSlider*)sender {
    WDSearchTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    cell.lbPriceMin.text = [NSString stringWithFormat:@"$%d",(int)sender.leftValue];
    cell.lbPriceMax.text = [NSString stringWithFormat:@"$%d",(int)sender.rightValue];
    priceFrom = (int)sender.leftValue;
    priceTo = (int)sender.rightValue;
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if(indexPath.row==0){
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDSearchTableViewCellLocation" forIndexPath:indexPath];
        [((WDSearchTableViewCell*)cell).searchView  addSubview:_searchController.searchBar];
        [_searchController.searchBar sizeToFit];
      self.resultsViewController.view.frame = CGRectMake(0, 0, 0, 0);
    }else if(indexPath.row==1){
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDSearchTableViewCellDistance" forIndexPath:indexPath];
        
        [((WDSearchTableViewCell*)cell).sliderDistance setMinValue:0 maxValue:80];
        [((WDSearchTableViewCell*)cell).sliderDistance setLeftValue:0 rightValue:40];
        ((WDSearchTableViewCell*)cell).sliderDistance.leftThumbView.hidden = YES;
        ((WDSearchTableViewCell*)cell).sliderDistance.leftThumbImage = nil;
        ((WDSearchTableViewCell*)cell).sliderDistance.minimumDistance = 0;
        ((WDSearchTableViewCell*)cell).sliderDistance.disableOverlapping = NO;

        [((WDSearchTableViewCell*)cell).sliderDistance addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        ((WDSearchTableViewCell*)cell).lbDistance.text = [NSString stringWithFormat:@"%dkm",(int)((WDSearchTableViewCell*)cell).sliderDistance.rightValue];
        distance = (int)((WDSearchTableViewCell*)cell).sliderDistance.rightValue;
        
    }else if(indexPath.row==2){
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDFoodTypeTagTableViewCell" forIndexPath:indexPath];
        NSMutableArray *tags = [[NSMutableArray alloc] init];
        BOOL isFirst = YES;
        for (WDFoodType *object in self.foodTypes) {
            AHTag *tag = [AHTag new];
            tag.foodTypeID = object.foodTypeID;
            tag.title = object.foodTypeName;
            tag.color = [UIColor colorFromHexString:@"F77E51" withAlpha:1.0f];
            if(isFirst){
                tag.enabled = @YES;
                isFirst = NO;
            }else{
                tag.enabled = @NO;
            }
            [tags addObject:tag];
        }
        if(tags&&tags.count>0){
            [((WDFoodTypeTagTableViewCell*)cell) updateData:tags];
        }
        
    }else if(indexPath.row==3){
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDSearchTableViewCellPrice" forIndexPath:indexPath];
        [((WDSearchTableViewCell*)cell).sliderPrice setMinValue:0 maxValue:maxPrice];
        [((WDSearchTableViewCell*)cell).sliderPrice setLeftValue:0 rightValue:(long)maxPrice/2];
        
        ((WDSearchTableViewCell*)cell).sliderPrice.minimumDistance = 1;
        

        ((WDSearchTableViewCell*)cell).lbPriceMin.text = [NSString stringWithFormat:@"$%d",0];
        ((WDSearchTableViewCell*)cell).lbPriceMax.text = [NSString stringWithFormat:@"$%ld",(long)maxPrice/2];
        priceFrom = 0;
        priceTo = (long)maxPrice/2;
        [((WDSearchTableViewCell*)cell).sliderPrice addTarget:self action:@selector(priceSliderChanged:) forControlEvents:UIControlEventValueChanged];
        
    }
    
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0){
        return 56;
    }else if(indexPath.row==1||indexPath.row==3){
        return 96;
    }
    return 250;
}

#pragma mark - IBAction

- (IBAction)clickedSearch:(id)sender {
    [self.btnSearch setBackgroundColor:[UIColor colorFromHexString:@"e7663f" withAlpha:1.0]];
    [self getDataSearch];
}

- (IBAction)touchDown:(id)sender {
    [self.btnSearch setBackgroundColor:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0]];
}

- (IBAction)clickedBack:(id)sender {
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
        [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
}

#pragma mark - API

- (void) getListFoodTypeAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getFoodTypes:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    maxPrice = [[[responseObject valueForKey:@"data"] valueForKey:@"max_price"] integerValue];
                    self.foodTypes = [[NSMutableArray alloc] init];
                    NSDictionary *dataFoodType = [[responseObject valueForKey:@"data"] valueForKey:@"food_types"];
                    for (NSDictionary *object in dataFoodType) {
                        WDFoodType *foodType = [[WDFoodType alloc] init];
                        [foodType setFoodTypeObject:object];
                        [self.foodTypes addObject:foodType];
                    };
                    [self.tableView reloadData];
                }
                
            }
        }];
    }
}

@end
