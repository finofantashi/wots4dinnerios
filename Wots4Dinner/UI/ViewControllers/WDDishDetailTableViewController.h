//
//  WDDishDetailViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/25/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDDish.h"
#import "WDADS.h"
#import "HCSStarRatingView.h"
#import "KASlideShow.h"
#import <CTVideoView.h>
#import "DZProgressIndicatorSlider.h"
#import "DZVideoPlayerViewControllerConfiguration.h"

@interface WDDishDetailTableViewController : UITableViewController
@property (nonatomic, assign) NSInteger dishID;

@property (weak, nonatomic) IBOutlet UIButton *btnMessage;
@property (weak, nonatomic) IBOutlet UIImageView *ivDish;
@property (weak, nonatomic) IBOutlet UILabel *nameDish;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantityLeft;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;
@property (nonatomic, strong) NSMutableArray *datasource;
@property (weak, nonatomic) IBOutlet UILabel *lbCategory;

@property (nonatomic, strong) WDDish *dish;
@property (nonatomic, strong) WDADS *ads;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatarSeller;
@property (weak, nonatomic) IBOutlet UILabel *lbNameSeller;
@property (weak, nonatomic) IBOutlet UILabel *lbReviews;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewRating;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderNow;
@property (weak, nonatomic) IBOutlet UILabel *lbAddress;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleQtyLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnFavoriteSeller;
@property (weak, nonatomic) IBOutlet UIButton *btnFavoriteDish;
@property (weak, nonatomic) IBOutlet KASlideShow *slideView;
@property (weak, nonatomic) IBOutlet UIStackView *viewCertified;
@property (weak, nonatomic) IBOutlet CTVideoView *viewVideo;

// Readonly properties
@property (readonly, nonatomic) NSTimeInterval currentPlaybackTime;
@property (readonly, nonatomic) NSTimeInterval currentPlayerItemDuration;
@property (readonly, nonatomic) NSTimeInterval availableDuration;
@property (strong, nonatomic) NSTimer *idleTimer;
@property (assign, nonatomic) BOOL isControlsHidden;
@property (assign, nonatomic) BOOL isSeeking;

@property (strong, nonatomic) DZVideoPlayerViewControllerConfiguration *configuration;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (weak, nonatomic) IBOutlet UIView *topToolbarView;
@property (weak, nonatomic) IBOutlet UIView *bottomToolbarView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *pauseButton;
@property (weak, nonatomic) IBOutlet DZProgressIndicatorSlider *progressIndicator;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingTimeLabel;

- (IBAction)clickedMap:(id)sender;
- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedFavoriteDish:(id)sender;
- (IBAction)clickedMoreInfo:(id)sender;
- (IBAction)clickedReviews:(id)sender;
- (IBAction)clickedLike:(id)sender;
- (IBAction)clickedOrder:(id)sender;
- (IBAction)clickedFavoriteSeller:(id)sender;
- (IBAction)clickedAddCart:(id)sender;
- (IBAction)clickedShare:(id)sender;
- (IBAction)clickedSendMessage:(id)sender;



@end
