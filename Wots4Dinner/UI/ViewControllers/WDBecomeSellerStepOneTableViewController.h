//
//  WDStepOneTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/12/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDownPicker.h"
#import "MVPlaceSearchTextField.h"
#import "WDPayoutMethod.h"

@interface WDBecomeSellerStepOneTableViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray* categories;
@property (nonatomic, strong) NSMutableArray* categoriesData;
@property (nonatomic, assign) NSInteger sellerID;
@property (nonatomic, assign) BOOL isBasic;
@property (nonatomic, strong) WDPayoutMethod* payoutMethod;


@property (weak, nonatomic) IBOutlet UITextField *pickerCategory;
@property (weak, nonatomic) IBOutlet UITextField *tfKitchenName;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *tfLocation;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPayout;
@property (nonatomic) DownPicker *picker;

- (IBAction)clickedPayout:(id)sender;
- (IBAction)clickedNext:(id)sender;
- (IBAction)clickedBack:(id)sender;

@end
