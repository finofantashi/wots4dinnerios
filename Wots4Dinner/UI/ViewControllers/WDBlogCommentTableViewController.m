//
//  WDBlogCommentTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBlogCommentTableViewController.h"
#import "WDBlogCommentTableViewCell.h"
#import "WDUserProfileViewController.h"
#import "WDSellerProfileViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDBlogAPIController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "WDUserTemp.h"
#import "WDSourceConfig.h"
#import "WDComment.h"

@interface WDBlogCommentTableViewController ()<WDBlogCommentTableViewCellDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray *listComment;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;

@end

@implementation WDBlogCommentTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.listComment = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    [self loadData:self.currentPage];
}

# pragma mark - Cell Setup

- (void)clickedAvatar:(WDUserTemp *)user {
    if(user.roleID == 2){
        WDSellerProfileViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDSellerProfileViewController"];
        viewController.user = user;
        [self.navigationController pushViewController:viewController animated:YES];
    }else{
        WDUserProfileViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDUserProfileViewController"];
        viewController.user = user;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)setUpCell:(WDBlogCommentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

# pragma mark - UITableViewControllerDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.currentPage == self.totalPages
        || self.totalItems == self.listComment.count) {
        return self.listComment.count;
    }
    return self.listComment.count + 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentPage != self.totalPages && indexPath.row == [self.listComment count] - 1 ) {
        [self loadData:++self.currentPage];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDBlogCommentTableViewCell";
    UITableViewCell *cell;
    if (indexPath.row == [self.listComment count]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:100];
        [activityIndicator startAnimating];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        WDComment *comment = self.listComment[indexPath.row];
        ((WDBlogCommentTableViewCell*)cell).user = comment.user;
        //Avatar
        ((WDBlogCommentTableViewCell*)cell).ivAvatar.layer.cornerRadius = 50 / 2;
        ((WDBlogCommentTableViewCell*)cell).ivAvatar.layer.masksToBounds = YES;
        ((WDBlogCommentTableViewCell*)cell).ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
        ((WDBlogCommentTableViewCell*)cell).ivAvatar.layer.borderWidth = 2.0f;
        [((WDBlogCommentTableViewCell*)cell).ivAvatar setNeedsDisplay];
        
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,comment.user.avatarURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        [((WDBlogCommentTableViewCell*)cell).ivAvatar sd_setImageWithURL:[NSURL URLWithString:stringURL] placeholderImage:[UIImage imageNamed:@"empty_avatar"]];
        if(comment.user.firstName&&comment.user.lastName){
            ((WDBlogCommentTableViewCell*)cell).lbName.text = [NSString stringWithFormat:@"%@ %@",comment.user.firstName,comment.user.lastName];
        }
        
        ((WDBlogCommentTableViewCell*)cell).lbLocation.text = comment.user.living;
        
        ((WDBlogCommentTableViewCell*)cell).lbDescription.text = comment.content;
        
        //Date Time
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:comment.createdAt==nil?@"":comment.createdAt];
        NSString *stringDate = [NSDateFormatter localizedStringFromDate:dateFromString
                                                              dateStyle:NSDateFormatterMediumStyle
                                                              timeStyle:NSDateFormatterNoStyle];
        if(!stringDate){
            stringDate = @"";
        }
        ((WDBlogCommentTableViewCell*)cell).lbDate.text = [NSString stringWithFormat:@"%@",stringDate];
        ((WDBlogCommentTableViewCell*)cell).delegate = self;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != [self.listComment count]) {
        WDComment *comment = self.listComment[indexPath.row];
        float sizeHeight = 88 + [self sizeOfMultiLineLabel:comment.content];
        return sizeHeight;
    }
    return 44;
}

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:14];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.view.frame.size.width-16;
    CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{
                                                                 NSFontAttributeName : aLabelFont
                                                                 }
                                                       context:nil].size;
    return labelSize.height;
}

- (void)loadData:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDBlogAPIController sharedInstance] getCommentByBlog:self.blogID page:(long)page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDComment *comment = [[WDComment alloc] init];
                        [comment setCommentObject:object];
                        [self.listComment addObject:comment];
                    };
                    
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==1){
                        self.title = [NSString stringWithFormat:@"%ld Comment", self.totalItems];
                    }else{
                        self.title = [NSString stringWithFormat:@"%ld Comments", self.totalItems];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

@end
