//
//  WDShippingInfoTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDShipping.h"

@interface WDShippingInfoTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIButton *btnCheckout;

@property (weak, nonatomic) IBOutlet UIButton *btnPickup;
@property (weak, nonatomic) IBOutlet UIButton *btnPayDelivery;

@property (weak, nonatomic) IBOutlet UITextField *tfName;
@property (weak, nonatomic) IBOutlet UITextField *tfShipping;
@property (weak, nonatomic) IBOutlet UITextField *tfPhone;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) IBOutlet UILabel *lbDelivery;
@property (weak, nonatomic) IBOutlet UILabel *lbCouponCode;
@property (weak, nonatomic) IBOutlet UILabel *orderTotal;
@property (weak, nonatomic) IBOutlet UILabel *cartTotal;

@property (nonatomic, strong) WDShipping *ship;
@property (nonatomic, strong) NSMutableArray *listCart;
@property (nonatomic, assign) float totalPrice;
@property (nonatomic, strong) NSString *strCartTotal;
@property (nonatomic, strong) NSString *strCoupon;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;
@property (assign, nonatomic) BOOL isDeliveryShip;

- (IBAction)clickedSave:(id)sender;
- (IBAction)clickedPickup:(id)sender;
- (IBAction)clickedPayDelivery:(id)sender;
- (IBAction)clickCheckout:(id)sender;
- (IBAction)clickedBack:(id)sender;

@end
