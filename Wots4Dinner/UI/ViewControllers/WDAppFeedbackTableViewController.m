//
//  WDAppFeedbackTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDAppFeedbackTableViewController.h"
#import "UIColor+Style.h"
#import "SDVersion.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <FCFileManager/FCFileManager.h>
#import "UIImage+ResizeMagick.h"
#import "WDAppFeedbackAPIController.h"

@interface WDAppFeedbackTableViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (nonatomic, strong) UIPopoverController *activityPopoverController;
@property (nonatomic, strong) UIImage *image;           // The image we'll be cropping

@end

@implementation WDAppFeedbackTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"App Feedback";
    [self setupData];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedScreenShot:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.ivScreen addGestureRecognizer:singleTap];
    [self.ivScreen setUserInteractionEnabled:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupData {
    self.lbDeviceModel.text = [SDVersion deviceNameString];
    self.lbIOSVersion.text = [[UIDevice currentDevice] systemVersion];
    self.lbAppBuild.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    self.lbAppVersion.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];

}

- (void)viewDidLayoutSubviews {
    [self drawDashedBorderAroundView:self.btnScreenShot];
}

- (void)drawDashedBorderAroundView:(UIView *)v {
    //border definitions
    CGFloat cornerRadius = 10;
    CGFloat borderWidth = 2;
    NSInteger dashPattern1 = 8;
    NSInteger dashPattern2 = 8;
    UIColor *lineColor = [UIColor colorFromHexString:@"cccccc" withAlpha:1.0];
    
    //drawing
    CGRect frame = v.bounds;
    
    CAShapeLayer *_shapeLayer = [CAShapeLayer layer];
    
    //creating a path
    CGMutablePathRef path = CGPathCreateMutable();
    
    //drawing a border around a view
    CGPathMoveToPoint(path, NULL, 0, frame.size.height - cornerRadius);
    CGPathAddLineToPoint(path, NULL, 0, cornerRadius);
    CGPathAddArc(path, NULL, cornerRadius, cornerRadius, cornerRadius, M_PI, -M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, frame.size.width - cornerRadius, 0);
    CGPathAddArc(path, NULL, frame.size.width - cornerRadius, cornerRadius, cornerRadius, -M_PI_2, 0, NO);
    CGPathAddLineToPoint(path, NULL, frame.size.width, frame.size.height - cornerRadius);
    CGPathAddArc(path, NULL, frame.size.width - cornerRadius, frame.size.height - cornerRadius, cornerRadius, 0, M_PI_2, NO);
    CGPathAddLineToPoint(path, NULL, cornerRadius, frame.size.height);
    CGPathAddArc(path, NULL, cornerRadius, frame.size.height - cornerRadius, cornerRadius, M_PI_2, M_PI, NO);
    
    //path is set as the _shapeLayer object's path
    _shapeLayer.path = path;
    CGPathRelease(path);
    
    _shapeLayer.backgroundColor = [[UIColor clearColor] CGColor];
    _shapeLayer.frame = frame;
    _shapeLayer.masksToBounds = NO;
    [_shapeLayer setValue:[NSNumber numberWithBool:NO] forKey:@"isCircle"];
    _shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    _shapeLayer.strokeColor = [lineColor CGColor];
    _shapeLayer.lineWidth = borderWidth;
    _shapeLayer.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:dashPattern1], [NSNumber numberWithInt:dashPattern2], nil];
    _shapeLayer.lineCap = kCALineCapRound;
    
    //_shapeLayer is added as a sublayer of the view, the border is visible
    [v.layer addSublayer:_shapeLayer];
    v.layer.cornerRadius = cornerRadius;
}

#pragma mark - Image Picker Delegate -

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    self.image = image;
    [self.ivScreen setImage:image];
    [self.ivScreen setHidden:NO];
    [self.btnScreenShot setHidden:YES];
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Bar Button Items -

- (IBAction)clickedSubmit:(id)sender {
    [self sendAppFeedbackAPI];
}

- (IBAction)clickedScreenShot:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *profileAction = [UIAlertAction actionWithTitle:@"Choose Photo"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              UIImagePickerController *profilePicker = [[UIImagePickerController alloc] init];
                                                              profilePicker.modalPresentationStyle = UIModalPresentationPopover;
                                                              profilePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                              profilePicker.allowsEditing = NO;
                                                              profilePicker.delegate = self;
                                                              profilePicker.preferredContentSize = CGSizeMake(512,512);
                                                              [self presentViewController:profilePicker animated:YES completion:nil];
                                                          }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction *action) {
                                                       
                                                   }];
    
    [alertController addAction:profileAction];
    [alertController addAction:cancel];
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) sendAppFeedbackAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        UIWindow * window = [[UIApplication sharedApplication] keyWindow];

        [MBProgressHUD showHUDAddedTo:window animated:YES];
        NSString *urlString = @"";
        NSString *stringFromDate = @"";
        if(self.image){
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyyddMMHHmm"];
            [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
            stringFromDate = [formatter stringFromDate:[NSDate date]];
            
            NSError *error;
            NSData *imageData = UIImagePNGRepresentation([self.image resizedImageWithMaximumSize:CGSizeMake(720, 720)]);
            urlString = [NSString stringWithFormat:@"%@/%@.png",[FCFileManager pathForDocumentsDirectory],stringFromDate];
            [FCFileManager writeFileAtPath:urlString content:imageData error:&error];
            if(error){
                [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:error.description preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                return;
            }
        }

        //Call Api
        [[WDAppFeedbackAPIController sharedInstance] sendAppFeedback:[urlString isEqualToString:@""]?nil:[NSURL URLWithString:urlString] nameFile:[NSString stringWithFormat:@"%@.png",stringFromDate] mimeType:@"" description:self.tvDescription.text completion:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:window animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                if(![urlString isEqualToString:@""]){
                    [FCFileManager removeItemAtPath:urlString error:nil];
                }
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Your Feedback is received, thank you!" preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }
        }];
    }
}

@end
