//
//  WDWebViewViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface WDWebViewViewController : UIViewController
@property (weak, nonatomic) IBOutlet WKWebView *webView;
@property (nonatomic, assign)  NSInteger fromURL;
@property (nonatomic, assign)  BOOL isFromFirstLaucher;

@end
