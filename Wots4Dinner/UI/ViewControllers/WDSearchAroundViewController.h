//
//  WDSearchAroundViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/20/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"

@import GoogleMaps;

@interface WDSearchAroundViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet GMSMapView *viewMap;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *tfLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchAround;
@property (weak, nonatomic) IBOutlet UIView *viewSearch;
@property (weak, nonatomic) IBOutlet UIView *viewPopular1;
@property (weak, nonatomic) IBOutlet UIView *viewPopular2;
@property (weak, nonatomic) IBOutlet UIView *viewPopular3;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPopular1;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPopular2;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPopular3;
@property (nonatomic, strong) NSMutableArray *listDish;
@property (weak, nonatomic) IBOutlet UILabel *lbPopular1;
@property (weak, nonatomic) IBOutlet UILabel *lbPopular2;
@property (weak, nonatomic) IBOutlet UILabel *lbPopular3;
@property (weak, nonatomic) IBOutlet UIView *viewPopular;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLbLeft;
@property (nonatomic, assign) NSInteger tag;

- (IBAction)clickedSearchAround:(id)sender;
- (IBAction)clickedSkip:(id)sender;
- (IBAction)clickedPopular:(id)sender;

@end
