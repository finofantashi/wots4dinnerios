//
//  WDSelectPaymentTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSelectPaymentTableViewController.h"
#import "WDCheckOutViewController.h"
#import "WDPaymentDetailTableViewController.h"

@interface WDSelectPaymentTableViewController ()

@end

@implementation WDSelectPaymentTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Select Payment Method";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction
- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedCreditCard:(id)sender {
    WDPaymentDetailTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDPaymentDetailTableViewController"];
    viewController.listCart = self.listCart;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedPaypal:(id)sender {
    WDCheckOutViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDCheckOutViewController"];
    viewController.listCart = self.listCart;
    [self.navigationController pushViewController:viewController animated:YES];
}
@end
