//
//  WDDishTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCSStarRatingView/HCSStarRatingView.h>

@protocol WDDishTableViewCellDelegate <NSObject>

- (void)clickedFavorite:(NSInteger) wishListId;

@end

@interface WDDishTableViewCell : UITableViewCell

@property (nonatomic, strong, readwrite) IBOutlet UIImageView *ivDish;
@property (weak, nonatomic) IBOutlet UILabel *lbNameDish;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbSoldBy;
@property (weak, nonatomic) IBOutlet UILabel *lbDistance;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIView *viewDistance;

@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;
@property (assign, nonatomic) NSInteger wishListId;
@property (assign, nonatomic) id <WDDishTableViewCellDelegate> dishDelegate;

- (IBAction)clickedFavorite:(id)sender;
- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view;

@end
