//
//  WDOurBlogViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDOurBlogViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
