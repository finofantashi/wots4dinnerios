//
//  WDMyMenuTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/21/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMyMenuTableViewCell.h"

@implementation WDMyMenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)clickedEdit:(id)sender {
    if([self.myMenuDelegate respondsToSelector:@selector(clickedEdit:)]) {
        [self.myMenuDelegate clickedEdit:self.dishId];
    }
}

- (IBAction)clickedActivate:(id)sender {
    if([self.myMenuDelegate respondsToSelector:@selector(clickedActived:)]) {
        [self.myMenuDelegate clickedActived:self.dish];
    }
}

- (IBAction)clickedDelete:(id)sender {
    if([self.myMenuDelegate respondsToSelector:@selector(clickedDelete:)]) {
        [self.myMenuDelegate clickedDelete:self.dish];
    }
}

- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view {
    CGRect rectInSuperview = [tableView convertRect:self.frame toView:view];
    
    float distanceFromCenter = CGRectGetHeight(view.frame)/2 - CGRectGetMinY(rectInSuperview);
    float difference = CGRectGetHeight(self.ivDish.frame) - CGRectGetHeight(self.frame);
    float move = (distanceFromCenter / CGRectGetHeight(view.frame)) * difference;
    
    CGRect imageRect = self.ivDish.frame;
    imageRect.origin.y = -(difference/2)+move;
    self.ivDish.frame = imageRect;
}
@end
