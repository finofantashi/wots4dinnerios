//
//  WDPopupRatingViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HCSStarRatingView/HCSStarRatingView.h>
#import "WDUserTemp.h"

@protocol WDPopupRatingViewControllerDelegate <NSObject>
@optional
- (void)dismissPopup;

@end

@interface WDPopupRatingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbCategory;

@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;
@property (strong, nonatomic) WDUserTemp *user;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextView *tvDish;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewRateThisSeller;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *viewRateSeller;
@property (weak, nonatomic) IBOutlet UITextView *tfComment;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIView *viewRating;
@property (weak, nonatomic) IBOutlet UIView *viewRatingShowHiden;
@property (nonatomic, weak) id <WDPopupRatingViewControllerDelegate> delegate;

- (IBAction)clickedSend:(id)sender;

@end
