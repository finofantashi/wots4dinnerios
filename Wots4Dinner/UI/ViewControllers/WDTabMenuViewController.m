//
//  WDTabMenuViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/21/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDTabMenuViewController.h"
#import "UIColor+Style.h"
#import <PureLayout/PureLayout.h>
#import "WDMyMenuDishViewController.h"
#import "WDMainViewController.h"
#import "WDMyOrderViewController.h"
#import "WDBecomeSellerStepTwoTableViewController.h"

@interface WDTabMenuViewController () <MSSPageViewControllerDelegate, MSSPageViewControllerDataSource>{
    MSSTabBarView *_tabBarView; // creating the tab bar in code - (self.tabBarView is weak)
}
@end

@implementation WDTabMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.isMyOrder){
        self.title = @"My Orders";
        self.navigationItem.rightBarButtonItem = nil;
    }else{
        self.title = @"My menu";
    }
    // create tab bar
    _tabBarView = [MSSTabBarView new];
    self.tabBarView = _tabBarView;
    
    [self.view addSubview:self.tabBarView];
    [self.tabBarView autoPinEdgesToSuperviewEdgesWithInsets:UIEdgeInsetsMake(64.0f, 0.0f, 0.0f, 0.0f) excludingEdge:ALEdgeBottom];
    [self.tabBarView autoSetDimension:ALDimensionHeight toSize:44.0f];
    self.tabBarView.sizingStyle = MSSTabSizingStyleDistributed;
    [self createTabBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createTabBar {
    // set up tab bar
    self.tabBarView = _tabBarView;
    self.tabBarView.delegate = self;
    self.tabBarView.dataSource = self;
    
    [self.tabBarView setBackgroundColor:[UIColor whiteColor]];
    self.tabBarView.tabAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"Poppins-Medium" size:12],
                                      NSForegroundColorAttributeName : [UIColor colorFromHexString:@"959595" withAlpha:1.0],MSSTabTitleAlpha: @(0.8f),MSSTabTransitionAlphaEffectEnabled:@(YES)};
    
    self.tabBarView.selectedTabAttributes = @{NSFontAttributeName : [UIFont fontWithName:@"Poppins-Medium" size:12],
                                              NSForegroundColorAttributeName : [UIColor colorFromHexString:@"e7663f" withAlpha:1.0],MSSTabIndicatorLineHeight:@(2.0f)};
    self.tabBarView.indicatorAttributes = @{NSForegroundColorAttributeName : [UIColor colorFromHexString:@"e7663f" withAlpha:1.0]};
    [self.tabBarContainerView mss_addExpandingSubview:self.tabBarView];
     [self.pageContainerView mss_addExpandingSubview:self.pageViewController.view];
     self.tabBarView.delegate = self;
     self.tabBarView.dataSource = self;
     self.pageViewController.dataSource = self;
     self.pageViewController.delegate = self;
}

#pragma mark - MSSPageViewControllerDataSource

- (NSArray<UIViewController *> *)viewControllersForPageViewController:(MSSPageViewController *)pageViewController {
    if(self.isMyOrder){
        WDMyOrderViewController *todayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDMyOrderViewController"];
        todayViewController.isToday = YES;
        
        WDMyOrderViewController *allViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDMyOrderViewController"];
        return @[todayViewController, allViewController];
    }else{
        WDMyMenuDishViewController *todayViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDMyMenuDishViewController"];
        todayViewController.isToday = YES;
        
        WDMyMenuDishViewController *allViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDMyMenuDishViewController"];
        return @[todayViewController, allViewController];
    }
}

- (void)tabBarView:(MSSTabBarView *)tabBarView
       populateTab:(MSSTabBarCollectionViewCell *)tab
           atIndex:(NSInteger)index {
    if(index==0){
        if(self.isMyOrder){
            tab.title = @"Today's order";
        }else{
            tab.title = @"Today's Menu";
        }
    }else if(index==1){
        tab.title = @"All";
    }
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedMenu:(id)sender {
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
        [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
}

- (IBAction)clickedAdd:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
    WDBecomeSellerStepTwoTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDBecomeSellerStepTwoTableViewController"];
    viewController.isFromSlideMenu = YES;
    //Remove
    NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
    NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
    if(newControllers.count>1){
        for (int i = 1; i < newControllers.count; i++) {
            [newControllers removeObjectAtIndex:i];
        }
    }
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
    
    [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
    [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
    
}
@end
