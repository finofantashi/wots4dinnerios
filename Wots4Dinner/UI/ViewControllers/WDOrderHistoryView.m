//
//  WDOrderHistoryView.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/17/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDOrderHistoryView.h"

@interface WDOrderHistoryView()

@property (nonatomic, weak) IBOutlet UILabel *lbNumberOrder;
@property (nonatomic, weak) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UIView *cardView;

@end

@implementation WDOrderHistoryView

+ (WDOrderHistoryView *)getViewWithNumberOrder:(NSString *)numberOrder
                                     date:(NSString *)date {
    
    WDOrderHistoryView *view = [[[NSBundle mainBundle] loadNibNamed:@"WDOrderHistoryView" owner:self options:nil] objectAtIndex:0];
    [view cardSetup];
    [view configureWithNumberOrder:numberOrder
                              date:date];
    return view;
}

- (void)configureWithNumberOrder:(NSString *)numberOrder
                            date:(NSString* )date {
    self.lbNumberOrder.text = numberOrder;
    self.lbDate.text = date;
}

- (void)cardSetup {
    [self.cardView setAlpha:1];
    self.cardView.layer.masksToBounds = NO;
    self.cardView.layer.cornerRadius = 1; // if you like rounded corners
    self.cardView.layer.shadowOffset = CGSizeMake(0, 2); //%%% this shadow will hang slightly down and to the right
    self.cardView.layer.shadowRadius = 1; //%%% I prefer thinner, subtler shadows, but you can play with this
    self.cardView.layer.shadowOpacity = 0.5; //%%% same thing with this, subtle is better for me
    
    //%%% This is a little hard to explain, but basically, it lowers the performance required to build shadows.  If you don't use this, it will lag
    //UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.cardView.bounds];
    //self.cardView.layer.shadowPath = path.CGPath;
    
    self.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
}

@end
