//
//  WDOrderCompleteViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/12/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDOrderCompleteViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lbOrderNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (strong, nonatomic) NSString *orderNumber;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbCoupon;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbFee;
@property (weak, nonatomic) IBOutlet UILabel *lbDeliveryFee;

- (IBAction)clickedBack:(id)sender;

@end
