//
//  WDDishCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDDishCell.h"

@implementation WDDishCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.clipsToBounds = YES;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.contentView.translatesAutoresizingMaskIntoConstraints = YES;
    self.ivDish.clipsToBounds = NO;
}

- (void)setImageOffset:(CGPoint)imageOffset {
    // Store padding value
    _imageOffset = imageOffset;
    
    // Grow image view
    CGRect frame = self.ivDish.bounds;
    CGRect offsetFrame = CGRectOffset(frame, _imageOffset.x, _imageOffset.y);
    self.ivDish.frame = offsetFrame;
    
    CGRect offsetFrameContent = CGRectOffset(self.viewContent.bounds, _imageOffset.x, (self.frame.size.height - self.viewContent.frame.size.height) + _imageOffset.y);

    self.viewContent.frame = offsetFrameContent;

}

@end
