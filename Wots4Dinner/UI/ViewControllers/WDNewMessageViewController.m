//
//  WDNewMessageViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDNewMessageViewController.h"
#import "WDNewMessageTableViewCell.h"
#import "WDSourceConfig.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDMessageAPIController.h"
#import "WDReceiver.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDContentMessageViewController.h"

@interface WDNewMessageViewController () <THContactPickerDelegate>
@property (nonatomic, strong) NSMutableArray *privateSelectedContacts;
@property (nonatomic, strong) NSMutableArray *filteredContacts;
@property (nonatomic, strong) NSMutableArray *selectedReceivers;

@end

@implementation WDNewMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"New Message";
    self.contacts = [[NSMutableArray alloc] init];
    [self loadAllData];
    
    self.contactPickerView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth;
    self.contactPickerView.delegate = self;
    [self.contactPickerView setPlaceholderLabelText:@"Search for people"];
    [self.contactPickerView setPromptLabelText:@"To:"];
    //[self.contactPickerView setLimitToOne:YES];
    [self.contactPickerView bringSubviewToFront:self.btnSelectAll];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Publick properties

- (NSArray *)filteredContacts {
    if (!_filteredContacts) {
        _filteredContacts = _contacts;
    }
    return _filteredContacts;
}

- (NSInteger)selectedCount {
    return self.privateSelectedContacts.count;
}

#pragma mark - Private properties

- (NSMutableArray *)privateSelectedContacts {
    if (!_privateSelectedContacts) {
        _privateSelectedContacts = [NSMutableArray array];
    }
    return _privateSelectedContacts;
}

- (NSMutableArray *)selectedReceivers {
    if (!_selectedReceivers) {
        _selectedReceivers = [NSMutableArray array];
    }
    return _selectedReceivers;
}

- (void)configureCell:(WDNewMessageTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    WDReceiver *receiver = self.filteredContacts[indexPath.row];
    cell.lbName.text = [NSString stringWithFormat:@"%@ %@",receiver.firstName, receiver.lastName];
    //get image name and assign
    cell.ivAvatar.layer.cornerRadius = cell.ivAvatar.frame.size.width / 2;
    cell.ivAvatar.layer.masksToBounds = YES;
    cell.ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
    cell.ivAvatar.layer.borderWidth = 2.0f;
    [cell.ivAvatar setNeedsDisplay];

    NSString *imageURL = [[NSString stringWithFormat:@"%@%@", kImageDomain,receiver.avatar] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    [cell.ivAvatar sd_setImageWithURL:[NSURL URLWithString:imageURL]
                     placeholderImage:[UIImage imageNamed:@"empty_avatar"]];
}

- (NSPredicate *)newFilteringPredicateWithText:(NSString *) text {
    return [NSPredicate predicateWithFormat:@"self.fullName contains[cd] %@", text];
}

- (void) didChangeSelectedItems {
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredContacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDNewMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WDNewMessageTableViewCell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    if ([self.privateSelectedContacts containsObject:[self.filteredContacts objectAtIndex:indexPath.row]]){
        [cell.btnChecked setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
    } else {
        [cell.btnChecked setImage:nil forState:UIControlStateNormal];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    WDNewMessageTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    WDReceiver *receiver = self.filteredContacts[indexPath.row];

    id contact = [self.filteredContacts objectAtIndex:indexPath.row];
    
    if ([self.privateSelectedContacts containsObject:contact]){ // contact is already selected so remove it from ContactPickerView
        [cell.btnChecked setImage:nil forState:UIControlStateNormal];
        [self.privateSelectedContacts removeObject:contact];
        [self.selectedReceivers removeObject:contact];
        [self.contactPickerView removeContact:contact];
    } else {
        // Contact has not been selected, add it to THContactPickerView
        [cell.btnChecked setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
        [self.privateSelectedContacts addObject:contact];
        [self.selectedReceivers addObject:contact];
        [self.contactPickerView addContact:contact withName:[NSString stringWithFormat:@"%@ %@",receiver.firstName, receiver.lastName]];
    }
    
    self.filteredContacts = self.contacts;
    [self didChangeSelectedItems];
    [self.tableView reloadData];
}

#pragma mark - THContactPickerTextViewDelegate

- (void)contactPicker:(THContactPickerView *)contactPicker textFieldDidChange:(UITextField *)textField {
    if ([textField.text isEqualToString:@""]){
        self.filteredContacts = self.contacts;
    } else {
        NSPredicate *predicate = [self newFilteringPredicateWithText:textField.text];
        self.filteredContacts = [[self.contacts filteredArrayUsingPredicate:predicate] mutableCopy];
    }
    [self.tableView reloadData];
}

- (void)contactPickerDidResize:(THContactPickerView *)contactPickerView {
    CGRect frame = self.tableView.frame;
    frame.origin.y = contactPickerView.frame.size.height + contactPickerView.frame.origin.y;
    self.tableView.frame = frame;
}

- (void)contactPicker:(THContactPickerView *)contactPicker didRemoveContact:(id)contact {
    [self.privateSelectedContacts removeObject:contact];
    [self.selectedReceivers removeObject:contact];

    NSInteger index = [self.contacts indexOfObject:contact];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    cell.accessoryType = UITableViewCellAccessoryNone;
    [self didChangeSelectedItems];
}

- (BOOL)contactPicker:(THContactPickerView *)contactPicker textFieldShouldReturn:(UITextField *)textField {
    if (textField.text.length > 0){
        NSString *contact = [[NSString alloc] initWithString:textField.text];
        [self.privateSelectedContacts addObject:contact];
        [self.selectedReceivers addObject:contact];
        [self.contactPickerView addContact:contact withName:textField.text];
    }
    return YES;
}

#pragma mark - IBAction

- (IBAction)clickedSelectAll:(id)sender {
    BOOL isSelectAll = YES;
    for(int i=0;i<self.filteredContacts.count;i++){
        id contact = [self.filteredContacts objectAtIndex:i];
        if (![self.privateSelectedContacts containsObject:contact]){
            isSelectAll = NO;
            break;
        }
    }
    
    if(isSelectAll){
        for(int i=0;i<self.filteredContacts.count;i++){
            id contact = [self.filteredContacts objectAtIndex:i];
            [self.privateSelectedContacts removeObject:contact];
            [self.selectedReceivers removeObject:contact];
            [self.contactPickerView removeContact:contact];
        }
    }else{
        for(int i=0;i<self.filteredContacts.count;i++){
            id contact = [self.filteredContacts objectAtIndex:i];
            WDReceiver *receiver = self.filteredContacts[i];
            
            if (![self.privateSelectedContacts containsObject:contact]){ // contact is already selected so remove it from ContactPickerView
                [self.privateSelectedContacts addObject:contact];
                [self.selectedReceivers addObject:contact];
                [self.contactPickerView addContact:contact withName:[NSString stringWithFormat:@"%@ %@",receiver.firstName, receiver.lastName]];
            }
        }
    }
    
    self.filteredContacts = self.contacts;
    [self didChangeSelectedItems];
    [self.tableView reloadData];
}

- (IBAction)clickedNext:(id)sender {
    if(self.selectedReceivers.count>0){
      //WDContentMessageViewController *viewController = [WDContentMessageViewController messagesViewController];
      WDContentMessageViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDContentMessageViewController"];

        viewController.listReceiver = self.selectedReceivers;
        viewController.isNewMessage = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }else{
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"" preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];

    }
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - API

- (void)loadAllData {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        UIWindow * window = [[UIApplication sharedApplication] keyWindow];
        
        [MBProgressHUD showHUDAddedTo:window animated:YES];
        //Call Api
        [[WDMessageAPIController sharedInstance] getListReceive:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:window animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataReceiver = [responseObject valueForKey:@"data"];
                    for (NSDictionary *object in dataReceiver) {
                        WDReceiver *receiver = [[WDReceiver alloc] init];
                        [receiver setReceiverObject:object];
                        [self.contacts addObject:receiver];
                    };
                    
                    if(self.contacts.count==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No records found." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}
@end
