//
//  WDIntroViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDIntroViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewCenter;

@end
