//
//  WDNewMessageViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THContactPickerView.h"

@interface WDNewMessageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectAll;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (nonatomic, strong) IBOutlet THContactPickerView *contactPickerView;

@property (nonatomic, strong) NSMutableArray *contacts;
@property (nonatomic, readonly) NSMutableArray *selectedContacts;
@property (nonatomic) NSInteger selectedCount;
@property (nonatomic, readonly) NSMutableArray *filteredContacts;

- (IBAction)clickedSelectAll:(id)sender;
- (IBAction)clickedNext:(id)sender;
- (IBAction)clickedBack:(id)sender;

- (NSPredicate *)newFilteringPredicateWithText:(NSString *) text;
- (void) didChangeSelectedItems;

@end
