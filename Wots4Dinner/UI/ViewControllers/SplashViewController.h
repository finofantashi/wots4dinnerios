//
//  SplashViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/29/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFLHintLabel.h"

@interface SplashViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ivLogoSplash;
@property (weak, nonatomic) IBOutlet UIView *viewCircle;
@property (weak, nonatomic) IBOutlet UIImageView *ivFork;
@property (weak, nonatomic) IBOutlet UIImageView *ivSpoon;
@property (weak, nonatomic) IBOutlet UIImageView *ivKnifer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftFork;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftSpoon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopKnifer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomKnifer;
@property (weak, nonatomic) IBOutlet UIView *viewCenter;
@property (weak, nonatomic) IBOutlet UILabel *lbName;

@property (nonatomic, strong) MFLHintLabel *hintAnimation;

@end
