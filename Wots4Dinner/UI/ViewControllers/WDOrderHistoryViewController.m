//
//  WDOrderHistoryViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/17/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDOrderHistoryViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDFoodMenuAPIController.h"
#import "WDOrderHistoryView.h"
#import "WDOrderHistoryItemTableViewCell.h"
#import "WDOrderHistory.h"
#import "WDOrderHistoryItem.h"
#import "WDOrderHistoryKitchen.h"
#import "WDMainViewController.h"
#import "WDMapViewController.h"

@interface WDOrderHistoryViewController ()<MIResizableTableViewDataSource, MIResizableTableViewDelegate, WDOrderHistoryItemTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *listMyOrder;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;

@end

@implementation WDOrderHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView configureWithDelegate:self andDataSource:self];
    self.view.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
    
    [self.tableView registerNib:[WDOrderHistoryItemTableViewCell cellNib] forCellReuseIdentifier:[WDOrderHistoryItemTableViewCell cellIdentifier]];
    self.listMyOrder = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    [self loadAllData:self.currentPage search:self.tfSearchView.text];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)clickedBack:(id)sender {
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
        [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
}

- (void)clickedMap:(double)lat lng:(double)lng kitchenName:(NSString *)kitchenName location:(NSString *)location {
    if(lat!=0&&lng!=0){
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        WDMapViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMapViewController"];
        viewController.kitchen = kitchenName;
        viewController.location = location;
        viewController.lat = lat;
        viewController.lng = lng;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

#pragma mark - MIResizableTableViewDataSource

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:14];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.view.frame.size.width-42;
    CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{
                                                                 NSFontAttributeName : aLabelFont
                                                                 }
                                                       context:nil].size;
    return labelSize.height;
}

- (NSInteger)numberOfSectionsInResizableTableView:(MIResizableTableView *)resizableTableView {
    return self.listMyOrder.count;
}

- (NSInteger)resizableTableView:(MIResizableTableView *)resizableTableView numberOfRowsInSection:(NSInteger)section {
    WDOrderHistory *order = [self.listMyOrder objectAtIndex:section];
    if(order.listKitchen.count>0){
        long count = order.listKitchen.count;
        [order.kitchenIndex removeAllObjects];
        [order.kitchenIndex addObject:@0];
        for (int i = 0; i < order.listKitchen.count; i++) {
            WDOrderHistoryKitchen *historyKichen = [order.listKitchen objectAtIndex:i];
            count = count + historyKichen.listOrderItem.count;
            if(i != order.listKitchen.count-1){
                
                [order.kitchenIndex addObject:[NSNumber numberWithInteger:[(NSNumber*)[order.kitchenIndex lastObject] integerValue] + historyKichen.listOrderItem.count+1]];
            }
        }
        order.totalRow = count + 1;
        return count + 1;
    }
    return 0;
}

- (UIView *)resizableTableView:(MIResizableTableView *)resizableTableView viewForHeaderInSection:(NSInteger)section {
    
    WDOrderHistory *order = [self.listMyOrder objectAtIndex:section];
    NSTimeZone *inputTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:inputTimeZone];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *createDate = [dateFormatter dateFromString:order.createAt];
    
    NSTimeZone *outputTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:outputTimeZone];
    dateFormatter.dateFormat = @"MM/dd/yy HH:mm";
    NSString *strCreateDate = [dateFormatter stringFromDate:createDate];
    return [WDOrderHistoryView getViewWithNumberOrder:order.code
                                                 date:strCreateDate];
}

- (UITableViewCell *)resizableTableView:(MIResizableTableView *)resizableTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WDOrderHistoryItemTableViewCell *cell = [resizableTableView dequeueReusableCellWithIdentifier:[WDOrderHistoryItemTableViewCell cellIdentifier] forIndexPath:indexPath];
    cell.orderHistoryItemDelegate = self;
    float totalPrice = 0;
    WDOrderHistory * orderHistory = ((WDOrderHistory*)[self.listMyOrder objectAtIndex:indexPath.section]);
    
    if([orderHistory.kitchenIndex containsObject:[NSNumber numberWithInteger:indexPath.row]]){
        NSInteger currentIndexChicken = [orderHistory.kitchenIndex indexOfObject:[NSNumber numberWithInteger:indexPath.row]];
        WDOrderHistoryKitchen *orderHistoryKitchen = [orderHistory.listKitchen objectAtIndex:currentIndexChicken];
        if(indexPath.row!=0){
            [cell configureWithKitchen:orderHistoryKitchen.kitchenName location:orderHistoryKitchen.location delivery:orderHistoryKitchen.delivery lng:orderHistoryKitchen.lng lat:orderHistoryKitchen.lat isMargin:YES];
        }else{
            [cell configureWithKitchen:orderHistoryKitchen.kitchenName location:orderHistoryKitchen.location delivery:orderHistoryKitchen.delivery lng:orderHistoryKitchen.lng lat:orderHistoryKitchen.lat isMargin:NO];
        }
        
    }else if(indexPath.row == orderHistory.totalRow - 1){
      float deliveryFee = 0;
        for (WDOrderHistoryKitchen *kitchen in orderHistory.listKitchen) {
            for (WDOrderHistoryItem *object in kitchen.listOrderItem) {
                totalPrice = totalPrice + object.totalPrice;
                deliveryFee = object.deliveryFee;
            }
        }
        [cell configureWithTotalPrice:totalPrice currencyCode:self.currencyCode
                       currencySymbol:self.currencySymbol
                           couponType:orderHistory.couponType
                           couponCode:orderHistory.couponCode
                          couponValue:orderHistory.couponValue
                                  fee:orderHistory.fee
                          deliveryFee:deliveryFee];
    }else{
        int index=0;
        for (int i = 0; i < orderHistory.kitchenIndex.count; i++) {
            if([[orderHistory.kitchenIndex objectAtIndex:i] integerValue] < indexPath.row){
                index = i;
            }else{
                break;
            }
        }
        
        WDOrderHistoryItem  *item = [((WDOrderHistoryKitchen*)[orderHistory.listKitchen objectAtIndex:index]).listOrderItem objectAtIndex:indexPath.row - ([[orderHistory.kitchenIndex objectAtIndex:index] integerValue]+1)];
        self.currencyCode = item.currencyCode;
        self.currencySymbol = item.currencySymbol;
        
        [cell configureWithItem:item.dishName
                       quantity:item.quantity
                    description:item.note
                   currencyCode:self.currencyCode
                 currencySymbol:self.currencySymbol
                          price:item.price
                       delivery:item.delivery
                     pickupFrom:item.pickupTimeFrom
                       pickupTo:item.pickupTimeTo
                    homeService:item.specialOffer
                         status:item.status];
    }
    
    return cell;
}

#pragma mark - MIResizableTableViewDelegate

- (void)resizableTableView:(MIResizableTableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if (self.currentPage != self.totalPages && section == [self.listMyOrder count] - 1) {
        [self loadAllData:++self.currentPage search:self.tfSearchView.text];
    }
}

- (CGFloat)resizableTableView:(MIResizableTableView *)resizableTableView heightForHeaderInSection:(NSInteger)section {
    return 90;
}

- (CGFloat)resizableTableView:(MIResizableTableView *)resizableTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    WDOrderHistory * orderHistory = ((WDOrderHistory*)[self.listMyOrder objectAtIndex:indexPath.section]);
    if([orderHistory.kitchenIndex containsObject:[NSNumber numberWithInteger:indexPath.row]]){
        int index=0;
        for (int i = 0; i < orderHistory.kitchenIndex.count; i++) {
            if([[orderHistory.kitchenIndex objectAtIndex:i] integerValue] < indexPath.row){
                index = i;
            }else{
                break;
            }
        }
        WDOrderHistoryKitchen *orderHistoryKitchen = [orderHistory.listKitchen objectAtIndex:index];
        if(orderHistoryKitchen.delivery==1){
            return 44;
        }
        return 68;
    }
    
    if(indexPath.row == orderHistory.totalRow - 1){
        return 78 + 30;
    }
    int index=0;
    for (int i = 0; i < orderHistory.kitchenIndex.count; i++) {
        if([[orderHistory.kitchenIndex objectAtIndex:i] integerValue] < indexPath.row){
            index = i;
        }else{
            break;
        }
    }
    
    WDOrderHistoryItem  *item = [((WDOrderHistoryKitchen*)[orderHistory.listKitchen objectAtIndex:index]).listOrderItem objectAtIndex:indexPath.row - ([[orderHistory.kitchenIndex objectAtIndex:index] integerValue]+1)];
    float size = 0;
    if(item.delivery==1){
        size = 74;
        if(item.specialOffer.length>0){
            size = size + [self sizeOfMultiLineLabel:[NSString stringWithFormat:@"Delivery service:%@",item.specialOffer]];
        }
    }else{
        size = 84;
    }
    
    if(item.note.length>0){
        float sizeHeight = size + [self sizeOfMultiLineLabel:[NSString stringWithFormat:@"Your notes: %@",item.note]];
        return sizeHeight;
    }
    return size;
}

- (UITableViewRowAnimation)resizableTableViewInsertRowAnimation {
    return UITableViewRowAnimationFade;
}

- (UITableViewRowAnimation)resizableTableViewDeleteRowAnimation {
    return UITableViewRowAnimationFade;
}

- (BOOL)resizableTableViewSectionShouldExpandSection:(NSInteger)section {
    if(((WDOrderHistory*)[self.listMyOrder objectAtIndex:section]).listKitchen.count==0){
        [self getListItemWithOrderId:((WDOrderHistory*)[self.listMyOrder objectAtIndex:section]).orderID index:section];
    }
    return YES;
}

- (void)resizableTableView:(MIResizableTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - API

- (void)loadAllData:(NSInteger)page
             search:(NSString*)search{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getOrderHistory:search page:page completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDOrderHistory *order = [[WDOrderHistory alloc] init];
                        [order setOrderHistoryObject:object];
                        order.kitchenIndex = [[NSMutableArray alloc] init];
                        order.listKitchen = [[NSMutableArray alloc] init];
                        [self.listMyOrder addObject:order];
                    };
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No Order." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)getListItemWithOrderId:(NSInteger)orderID
                         index:(NSInteger)index{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getOrderHistoryItem:orderID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    [((WDOrderHistory*)[self.listMyOrder objectAtIndex:index]).listKitchen removeAllObjects];
                    [((WDOrderHistory*)[self.listMyOrder objectAtIndex:index]).kitchenIndex removeAllObjects];
                    
                    NSArray *kitchenNames = [[responseObject valueForKey:@"data"] valueForKeyPath:@"@distinctUnionOfObjects.kitchen_name"];
                    for (NSString *kitchenName in kitchenNames) {
                        WDOrderHistoryKitchen *result = [[WDOrderHistoryKitchen alloc] init];
                        result.listOrderItem  = [[NSMutableArray alloc] init];
                        result.kitchenName = kitchenName;
                        NSArray *groupKitchens = [[responseObject valueForKey:@"data"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(kitchen_name == %@)", kitchenName]];
                        for (int i = 0; i < groupKitchens.count; i++) {
                            WDOrderHistoryItem *orderItem = [[WDOrderHistoryItem alloc] init];
                            [orderItem setOrderHistoryItemObject:[groupKitchens objectAtIndex:i]];
                            result.lat = orderItem.lat;
                            result.lng = orderItem.lng;
                            result.location = orderItem.location;
                            result.delivery = orderItem.delivery;
                            [result.listOrderItem addObject:orderItem];
                        }
                        [((WDOrderHistory*)[self.listMyOrder objectAtIndex:index]).listKitchen addObject:result];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                        [self.tableView expand:YES rowsInSection:index];
                    });
                }
            }
        }];
    }
}

- (IBAction)clickedSearch:(id)sender {
    [self.tfSearchView resignFirstResponder];
    self.listMyOrder = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    [self loadAllData:self.currentPage search:self.tfSearchView.text];
}


@end
