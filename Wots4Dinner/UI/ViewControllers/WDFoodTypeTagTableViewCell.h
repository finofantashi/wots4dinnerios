//
//  WDFoodTypeTagTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHTag.h"
#import "AHTagTableViewCell.h"

@interface WDFoodTypeTagTableViewCell : UITableViewCell <UITableViewDataSource , UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<AHTag *> *dataSource;
- (void) updateData:(NSMutableArray<AHTag *> *)dataSource;

@end
