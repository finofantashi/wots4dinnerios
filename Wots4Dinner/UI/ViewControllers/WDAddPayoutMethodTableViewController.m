//
//  WDAddPayoutMethodTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDAddPayoutMethodTableViewController.h"
#import "UIColor+Style.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDPayout.h"
#import "WDOrderAPIController.h"
#import "WDFoodMenuAPIController.h"
#import "WDCurrency.h"

@interface WDAddPayoutMethodTableViewController ()

@end

@implementation WDAddPayoutMethodTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnConfirm.layer.cornerRadius = self.btnConfirm.frame.size.height/2; // this value vary as per your desire
    self.btnConfirm.layer.borderWidth = 1.0f;
    self.btnConfirm.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
    self.btnConfirm.clipsToBounds = YES;
    [self getCurrenciesAPI];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickPaypal:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.tvPaypal addGestureRecognizer:tapGestureRecognizer];
    self.tvPaypal.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupPickerCurrency {
    // bind yourTextField to DownPicker
    self.picker = [[DownPicker alloc] initWithTextField:self.tfCurrency withData:self.currenciesData];
    [self.tfCurrency setFont:[UIFont fontWithName:@"Poppins-Medium" size:14]];
    [self.tfCurrency setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:1.0]];
    [self.picker setPlaceholder:@"Currency you would like to be paid"];
}


- (BOOL)validate {
    if([[self.tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Email is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    if([[self.tfCurrency.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Currency is required." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

-(void)clickPaypal:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://www.paypal.com"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedConfirm:(id)sender {
    if([self validate]){
        WDPayout *payout = [[WDPayout alloc] init];
        payout.account = self.tfEmail.text;
        payout.currency = self.tfCurrency.text;
        [self addPayout:payout];
    }
}

- (void) addPayout:(WDPayout *)payout {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDOrderAPIController sharedInstance] savePayout:payout completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}

- (void) getCurrenciesAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getCurrencies:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    self.currencies = [[NSMutableArray alloc] init];
                    self.currenciesData = [[NSMutableArray alloc] init];
                    
                    NSDictionary *dataCurrencies = [responseObject valueForKey:@"data"];
                    for (NSDictionary *object in dataCurrencies) {
                        WDCurrency *currency = [[WDCurrency alloc] init];
                        [currency setCurrencyObject:object];
                        [self.currencies addObject:currency];
                        [self.currenciesData addObject:currency.code];
                    };
                    [self setupPickerCurrency];
                    [self.tableView reloadData];
                }
            }
        }];
    }
}
@end
