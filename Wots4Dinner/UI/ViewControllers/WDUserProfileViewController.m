//
//  WDUserProfile.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDUserProfileViewController.h"
#import "WDUser.h"
#import "WDUserTemp.h"
#import "UIImage+ResizeMagick.h"
#import "UIImageView+AFNetworking.h"
#import "WDSourceConfig.h"
#import "WDFavoriteSellerTableViewController.h"
#import "WDWishListTableViewController.h"
@import BonMot;

@implementation WDUserProfileViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLayout];
    [self setData:self.user];
}

#pragma mark Private

- (void) setUpLayout {
    self.ivAvatar.layer.cornerRadius = 120 / 2;
    self.ivAvatar.layer.masksToBounds = YES;
    self.ivAvatar.layer.borderColor = [UIColor grayColor].CGColor;
    self.ivAvatar.layer.borderWidth = 2.0f;
    [self.ivAvatar setNeedsDisplay];
    self.title = @"Profile";
    self.tvAbout.textContainerInset = UIEdgeInsetsMake(14, 14, 14, 14);

}

- (void)setData: (WDUserTemp*)user{
    if(![user.avatarURL isEqualToString:@""]){
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain, user.avatarURL] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:stringURL]];
        [self.ivAvatar setImageWithURLRequest:urlRequest placeholderImage:[UIImage imageNamed:@"empty_avatar"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            self.ivAvatar.image = image;
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            self.ivAvatar.image = [UIImage imageNamed:@"empty_avatar"];
        }];
    }
    
    if(user.firstName&&user.lastName){
        self.lbName.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    }
    
    NSString *sinceday = user.sinceday;
    if(![sinceday isEqualToString:@""]){
        self.lbSinceDay.text = [NSString stringWithFormat:@"Member since %@",[[WDUser sharedInstance] stringWithSinceDate:sinceday]];
    }
    BONChain *space = BONChain.new.string(@" ");
    BONChain *chain = BONChain.new;
    [chain appendLink:BONChain.new.image([UIImage imageNamed:@"ic_location"]).baselineOffset(-4.0)];
    [chain appendLink:BONChain.new.string(user.living) separatorTextable: space];
    NSAttributedString *string = chain.attributedString;
    self.lbLocation.attributedText = string;
    if(user.about.length>0){
        self.tvAbout.text = user.about;
    }else{
        self.tvAbout.text = @"About me";
    }
}

#pragma mark - IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedWishList:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
    WDWishListTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDWishListTableViewController"];
    if(self.user.userID==[WDUser sharedInstance].userID){
        viewController.isMyProfile = YES;
    }else{
        viewController.userID = self.user.userID;
        viewController.isMyProfile = NO;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedFavorite:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
    WDFavoriteSellerTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDFavoriteSellerTableViewController"];
    viewController.isSeller = YES;
    if(self.user.userID==[WDUser sharedInstance].userID){
        viewController.isMyProfile = YES;
    }else{
        viewController.userID = self.user.userID;
        viewController.isMyProfile = NO;
    }
    [self.navigationController pushViewController:viewController animated:YES];
}
@end
