//
//  AddPayoutMethodStep1TableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/2/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDownPicker.h"
#import "WDPayoutMethod.h"

@interface AddPayoutMethodStep1TableViewController : UITableViewController
@property (nonatomic, strong) WDPayoutMethod* payoutMethod;
@property (nonatomic, assign) BOOL isAddPayout;

@property (nonatomic, strong) NSMutableArray* countries;
@property (nonatomic, strong) NSMutableArray* countriesData;
@property (weak, nonatomic) IBOutlet UITextField *pickerCountry;
@property (weak, nonatomic) IBOutlet UITextField *tfAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfCity;
@property (weak, nonatomic) IBOutlet UITextField *tfState;
@property (weak, nonatomic) IBOutlet UITextField *tfZip;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (nonatomic) DownPicker *picker;

- (IBAction)clickedNext:(id)sender;

@end
