//
//  FAQsDetailViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/27/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "FAQsDetailViewController.h"
#import "WDQuestion.h"
#import "WDFAQsTableViewCell.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFAQsAPIController.h"
#import "FAQsViewController.h"
#import "NSString+HTML.h"

@interface FAQsDetailViewController ()
@property (nonatomic, strong) NSMutableArray *listQuestion;

@end

@implementation FAQsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listQuestion = [[NSMutableArray alloc] init];
    self.title = @"FAQ Details";
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.listQuestion.count!=0){
        return self.listQuestion.count + 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDFAQsTableViewCell";
    UITableViewCell *cell;
    if (indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"WDFAQsSuggestedTableViewCell" forIndexPath:indexPath];
    }else{
        if(indexPath.row==0){
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            WDQuestion *question = self.listQuestion[0];
            
            NSString *myDescriptionHTML = [NSString stringWithFormat:@"<html> \n"
                                           "<head> \n"
                                           "<style type=\"text/css\"> \n"
                                           "body {font-family: \"%@\"; font-size: %@;}\n"
                                           "</style> \n"
                                           "</head> \n"
                                           "<body text=\"#464646\">%@</body> \n"
                                           "</html>", @"Poppins-Light", [NSNumber numberWithInt:17], [question.answer stringByReplacingOccurrencesOfString:@"\n" withString:@""]];
            [((WDFAQsTableViewCell*)cell).webView loadHTMLString:myDescriptionHTML baseURL:nil];
            [((WDFAQsTableViewCell*)cell).viewLine setHidden:YES];
            
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:@"WDFAQsTableViewCell1"];
            WDQuestion *question = self.listQuestion[indexPath.row-1];
            ((WDFAQsTableViewCell*)cell).lbQuestion.text = question.question;
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    WDQuestion *question = self.listQuestion[indexPath.row-1];
    FAQsDetailViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FAQsDetailViewController"];
    viewController.faqID = question.questionID;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 1) {
        if(indexPath.row>0){
            WDQuestion *question = self.listQuestion[indexPath.row-1];
            float sizeHeight = 21 + [self sizeOfMultiLineLabel:question.question];
            return sizeHeight;
        }else{
            WDQuestion *question = self.listQuestion[0];
            float sizeHeight = 80 + [self sizeOfMultiLineLabel:[[[question.answer stringByStrippingTags] stringByRemovingNewLinesAndWhitespace] stringByDecodingHTMLEntities]];
            return sizeHeight;
        }
    }
    return 44;
}

-(CGFloat)sizeOfMultiLineLabel: (NSString *)text{
    //Label text
    NSString *aLabelTextString = text;
    
    //Label font
    UIFont *aLabelFont = [UIFont fontWithName:@"Poppins" size:17];
    
    //Width of the Label
    CGFloat aLabelSizeWidth = self.view.frame.size.width;
    CGSize labelSize =  [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                    attributes:@{
                                                                 NSFontAttributeName : aLabelFont
                                                                 }
                                                       context:nil].size;
    return labelSize.height;
}

- (void)loadData {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //[MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call Api
        [[WDFAQsAPIController sharedInstance] getQuestion:self.faqID completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                     NSDictionary *faqDict = [[responseObject valueForKey:@"data"] valueForKey:@"faq"];
                    WDQuestion *question = [[WDQuestion alloc] init];
                    [question setQuestionObject:faqDict];
                    [self.listQuestion addObject:question];
                    self.lbQuestion.text = question.question;
                    
                    NSDictionary *dataSearch = [[responseObject valueForKey:@"data"] valueForKey:@"others"];
                    for (NSDictionary *object in dataSearch) {
                        WDQuestion *question = [[WDQuestion alloc] init];
                        [question setQuestionObject:object];
                        [self.listQuestion addObject:question];
                    };
                    [self.tableView reloadData];
                }
            }
        }];
    }
}

- (IBAction)clickedBack:(id)sender {
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[FAQsViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
            break;
        }
    }
}

@end
