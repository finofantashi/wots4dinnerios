//
//  WDContactUsViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/20/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDContactUsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnContactUs;

@end
