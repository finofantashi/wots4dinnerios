//
//  WDReviewDishViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/29/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDReviewDishViewController : UIViewController
@property (nonatomic, assign) NSInteger dishID;
@property (nonatomic, assign) NSInteger numOrders;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *tfComment;
@property (weak, nonatomic) IBOutlet UIButton *btnReview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

- (IBAction)clickedReview:(id)sender;
@end
