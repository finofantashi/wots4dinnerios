//
//  CategoryView.m
//  MIResizableTableViewDemo
//
//  Created by Mario on 17/01/16.
//  Copyright © 2016 Mario. All rights reserved.
//

#import "WDMyOrderView.h"
#import "UIColor+Style.h"

@interface WDMyOrderView()

@property (nonatomic, weak) IBOutlet UILabel *lbNumberOrder;
@property (nonatomic, weak) IBOutlet UILabel *lbDate;
@property (nonatomic, weak) IBOutlet UILabel *lbName;
@property (nonatomic, weak) IBOutlet UILabel *lbShippingInfo;
@property (weak, nonatomic) IBOutlet UIView *cardView;

@end

@implementation WDMyOrderView

+ (WDMyOrderView *)getViewWithNumberOrder:(NSString *)numberOrder
                                     date:(NSString *)date
                             nameCustomer:(NSString *)nameCustomer
                             shippingInfo:(NSString *)shippingInfo
                                   isRead:(BOOL)isRead {
    
    WDMyOrderView *view = [[[NSBundle mainBundle] loadNibNamed:@"WDMyOrderView" owner:self options:nil] objectAtIndex:0];
    [view cardSetup];
    [view configureWithNumberOrder:numberOrder
                              date:date
                      nameCustomer:nameCustomer
                      shippingInfo:shippingInfo
                            isRead:isRead];
    
    return view;
}

- (void)configureWithNumberOrder:(NSString *)numberOrder
                            date:(NSString* )date
                    nameCustomer:(NSString* )nameCustomer
                    shippingInfo:(NSString* )shippingInfo
                          isRead:(BOOL)isRead {
    
    self.lbNumberOrder.text = numberOrder;
    self.lbDate.text = date;
    self.lbName.text = nameCustomer;
    if(shippingInfo.length>0){
        self.lbShippingInfo.text = [NSString stringWithFormat:@"Delivery Info: %@", shippingInfo];
    }else{
        self.lbShippingInfo.text = [NSString stringWithFormat:@"Delivery Info: N/A"];

    }
    if(!isRead){
        self.lbNumberOrder.textColor = [UIColor colorFromHexString:@"F77E51" withAlpha:1.0];
    }else{
        self.lbNumberOrder.textColor = [UIColor colorFromHexString:@"464646" withAlpha:1.0];
    }
}

- (void)cardSetup {
    [self.cardView setAlpha:1];
    self.cardView.layer.masksToBounds = NO;
    self.cardView.layer.cornerRadius = 1; // if you like rounded corners
    self.cardView.layer.shadowOffset = CGSizeMake(0, 2); //%%% this shadow will hang slightly down and to the right
    self.cardView.layer.shadowRadius = 1; //%%% I prefer thinner, subtler shadows, but you can play with this
    self.cardView.layer.shadowOpacity = 0.5; //%%% same thing with this, subtle is better for me
    
    self.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
}
@end
