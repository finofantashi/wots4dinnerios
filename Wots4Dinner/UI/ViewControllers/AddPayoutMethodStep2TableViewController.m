//
//  AddPayoutMethodStep2TableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "AddPayoutMethodStep2TableViewController.h"
#import "UIColor+Style.h"
#import "PayPalMethodTableViewController.h"
#import "BankMethodTableViewController.h"

@interface AddPayoutMethodStep2TableViewController ()
@property (assign, nonatomic) BOOL isPayPal;

@end

@implementation AddPayoutMethodStep2TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(!self.payoutMethod){
        self.payoutMethod =  [[WDPayoutMethod alloc] init];
    }
    self.btnNext.layer.cornerRadius = self.btnNext.frame.size.height/2; // this value vary as per your desire
    self.btnNext.layer.borderWidth = 1.0f;
    self.btnNext.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
    self.btnNext.clipsToBounds = YES;
    if([self.payoutMethod.method isEqualToString:@""]&&![self.payoutMethod.method isEqualToString:@"PayPal"]){
        [self setPayPalMethod:NO];
    }else{
        [self setPayPalMethod:YES];
    }
    self.lbDescription.text = [NSString stringWithFormat:@"We can send money to people with these payout methods. Which do you prefer?"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

-(void)setPayPalMethod:(BOOL)isChecked {
    if(isChecked){
        self.isPayPal = YES;
        [self.btnBank setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
        [self.btnPaypal setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
        
    }else{
        self.isPayPal = NO;
        [self.btnBank setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateNormal];
        [self.btnPaypal setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];

    }
    [self.tableView reloadData];
}

#pragma mark - IBAction


- (IBAction)clickedNext:(id)sender {
    if(self.isPayPal){
        PayPalMethodTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PayPalMethodTableViewController"];
        self.payoutMethod.method = @"PayPal";
        viewController.payoutMethod = self.payoutMethod;
        viewController.isAddPayout = self.isAddPayout;
        [self.navigationController pushViewController:viewController animated:YES];
    }else{
        BankMethodTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BankMethodTableViewController"];
        self.payoutMethod.method = @"Bank transfer";
        viewController.payoutMethod = self.payoutMethod;
        viewController.isAddPayout = self.isAddPayout;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (IBAction)clickedPaypal:(id)sender {
    [self setPayPalMethod:YES];
}

- (IBAction)clickedBank:(id)sender {
    [self setPayPalMethod:NO];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
