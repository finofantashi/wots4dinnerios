//
//  WDBlogCommentViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBlogCommentTableViewCell.h"

@implementation WDBlogCommentTableViewCell

-(void)awakeFromNib {

}

- (IBAction)clickedAvatar:(id)sender {
    if([self.delegate respondsToSelector:@selector(clickedAvatar:)]) {
        [self.delegate clickedAvatar:self.user];
    }
}
@end
