//
//  WDCheckOutViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDCheckOutViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSMutableArray *listCart;

@end
