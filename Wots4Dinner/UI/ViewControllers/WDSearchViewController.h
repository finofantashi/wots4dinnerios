//
//  WDSearchViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlaces/GooglePlaces.h>
#import <CoreLocation/CoreLocation.h>

@interface WDSearchViewController : UIViewController <GMSAutocompleteResultsViewControllerDelegate, UISearchBarDelegate,CLLocationManagerDelegate>
@property (nonatomic, strong) GMSAutocompleteResultsViewController *resultsViewController;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray* foodTypes;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
- (IBAction)clickedSearch:(id)sender;
- (IBAction)touchDown:(id)sender;
- (IBAction)clickedBack:(id)sender;

@end
