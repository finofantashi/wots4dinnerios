//
//  WDOtherTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDOtherTableViewController.h"
#import "WDFavoriteSellerTableViewController.h"
#import "WDWebViewViewController.h"
#import "WDWishListTableViewController.h"
#import "WDMainViewController.h"

@interface WDOtherTableViewController () <UITableViewDelegate>

@end

@implementation WDOtherTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Others";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==8||indexPath.row==9) {
        return 0;
    }else {
        // return height from the storyboard
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

#pragma mark - IBAction

- (IBAction)clickedWishList:(id)sender {
    WDWishListTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDWishListTableViewController"];
    viewController.isMyProfile = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedFavoriteSeller:(id)sender {
    WDFavoriteSellerTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDFavoriteSellerTableViewController"];
    viewController.isSeller = YES;
    viewController.isMyProfile = YES;
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)clickedFavoriteOf:(id)sender {
    WDFavoriteSellerTableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDFavoriteSellerTableViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedBack:(id)sender {
    [[WDMainViewController sharedInstance] getUnreadMessagesAPI];
    [[WDMainViewController sharedInstance] showLeftViewAnimated:YES completionHandler:^{
        [[WDMainViewController sharedInstance].leftViewController.tableView reloadData];
    }];
}

- (IBAction)clickedSetting:(id)sender {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDSettingTableViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedContactUs:(id)sender {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDAppFeedbackTableViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedFAQs:(id)sender {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FAQsViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedInvite:(id)sender {
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDInviteFriendViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickedTerms:(id)sender {
    WDWebViewViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDWebViewViewController"];
    viewController.fromURL = 0;
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)clickedAboutUs:(id)sender {
    WDWebViewViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDWebViewViewController"];
    viewController.fromURL = 1;
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)clickedRefund:(id)sender {
    WDWebViewViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDWebViewViewController"];
    viewController.fromURL = 2;
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)clickedSeller:(id)sender {
    WDWebViewViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDWebViewViewController"];
    viewController.fromURL = 3;
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (IBAction)clickedSelectPayment:(id)sender {
    UITableViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDPaymentMethodTableViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
