//
//  CustomTableViewCell.m
//  MIResizableTableViewDemo
//
//  Created by Mario on 17/01/16.
//  Copyright © 2016 Mario. All rights reserved.
//

#import "WDMyOrderItemTableViewCell.h"
#import "NSString+StringAdditions.h"
#import "UIColor+Style.h"


@interface WDMyOrderItemTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *lbDishName;
@property (nonatomic, weak) IBOutlet UILabel *lbQuantity;
@property (nonatomic, weak) IBOutlet UILabel *lbPrice;
@property (nonatomic, weak) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UIStackView *viewStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnDeclined;
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintStatus;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLbStatus;
@property (nonatomic, strong) WDMyOrderItem *dish;
@property (nonatomic, weak) IBOutlet UILabel *lbTitleTotal;
@property (nonatomic, weak) IBOutlet UILabel *lbPriceTotal;

@end

@implementation WDMyOrderItemTableViewCell

+ (UINib *)cellNib {
  return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

+ (NSString *)cellIdentifier {
  return NSStringFromClass([self class]);
}

- (void)layoutSubviews {
}

- (void)awakeFromNib {
  [super awakeFromNib];
  [self cardSetup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];
  
  // Configure the view for the selected state
}

- (void)configureWithItem:(WDMyOrderItem *)dish
             currencyCode:(NSString *)currencyCode
           currencySymbol:(NSString *)currencySymbol {
  [self.lbTitleTotal setHidden:YES];
  [self.lbPriceTotal setHidden:YES];
  
  self.dish = dish;
  self.lbDishName.text = dish.dishName;
  self.lbQuantity.text = [NSString stringWithFormat:@"x%ld",dish.quantity];
  self.lbPrice.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:dish.price];
  
  if(dish.note.length>0) {
    self.lbDescription.text = [NSString stringWithFormat:@"Note: %@",dish.note];
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc]
     initWithAttributedString: self.lbDescription.attributedText];
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor colorFromHexString:@"00acd7" withAlpha:1.0]
                 range:NSMakeRange(0, 5)];
    [self.lbDescription setAttributedText: text];
    self.constraintStatus.constant = 4;
    self.constraintLbStatus.constant = 0;
    
  }else{
    self.lbDescription.text = @"";
    self.constraintStatus.constant = -2;
    self.constraintLbStatus.constant = -4;
  }
  
  if([dish.status isEqualToString:@"confirmed"]){
    self.viewStatus.hidden = YES;
    self.lbStatus.hidden = NO;
    self.lbStatus.text = @"Status: Confirmed";
    [self.lbStatus setTextColor:[UIColor colorFromHexString:@"a8ba4c" withAlpha:1.0]];
  } else if([dish.status isEqualToString:@"declined"]){
    self.viewStatus.hidden = YES;
    self.lbStatus.hidden = NO;
    self.lbStatus.text = @"Status: Declined";
    [self.lbStatus setTextColor:[UIColor colorFromHexString:@"da6a68" withAlpha:1.0]];
  }else{
    self.viewStatus.hidden = NO;
    self.lbStatus.hidden = YES;
  }
  
}

- (void)configureWithTotalPrice:(float)totalPrice
                     fee:(float)fee
                   currencyCode:(NSString *)currencyCode
                 currencySymbol:(NSString *)currencySymbol {
  [self.lbTitleTotal setHidden:NO];
  [self.lbPriceTotal setHidden:NO];
  self.lbTitleTotal.text = @"Total: ";
  self.lbDishName.text = @"Delivery: ";

  self.lbQuantity.text = @"";
  self.lbPrice.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:fee];
  self.lbPriceTotal.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:totalPrice];

  self.lbDescription.text = @"";
  self.viewStatus.hidden = YES;
  self.lbStatus.hidden = YES;
}

- (void)cardSetup {
  [self.cardView setAlpha:1];
  self.cardView.layer.masksToBounds = NO;
  self.cardView.layer.cornerRadius = 1; // if you like rounded corners
  self.cardView.layer.shadowOffset = CGSizeMake(0, 3); //%%% this shadow will hang slightly down and to the right
  self.cardView.layer.shadowRadius = 1; //%%% I prefer thinner, subtler shadows, but you can play with this
  self.cardView.layer.shadowOpacity = 0.5; //%%% same thing with this, subtle is better for me
  
  self.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
}

- (IBAction)clickedDeclined:(id)sender {
  if([self.myOrderItemDelegate respondsToSelector:@selector(clickedDeclined:)]) {
    [self.myOrderItemDelegate clickedDeclined:self.dish];
  }
}

- (IBAction)clickedConfirm:(id)sender {
  if([self.myOrderItemDelegate respondsToSelector:@selector(clickedConfirm:)]) {
    [self.myOrderItemDelegate clickedConfirm:self.dish];
  }
}

@end
