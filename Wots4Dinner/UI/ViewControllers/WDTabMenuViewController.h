//
//  WDTabMenuViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/21/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSSTabbedPageViewController.h"

@interface WDTabMenuViewController : MSSTabbedPageViewController
@property (nonatomic, assign) BOOL isMyOrder;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnAdd;
@property (weak, nonatomic) IBOutlet UIView *tabBarContainerView;
@property (weak, nonatomic) IBOutlet UIView *pageContainerView;

- (IBAction)clickedMenu:(id)sender;
- (IBAction)clickedAdd:(id)sender;

@end
