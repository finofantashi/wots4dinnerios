//
//  WDBlogCommentTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDBlogCommentTableViewController : UITableViewController
@property (nonatomic, assign) NSInteger blogID;

@end
