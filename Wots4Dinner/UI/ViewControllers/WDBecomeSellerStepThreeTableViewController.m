//
//  WDBecomeSellerStepThreeTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBecomeSellerStepThreeTableViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "WDFoodMenuAPIController.h"
#import "UIColor+Style.h"
#import "WDMainViewController.h"
#import "NSDictionary+SafeValues.h"
#import "WDWebViewViewController.h"

@interface WDBecomeSellerStepThreeTableViewController ()

@end

@implementation WDBecomeSellerStepThreeTableViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.btnFinish.layer.cornerRadius = self.btnFinish.frame.size.height/2; // this value vary as per your desire
  self.btnFinish.layer.borderWidth = 1.0f;
  self.btnFinish.layer.borderColor = [UIColor colorFromHexString:@"f77e51" withAlpha:1.0f].CGColor;
  self.btnFinish.clipsToBounds = YES;
  
  if(self.isFromSlideMenu){
    self.lbTitle.text = @"Step 2: Home delivery service";
  }
  
  if(self.isUpdate){
    self.title = @"Edit Dish";
  }
  
  if(self.isHomeDeliveryService){
    self.title = @"Home Delivery";
    [self.btnFinish setTitle:@"Update" forState:UIControlStateNormal];
  }
  [self getCurrenciesAPI];
  self.tableView.estimatedRowHeight = 44;
  [self.btnFree setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateSelected];
  [self.btnFree setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
  [self.btnNoDelivery setImage:[UIImage imageNamed:@"ic_check_enable"] forState:UIControlStateSelected];
  [self.btnNoDelivery setImage:[UIImage imageNamed:@"ic_check_disable"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)setupPickerCurrency {
  // bind yourTextField to DownPicker
  UIColor *color = [UIColor colorFromHexString:@"636363" withAlpha:1.0]; // select needed color
  NSString *string = @"Currency"; // the string to colorize
  NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
  NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];
  self.picker1 = [[DownPicker alloc] initWithTextField:self.pickerCurrency  withData:self.currenciesData];
  [self.pickerCurrency setFont:[UIFont fontWithName:@"Poppins-Medium" size:14]];
  [self.pickerCurrency setTextColor:[UIColor colorFromHexString:@"464646" withAlpha:1.0]];
  [self.picker1  setPlaceholder:@"Currency"];
  [self.picker1  setAttributedPlaceholder:attrStr];
  [self.picker1 addTarget:self
                   action:@selector(currencySelected:)
         forControlEvents:UIControlEventValueChanged];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if(self.isHomeDeliveryService&&indexPath.row==0){
    return 0;
  }else{
    return UITableViewAutomaticDimension;
  }
}

- (void) loadData:(NSDictionary*) responseObject {
  NSDictionary *localeInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"USD", NSLocaleCurrencyCode,
                              [[NSLocale preferredLanguages] objectAtIndex:0], NSLocaleLanguageCode,
                              nil];
  NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:
                      [NSLocale localeIdentifierFromComponents:localeInfo]];
  NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
  [fmt setNumberStyle:NSNumberFormatterCurrencyStyle];
  [fmt setLocale:locale];
  [fmt setCurrencySymbol:@""];
  NSInteger currencyID = [[[responseObject valueForKey:@"data"] safeNumberForKey:@"currency_id"] integerValue];
  NSNumber *fee = [[responseObject valueForKey:@"data"] safeNumberForKey:@"fee"];
  if (currencyID != 0 && [fee doubleValue] > 0) {
    self.tfPrice.text = [fmt stringFromNumber:fee];
    self.pickerCurrency.text = [self getCurrencyName: currencyID];
    self.lbPrice.text = [self getCurrencySymbol:self.pickerCurrency.text];
    self.tfRadius.text = [NSString stringWithFormat:@"%@", [[responseObject valueForKey:@"data"] safeStringForKey:@"radius"]];
    self.btnFree.selected = NO;
  } else {
    if (self.currencyID && self.currencyID !=0) {
      self.pickerCurrency.text = [self getCurrencyName: self.currencyID];
    }
    self.btnFree.selected = YES;
  }
  self.btnNoDelivery.selected = ![[[responseObject valueForKey:@"data"] safeNumberForKey:@"is_delivery"] boolValue];
  [self toggleFree];
  if (self.specialOffer) {
    self.tfHomeDelivery.text = self.specialOffer;
  } else {
    self.tfHomeDelivery.text = [NSString stringWithFormat:@"%@", [[responseObject valueForKey:@"data"] safeStringForKey:@"special_offer"]];
  }
}

- (BOOL) isValidate {
  if([[self.pickerCurrency.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0 && [[self.tfPrice.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0 &&
     [[self.tfRadius.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    return YES;
  }
  
  if(self.btnNoDelivery.selected) {
    return YES;
  }
  
  if(self.btnFree.selected) {
    return YES;
  }
  
  if([[self.tfPrice.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Price is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.pickerCurrency.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Currency is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  
  if([[self.tfRadius.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] == 0) {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Radius is required." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
    return NO;
  }
  return YES;
}

#pragma mark - IBAction

- (IBAction)clickedFinish:(id)sender {
  if ([self isValidate]) {
    if(self.isHomeDeliveryService){
      [self addDeliveryService];
    }else{
      [self addDelivery];
    }
  }
}

- (IBAction)clickedBack:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedDelivery:(id)sender {
  self.btnNoDelivery.selected = !self.btnNoDelivery.selected;
  [self toggleFree];
}

- (IBAction)clickedFree:(id)sender {
  self.btnFree.selected = !self.btnFree.selected;
  [self toggleFree];
}

- (IBAction)clickedTerm:(id)sender {
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
  WDWebViewViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDWebViewViewController"];
  viewController.fromURL = 0;
  [self.navigationController setNavigationBarHidden:NO];
  [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Picker

-(void)currencySelected:(id)dp {
  self.lbPrice.text = [self getCurrencySymbol:self.pickerCurrency.text];
}

- (NSString*)getCurrencySymbol:(NSString*)currencyCode {
  for (WDCurrency *object in self.currencies) {
    if([object.code isEqualToString:currencyCode]){
      return object.symbol;
    }
  }
  return @"";
}

- (void) toggleFree {
  self.tfPrice.enabled = !self.btnFree.selected;
  self.tfRadius.enabled = !self.btnFree.selected;
  self.pickerCurrency.enabled = !self.btnFree.selected;

  self.tfPrice.backgroundColor = self.btnFree.selected ? [UIColor colorFromHexString:@"dddddd" withAlpha:1.0] : [UIColor colorFromHexString:@"464646" withAlpha:0];
  self.lbPrice.backgroundColor = self.btnFree.selected ? [UIColor colorFromHexString:@"dddddd" withAlpha:1.0] : [UIColor colorFromHexString:@"464646" withAlpha:0];
  self.pickerCurrency.backgroundColor = self.btnFree.selected ? [UIColor colorFromHexString:@"dddddd" withAlpha:1.0] : [UIColor colorFromHexString:@"464646" withAlpha:0];
  self.tfRadius.backgroundColor = self.btnFree.selected ? [UIColor colorFromHexString:@"dddddd" withAlpha:1.0] : [UIColor colorFromHexString:@"464646" withAlpha:0];
  
  self.btnFree.enabled = !self.btnNoDelivery.selected;
  self.lbFree.textColor = self.btnNoDelivery.selected ? [UIColor colorFromHexString:@"dddddd" withAlpha:1.0] : [UIColor colorFromHexString:@"F77E51" withAlpha:1];

}

#pragma mark - API

- (void)addDelivery {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    NSString *currencyID = nil;
    if ([[self.pickerCurrency.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] != 0) {
      currencyID = [NSString stringWithFormat:@"%ld", [self getCurrencyID:self.pickerCurrency.text]];
    }
    [[WDFoodMenuAPIController sharedInstance] addDeliveries:self.dishID isDelivery:self.btnNoDelivery.selected ? 0:1 homeService:self.tfHomeDelivery.text currencyID:currencyID price:self.btnFree.selected ? 0:[self.tfPrice.text floatValue]  radius: self.tfRadius.text completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"BecomeSeller" bundle:[NSBundle mainBundle]];
          UIViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDMyMenu"];
          //Remove
          NSArray *currentControllers = ((UINavigationController*)[WDMainViewController sharedInstance].rootViewController).viewControllers;
          NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
          if(newControllers.count>1){
            for (int i = 1; i < newControllers.count; i++) {
              [newControllers removeObjectAtIndex:i];
            }
          }
          [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController setViewControllers:newControllers];
          [(UINavigationController*)[WDMainViewController sharedInstance].rootViewController pushViewController:viewController animated:YES];
          [[WDMainViewController sharedInstance] setLeftViewSwipeGestureEnabled:NO];
        }
      }
    }];
  }
}

- (void)addDeliveryService {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    NSString *currencyID = nil;
    if ([[self.pickerCurrency.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ] length] != 0) {
      currencyID = [NSString stringWithFormat:@"%ld", [self getCurrencyID:self.pickerCurrency.text]];
    }
    [[WDFoodMenuAPIController sharedInstance] addDeliveryService:self.btnNoDelivery.selected ? 0:1 homeService:self.tfHomeDelivery.text currencyID:currencyID price:self.btnFree.selected ? 0:[self.tfPrice.text floatValue] radius: self.tfRadius.text completionBlock:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          [self.navigationController popViewControllerAnimated:YES];
        }
      }
    }];
  }
}

- (void)getDelivery {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getDeliveries:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(![[responseObject valueForKey:@"data"] isKindOfClass:[NSNull class]]) {
          [self loadData:responseObject];
        }
      }
    }];
  }
}

- (void) getCurrenciesAPI {
  Reachability *reachability = [Reachability reachabilityForInternetConnection];
  NetworkStatus networkStatus = [reachability currentReachabilityStatus];
  if(networkStatus == NotReachable){
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
      // handle default button action
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
  }else{
    [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Call Api
    [[WDFoodMenuAPIController sharedInstance] getCurrencies:^(NSString *errorString, id responseObject) {
      [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
      if(errorString){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
          [self getDelivery];
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
      }else{
        if(responseObject) {
          self.currencies = [[NSMutableArray alloc] init];
          self.currenciesData = [[NSMutableArray alloc] init];
          
          NSDictionary *dataCurrencies = [responseObject valueForKey:@"data"];
          for (NSDictionary *object in dataCurrencies) {
            WDCurrency *currency = [[WDCurrency alloc] init];
            [currency setCurrencyObject:object];
            [self.currencies addObject:currency];
            [self.currenciesData addObject:currency.code];
          };
          [self setupPickerCurrency];
          [self getDelivery];
          [self.tableView reloadData];
        }
      }
    }];
  }
}

- (NSInteger)getCurrencyID:(NSString*)currencyCode {
  for (WDCurrency *object in self.currencies) {
    if([object.code isEqualToString:currencyCode]){
      return object.currencyID;
    }
  }
  return 0;
}

- (NSString*)getCurrencyName:(NSInteger)currencyID {
  for (WDCurrency *object in self.currencies) {
    if(object.currencyID == currencyID){
      return object.code;
    }
  }
  return @"";
}
@end
