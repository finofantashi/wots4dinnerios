//
//  WDOrderHistoryTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/17/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDOrderHistoryItemTableViewCell.h"
#import "NSString+StringAdditions.h"
#import "UIColor+Style.h"
#import "WDMapViewController.h"

@interface WDOrderHistoryItemTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *lbFee;

@property (nonatomic, weak) IBOutlet UILabel *lbDishName;
@property (nonatomic, weak) IBOutlet UILabel *lbQuantity;
@property (nonatomic, weak) IBOutlet UILabel *lbPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbTotalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lbCoupon;
@property (weak, nonatomic) IBOutlet UILabel *lbOrderTotal;

@property (nonatomic, weak) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UILabel *lbKitchenName;
@property (weak, nonatomic) IBOutlet UILabel *lbLocation;
@property (weak, nonatomic) IBOutlet UIView *viewKitchen;
@property (weak, nonatomic) IBOutlet UILabel *lbPickupTime;
@property (weak, nonatomic) IBOutlet UIView *viewDish;
@property (weak, nonatomic) IBOutlet UIView *viewTotal;
@property (weak, nonatomic) IBOutlet UIView *viewLocation;
@property (weak, nonatomic) IBOutlet UILabel *lbTypeDelivery;
@property (weak, nonatomic) IBOutlet UILabel *lbDeliveryFee;
@property (weak, nonatomic) IBOutlet UILabel *lbStatus;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation;
@property (weak, nonatomic) IBOutlet UIImageView *icLocation;
@property (nonatomic, assign) double lng;
@property (nonatomic, assign) double lat;
@end


@implementation WDOrderHistoryItemTableViewCell

+ (UINib *)cellNib {
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

+ (NSString *)cellIdentifier {
    return NSStringFromClass([self class]);
}

- (void)layoutSubviews {
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self cardSetup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)configureWithItem:(NSString *)dishName
                 quantity:(NSInteger)quantity
              description:(NSString *)description
             currencyCode:(NSString *)currencyCode
           currencySymbol:(NSString *)currencySymbol
                    price:(float)price
                 delivery:(NSInteger)delievey
               pickupFrom:(NSString *)pickupFrom
                 pickupTo:(NSString *)pickupTo
              homeService:(NSString *)homeService
                   status:(NSString *)status{
    [self.viewKitchen setHidden:YES];
    [self.viewTotal setHidden:YES];
    [self.viewDish setHidden:NO];
    
    self.lbDishName.text = dishName;
    self.lbQuantity.text = [NSString stringWithFormat:@"x%ld",quantity];
    self.lbPrice.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:price];

    if(description.length>0){
        self.lbDescription.text = [NSString stringWithFormat:@"Note: %@",description];
        NSMutableAttributedString *text =
        [[NSMutableAttributedString alloc]
         initWithAttributedString: self.lbDescription.attributedText];
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor colorFromHexString:@"00acd7" withAlpha:1.0]
                     range:NSMakeRange(0, 5)];
        [self.lbDescription setAttributedText: text];
    }else {
        self.constraintStatus.constant = -4;
        self.lbDescription.text = @"";
        
    }
    if(delievey==1){
        if(homeService.length > 0){
            self.lbPickupTime.text = [NSString stringWithFormat:@"Delivery service: %@",homeService];
        } else {
            self.lbPickupTime.text = [NSString stringWithFormat:@"No service"];
        }
    }else{
        if(pickupFrom.length>6){
            pickupFrom = [pickupFrom substringWithRange:NSMakeRange(0, 5)];
        }
        if(pickupTo.length>6){
            pickupTo = [pickupTo substringWithRange:NSMakeRange(0, 5)];
        }
        self.lbPickupTime.text = [NSString stringWithFormat:@"Pickup time: %@-%@",pickupFrom,pickupTo];
    }
    
    if([status isEqualToString:@"confirmed"]){
        self.lbStatus.text = @"Status: Confirmed";
        [self.lbStatus setTextColor:[UIColor colorFromHexString:@"a8ba4c" withAlpha:1.0]];
    } else if([status isEqualToString:@"declined"]){
        self.lbStatus.text = @"Status: Declined";
        [self.lbStatus setTextColor:[UIColor colorFromHexString:@"da6a68" withAlpha:1.0]];
    }else{
        self.lbStatus.text = @"Status: Pending";
        [self.lbStatus setTextColor:[UIColor colorFromHexString:@"FFAD47" withAlpha:1.0]];
    }
    
    
}

- (void)configureWithTotalPrice:(float)totalPrice
                   currencyCode:(NSString *)currencyCode
                 currencySymbol:(NSString *)currencySymbol
                     couponType:(NSString *)couponType
                     couponCode:(NSString *)couponCode
                    couponValue:(float) couponValue
                            fee:(float)fee
                    deliveryFee:(float)deliveryFee
{
    [self.viewKitchen setHidden:YES];
    [self.viewTotal setHidden:NO];
    [self.viewDish setHidden:YES];
    
    self.lbDishName.text = @"";
    self.lbQuantity.text = @"";
    self.lbPickupTime.text = @"";
    self.lbDescription.text = @"";
    self.lbPrice.text = @"";
    self.lbStatus.text = @"";
    
    self.lbFee.text = [NSString stringWithFormat:@"%@",[[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:fee]];
    self.lbDeliveryFee.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:deliveryFee];
    self.lbTotalPrice.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:totalPrice];
    self.lbOrderTotal.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:totalPrice+fee+deliveryFee];
    if(couponType.length>0){
        if([couponType isEqualToString:@"percentage"]){
            float coupon = couponValue/100*totalPrice;
            self.lbCoupon.text = [NSString stringWithFormat:@"-%@",[[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:coupon]];
            self.lbOrderTotal.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:(totalPrice-coupon)+fee+deliveryFee];
            
        }else{
            self.lbCoupon.text = [NSString stringWithFormat:@"-%@",[[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:couponValue]];
            self.lbOrderTotal.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:(totalPrice-couponValue)+fee+deliveryFee];
        }
        
    }else{
        self.lbCoupon.text = [[NSString string] formatCurrency:currencyCode symbol:currencySymbol price:0];
    }
    
}

- (void)configureWithKitchen:(NSString *)kitchen
                    location:(NSString *)location
                    delivery:(NSInteger)delievey
                         lng:(double)lng
                         lat:(double)lat
                    isMargin:(BOOL)isMargin{
    if(isMargin){
        self.topKitchenConstraint.constant = 8;
    }else{
        self.topKitchenConstraint.constant = 0;
    }
    [self.viewKitchen setHidden:NO];
    [self.viewTotal setHidden:YES];
    [self.viewDish setHidden:YES];
    self.lbDishName.text = @"";
    self.lbQuantity.text = @"";
    self.lbPickupTime.text = @"";
    self.lbDescription.text = @"";
    self.lbPrice.text = @"";
    self.lbStatus.text = @"";
    
    self.lbKitchenName.text = kitchen;
    self.lbLocation.text = location;
    self.lat = lat;
    self.lng = lng;
    if(delievey==1){
        self.lbTypeDelivery.text = @"Delivery";
        self.lbLocation.text = @"";
        self.btnLocation.hidden = YES;
        self.icLocation.hidden = YES;
    }else{
        self.lbLocation.text = location;
        self.icLocation.hidden = NO;
        self.lbTypeDelivery.text = @"Pick-up";
        self.viewLocation.alpha = 0;
    }
}

- (void)cardSetup {
    [self.cardView setAlpha:1];
    self.cardView.layer.masksToBounds = NO;
    self.cardView.layer.cornerRadius = 1; // if you like rounded corners
    self.cardView.layer.shadowOffset = CGSizeMake(0, 3); //%%% this shadow will hang slightly down and to the right
    self.cardView.layer.shadowRadius = 1; //%%% I prefer thinner, subtler shadows, but you can play with this
    self.cardView.layer.shadowOpacity = 0.5; //%%% same thing with this, subtle is better for me
    
    //%%% This is a little hard to explain, but basically, it lowers the performance required to build shadows.  If you don't use this, it will lag
    // UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.cardView.bounds];
    //self.cardView.layer.shadowPath = path.CGPath;
    
    self.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
}

- (IBAction)clickedLocation:(id)sender {
    if([self.orderHistoryItemDelegate respondsToSelector:@selector(clickedMap:lng:kitchenName:location:)]) {
        [self.orderHistoryItemDelegate clickedMap:self.lat lng:self.lng kitchenName:self.lbKitchenName.text location:self.lbLocation.text];
    }
}


@end
