//
//  AddPayoutMethodStep2TableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WDPayoutMethod.h"

@interface AddPayoutMethodStep2TableViewController : UITableViewController
@property (nonatomic, strong) WDPayoutMethod* payoutMethod;
@property (nonatomic, assign) BOOL isAddPayout;

@property (weak, nonatomic) IBOutlet UIButton *btnPaypal;
@property (weak, nonatomic) IBOutlet UIButton *btnBank;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;

- (IBAction)clickedNext:(id)sender;

- (IBAction)clickedPaypal:(id)sender;

- (IBAction)clickedBank:(id)sender;
@end
