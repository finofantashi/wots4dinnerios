
#import <UIKit/UIKit.h>

@interface LeftViewCell : UITableViewCell

@property (assign, nonatomic) IBOutlet UIView *separatorView;
@property (strong, nonatomic) UIColor *tintColor;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ivIcon;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbSince;
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;

@end
