//
//  WDVerifyResetCodeViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDVerifyResetCodeViewController.h"
#import "WDResetPasswordViewController.h"
#import "MBProgressHUD.h"
#import "LEAlertController.h"
#import "Reachability.h"
#import "WDUserAPIController.h"
#import "WDUser.h"
#import "NSString+IsValidEmail.h"
#import "UIColor+Style.h"

@interface WDVerifyResetCodeViewController () <UITextFieldDelegate,UIGestureRecognizerDelegate>

@end

@implementation WDVerifyResetCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.phoneNumber.length>0&&[self.phoneNumber characterAtIndex:0] == '0'){
        self.phoneNumber = [self.phoneNumber stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
    }
    if(self.isFromSignUp||self.isChangePhone){
        self.tfResetCode.placeholder = @"Verification code";
        [self.btnVerify setTitle:@"Verify" forState:UIControlStateNormal];
        [self.btnResendCode setTitle:@"Resend verification code" forState:UIControlStateNormal];
        self.lbResetCode.text = [NSString stringWithFormat:@"%@\nPlease enter the verification code received by SMS",[NSString stringWithFormat:@"%@ %@",self.prefixPhone, self.phoneNumber]];
        
    }else{
        self.lbResetCode.text = [NSString stringWithFormat:@"%@\nPlease enter the reset code received by SMS",[NSString stringWithFormat:@"%@ %@",self.prefixPhone, self.phoneNumber]];
        
    }
    self.userName = [[NSString stringWithFormat:@"%@%@",self.prefixPhone, self.phoneNumber] stringByReplacingOccurrencesOfString:@"+" withString:@""];
    [self setupDismissKeyboard];
    self.btnVerify.layer.cornerRadius = self.btnVerify.frame.size.height/2; // this value vary as per your desire
    self.btnVerify.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setupDismissKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.delegate = self;
    
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    [self.tfResetCode resignFirstResponder];
}

- (void)enableSignin {
    if(self.tfResetCode.text.length!=0){
        self.btnVerify.enabled = YES;
        self.btnVerify.alpha = 1.0f;
    }else{
        self.btnVerify.enabled = NO;
        self.btnVerify.alpha = 0.5f;
    }
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(enableSignin) withObject:nil afterDelay:0.1];
    return YES;
}

#pragma mark IBAction

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickedResend:(id)sender {
    if(self.isChangePhone){
        [self changePhoneAPI];
    }else if(self.isFromSignUp){
        [self sendVerifyCodeAPI];
    }else{
        [self forgotPassAPI];
    }
}

- (IBAction)touchDown:(id)sender {
    [self.btnVerify setBackgroundColor:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0]];

}

- (IBAction)clickdedVerify:(id)sender {
    [self.btnVerify setBackgroundColor:[UIColor colorFromHexString:@"e7663f" withAlpha:1.0]];
    if(self.isChangePhone){
        [self verifyCodeChangePhoneAPI];
    }else if(_isVerify&&self.isFromSignUp){
        WDResetPasswordViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDResetPasswordViewController"];
        viewController.isFromSignUp = self.isFromSignUp;
        [self.navigationController pushViewController:viewController animated:YES];
    }else{
        [self verifyCodeAPI:self.isFromSignUp?NO:YES];
    }
}

#pragma mark API

- (void) verifyCodeAPI:(BOOL) isForgotPass {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call API
        [[WDUserAPIController sharedInstance] verifyCode:self.userName code:[self.tfResetCode.text trim] isForgotPass:isForgotPass completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                _isVerify = YES;
                if(isForgotPass){
                    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"+ "];
                    NSString *phonePrefix = [[self.prefixPhone componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
                    NSString *phoneNumber = [[self.phoneNumber componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
                    NSString *userName = [NSString stringWithFormat:@"%@%@",phonePrefix,phoneNumber];
                    
                    WDResetPasswordViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDResetPasswordViewController"];
                    viewController.code = [self.tfResetCode.text trim];
                    viewController.userName = userName;
                    [self.navigationController pushViewController:viewController animated:YES];
                }else{
                    WDResetPasswordViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDResetPasswordViewController"];
                    viewController.isFromSignUp = self.isFromSignUp;
                    [self.navigationController pushViewController:viewController animated:YES];
                }
            }
        }];
    }
}

- (void) sendVerifyCodeAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call API
        [[WDUserAPIController sharedInstance] sendVerificationCode:self.userName completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                NSString *message = @"Verification code has been sent to your phone.";
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:message preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }
        }];
    }
}

- (void) forgotPassAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //Call API
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        NSString *phoneNumber = [self.phoneNumber trim];
        if(phoneNumber.length>0&&[phoneNumber characterAtIndex:0] == '0'){
            phoneNumber = [phoneNumber stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }
        [[WDUserAPIController sharedInstance] forgotPassword:phoneNumber phonePrefix:self.prefixPhone completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                NSString *message = @"Reset code has been sent to your phone.";
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:message preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }
        }];
    }
}

- (void) verifyCodeChangePhoneAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        //Call API
        NSString *phoneNumber = [self.phoneNumber trim];
        if(phoneNumber.length>0&&[phoneNumber characterAtIndex:0] == '0'){
            phoneNumber = [phoneNumber stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }
        [[WDUserAPIController sharedInstance] verifyCodeChangePhone:phoneNumber code:[self.tfResetCode.text trim] phonePrefix:self.countryCode completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"You have updated your phone number successfully!" preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }
        }];
    }
}

- (void) changePhoneAPI {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        //Call API
        [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        NSString *phoneNumber = [self.phoneNumber trim];
        if(phoneNumber.length>0&&[phoneNumber characterAtIndex:0] == '0'){
            phoneNumber = [phoneNumber stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
        }
        [[WDUserAPIController sharedInstance] changePhoneNumber:phoneNumber phonePrefix:self.countryCode completion:^(NSString *errorString) {
            [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
                
            }else{
                NSString *message = @"Verification code has been sent to your phone.";
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:message preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];

            }
        }];
    }
}

@end
