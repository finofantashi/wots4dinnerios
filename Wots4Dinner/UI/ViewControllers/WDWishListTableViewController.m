//
//  WishListTableViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/25/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDWishListTableViewController.h"
#import "Reachability.h"
#import "LEAlertController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "WDDish.h"
#import "WDSourceConfig.h"
#import "WDDishTableViewCell.h"
#import "WDUserTemp.h"
#import "WDDishDetailTableViewController.h"
#import "WDFoodMenuAPIController.h"
#import "NSString+StringAdditions.h"

@interface WDWishListTableViewController () <UIScrollViewDelegate, WDDishTableViewCellDelegate>
@property (nonatomic, strong) NSMutableArray *listDish;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger totalPages;
@property (nonatomic, assign) NSInteger totalItems;
@end

@implementation WDWishListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.listDish = [[NSMutableArray alloc] init];
    self.currentPage = 0;
    self.totalPages = 0;
    self.totalItems = 0;
    [self loadAllData:self.currentPage];
    self.title = @"Wish List";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.currentPage == self.totalPages
        || self.totalItems == self.listDish.count) {
        return self.listDish.count;
    }
    return self.listDish.count + 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.currentPage != self.totalPages && indexPath.row == [self.listDish count] - 1 ) {
            [self loadAllData:++self.currentPage];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"WDDishTableViewCell";
    UITableViewCell *cell;
    if (indexPath.row == [self.listDish count]) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        UIActivityIndicatorView *activityIndicator = (UIActivityIndicatorView *)[cell.contentView viewWithTag:100];
        [activityIndicator startAnimating];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        ((WDDishTableViewCell*)cell).dishDelegate = self;
        WDDish *dish = self.listDish[indexPath.row];
        if(!self.isMyProfile){
            [((WDDishTableViewCell*)cell).btnFavorite setHidden:YES];
            [((WDDishTableViewCell*)cell).btnFavorite setEnabled:NO];
        }
        //get image name and assign
        NSString *imageURL = [[NSString stringWithFormat:@"%@%@", kImageDomain,dish.dishImage] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        [((WDDishTableViewCell*)cell).ivDish sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                               placeholderImage:[UIImage imageNamed:@"placeholder.jpg"]];
        ((WDDishTableViewCell*)cell).lbNameDish.text = dish.dishName;
        ((WDDishTableViewCell*)cell).lbDescription.text = dish.foodTypeName;
        ((WDDishTableViewCell*)cell).lbSoldBy.text = [NSString stringWithFormat:@"Sold by %@ %@",dish.user.firstName,dish.user.lastName];
        ((WDDishTableViewCell*)cell).wishListId = dish.hasWishList;
        
        ((WDDishTableViewCell*)cell).lbPrice.text = [[NSString string] formatCurrency:dish.currencyCode symbol:dish.currencySymbol price:dish.price];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    WDDish *dish = self.listDish[indexPath.row];
    WDDishDetailTableViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDDishDetailTableViewController"];
    viewController.dishID = dish.dishID;
    [self.navigationController pushViewController:viewController animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

- (void)loadAllData:(NSInteger)page {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        UIWindow * window = [[UIApplication sharedApplication] keyWindow];

        [MBProgressHUD showHUDAddedTo:window animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] getWishList:page
                                                       userID:self.userID
                                              completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:window animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    NSDictionary *dataDish = [[responseObject valueForKey:@"data"] valueForKey:@"data"];
                    for (NSDictionary *object in dataDish) {
                        WDDish *dish = [[WDDish alloc] init];
                        [dish setDishObject:[object valueForKey:@"dish"]];
                        [dish setHasWishList:[[object valueForKey:@"id"] integerValue]];
                        [self.listDish addObject:dish];
                    };
                    
                    self.currentPage = [[[responseObject valueForKey:@"data"] objectForKey:@"current_page"] integerValue];
                    self.totalPages  = [[[responseObject valueForKey:@"data"] objectForKey:@"last_page"] integerValue];
                    self.totalItems  = [[[responseObject valueForKey:@"data"] objectForKey:@"total"] integerValue];
                    if(self.totalItems==0){
                        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"No records found." preferredStyle:LEAlertControllerStyleAlert];
                        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                            // handle default button action
                        }];
                        [alertController addAction:defaultAction];
                        [self presentAlertController:alertController animated:YES completion:nil];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                }
            }
        }];
    }
}

- (void)deleteWishListDish:(NSInteger) wishListId {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable){
        LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:@"Please check your internet connection or try again later." preferredStyle:LEAlertControllerStyleAlert];
        LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
            // handle default button action
        }];
        [alertController addAction:defaultAction];
        [self presentAlertController:alertController animated:YES completion:nil];
    }else{
        UIWindow * window = [[UIApplication sharedApplication] keyWindow];

        [MBProgressHUD showHUDAddedTo:window animated:YES];
        //Call Api
        [[WDFoodMenuAPIController sharedInstance] deleteWishListDish:wishListId completionBlock:^(NSString *errorString, id responseObject) {
            [MBProgressHUD hideHUDForView:window animated:YES];
            if(errorString){
                LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:LEAlertControllerStyleAlert];
                LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
                    // handle default button action
                }];
                [alertController addAction:defaultAction];
                [self presentAlertController:alertController animated:YES completion:nil];
            }else{
                if(responseObject) {
                    [self.listDish removeAllObjects];
                    self.currentPage = 0;
                    self.totalPages = 0;
                    self.totalItems = 0;
                    [self loadAllData:self.currentPage];
                }
            }
        }];
    }
}

#pragma mark - IBAction

- (void)clickedFavorite:(NSInteger)wishListId {
    [self deleteWishListDish:wishListId];
}

- (IBAction)clickedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
