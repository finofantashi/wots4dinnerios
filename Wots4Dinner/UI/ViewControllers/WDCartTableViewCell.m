//
//  WDCartTableViewCell.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDCartTableViewCell.h"
#import "NSString+StringAdditions.h"

@implementation WDCartTableViewCell

- (IBAction)clickedPlus:(id)sender {
    if([self.lbQuantity.text integerValue] < self.cart.dish.quantity){
        if([self.delegate respondsToSelector:@selector(clickedPlus:cart:)]) {
            [self.delegate clickedPlus:self cart:self.cart];
        }
    }
}

- (IBAction)clickedMinus:(id)sender {
    if([self.lbQuantity.text integerValue]>1){
        if([self.delegate respondsToSelector:@selector(clickedMinus:cart:)]) {
            [self.delegate clickedMinus:self cart:self.cart];
        }
        
    }
}

- (IBAction)clickedDelete:(id)sender {
    if([self.delegate respondsToSelector:@selector(clickedDelete:)]) {
        [self.delegate clickedDelete:self.cart.cartID];
    }
}

- (void)enableButtonPlus:(BOOL)isEnable {
    if(isEnable){
        [self.btnPlus setImage:[UIImage imageNamed:@"ic_plus_enable"] forState:UIControlStateNormal];
        self.btnPlus.userInteractionEnabled = YES;
    }else{
        [self.btnPlus setImage:[UIImage imageNamed:@"ic_plus_disable"] forState:UIControlStateNormal];
        self.btnPlus.userInteractionEnabled = NO;
    }
}

- (void)enableButtonMinus:(BOOL)isEnable {
    if(isEnable){
        [self.btnMinus setImage:[UIImage imageNamed:@"ic_minus_enable"] forState:UIControlStateNormal];
        self.btnMinus.userInteractionEnabled = YES;
    }else{
        [self.btnMinus setImage:[UIImage imageNamed:@"ic_minus_disable"] forState:UIControlStateNormal];
        self.btnMinus.userInteractionEnabled = NO;
    }
}

- (void)updateTotalPricePlus {
    self.lbQuantity.text = [NSString stringWithFormat:@"%ld", [self.lbQuantity.text integerValue] +1];
    self.lbQuantityLeft.text = [NSString stringWithFormat:@"Qty left: %ld", self.cart.dish.quantity - [self.lbQuantity.text integerValue]];
    if([self.lbQuantity.text integerValue] < self.cart.dish.quantity){
        [self enableButtonPlus:YES];
    }else {
        [self enableButtonPlus:NO];
    }
    if([self.lbQuantity.text integerValue]>1){
        [self enableButtonMinus:YES];
    }
    
    self.lbTotalPrice.text = [[NSString string] formatCurrency:self.cart.dish.currencyCode symbol:self.cart.dish.currencySymbol price:self.cart.dish.price*[self.lbQuantity.text integerValue]];
}

- (void)updateTotalPriceMinus {
    self.lbQuantity.text = [NSString stringWithFormat:@"%ld", [self.lbQuantity.text integerValue] -1];
    self.lbQuantityLeft.text = [NSString stringWithFormat:@"Qty left: %ld", self.cart.dish.quantity - [self.lbQuantity.text integerValue]];
    if([self.lbQuantity.text integerValue] < self.cart.dish.quantity){
        [self enableButtonPlus:YES];
    }else {
        [self enableButtonPlus:NO];
    }
    
    if([self.lbQuantity.text integerValue] <= 1){
        [self enableButtonMinus:NO];
    }
    
    self.lbTotalPrice.text = [[NSString string] formatCurrency:self.cart.dish.currencyCode symbol:self.cart.dish.currencySymbol price:self.cart.dish.price*[self.lbQuantity.text integerValue]];
}

- (IBAction)clickedEditNote:(id)sender {
    if([self.delegate respondsToSelector:@selector(clickedEditNote:note:)]) {
        [self.delegate clickedEditNote:self.cart note:self.lbNote.text];
    }
}
@end
