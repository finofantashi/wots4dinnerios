//
//  WDAddPayoutMethodTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDownPicker.h"

@interface WDAddPayoutMethodTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *tvPaypal;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfCurrency;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (nonatomic, strong) NSMutableArray* currencies;
@property (nonatomic, strong) NSMutableArray* currenciesData;
@property (nonatomic) DownPicker *picker;

- (IBAction)clickedBack:(id)sender;
- (IBAction)clickedConfirm:(id)sender;
@end
