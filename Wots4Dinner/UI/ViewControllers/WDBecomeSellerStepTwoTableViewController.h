//
//  WDBecomeSellerStepTwoTableViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDownPicker.h"
#import "DLRadioButton.h"
#import "WDDishSearch.h"

@interface WDBecomeSellerStepTwoTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIImageView *ivDish;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMore;
@property (weak, nonatomic) IBOutlet UITextField *tfDishName;
@property (weak, nonatomic) IBOutlet UITextField *tfPrice;
@property (weak, nonatomic) IBOutlet UITextField *pickerFoodType;
@property (weak, nonatomic) IBOutlet UITextField *tfQuantity;
@property (weak, nonatomic) IBOutlet UITextField *pickerCurrency;
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet UITextField *tfPickFrom;
@property (weak, nonatomic) IBOutlet UITextField *tfPickTo;
@property (weak, nonatomic) IBOutlet UIView *viewPickFrom;
@property (weak, nonatomic) IBOutlet UIView *viewPickTo;
@property (weak, nonatomic) IBOutlet UITextField *tfDate;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIView *viewServingDate;
@property (weak, nonatomic) IBOutlet DLRadioButton *rbtnMonday;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barBtnBack;

@property (nonatomic, strong) NSMutableArray* foodTypes;
@property (nonatomic, strong) NSMutableArray* foodTypesData;
@property (nonatomic, strong) NSMutableArray* currencies;
@property (nonatomic, strong) NSMutableArray* currenciesData;

@property (nonatomic, assign) BOOL isFromSlideMenu;
@property (nonatomic, assign) BOOL isUpdate;
@property (nonatomic, assign) NSInteger dishID;
@property (weak, nonatomic) IBOutlet UILabel *lbPrice;

@property (nonatomic) DownPicker *picker1;
@property (nonatomic) DownPicker *picker2;

- (IBAction)clickedNext:(id)sender;
- (IBAction)clickedAddMore:(id)sender;
@end
