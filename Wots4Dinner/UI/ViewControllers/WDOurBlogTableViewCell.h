//
//  WDOurBlogTableViewCell.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDOurBlogTableViewCell : UITableViewCell

@property (nonatomic, strong, readwrite) IBOutlet UIImageView *ivOurBlog;
@property (weak, nonatomic) IBOutlet UILabel *lbName;
@property (weak, nonatomic) IBOutlet UILabel *lbDate;

- (void)cellOnTableView:(UITableView *)tableView didScrollOnView:(UIView *)view;

@end
