//
//  WDHomeViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/16/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSSTabbedPageViewController.h"
#import "TabControllerStyle.h"

@interface WDHomeViewController : MSSTabbedPageViewController
@property (nonatomic, strong) TabControllerStyle *style;
@property (nonatomic, assign) BOOL isFirst;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnMenu;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSearch;


@end
