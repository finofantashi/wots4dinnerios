//
//  ViewController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDFirstLaucherViewController.h"
#import "GBDeviceInfo.h"
#import "WDSourceConfig.h"
#import "WDUserDefaultsManager.h"
#import "WDSignInViewController.h"
#import "WDGetResetCodeViewController.h"
#import "WDUserDefaultsManager.h"
#import "WDSourceConfig.h"
#import "WDMainViewController.h"
#import "WDUser.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Style.h"
#import "WDWebViewViewController.h"
#import "LEAlertController.h"
#import "INTULocationManager.h"

@interface WDFirstLaucherViewController ()

@end

@implementation WDFirstLaucherViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLayout];
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                       timeout:10.0
                          delayUntilAuthorized:YES    // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             if (status == INTULocationStatusSuccess) {
                                                 NSDictionary *current = @{@"kLng" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude] doubleValue]],
                                                                           @"kLat" : [NSNumber numberWithDouble:[[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude] doubleValue]]};
                                                 [WDUserDefaultsManager setValue:current forKey:@"kCurrentLocation"];
                                             }
                                         }];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
     if(![[WDUserDefaultsManager valueForKey:kUserToken] isEqualToString:@""]&&[[WDUserDefaultsManager valueForKey:kSessionSignIn] isEqualToNumber:[NSNumber numberWithBool:YES]]){
        UIWindow *window = UIApplication.sharedApplication.delegate.window;
        window.rootViewController = [WDMainViewController sharedInstance];
    }else {
        self.btnSignin.alpha = 1.0f;
        self.btnSignup.alpha = 1.0f;
        self.lbTerm.alpha = 1.0f;
        self.btnTerm.alpha = 1.0f;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)setupLayout {
    self.lbTerm.alpha = 0;
    self.btnTerm.alpha = 0;
    self.btnSignin.alpha = 0;
    self.btnSignup.alpha = 0;
    self.btnSignup.layer.cornerRadius = self.btnSignup.frame.size.height/2; // this value vary as per your desire
    self.btnSignup.clipsToBounds = YES;
    
    self.btnSignin.layer.cornerRadius = self.btnSignin.frame.size.height/2; // this value vary as per your desire
    self.btnSignin.clipsToBounds = YES;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    if(screenWidth == 375.0||screenWidth == 414.0){
        self.logoWidthConstraint.constant = 89;
        self.logoHeightConstraint.constant = 107;
        self.titleMarginTop.constant = 24;
        self.buttonMarginTop.constant = 40;
        self.lbTitle.font = [UIFont fontWithName:@"Poppins-SemiBold" size:27];
    }
}

#pragma mark - IBAction

- (IBAction)clickedTerm:(id)sender {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Other" bundle:[NSBundle mainBundle]];
    WDWebViewViewController *viewController = [storyBoard instantiateViewControllerWithIdentifier:@"WDWebViewViewController"];
    viewController.fromURL = 0;
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)clickOutSign:(id)sender {
    [self.btnSignin setBackgroundColor:[UIColor colorFromHexString:@"F77E51" withAlpha:1.0]];
    
}

- (IBAction)clickedSignup:(id)sender {
    [self.btnSignup setBackgroundColor:[UIColor colorFromHexString:@"e7663f" withAlpha:1.0]];
    WDGetResetCodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDGetResetCodeViewController"];
    viewController.isFromSignUp = YES;
    [self.navigationController pushViewController:viewController animated:YES];
//    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"Thank you for downloading the MealsAround App. MealsAround goes live on 7 August!\nPlease do not order anything prior to this date unless you are a Participant in the Home Cooked Heroes Event on 6 August when you will be directed how to order." preferredStyle:LEAlertControllerStyleAlert];
//    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"OK" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
//        [self popupRegister];
//    }];
//    [alertController addAction:defaultAction];
//    [self presentAlertController:alertController animated:YES completion:nil];
}

- (void) popupRegister {
    LEAlertController *alertController = [LEAlertController alertControllerWithTitle:@"Notice" message:@"PLEASE READ THESE TERMS OF SERVICE CAREFULLY AS THEY CONTAIN IMPORTANT INFORMATION REGARDING YOUR LEGAL RIGHTS, REMEDIES AND OBLIGATIONS. IN PARTICULAR, SELLERS SHOULD UNDERSTAND HOW THE LAWS WORK IN THEIR RESPECTIVE CITIES. SOME CITIES HAVE LAWS THAT RESTRICT THEIR ABILITY TO SELL FOOD FROM THEIR RESIDENCE. THESE LAWS ARE OFTEN PART OF A CITY’S ZONING OR ADMINISTRATIVE CODES. IN MANY CITIES, SELLERS MUST REGISTER, GET A PERMIT, OR OBTAIN THE APPROPRIATE LICENSE BEFORE LISTING A FOOD PRODUCT OR ACCEPTING A FOOD ORDER. CERTAIN TYPES OF FOOD MAY BE PROHIBITED ALTOGETHER. LOCAL GOVERNMENTS VARY GREATLY IN HOW THEY ENFORCE THESE LAWS. PENALTIES MAY INCLUDE FINES OR OTHER ENFORCEMENT. SELLERS SHOULD REVIEW LOCAL LAWS BEFORE LISTING A FOOD PRODUCT ON MEALSAROUND." preferredStyle:LEAlertControllerStyleAlert];
    LEAlertAction *defaultAction = [LEAlertAction actionWithTitle:@"GOT IT" style:LEAlertActionStyleDefault handler:^(LEAlertAction *action) {
        WDGetResetCodeViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDGetResetCodeViewController"];
        viewController.isFromSignUp = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }];
    [alertController addAction:defaultAction];
    [self presentAlertController:alertController animated:YES completion:nil];
}

- (IBAction)clickedSignin:(id)sender {
    [self.btnSignin setBackgroundColor:[UIColor colorFromHexString:@"e7663f" withAlpha:1.0]];
    WDSignInViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WDSignInViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
