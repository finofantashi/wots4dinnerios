//
//  WDBlogAPIController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBlogAPIController.h"
#import "WDSourceConfig.h"
#import "NSDictionary+SafeValues.h"
#import "WDFoodType.h"
#import "WDUser.h"
#import <FCFileManager/FCFileManager.h>
#import "WDUserDefaultsManager.h"

static NSString * const APIBlogList               = @"/posts";
static NSString * const APICommentByPost          = @"/comments/ofPost/";
static NSString * const APIExploreMenu            = @"/explore-menu/";
static NSString * const APIPostComment            = @"/comments";

@implementation WDBlogAPIController

#pragma mark -
#pragma mark Init Methods

+ (WDBlogAPIController*)sharedInstance{
    static dispatch_once_t onceToken;
    static WDBlogAPIController* instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[WDBlogAPIController alloc]init];
    });
    return instance;
}

- (void)getBlogs:(long)page
 completionBlock:(WDBlogCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIBlogList] stringByAppendingString:[NSString stringWithFormat:@"?page=%ld",page]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getBlogDetail:(NSInteger)blogID
      completionBlock:(WDBlogCompletionBlock)completionBlock{
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIBlogList] stringByAppendingString:[NSString stringWithFormat:@"/%ld",blogID]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getCommentByBlog:(NSInteger)blogID
                    page:(long)page
 completionBlock:(WDBlogCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APICommentByPost] stringByAppendingString:[NSString stringWithFormat:@"%ld?page=%ld",(long)blogID,page]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getExploreMenuToday:(NSInteger)sellerID
                    page:(long)page
         completionBlock:(WDBlogCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
    NSDictionary* parameters = nil;
    if(current){
        parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", @"page", nil]];
    }else {
        parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"page", nil]];
    }
    
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIExploreMenu] stringByAppendingString:[NSString stringWithFormat:@"%ld/today",(long)sellerID]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getExploreMenuAll:(NSInteger)sellerID
                       page:(long)page
            completionBlock:(WDBlogCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
    NSDictionary* parameters = nil;
    if(current){
        parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", @"page", nil]];
    }else {
        parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"page", nil]];
    }
    
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIExploreMenu] stringByAppendingString:[NSString stringWithFormat:@"%ld/all",(long)sellerID]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)postComment:(NSNumber*)blogID
            content:(NSString *)content
    completionBlock:(WDBlogCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:blogID, content,nil] forKeys:[NSArray arrayWithObjects:@"post_id", @"content",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIPostComment] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

@end
