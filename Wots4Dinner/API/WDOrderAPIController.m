//
//  WDOrderAPIController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDOrderAPIController.h"
#import "WDUser.h"
#import "WDSourceConfig.h"
#import "WDShipping.h"
#import "WDCard.h"
#import "WDPayout.h"

static NSString * const APICart              = @"/carts";
static NSString * const APIShipping          = @"/shippingInfos";
static NSString * const APISaveShipping      = @"/carts/shipping-info";
static NSString * const APICard              = @"/paymentMethods";
static NSString * const APICheckout          = @"/checkout-credit-card";
static NSString * const APIPayout            = @"/payoutMethods";
static NSString * const APIUnreadOrder       = @"/orders/unread-orders";
static NSString * const APICoupon            = @"/applyCoupon";
static NSString * const APICancelCoupon      = @"/cancelCoupon";


@implementation WDOrderAPIController

#pragma mark -
#pragma mark Init Methods

+ (WDOrderAPIController*)sharedInstance{
    static dispatch_once_t onceToken;
    static WDOrderAPIController* instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[WDOrderAPIController alloc]init];
    });
    return instance;
}

- (void)addCart:(NSNumber *)dishID quantity:(NSNumber *)quantity completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:dishID, quantity,nil] forKeys:[NSArray arrayWithObjects:@"dish_id", @"quantity",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APICart] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getCarts:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APICart] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)updateCart:(NSNumber *)dishID cartID:(NSNumber *)cartID quantity:(NSNumber *)quantity completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:dishID, quantity,nil] forKeys:[NSArray arrayWithObjects:@"dish_id", @"quantity",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager PUT:[[[WDSourceConfig config].apiKey stringByAppendingString:APICart] stringByAppendingString:[NSString stringWithFormat:@"/%ld",[cartID longValue]]]
      parameters:parameters
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             completionBlock(nil, responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
                 completionBlock([serializedData valueForKey:@"message"],nil);
             }else{
                 completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
             }
         }];
}

- (void)updateCart:(NSNumber *)dishID cartID:(NSNumber *)cartID note:(NSString *)note completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:dishID, note,nil] forKeys:[NSArray arrayWithObjects:@"dish_id", @"note",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager PUT:[[[WDSourceConfig config].apiKey stringByAppendingString:APICart] stringByAppendingString:[NSString stringWithFormat:@"/%ld",[cartID longValue]]]
      parameters:parameters
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             completionBlock(nil, responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
                 completionBlock([serializedData valueForKey:@"message"],nil);
             }else{
                 completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
             }
         }];
}

- (void)deleteCart:(NSNumber *)cartID completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager DELETE:[[[WDSourceConfig config].apiKey stringByAppendingString:APICart] stringByAppendingString:[NSString stringWithFormat:@"/%ld",[cartID longValue]]]
         parameters:nil
            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                completionBlock(nil, responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
                    completionBlock([serializedData valueForKey:@"message"],nil);
                }else{
                    completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
                }
            }];
}

- (void)getShipping:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIShipping] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)saveShipping:(WDShipping *)shipping
            saveInfo:(NSInteger)saveInfo
            delivery:(NSInteger)delivery
     completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:shipping.name, shipping.address, shipping.phone, [NSNumber numberWithInteger:saveInfo],[NSNumber numberWithInteger:delivery],nil] forKeys:[NSArray arrayWithObjects:@"shipping_name", @"shipping_address",@"shipping_phone",@"save_info",@"delivery",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APISaveShipping] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getCards:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APICard] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)saveCard:(WDCard *)card
       isDefault:(NSString*)isDefault
 completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:card.cardNumber, card.firstName, card.lastName,[NSNumber numberWithInteger:card.expireMonth],[NSNumber numberWithInteger:card.expireYear],isDefault, nil] forKeys:[NSArray arrayWithObjects:@"card_number", @"first_name",@"last_name",@"expire_month",@"expire_year", @"default", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APICard] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)updateCard:(WDCard *)card
         isDefault:(NSString*)isDefault
   completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:card.cardNumber, card.firstName, card.lastName,[NSNumber numberWithInteger:card.expireMonth],[NSNumber numberWithInteger:card.expireYear],isDefault, nil] forKeys:[NSArray arrayWithObjects:@"card_number", @"first_name",@"last_name",@"expire_month",@"expire_year", @"default", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager PUT:[[[WDSourceConfig config].apiKey stringByAppendingString:APICard] stringByAppendingString:[NSString stringWithFormat:@"/%ld", card.cardID]]
      parameters:parameters
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             completionBlock(nil, responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
                 completionBlock([serializedData valueForKey:@"message"],nil);
             }else{
                 completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
             }
         }];
}

- (void)deleteCard:(NSNumber *)cardID
   completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager DELETE:[[[WDSourceConfig config].apiKey stringByAppendingString:APICard] stringByAppendingString:[NSString stringWithFormat:@"/%ld",[cardID longValue]]]
      parameters:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             completionBlock(nil, responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
                 completionBlock([serializedData valueForKey:@"message"],nil);
             }else{
                 completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
             }
         }];
}

- (void)checkOutCreditCard:(WDCard *)card
                       cvv:(NSString *)cvv
           completionBlock:(WDOrderCompletionBlock)completionBlock {
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:card.cardNumber, card.firstName, card.lastName, [NSNumber numberWithInteger:card.expireMonth],[NSNumber numberWithInteger:card.expireYear], card.cardType==nil?@"":card.cardType, cvv, nil] forKeys:[NSArray arrayWithObjects:@"card_number", @"first_name",@"last_name",@"expire_month",@"expire_year",@"card_type",@"cvv", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APICheckout] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getPayouts:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIPayout] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)savePayout:(WDPayout *)payout
   completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:payout.account, payout.currency, nil] forKeys:[NSArray arrayWithObjects:@"account", @"currency", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIPayout] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)savePayoutMethod:(WDPayoutMethod *)payoutMethod completionBlock:(WDOrderCompletionBlock)completionBlock {
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters;
    payoutMethod.address = @"";
    payoutMethod.city = @"";
    payoutMethod.state = @"";
    payoutMethod.zip = @"";
    payoutMethod.currency = @"";
    payoutMethod.bankSwift = @"";
    payoutMethod.bankAddress = @"";
    
    if([payoutMethod.method isEqualToString:@"PayPal"]){
        parameters =  [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:payoutMethod.method,payoutMethod.account,payoutMethod.currency,payoutMethod.address,payoutMethod.city,payoutMethod.state,payoutMethod.zip,[NSNumber numberWithInteger:payoutMethod.countryID],@"",@"",@"",@"", nil] forKeys:[NSArray arrayWithObjects:@"method",@"account",@"currency",@"address",@"city",@"state",@"zip_code", @"country_code_id",@"bank_swift",@"bank_name",@"bank_address",@"bank_account_name",nil]];
        
    }else{
        parameters =  [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:payoutMethod.method,payoutMethod.account,payoutMethod.bankSwift,payoutMethod.bankName,payoutMethod.bankAddress,payoutMethod.bankAccountName,payoutMethod.address,payoutMethod.city,payoutMethod.state,payoutMethod.zip,[NSNumber numberWithInteger:payoutMethod.countryID],@"", nil] forKeys:[NSArray arrayWithObjects:@"method",@"account",@"bank_swift",@"bank_name",@"bank_address",@"bank_account_name",@"address",@"city",@"state",@"zip_code", @"country_code_id", @"currency",nil]];
    }

    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIPayout] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)updatePayout:(NSInteger)payoutID
           isDefault:(NSString *)isDefault
     completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:isDefault, nil] forKeys:[NSArray arrayWithObjects:@"default", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager PUT:[[[WDSourceConfig config].apiKey stringByAppendingString:APIPayout] stringByAppendingString:[NSString stringWithFormat:@"/%ld", payoutID]]
      parameters:parameters
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             completionBlock(nil, responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
             NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
             if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
                 completionBlock([serializedData valueForKey:@"message"],nil);
             }else{
                 completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
             }
         }];
}

- (void)deletePayout:(NSInteger)payoutID
     completionBlock:(WDOrderCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager DELETE:[[[WDSourceConfig config].apiKey stringByAppendingString:APIPayout] stringByAppendingString:[NSString stringWithFormat:@"/%ld", payoutID]]
         parameters:nil
            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                completionBlock(nil, responseObject);
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
                    completionBlock([serializedData valueForKey:@"message"],nil);
                }else{
                    completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
                }
            }];
}

- (void)unreadOrders:(WDOrderCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIUnreadOrder] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)applyCoupon:(NSString *)code
    completionBlock:(WDOrderCompletionBlock)completionBlock {
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:code, tzName, nil] forKeys:[NSArray arrayWithObjects:@"code", @"timezone", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APICoupon] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)cancelCoupon:(WDOrderCompletionBlock)completionBlock {
    NSString *token = [[WDUser sharedInstance] getToken];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APICancelCoupon] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

@end
