//
//  WDMessageAPIController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/19/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMessageAPIController.h"
#import "WDUser.h"
#import "WDSourceConfig.h"

static NSString * const APIMessage                    = @"/messages";
static NSString * const APIReceivers                  = @"/receivers";
static NSString * const APIDeleteConversations        = @"/messages/deleteConversations";
static NSString * const APIMarkMesagesAsRead          = @"/messages/markMesagesAsRead";
static NSString * const APIUnreadMessage              = @"/messages/unread-messages";

@implementation WDMessageAPIController

#pragma mark Init Methods

+ (WDMessageAPIController*)sharedInstance{
    static dispatch_once_t onceToken;
    static WDMessageAPIController* instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[WDMessageAPIController alloc]init];
    });
    return instance;
}

- (void)getListMessage:(long)page
       completionBlock:(WDMessageCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMessage] stringByAppendingString:[NSString stringWithFormat:@"?page=%ld", page]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getListReceive:(WDMessageCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIReceivers] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)sendMessage:(long)groupID content:(NSString *)content receivers:(NSMutableArray *)receivers completionBlock:(WDMessageCompletionBlock)completionBlock {
    NSString *token = [[WDUser sharedInstance] getToken];
    NSArray *listReceiver = [NSArray arrayWithArray:[receivers mutableCopy]];
    NSDictionary* parameters;
    if(groupID!=0){
        parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithLong:groupID], content,listReceiver, nil] forKeys:[NSArray arrayWithObjects:@"group_id", @"content", @"receiver",nil]];
    }else{
        parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:content,listReceiver, nil] forKeys:[NSArray arrayWithObjects:@"content", @"receiver",nil]];

    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMessage] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getListMessageDetail:(long)recordID
                        page:(long)page
             completionBlock:(WDMessageCompletionBlock)completionBlock{
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMessage] stringByAppendingString:[NSString stringWithFormat:@"/%ld?page=%ld", recordID, page]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)deleteConversations:(NSMutableArray *)listConversation
                  listGroup:(NSMutableArray *)listGroup
            completionBlock:(WDMessageCompletionBlock)completionBlock {
    NSString *token = [[WDUser sharedInstance] getToken];
    NSArray *conversationIDs = [NSArray arrayWithArray:[listConversation mutableCopy]];
    NSArray *groupIDs = [NSArray arrayWithArray:[listGroup mutableCopy]];

    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:conversationIDs, groupIDs, nil] forKeys:[NSArray arrayWithObjects:@"conversation_id", @"group_id", nil]];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDeleteConversations] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)markMesagesAsRead:(NSMutableArray *)listMessage
          completionBlock:(WDMessageCompletionBlock)completionBlock {
    NSString *token = [[WDUser sharedInstance] getToken];
    NSArray *messageIDs = [NSArray arrayWithArray:[listMessage mutableCopy]];
    
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:messageIDs, nil] forKeys:[NSArray arrayWithObjects:@"message_id", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMarkMesagesAsRead] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)unreadMessages:(WDMessageCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIUnreadMessage] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)showWithUser:(long)userID
                page:(long)page
     completionBlock:(WDMessageCompletionBlock)completionBlock{
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMessage] stringByAppendingString:[NSString stringWithFormat:@"/showWithUser/%ld?page=%ld", userID, page]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

@end
