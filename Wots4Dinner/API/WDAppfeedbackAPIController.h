//
//  WDAppfeedbackAPIController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBaseSource.h"
#import "AFNetworking.h"

typedef void(^WDAppFeedbackCompletionBlock)(NSString* errorString, id responseObject);

@interface WDAppFeedbackAPIController : WDBaseSource

+ (WDAppFeedbackAPIController*)sharedInstance;

- (void)sendAppFeedback:(NSURL*)imageURL
               nameFile:(NSString*)nameFile
               mimeType:(NSString*)mimeType
            description:(NSString*)description
             completion:(WDAppFeedbackCompletionBlock)completionBlock;

- (void)sendCertified:(NSURL *)fileURL
                        note:(NSString *)note
                  completion:(WDAppFeedbackCompletionBlock)completionBlock;

- (void)sendKitchenVideo:(NSURL *)fileURL
completion:(WDAppFeedbackCompletionBlock)completionBlock;
@end
