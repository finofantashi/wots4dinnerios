//
//  WDFoodMenuAPIController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDFoodMenuAPIController.h"
#import "WDSourceConfig.h"
#import "NSDictionary+SafeValues.h"
#import "WDFoodType.h"
#import "WDUser.h"
#import <FCFileManager/FCFileManager.h>
#import "WDUserDefaultsManager.h"
#import "WDSeller.h"
#import "WDDish.h"
#import "WDGallery.h"

static NSString * const APITodayMenu              = @"/todayMenu";
static NSString * const APIPopularOrders          = @"/popular-orders";
static NSString * const APIDishByFoodType         = @"/dishes/foodType/";
static NSString * const APIDish                   = @"/dishes";
static NSString * const APIDishStatus             = @"/dishes/changeStatus";
static NSString * const APIListFoodType           = @"/foodTypes";
static NSString * const APIListCategory           = @"/categories";
static NSString * const APISearchDish             = @"/dishes-search";
static NSString * const APIFavoriteSellers        = @"/favouriteSellers";
static NSString * const APIWishLists              = @"/wishLists";
static NSString * const APIFavoriteSellersOf      = @"/favouriteOf";
static NSString * const APIYouMayAlsoLike         = @"/youMayAlsoLike";
static NSString * const APISeller                 = @"/sellers";
static NSString * const APISellerUpdate           = @"/sellers/updateInfo";
static NSString * const APIDelivery               = @"/deliveries";
static NSString * const APIMyMenu                 = @"/myMenu";
static NSString * const APIDeliveryServices       = @"/deliveryServices";
static NSString * const APIMyOrder                = @"/myOrder";
static NSString * const APIMyOrderItem            = @"/myOrderItems";
static NSString * const APICurrency               = @"/currencies";
static NSString * const APICountryCode            = @"/countryCodes?orderBy=country";
static NSString * const APICurrencyByCountry      = @"/currencies/getByCountry";
static NSString * const APIImageDish              = @"/galleries";
static NSString * const APIOrderHistory           = @"/orderHistory";
static NSString * const APIOrderHistoryItem       = @"/orderHistoryItems";
static NSString * const APIConfirmOrderItem       = @"/confirm-order-item";
static NSString * const APIdeclineOrderItem       = @"/decline-order-item";
static NSString * const APIOrderComplete          = @"/order-complete";

@implementation WDFoodMenuAPIController

#pragma mark -
#pragma mark Init Methods

+ (WDFoodMenuAPIController*)sharedInstance{
  static dispatch_once_t onceToken;
  static WDFoodMenuAPIController* instance = nil;
  dispatch_once(&onceToken, ^{
    instance = [[WDFoodMenuAPIController alloc]init];
  });
  return instance;
}

- (void)getTodayMenu:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  NSDictionary* parameters = nil;
  if(current){
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName,[current safeNumberForKey:@"kLat"],[current safeNumberForKey:@"kLng"], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat",  @"lng", nil]];
  }else {
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, nil] forKeys:[NSArray arrayWithObjects:@"timezone", nil]];
  }
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APITodayMenu] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getPopularOrders:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  NSDictionary* parameters = nil;
  if(current){
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", nil]];
  }else {
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, nil] forKeys:[NSArray arrayWithObjects:@"timezone", nil]];
  }
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIPopularOrders] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getDishByFoodType:(NSInteger)foodTypeID
                     page:(long)page
          completionBlock:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  NSDictionary* parameters = nil;
  if(current){
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], [NSNumber numberWithDouble:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", @"page", nil]];
  }else {
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [NSNumber numberWithDouble:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"page", nil]];
  }
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDishByFoodType] stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)foodTypeID]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getYouMayAlsoLike:(NSInteger)foodTypeID
          completionBlock:(WDFoodCompletionBlock)completionBlock{
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  NSDictionary* parameters = nil;
  if(current){
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", @"page", nil]];
  }else {
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"page", nil]];
  }
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIYouMayAlsoLike] stringByAppendingString:[NSString stringWithFormat:@"/%ld",(long)foodTypeID]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getDishDetail:(NSInteger)dishID
      completionBlock:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDish] stringByAppendingString:[NSString stringWithFormat:@"/%ld",(long)dishID]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getFoodTypes:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIListFoodType] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getCategories:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIListCategory] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)searchDish:(double)lat
               lng:(double)lng
          distance:(long)distance
       foodTypeIDs:(NSMutableArray*)foodTypeIDs
         priceFrom:(long)priceFrom
           priceTo:(long)priceTo
              page:(long)page
   completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSString *stringFoodTypeIDs = [foodTypeIDs componentsJoinedByString:@","];
  NSLog(@"%@",[[[WDSourceConfig config].apiKey stringByAppendingString:APISearchDish] stringByAppendingString:[NSString stringWithFormat:@"?lat=%f&lng=%f&distance=%ld&foodtype=%@&price_from=%ld&price_to=%ld&page=%ld",lat,lng,distance, stringFoodTypeIDs ,priceFrom,priceTo,page]]);
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APISearchDish] stringByAppendingString:[NSString stringWithFormat:@"?lat=%f&lng=%f&distance=%ld&foodtype=%@&price_from=%ld&price_to=%ld&page=%ld",lat,lng,distance,stringFoodTypeIDs,priceFrom,priceTo,page]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"], nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)favoriteSeller:(NSInteger)sellerId
       completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInteger:sellerId], nil] forKeys:[NSArray arrayWithObjects:@"seller_id", nil]];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIFavoriteSellers] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)wishListsDish:(NSInteger)dishId
      completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInteger:dishId], nil] forKeys:[NSArray arrayWithObjects:@"dish_id", nil]];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIWishLists] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getWishList:(long)page
             userID:(NSInteger)userID
    completionBlock:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  NSString *urlString = @"";
  if(userID!=0){
    urlString = [[[WDSourceConfig config].apiKey stringByAppendingString:APIWishLists] stringByAppendingString:[NSString stringWithFormat:@"/%ld?page=%ld",(long)userID,page]];
  }else{
    urlString = [[[WDSourceConfig config].apiKey stringByAppendingString:APIWishLists] stringByAppendingString:[NSString stringWithFormat:@"?page=%ld",page]];
  }
  [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getFavoriteSellers:(long)page
                    userID:(NSInteger)userID
        isFavoriteSellerOf:(BOOL)isFavoriteSellerOf
           completionBlock:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  NSString *API = APIFavoriteSellers;
  if(isFavoriteSellerOf){
    API = APIFavoriteSellersOf;
  }
  NSString *urlString = @"";
  if(userID!=0){
    urlString = [[[WDSourceConfig config].apiKey stringByAppendingString:API] stringByAppendingString:[NSString stringWithFormat:@"/%ld?page=%ld",(long)userID,page]];
  }else{
    urlString = [[[WDSourceConfig config].apiKey stringByAppendingString:API] stringByAppendingString:[NSString stringWithFormat:@"?page=%ld",page]];
  }
  
  [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)deleteFavoriteSeller:(NSInteger)favoriteSellerID
             completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager DELETE:[[[WDSourceConfig config].apiKey stringByAppendingString:APIFavoriteSellers] stringByAppendingString:[NSString stringWithFormat:@"/%ld",(long)favoriteSellerID]]
       parameters:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil, responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)deleteWishListDish:(NSInteger)wishListID
           completionBlock:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager DELETE:[[[WDSourceConfig config].apiKey stringByAppendingString:APIWishLists] stringByAppendingString:[NSString stringWithFormat:@"/%ld",(long)wishListID]]
       parameters:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil, responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)registerSeller:(WDSeller *)seller
       completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  NSDictionary* parameters;
  seller.payoutMethod.address = @"";
  seller.payoutMethod.city = @"";
  seller.payoutMethod.state = @"";
  seller.payoutMethod.zip = @"";
  seller.payoutMethod.currency = @"";
  seller.payoutMethod.bankSwift = @"";
  seller.payoutMethod.bankAddress = @"";
  
  if([seller.payoutMethod.method isEqualToString:@"PayPal"]){
    parameters =  [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInteger:seller.categoryID], seller.kitchenName, seller.location, [NSNumber numberWithDouble:seller.lat],[NSNumber numberWithDouble:seller.lng], seller.sellerDescription, seller.payoutMethod.method,seller.payoutMethod.account,seller.payoutMethod.currency,seller.payoutMethod.address,seller.payoutMethod.city,seller.payoutMethod.state,seller.payoutMethod.zip,[NSNumber numberWithInteger:seller.payoutMethod.countryID],@"",@"",@"",@"", nil] forKeys:[NSArray arrayWithObjects:@"category_id",@"kitchen_name",@"location",@"lat",@"lng",@"description", @"method",@"account",@"currency",@"address",@"city",@"state",@"zip_code", @"country_code_id",@"bank_swift",@"bank_name",@"bank_address",@"bank_account_name",nil]];
    
  }else{
    parameters =  [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInteger:seller.categoryID], seller.kitchenName, seller.location, [NSNumber numberWithDouble:seller.lat],[NSNumber numberWithDouble:seller.lng], seller.sellerDescription, seller.payoutMethod.method,seller.payoutMethod.account,seller.payoutMethod.bankSwift,seller.payoutMethod.bankName,seller.payoutMethod.bankAddress,seller.payoutMethod.bankAccountName,seller.payoutMethod.address,seller.payoutMethod.city,seller.payoutMethod.state,seller.payoutMethod.zip,[NSNumber numberWithInteger:seller.payoutMethod.countryID],@"", nil] forKeys:[NSArray arrayWithObjects:@"category_id",@"kitchen_name",@"location",@"lat",@"lng",@"description", @"method",@"account",@"bank_swift",@"bank_name",@"bank_address",@"bank_account_name",@"address",@"city",@"state",@"zip_code", @"country_code_id", @"currency",nil]];
  }
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APISeller] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil, responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData objectForKey:@"message"]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)updateSeller:(WDSeller *)seller
     completionBlock:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  NSDictionary *parameters =  [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInteger:seller.categoryID], seller.kitchenName, seller.location, [NSNumber numberWithDouble:seller.lat],[NSNumber numberWithDouble:seller.lng], seller.sellerDescription, nil] forKeys:[NSArray arrayWithObjects:@"category_id",@"kitchen_name",@"location",@"lat",@"lng",@"description", nil]];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APISellerUpdate] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil, responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getSeller:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APISeller] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)addDish:(WDDish*)dish
          image:(UIImage*)image
      galleries:(NSMutableArray*)galleries
     completion:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDish] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    if(image){
      NSData * binaryImageData = UIImageJPEGRepresentation(image, 1.0);
      [formData appendPartWithFileData:binaryImageData name:@"image" fileName:@"dish.jpg" mimeType:@"image/jpeg"];
    }
    
    if(galleries.count>1){
      for (int i = 1; i < galleries.count; i++) {
        NSData * binaryImageData = UIImageJPEGRepresentation(((WDGallery*)[galleries objectAtIndex:i]).image, 1.0);
        [formData appendPartWithFileData:binaryImageData name:[NSString stringWithFormat:@"gallery[%i]",i-1] fileName:@"dish.jpg" mimeType:@"image/jpeg"];
      }
    }
    
    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%ld",dish.foodTypeID] dataUsingEncoding:NSUTF8StringEncoding] name:@"food_type_id"];
    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%ld",dish.currencyID] dataUsingEncoding:NSUTF8StringEncoding] name:@"currency_id"];
    [formData appendPartWithFormData:[dish.dishName dataUsingEncoding:NSUTF8StringEncoding] name:@"name"];
    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%f",dish.price] dataUsingEncoding:NSUTF8StringEncoding] name:@"price"];
    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%ld",dish.quantity] dataUsingEncoding:NSUTF8StringEncoding] name:@"quantity"];
    [formData appendPartWithFormData:[dish.dishDescription dataUsingEncoding:NSUTF8StringEncoding] name:@"description"];
    [formData appendPartWithFormData:[dish.pickupTimeFrom dataUsingEncoding:NSUTF8StringEncoding] name:@"pickup_time_from"];
    [formData appendPartWithFormData:[dish.pickupTimeTo dataUsingEncoding:NSUTF8StringEncoding] name:@"pickup_time_to"];
    [formData appendPartWithFormData:[dish.servingDate dataUsingEncoding:NSUTF8StringEncoding] name:@"serving_date"];
    [formData appendPartWithFormData:[dish.repeat dataUsingEncoding:NSUTF8StringEncoding] name:@"repeat"];
    
  } progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil, responseObject);
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
    
  }];
}

- (void)updateDish:(NSInteger)dishID
              dish:(WDDish *)dish
             image:(UIImage *)image
         galleries:(NSMutableArray *)galleries
        completion:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDish] stringByAppendingString:[NSString stringWithFormat:@"/%ld?token=%@",dishID,token]] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    if(image){
      NSData * binaryImageData = UIImageJPEGRepresentation(image, 1.0);
      [formData appendPartWithFileData:binaryImageData name:@"image" fileName:@"dish.jpg" mimeType:@"image/jpeg"];
    }
    
    if(galleries.count>1){
      for (int i = 1; i < galleries.count; i++) {
        WDGallery *object = [galleries objectAtIndex:i];
        if(object.galleryID < 0){
          NSData * binaryImageData = UIImageJPEGRepresentation(object.image, 1.0);
          [formData appendPartWithFileData:binaryImageData name:[NSString stringWithFormat:@"gallery[%i]",i-1] fileName:@"dish.jpg" mimeType:@"image/jpeg"];
        }
      }
    }
    
    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%ld",dish.foodTypeID] dataUsingEncoding:NSUTF8StringEncoding] name:@"food_type_id"];
    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%ld",dish.currencyID] dataUsingEncoding:NSUTF8StringEncoding] name:@"currency_id"];
    [formData appendPartWithFormData:[dish.dishName dataUsingEncoding:NSUTF8StringEncoding] name:@"name"];
    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%f",dish.price] dataUsingEncoding:NSUTF8StringEncoding] name:@"price"];
    [formData appendPartWithFormData:[[NSString stringWithFormat:@"%ld",dish.quantity] dataUsingEncoding:NSUTF8StringEncoding] name:@"quantity"];
    [formData appendPartWithFormData:[dish.dishDescription dataUsingEncoding:NSUTF8StringEncoding] name:@"description"];
    [formData appendPartWithFormData:[dish.pickupTimeFrom dataUsingEncoding:NSUTF8StringEncoding] name:@"pickup_time_from"];
    [formData appendPartWithFormData:[dish.pickupTimeTo dataUsingEncoding:NSUTF8StringEncoding] name:@"pickup_time_to"];
    [formData appendPartWithFormData:[dish.servingDate dataUsingEncoding:NSUTF8StringEncoding] name:@"serving_date"];
    [formData appendPartWithFormData:[dish.repeat dataUsingEncoding:NSUTF8StringEncoding] name:@"repeat"];
    
  } progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil, responseObject);
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
    
  }];
}

- (void)deleteDish:(NSInteger)dishID
        completion:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager DELETE:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDish] stringByAppendingString:[NSString stringWithFormat:@"/%ld",(long)dishID]]
       parameters:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil, responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)addDeliveries:(NSInteger)dishID
       isDelivery:(NSInteger)isDelivery
          homeService:(NSString *)homeService
           currencyID:(NSString *)currencyID
                price:(float)price
           radius:(NSString *)radius
      completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
  [parameters setObject:[NSNumber numberWithInteger:dishID] forKey:@"dish_id"];
  [parameters setObject:[NSNumber numberWithInteger:isDelivery] forKey:@"is_delivery"];
  [parameters setObject:homeService forKey:@"special_offer"];

  if (currencyID != nil) {
    [parameters setObject:currencyID forKey:@"currency_id"];
    [parameters setObject:[NSString stringWithFormat:@"%f",price] forKey:@"fee"];
    [parameters setObject:radius forKey:@"radius"];
  } else {
    [parameters setObject:[NSString stringWithFormat:@"%f",price] forKey:@"fee"];
  }
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
   [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
   manager.requestSerializer = [AFJSONRequestSerializer serializer];
   manager.securityPolicy.allowInvalidCertificates = YES;
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDelivery] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)addDeliveryService:(NSInteger )isDelivery
               homeService:(NSString *)homeService
                currencyID:(NSString *)currencyID
                     price:(float)price
                    radius:(NSString *)radius
           completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
  [parameters setObject:homeService forKey:@"special_offer"];
  [parameters setObject:[NSNumber numberWithInteger:isDelivery] forKey:@"is_delivery"];


  if (currencyID != nil) {
    [parameters setObject:currencyID forKey:@"currency_id"];
    [parameters setObject:[NSString stringWithFormat:@"%f",price] forKey:@"fee"];
    [parameters setObject:radius forKey:@"radius"];
  } else {
    [parameters setObject:[NSString stringWithFormat:@"%f",price] forKey:@"fee"];
  }
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDeliveryServices] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getDeliveries:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIDeliveryServices] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getMyMenuToday:(long)page
       completionBlock:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  NSDictionary* parameters = nil;
  if(current){
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", @"page", nil]];
  }else {
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"page", nil]];
  }
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMyMenu] stringByAppendingString:[NSString stringWithFormat:@"/today"]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getMyMenuAll:(long)page
     completionBlock:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  NSDictionary* parameters = nil;
  if(current){
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", @"page", nil]];
  }else {
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"page", nil]];
  }
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMyMenu] stringByAppendingString:[NSString stringWithFormat:@"/all"]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)changeStatusDish:(NSInteger)dishID
                  status:(NSString*)status
         completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDishStatus] stringByAppendingString:[NSString stringWithFormat:@"/%ld?token=%@", dishID, token]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getMyOrderToday:(NSString *)search
                   page:(long)page
        completionBlock:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  NSDictionary* parameters = nil;
  if(current){
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], [NSNumber numberWithInteger:page], search, nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", @"page", @"search", nil]];
  }else {
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"page", nil]];
  }
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMyOrder] stringByAppendingString:[NSString stringWithFormat:@"/today"]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getMyOrderAll:(NSString *)search
                 page:(long)page
      completionBlock:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSTimeZone *timeZone = [NSTimeZone localTimeZone];
  NSString *tzName = [timeZone name];
  NSDictionary *current = [WDUserDefaultsManager valueForKey:@"kCurrentLocation"];
  NSDictionary* parameters = nil;
  if(current){
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [current safeNumberForKey:@"kLat"], [current safeNumberForKey:@"kLng"], [NSNumber numberWithInteger:page], search, nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"lat", @"lng", @"page", @"search", nil]];
  }else {
    parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tzName, [NSNumber numberWithInteger:page], nil] forKeys:[NSArray arrayWithObjects:@"timezone", @"page", nil]];
  }
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMyOrder] stringByAppendingString:[NSString stringWithFormat:@"/all"]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getMyOrderItem:(NSInteger)orderID completionBlock:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIMyOrderItem] stringByAppendingString:[NSString stringWithFormat:@"/%ld", orderID]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getCurrencies:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APICurrency] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getCountryCodes:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APICountryCode] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getCurrencyByCountry:(NSInteger)countryID completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithInteger:countryID], nil] forKeys:[NSArray arrayWithObjects:@"id", nil]];
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APICurrencyByCountry] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)deleteImageDish:(NSInteger)imageID
             completion:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager DELETE:[[[WDSourceConfig config].apiKey stringByAppendingString:APIImageDish] stringByAppendingString:[NSString stringWithFormat:@"/%ld",(long)imageID]]
       parameters:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil, responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getOrderHistory:(NSString *)search
                   page:(long)page
        completionBlock:(WDFoodCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects: [NSNumber numberWithInteger:page], search, nil] forKeys:[NSArray arrayWithObjects:@"page", @"search", nil]];
  
  [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIOrderHistory] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getOrderHistoryItem:(NSInteger)orderID completionBlock:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIOrderHistoryItem] stringByAppendingString:[NSString stringWithFormat:@"/%ld", orderID]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)confirmOrderItem:(NSInteger)dishID
         completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIConfirmOrderItem] stringByAppendingString:[NSString stringWithFormat:@"/%ld", dishID]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)declineOrderItem:(NSInteger)dishID
         completionBlock:(WDFoodCompletionBlock)completionBlock{
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIdeclineOrderItem] stringByAppendingString:[NSString stringWithFormat:@"/%ld", dishID]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)getOrderCompleteItem:(NSString *)orderNum completionBlock:(WDFoodCompletionBlock)completionBlock {
  NSString *token = [[WDUser sharedInstance] getToken];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.securityPolicy.allowInvalidCertificates = YES;
  [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
  
  [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIOrderComplete] stringByAppendingString:[NSString stringWithFormat:@"/?order=%@", orderNum]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
    
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

@end
