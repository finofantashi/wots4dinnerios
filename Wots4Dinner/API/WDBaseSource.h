//
//  WDBaseSource.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDBaseSource : NSObject

@property (nonatomic, strong) NSOperationQueue* operationQueue;

- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData jsonPatternFile:(NSString*)jsonFile;

@end
