//
//  WDFAQsAPIController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBaseSource.h"
#import "AFNetworking.h"

typedef void(^WDFAQsCompletionBlock)(NSString* errorString, id responseObject);

@interface WDFAQsAPIController : WDBaseSource

+ (WDFAQsAPIController*)sharedInstance;

- (void)getListQuestion:(NSString *)question
   completionBlock:(WDFAQsCompletionBlock)completionBlock;

- (void)getQuestion:(NSInteger)questionID
        completionBlock:(WDFAQsCompletionBlock)completionBlock;
@end
