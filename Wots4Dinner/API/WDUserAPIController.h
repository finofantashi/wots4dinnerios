//
//  HCDemoMapSource.h
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//


#import "WDBaseSource.h"
#import "AFNetworking.h"

@class WDUser,WDUserTemp;

typedef void(^WDSignupCompletionBlock)(NSString* errorString);
typedef void(^WDUserCompletionBlock)(NSString* errorString, id responseObject);

@interface WDUserAPIController : WDBaseSource

+ (WDUserAPIController*)sharedInstance;

- (void)signin:(NSString*)userName
      password:(NSString*)password
    completion:(WDSignupCompletionBlock)completionBlock;

- (void)signup:(NSString*)phoneNumber
      password:(NSString*)password
   phonePrefix:(NSString*)phonePrefix
    completion:(WDSignupCompletionBlock)completionBlock;

- (void)getUserInformation:(WDSignupCompletionBlock)completionBlock;

- (void)verifyCode:(NSString*)userName
              code:(NSString*)code
      isForgotPass:(BOOL)isForgotPass
        completion:(WDSignupCompletionBlock)completionBlock;

- (void)sendVerificationCode:(NSString*)userName
                  completion:(WDSignupCompletionBlock)completionBlock;

- (void)updateUser:(NSString*)phoneNumber
       phonePrefix:(NSString*)phonePrefix
          password:(NSString*)password
         firstName:(NSString*)firstName
          lastName:(NSString*)lastName
             email:(NSString*)email
        completion:(WDSignupCompletionBlock)completionBlock;

- (void)updateUser:(WDUserTemp*)user
        completion:(WDSignupCompletionBlock)completionBlock;

- (void)forgotPassword:(NSString*)phoneNumber
           phonePrefix:(NSString*)phonePrefix
            completion:(WDSignupCompletionBlock)completionBlock;

- (void)resetPassword:(NSString*)code
             userName:(NSString*)userName
             password:(NSString*)password
           completion:(WDSignupCompletionBlock)completionBlock;

- (void)uploadAvatar:(NSURL*)avatarURL
            nameFile:(NSString*)nameFile
            mimeType:(NSString*)mimeType
          completion:(WDSignupCompletionBlock)completionBlock;

- (void)changePhoneNumber:(NSString*)phoneNumber
              phonePrefix:(NSString*)phonePrefix
               completion:(WDSignupCompletionBlock)completionBlock;

- (void)verifyCodeChangePhone:(NSString*)phoneNumber
                         code:(NSString*)code
                  phonePrefix:(NSString*)phonePrefix
                   completion:(WDSignupCompletionBlock)completionBlock;

- (void)changePassword:(NSString*)password
       currentPassword:(NSString*)currentPassword
            completion:(WDSignupCompletionBlock)completionBlock;

- (void)getUserInformationByID:(NSInteger)idUser
                    completion:(WDUserCompletionBlock)completionBlock;

- (void)logout;

@end
