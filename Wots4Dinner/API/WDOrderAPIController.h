//
//  WDOrderAPIController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBaseSource.h"
#import "AFNetworking.h"
#import "WDPayoutMethod.h"

@class WDShipping, WDCard, WDPayout;

typedef void(^WDOrderCompletionBlock)(NSString* errorString, id responseObject);

@interface WDOrderAPIController : WDBaseSource

+ (WDOrderAPIController*)sharedInstance;

- (void)addCart:(NSNumber*)dishID
            quantity:(NSNumber*)quantity
    completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)getCarts:(WDOrderCompletionBlock)completionBlock;

- (void)updateCart:(NSNumber*)dishID
            cartID:(NSNumber*)cartID
       quantity:(NSNumber*)quantity
completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)updateCart:(NSNumber*)dishID
            cartID:(NSNumber*)cartID
          note:(NSString*)note
   completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)deleteCart:(NSNumber *)cartID
   completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)getShipping:(WDOrderCompletionBlock)completionBlock;

- (void)saveShipping:(WDShipping *)shipping
            saveInfo:(NSInteger)saveInfo
            delivery:(NSInteger)delivery
     completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)getCards:(WDOrderCompletionBlock)completionBlock;

- (void)saveCard:(WDCard *)card
 isDefault:(NSString*)isDefault
 completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)updateCard:(WDCard *)card
         isDefault:(NSString*)isDefault
   completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)deleteCard:(NSNumber *)cardID
           completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)checkOutCreditCard:(WDCard *)card
                 cvv:(NSString*)cvv
           completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)getPayouts:(WDOrderCompletionBlock)completionBlock;

- (void)savePayout:(WDPayout *)payout
 completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)savePayoutMethod:(WDPayoutMethod *)payoutMethod
   completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)updatePayout:(NSInteger)payoutID
         isDefault:(NSString*)isDefault
   completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)deletePayout:(NSInteger)payoutID
   completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)unreadOrders:(WDOrderCompletionBlock)completionBlock;

- (void)applyCoupon:(NSString*)code
     completionBlock:(WDOrderCompletionBlock)completionBlock;

- (void)cancelCoupon:(WDOrderCompletionBlock)completionBlock;

@end
