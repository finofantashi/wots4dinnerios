//
//  WDSourceConfig.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSourceConfig.h"
#import "NSDictionary+SafeValues.h"




/*!
 *  @brief API Key
 */
#define kConfigApiKey           @"api_url_uat"
#define kConfigApiKeyDev        @"api_url_dev"
#define kConfigApiKeyProduction @"api_url_production"

#define kConfigApiFAQ           @"api_url_faq"
#define kConfigApiHelp          @"url_help"

@implementation WDSourceConfig

#pragma mark -
#pragma mark Init Methods

+ (WDSourceConfig*)config{
    static dispatch_once_t onceToken;
    static WDSourceConfig* instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[WDSourceConfig alloc] init];
    });
    return instance;
}

- (id) init{
    self = [super init];
    if (self){
        NSBundle* bundle = [NSBundle bundleForClass:[self class]];
        NSDictionary* config = [[NSDictionary alloc]initWithContentsOfFile:[bundle pathForResource:@"WDAPIConfig" ofType:@"plist"]];
        _apiKey  = [config safeStringForKey:kConfigApiKeyProduction];
        _webURL = @"https://mealsaround.com/";
//        _apiFAQ  = [config safeStringForKey:kConfigApiFAQProduction];
//        _apiHelp = [config safeStringForKey:kConfigApiHelp];
    }
    return self;
}

@end
