//
//  WDMessageAPIController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/19/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBaseSource.h"
#import "AFNetworking.h"

typedef void(^WDMessageCompletionBlock)(NSString* errorString, id responseObject);

@interface WDMessageAPIController : WDBaseSource
+ (WDMessageAPIController*)sharedInstance;

- (void)getListMessage:(long)page
       completionBlock:(WDMessageCompletionBlock)completionBlock;

- (void)getListReceive:(WDMessageCompletionBlock)completionBlock;

- (void)sendMessage:(long)groupID
            content:(NSString*)content
          receivers:(NSMutableArray*)receivers
    completionBlock:(WDMessageCompletionBlock)completionBlock;

- (void)getListMessageDetail:(long)recordID
                        page:(long)page
             completionBlock:(WDMessageCompletionBlock)completionBlock;

- (void)deleteConversations:(NSMutableArray*)listConversation
                  listGroup:(NSMutableArray*)listGroup
            completionBlock:(WDMessageCompletionBlock)completionBlock;

- (void)markMesagesAsRead:(NSMutableArray*)listMessage
            completionBlock:(WDMessageCompletionBlock)completionBlock;

- (void)unreadMessages:(WDMessageCompletionBlock)completionBlock;

- (void)showWithUser:(long)userID
                        page:(long)page
             completionBlock:(WDMessageCompletionBlock)completionBlock;

@end
