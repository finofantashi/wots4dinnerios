//
//  WDFAQsAPIController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDReviewAPIController.h"
#import "WDUser.h"
#import "WDSourceConfig.h"

static NSString * const APISellerReviews              = @"/sellerReviews";
static NSString * const APIDishReviews                = @"/dishReviews";

@implementation WDReviewAPIController

#pragma mark -
#pragma mark Init Methods

+ (WDReviewAPIController*)sharedInstance{
    static dispatch_once_t onceToken;
    static WDReviewAPIController* instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[WDReviewAPIController alloc]init];
    });
    return instance;
}

- (void)getListSellerReview:(NSInteger)sellerID
                       page:(long)page
            completionBlock:(WDReviewCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APISellerReviews] stringByAppendingString:[NSString stringWithFormat:@"/%ld?page=%ld", (long)sellerID, page]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getListDishReview:(NSInteger)dishID
                    page:(long)page
         completionBlock:(WDReviewCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDishReviews] stringByAppendingString:[NSString stringWithFormat:@"/%ld?page=%ld", (long)dishID, page]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)dishReview:(NSNumber *)dishID
           content:(NSString *)content
   completionBlock:(WDReviewCompletionBlock)completionBlock{
    
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:dishID, content,nil] forKeys:[NSArray arrayWithObjects:@"dish_id", @"content",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIDishReviews] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)sellerReview:(NSNumber *)sellerID
           content:(NSString *)content
             rating:(float)rating
   completionBlock:(WDReviewCompletionBlock)completionBlock{
    if(!content){
        content = @"";
    }
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:sellerID, content, [NSNumber numberWithFloat:rating], nil] forKeys:[NSArray arrayWithObjects:@"seller_id", @"review", @"rating", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APISellerReviews] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getListNeedToReview:(WDReviewCompletionBlock)completionBlock {
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APISellerReviews] stringByAppendingString:[NSString stringWithFormat:@"/needToReview?timezone=%@", tzName]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

@end
