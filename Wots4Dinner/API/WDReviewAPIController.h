//
//  WDReviewAPIController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBaseSource.h"
#import "AFNetworking.h"

typedef void(^WDReviewCompletionBlock)(NSString* errorString, id responseObject);

@interface WDReviewAPIController : WDBaseSource

+ (WDReviewAPIController*)sharedInstance;

- (void)getListSellerReview:(NSInteger )sellerID
                  page:(long)page
   completionBlock:(WDReviewCompletionBlock)completionBlock;

- (void)getListDishReview:(NSInteger )dishID
                       page:(long)page
            completionBlock:(WDReviewCompletionBlock)completionBlock;

- (void)dishReview:(NSNumber*)dishID
            content:(NSString*)content
    completionBlock:(WDReviewCompletionBlock)completionBlock;

- (void)sellerReview:(NSNumber *)sellerID
             content:(NSString *)content
              rating:(float)rating
     completionBlock:(WDReviewCompletionBlock)completionBlock;

- (void)getListNeedToReview:(WDReviewCompletionBlock)completionBlock;
@end
