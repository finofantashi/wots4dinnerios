//
//  WDFoodMenuAPIController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBaseSource.h"
#import "AFNetworking.h"
@class WDSeller,WDDish;

typedef void(^WDFoodCompletionBlock)(NSString* errorString, id responseObject);

@interface WDFoodMenuAPIController : WDBaseSource

+ (WDFoodMenuAPIController*)sharedInstance;
- (void)getTodayMenu:(WDFoodCompletionBlock)completionBlock;

- (void)getPopularOrders:(WDFoodCompletionBlock)completionBlock;

- (void)getDishByFoodType:(NSInteger)foodTypeID
                     page:(long)page
          completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getYouMayAlsoLike:(NSInteger)foodTypeID
          completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getDishDetail:(NSInteger)dishID
      completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getFoodTypes:(WDFoodCompletionBlock)completionBlock;

- (void)getCategories:(WDFoodCompletionBlock)completionBlock;

- (void)searchDish:(double)lat
               lng:(double)lng
          distance:(long)distance
       foodTypeIDs:(NSMutableArray*)foodTypeIDs
         priceFrom:(long)priceFrom
           priceTo:(long)priceTo
              page:(long)page
   completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)favoriteSeller:(NSInteger)sellerId
       completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)wishListsDish:(NSInteger)dishId
      completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getWishList:(long)page
             userID:(NSInteger)userID
    completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getFavoriteSellers:(long)page
                    userID:(NSInteger)userID
        isFavoriteSellerOf:(BOOL)isFavoriteSellerOf
           completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)deleteFavoriteSeller:(NSInteger)favoriteSellerID
             completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)deleteWishListDish:(NSInteger)wishListID
           completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)registerSeller:(WDSeller*)seller
       completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)updateSeller:(WDSeller*)seller
     completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getSeller:(WDFoodCompletionBlock)completionBlock;

- (void)addDish:(WDDish*)dish
          image:(UIImage*)image
      galleries:(NSMutableArray*)galleries
     completion:(WDFoodCompletionBlock)completionBlock;

- (void)updateDish:(NSInteger)dishID
              dish:(WDDish*)dish
             image:(UIImage*)image
         galleries:(NSMutableArray*)galleries
        completion:(WDFoodCompletionBlock)completionBlock;

- (void)deleteDish:(NSInteger)dishID
        completion:(WDFoodCompletionBlock)completionBlock;

- (void)addDeliveries:(NSInteger)dishID
       isDelivery:(NSInteger)isDelivery
          homeService:(NSString*)homeService
           currencyID:(NSString *)currencyID
                price:(float)price
               radius:(NSString *)radius
      completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)addDeliveryService:(NSInteger)isDelivery
               homeService:(NSString*)homeService
                currencyID:(NSString *)currencyID
                     price:(float)price
                    radius:(NSString *)radius
           completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getDeliveries:(WDFoodCompletionBlock)completionBlock;

- (void)getMyMenuToday:(long)page
       completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getMyMenuAll:(long)page
     completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)changeStatusDish:(NSInteger)dishID
                  status:(NSString*)status
         completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getMyOrderToday:(NSString*)search
                   page:(long)page
        completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getMyOrderAll:(NSString*)search
                 page:(long)page
      completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getMyOrderItem:(NSInteger)orderID
       completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getCurrencies:(WDFoodCompletionBlock)completionBlock;

- (void)getCountryCodes:(WDFoodCompletionBlock)completionBlock;

- (void)getCurrencyByCountry:(NSInteger)countryID
             completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)deleteImageDish:(NSInteger)imageID
             completion:(WDFoodCompletionBlock)completionBlock;
- (void)getOrderHistory:(NSString*)search
                   page:(long)page
        completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getOrderHistoryItem:(NSInteger)orderID
            completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)confirmOrderItem:(NSInteger)dishID
         completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)declineOrderItem:(NSInteger)dishID
         completionBlock:(WDFoodCompletionBlock)completionBlock;

- (void)getOrderCompleteItem:(NSString*)orderNum
             completionBlock:(WDFoodCompletionBlock)completionBlock;
@end
