//
//  WDAppfeedbackAPIController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDAppFeedbackAPIController.h"
#import "SDVersion.h"
#import "WDUser.h"
#import "WDSourceConfig.h"
#import <FCFileManager/FCFileManager.h>

static NSString * const APIFeedback              = @"/sendAppFeedback";
static NSString * const APICertified             = @"/sendPromotionalVideo";
static NSString * const APIKitchenVideo          = @"/videos";

@implementation WDAppFeedbackAPIController

+ (WDAppFeedbackAPIController*)sharedInstance{
  static dispatch_once_t onceToken;
  static WDAppFeedbackAPIController* instance = nil;
  dispatch_once(&onceToken, ^{
    instance = [[WDAppFeedbackAPIController alloc]init];
  });
  return instance;
}

- (void)sendAppFeedback:(NSURL *)imageURL
               nameFile:(NSString *)nameFile
               mimeType:(NSString *)mimeType
            description:(NSString *)description
             completion:(WDAppFeedbackCompletionBlock)completionBlock{
  
  NSString *token = [[WDUser sharedInstance] getToken];
  
  NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:description, [SDVersion deviceNameString],[[UIDevice currentDevice] systemVersion],[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], nil] forKeys:[NSArray arrayWithObjects:@"description", @"device_model", @"ios_version", @"app_version", @"app_build", nil]];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIFeedback] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    if(imageURL){
      [formData appendPartWithFileData:[FCFileManager readFileAtPathAsData:[imageURL absoluteString]] name:@"attachment" fileName:nameFile mimeType:@"image/png"];
    }
    
  } progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
  
}

- (void)sendCertified:(NSURL *)fileURL
                note:(NSString *)note
          completion:(WDAppFeedbackCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
  
  NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:note, nil] forKeys:[NSArray arrayWithObjects:@"note", nil]];
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APICertified] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    if(fileURL){
      NSString *theFileName = [[fileURL lastPathComponent] stringByDeletingPathExtension];
      if ([fileURL.absoluteString containsString:@"pdf"]) {
        NSData *data = [NSData dataWithContentsOfURL:fileURL];
        [formData appendPartWithFileData:data name:@"certification_file" fileName:theFileName mimeType:@"application/pdf"];
      } else {
        [formData appendPartWithFileData:[FCFileManager readFileAtPathAsData:[fileURL absoluteString]] name:@"certification_file" fileName:theFileName mimeType:@"image/png"];
      }
    }
  } progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}

- (void)sendKitchenVideo:(NSURL *)fileURL
          completion:(WDAppFeedbackCompletionBlock)completionBlock {
  
  NSString *token = [[WDUser sharedInstance] getToken];
    
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIKitchenVideo] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    if(fileURL){
      NSString *mineType = @"video/mp4";
      NSString *theFileName = [[fileURL lastPathComponent] stringByDeletingPathExtension];
      NSData *data = [NSData dataWithContentsOfURL:fileURL];
      [formData appendPartWithFileData:data name:@"video" fileName:theFileName mimeType:mineType];
    }
  } progress:^(NSProgress * _Nonnull uploadProgress) {
    
  } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    completionBlock(nil,responseObject);
  } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
    if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
      completionBlock([serializedData valueForKey:@"message"],nil);
    }else{
      completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
    }
  }];
}
@end
