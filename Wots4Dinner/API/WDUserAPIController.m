//
//  HCDemoMapSource.m
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//

#import "WDUserAPIController.h"
#import "WDSourceConfig.h"
#import "NSDictionary+SafeValues.h"
#import "WDUser.h"
#import "WDUserTemp.h"
#import <FCFileManager/FCFileManager.h>

#define kMessError @"msg"
#define kStatus @"status"

static NSString * const APISignUp               = @"/auth/register";
static NSString * const APILogin                = @"/auth";
static NSString * const APIGetUser              = @"/auth/user";
static NSString * const APIVerifyCode           = @"/auth/verify";
static NSString * const APISendVerifyCode       = @"/auth/resendCode";
static NSString * const APIUpdateUser           = @"/users/1";
static NSString * const APIForgotPass           = @"/auth/forgotPassword";
static NSString * const APIVerifyCodeForgot     = @"/auth/verifyForgotPassword";
static NSString * const APIVerifyResetPassword  = @"/auth/resetPassword";
static NSString * const APIChangePhoneNumber    = @"/changePhoneNumber";
static NSString * const APIVerifyChangePhone    = @"/verifyChangePhoneNumber";
static NSString * const APIChangePassword       = @"/changePassword";
static NSString * const APIUser                 = @"/users";
static NSString * const APILogout               = @"/logout";


@implementation WDUserAPIController

#pragma mark -
#pragma mark Init Methods

+ (WDUserAPIController*)sharedInstance{
    static dispatch_once_t onceToken;
    static WDUserAPIController* instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[WDUserAPIController alloc]init];
    });
    return instance;
}

#pragma mark -
#pragma mark Request Methods

/**
 *  API SignIn
 *
 *  @param userName     userName is including phone_prefix and phone number. Ex: 84987654321
 *  @param password        password description
 *  @param completionBlock completionBlock description
 */
- (void)signin:(NSString*)userName
      password:(NSString*)password
    completion:(WDSignupCompletionBlock)completionBlock {
    NSString *tokenDevice = [[NSUserDefaults standardUserDefaults]
                             stringForKey:kDeviceToken];
    if(!tokenDevice){
        tokenDevice = @"";
    }
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:userName, password, tokenDevice, @"ios", nil] forKeys:[NSArray arrayWithObjects:@"username", @"password", @"token_device", @"os_device", nil]];
  
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[WDSourceConfig config].apiKey stringByAppendingString:APILogin] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"true"]){
            [[WDUser sharedInstance] setToken:[responseObject valueForKey:@"token"]];
            completionBlock(nil);
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

/**
 *  Api register account
 *
 *  @param phoneNumber     phoneNumber description
 *  @param password        password description
 *  @param phonePrefix     <#phonePrefix description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)signup:(NSString*)phoneNumber
      password:(NSString*)password
   phonePrefix:(NSString*)phonePrefix
    completion:(WDSignupCompletionBlock)completionBlock{
    NSString *tokenDevice = [[NSUserDefaults standardUserDefaults]
                             stringForKey:kDeviceToken];
    if(!tokenDevice){
        tokenDevice = @"";
    }
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:phoneNumber,password,phonePrefix,tokenDevice,@"ios", nil] forKeys:[NSArray arrayWithObjects:@"phone", @"password", @"phone_prefix",@"token_device",@"os_device", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[WDSourceConfig config].apiKey stringByAppendingString:APISignUp] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"true"]){
            [[WDUser sharedInstance] setPhoneNumber:[responseObject valueForKey:@"phone_number"]];
            [[WDUser sharedInstance] setPhonePrefix:[responseObject valueForKey:@"phone_prefix"]];
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"+ "];
            NSString *phonePrefix = [[[[WDUser sharedInstance] getPhonePrefix] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
            NSString *phoneNumber = [[[[WDUser sharedInstance] getPhoneNumber] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
            NSString *userName = [NSString stringWithFormat:@"%@%@",phonePrefix,phoneNumber];
            [[WDUser sharedInstance] setUserName:userName];
            [[WDUser sharedInstance] setToken:[responseObject valueForKey:@"token"]];

            completionBlock(nil);
            
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

/**
 *  API get user information
 *
 *  @param token           <#token description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)getUserInformation:(WDSignupCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager GET:[[WDSourceConfig config].apiKey stringByAppendingString:APIGetUser] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *data = [responseObject valueForKey:@"user"];
        [[WDUser sharedInstance] setUserObject:data];
        completionBlock(nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

/**
 *  API verify code
 *
 *  @param userName        userName is including phone_prefix and phone number. Ex: 84987654321
 *  @param code            <#code description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)verifyCode:(NSString*)userName
              code:(NSString*)code
      isForgotPass:(BOOL)isForgotPass
        completion:(WDSignupCompletionBlock)completionBlock{
    
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:userName,code,nil] forKeys:[NSArray arrayWithObjects:@"username", @"code",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    
    [manager POST:[[WDSourceConfig config].apiKey stringByAppendingString:isForgotPass?APIVerifyCodeForgot:APIVerifyCode] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"true"]){
            completionBlock(nil);
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

/**
 *  API Send code for user
 *
 *  @param userName        userName is including phone_prefix and phone number. Ex: 84987654321
 *  @param completionBlock <#completionBlock description#>
 */
- (void)sendVerificationCode:(NSString*)userName
                  completion:(WDSignupCompletionBlock)completionBlock {
    
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:userName,nil] forKeys:[NSArray arrayWithObjects:@"username",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[WDSourceConfig config].apiKey stringByAppendingString:APISendVerifyCode] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"true"]){
            completionBlock(nil);
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

- (void)updateUser:(NSString*)phoneNumber
       phonePrefix:(NSString*)phonePrefix
          password:(NSString*)password
         firstName:(NSString*)firstName
          lastName:(NSString*)lastName
          email:(NSString*)email
        completion:(WDSignupCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:password, phonePrefix, firstName, lastName, email, nil] forKeys:[NSArray arrayWithObjects:@"password", @"phone_prefix", @"first_name", @"last_name", @"email", nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIUpdateUser] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([responseObject valueForKey:@"success"]){
            completionBlock(nil);
            
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

- (void)updateUser:(WDUserTemp*)user
        completion:(WDSignupCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:user.firstName, user.lastName, user.about, user.gender,user.birthday,user.email,user.living, nil] forKeys:[NSArray arrayWithObjects:@"first_name", @"last_name", @"about", @"gender",@"birthday",@"email",@"living_in",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIUpdateUser] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([responseObject valueForKey:@"success"]){
            completionBlock(nil);
            
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

- (void)forgotPassword:(NSString*)phoneNumber
           phonePrefix:(NSString*)phonePrefix
            completion:(WDSignupCompletionBlock)completionBlock{
    
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:phoneNumber,phonePrefix,nil] forKeys:[NSArray arrayWithObjects:@"phone", @"phone_prefix",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[WDSourceConfig config].apiKey stringByAppendingString:APIForgotPass] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"true"]){
            completionBlock(nil);
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

- (void)resetPassword:(NSString*)code
             userName:(NSString*)userName
             password:(NSString*)password
           completion:(WDSignupCompletionBlock)completionBlock{
    
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:code,userName,password,nil] forKeys:[NSArray arrayWithObjects:@"code", @"username", @"password",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[WDSourceConfig config].apiKey stringByAppendingString:APIVerifyResetPassword] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"true"]){
            completionBlock(nil);
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

- (void)uploadAvatar:(NSURL*)avatarURL
                nameFile:(NSString*)nameFile
            mimeType:(NSString*)mimeType
           completion:(WDSignupCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIUpdateUser] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:[FCFileManager readFileAtPathAsData:[avatarURL absoluteString]] name:@"avatar" fileName:nameFile mimeType:@"image/png"];

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([responseObject valueForKey:@"success"]&&[[responseObject valueForKey:@"success"] boolValue]){
            NSDictionary *data = [responseObject valueForKey:@"data"];
            [[WDUser sharedInstance] setUserObject:data];
            completionBlock(nil);
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        if(!errorData){
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            return;
        }
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

- (void)changePhoneNumber:(NSString*)phoneNumber
   phonePrefix:(NSString*)phonePrefix
    completion:(WDSignupCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:phoneNumber, phonePrefix,nil] forKeys:[NSArray arrayWithObjects:@"phone", @"phone_prefix",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIChangePhoneNumber] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"true"]){
            completionBlock(nil);
            
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

- (void)verifyCodeChangePhone:(NSString*)phoneNumber
                  code:(NSString*)code
              phonePrefix:(NSString*)phonePrefix
               completion:(WDSignupCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    NSString *phoneNumberTemp = [NSString stringWithFormat:@"%@",phoneNumber];
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:phoneNumber,code, phonePrefix,nil] forKeys:[NSArray arrayWithObjects:@"phone", @"code", @"phone_prefix",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIVerifyChangePhone] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"success"] isEqualToString:@"true"]){
            [[WDUser sharedInstance] setPhoneNumber:phoneNumberTemp];
            [[WDUser sharedInstance] setPhonePrefix:phonePrefix];
            NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"+ "];
            NSString *phonePrefix = [[[[WDUser sharedInstance] getPhonePrefix] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
            NSString *phoneNumber = [[[[WDUser sharedInstance] getPhoneNumber] componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
            NSString *userName = [NSString stringWithFormat:@"%@%@",phonePrefix,phoneNumber];
            [[WDUser sharedInstance] setUserName:userName];

            completionBlock(nil);
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

- (void)changePassword:(NSString*)password
       currentPassword:(NSString*)currentPassword
                   completion:(WDSignupCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:currentPassword ,password,nil] forKeys:[NSArray arrayWithObjects:@"current_password", @"password",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APIChangePassword] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([responseObject valueForKey:@"success"]){
            [[WDUser sharedInstance] setPassword:password];
            completionBlock(nil);
        }else{
            completionBlock([responseObject valueForKey:@"message"]);
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"]);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey]);
            
        }
    }];
}

/**
 *  API get user information
 *
 *  @param token           <#token description#>
 *  @param completionBlock <#completionBlock description#>
 */
- (void)getUserInformationByID:(NSInteger)idUser
                    completion:(WDUserCompletionBlock)completionBlock{
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIUser] stringByAppendingString:[NSString stringWithFormat:@"/%ld",(long)idUser]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
            
        }
    }];
}

- (void)logout {
    NSString *token = [[WDUser sharedInstance] getToken];
    NSString *tokenDevice = [[NSUserDefaults standardUserDefaults]
                             stringForKey:kDeviceToken];
    if(!tokenDevice){
        tokenDevice = @"";
    }
    NSDictionary* parameters = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:tokenDevice,nil] forKeys:[NSArray arrayWithObjects:@"token_device",nil]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager POST:[[[WDSourceConfig config].apiKey stringByAppendingString:APILogout] stringByAppendingString:[NSString stringWithFormat:@"?token=%@",token]] parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

    }];
}
@end
