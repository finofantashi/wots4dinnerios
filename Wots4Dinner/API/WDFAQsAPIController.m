//
//  WDFAQsAPIController.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDFAQsAPIController.h"
#import "WDUser.h"
#import "WDSourceConfig.h"

static NSString * const APIFAQs              = @"/faqs";

@implementation WDFAQsAPIController

#pragma mark -
#pragma mark Init Methods

+ (WDFAQsAPIController*)sharedInstance{
    static dispatch_once_t onceToken;
    static WDFAQsAPIController* instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[WDFAQsAPIController alloc]init];
    });
    return instance;
}

- (void)getListQuestion:(NSString *)question
        completionBlock:(WDFAQsCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIFAQs] stringByAppendingString:[NSString stringWithFormat:@"?search=%@", [question stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

- (void)getQuestion:(NSInteger)questionID
    completionBlock:(WDFAQsCompletionBlock)completionBlock {
    
    NSString *token = [[WDUser sharedInstance] getToken];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:[@"Bearer " stringByAppendingString:token==nil?@"":token] forHTTPHeaderField:@"Authorization"];
    [manager GET:[[[WDSourceConfig config].apiKey stringByAppendingString:APIFAQs] stringByAppendingString:[NSString stringWithFormat:@"/%ld", (long)questionID]] parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionBlock(nil,responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if(serializedData&&[serializedData valueForKey:@"message"]&&![[serializedData valueForKey:@"message"] isEqualToString:@""]){
            completionBlock([serializedData valueForKey:@"message"],nil);
        }else{
            completionBlock([[error userInfo] valueForKey:NSLocalizedDescriptionKey],nil);
        }
    }];
}

@end
