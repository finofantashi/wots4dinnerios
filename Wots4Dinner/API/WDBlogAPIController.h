//
//  WDBlogAPIController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBaseSource.h"
#import "AFNetworking.h"

typedef void(^WDBlogCompletionBlock)(NSString* errorString, id responseObject);

@interface WDBlogAPIController : WDBaseSource

+ (WDBlogAPIController*)sharedInstance;

- (void)getBlogs:(long)page
 completionBlock:(WDBlogCompletionBlock)completionBlock;

- (void)getBlogDetail:(NSInteger)blogID
         completionBlock:(WDBlogCompletionBlock)completionBlock;

- (void)getCommentByBlog:(NSInteger)blogID
                    page:(long)page
         completionBlock:(WDBlogCompletionBlock)completionBlock;

- (void)getExploreMenuToday:(NSInteger)sellerID
                       page:(long)page
            completionBlock:(WDBlogCompletionBlock)completionBlock;

- (void)getExploreMenuAll:(NSInteger)sellerID
                       page:(long)page
            completionBlock:(WDBlogCompletionBlock)completionBlock;

- (void)postComment:(NSNumber*)blogID
                    content:(NSString*)content
         completionBlock:(WDBlogCompletionBlock)completionBlock;
@end
