//
//  WDSourceConfig.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  @brief Login Key
 */
#define kFirstLaucher @"WD_FIRST_LAUCHER"
#define kSessionSignUp @"WD_SESSION_SIGNUP"
#define kSessionSignIn @"WD_SESSION_SIGNIN"
#define kDeviceToken @"WD_DEVICE_TOKEN"
#define kCountNotificationMessage @"WD_NOTIFICATION_COUNT"
#define kCountNotificationOrder @"WD_NOTIFICATION_ORDER"
#define kLoginBecomeSeller @"WD_LOGIN_BECOME_SELLER"

//#define kImageDomain    @"https://s3-ap-southeast-1.amazonaws.com/wots4dinner-uat/"
#define kImageDomain      @"https://cdn.mealsaround.com/"
//#define kImageDomain    @"https://s3-ap-southeast-1.amazonaws.com/w4d-dev/"

@interface WDSourceConfig : NSObject

+ (WDSourceConfig*)config;

@property (nonatomic, copy, readonly) NSString* apiKey;
@property (nonatomic, copy, readonly) NSString* apiFAQ;
@property (nonatomic, copy, readonly) NSString* apiHelp;
@property (nonatomic, copy, readonly) NSString* webURL;

@end
