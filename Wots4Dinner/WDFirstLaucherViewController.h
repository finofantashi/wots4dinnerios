//
//  ViewController.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDFirstLaucherViewController : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleMarginTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *signupWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonMarginTop;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet UIButton *btnSignin;
@property (weak, nonatomic) IBOutlet UILabel *lbTerm;
@property (weak, nonatomic) IBOutlet UIButton *btnTerm;

- (IBAction)clickedTerm:(id)sender;
- (IBAction)clickOutSign:(id)sender;
- (IBAction)clickedSignup:(id)sender;
- (IBAction)clickedSignin:(id)sender;
@end

