//
//  Constants.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

//Status for event and lead
extern int const kStatusAdd;
extern int const kStatusUpdate;
extern int const kStatusDelete;
extern int const kStatusTranscribed;
extern int const kStatusDeleteNew;

FOUNDATION_EXPORT BOOL const IsFirstLaucher;
FOUNDATION_EXPORT NSString *const S3BucketName;

@end
