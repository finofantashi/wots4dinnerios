//
//  WDUserDefaultsManager.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDUserDefaultsManager : NSObject

+ (void)setValue:(id)value forKey:(NSString *)key;

+ (id)valueForKey:(NSString *)key;

+ (void)setBool:(BOOL)value forKey:(NSString *)defaultName;

+ (BOOL)boolForKey:(NSString *)defaultName;

+ (void)echo;

@end
