//
//  WDUserDefaultsManager.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDUserDefaultsManager.h"

@implementation WDUserDefaultsManager

+ (void)setValue:(id)value forKey:(NSString *)key {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:value forKey:key];
    [userDefaults synchronize];
}

+ (id)valueForKey:(NSString *)key {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults valueForKey:key];
}

+ (void)setBool:(BOOL)value forKey:(NSString *)defaultName {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:value forKey:defaultName];
    [userDefaults synchronize];
}

+ (BOOL)boolForKey:(NSString *)defaultName {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults boolForKey:defaultName];
}

+ (void)echo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictionary = [userDefaults dictionaryRepresentation];
    NSLog(@"%@", dictionary);
}

@end
