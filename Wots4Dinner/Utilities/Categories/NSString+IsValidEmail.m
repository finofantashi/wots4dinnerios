//
//  NSString+IsValidEmail.m
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//


#import "NSString+IsValidEmail.h"

@implementation NSString (IsValidEmail)

- (BOOL)isValidEmail {
	BOOL stricterFilter = YES;
	NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
	NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:self];
}
- (BOOL)isValidatePhoneNumber{
    NSString *Regex = @"^[0-9]{10}$";
    NSPredicate *testResult = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
    return [testResult evaluateWithObject:self];
}
//add new
- (BOOL)containsString:(NSString *)aString {
    NSRange range = [[self lowercaseString] rangeOfString:[aString lowercaseString]];
    return range.location != NSNotFound;
}
- (NSString *)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
@end
