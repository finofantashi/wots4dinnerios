//
//  UIFont+Style.m
//  My Menu
//
//  Created by nvnguyen on 11/17/13.
//  Copyright (c) 2013 My Menu, LLC. All rights reserved.
//

#import "UIFont+Style.h"

@implementation UIFont (Style)

+ (void) printAvailableFontNames {
    NSArray *familyNames = [UIFont familyNames];
    
    for( NSString *familyName in familyNames ){
        printf( "Family: %s \n", [familyName UTF8String] );
        
        NSArray *fontNames = [UIFont fontNamesForFamilyName:familyName];
        for( NSString *fontName in fontNames ){
            printf( "\tFont: %s \n", [fontName UTF8String] );
            
        }
    }
}

+ (UIFont *)signikaBold:(int)size{
    return [UIFont fontWithName:@"Signika-Bold" size:size];
}
+ (UIFont *)signikaLight:(int)size{
    return [UIFont fontWithName:@"Signika-Light" size:size];
}
+ (UIFont *)signikaRegular:(int)size{
    return [UIFont fontWithName:@"Signika-Regular" size:size];
}
+ (UIFont *)signikaSemiBold:(int)size{
    return [UIFont fontWithName:@"Signika-Semibold" size:size];
}
+ (UIFont *)glyphicons:(int)size{
    return [UIFont fontWithName:@"GLYPHICONS-Halflings-Regular" size:size];
}

+ (UIFont *)openSanBold:(int)size{
    return [UIFont fontWithName:@"OpenSans-Bold" size:size];
}
+ (UIFont *)openSanLight:(int)size{
    return [UIFont fontWithName:@"OpenSans-Light" size:size];
}
+ (UIFont *)openSanRegular:(int)size{
    return [UIFont fontWithName:@"OpenSans" size:size];
}
+ (UIFont *)openSanSemiBold:(int)size{
    return [UIFont fontWithName:@"OpenSans-SemiBold" size:size];
}
+ (UIFont *)openSanBoldItalic:(int)size{
    return [UIFont fontWithName:@"OpenSans-BoldItalic" size:size];
}
+ (UIFont *)openSanExtraBoldItalic:(int)size{
    return [UIFont fontWithName:@"OpenSans-ExtraBoldItalic" size:size];
}
+ (UIFont *)openSanExtraBold:(int)size{
    return [UIFont fontWithName:@"OpenSans-ExtraBold" size:size];
}
+ (UIFont *)openSanItalic:(int)size{
    return [UIFont fontWithName:@"OpenSans-Italic" size:size];
}
+ (UIFont *)openSanLightItalic:(int)size{
    return [UIFont fontWithName:@"OpenSansLight-Italic" size:size];
}
+ (UIFont *)openSanSemiBoldItalic:(int)size{
    return [UIFont fontWithName:@"OpenSans-SemiBoldItalic" size:size];
}

@end
