//
//  WSSPermissionsManager.m
//  WSSVideoCompression_Example
//
//  Created by smile on 2020/4/26.
//  Copyright © 2020 18566663687@163.com. All rights reserved.
//

#import "WSSPermissionsManager.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@implementation WSSPermissionsManager
+ (void)checkCameraPermissions:(PermissionsBlock)permissionsBlock {
    [self authorizationWithMediaType:AVMediaTypeVideo alertMessage:@"Go to settings to open camera permissions?" permissionsBlock:permissionsBlock];
}
+ (void)authorizationWithMediaType:(AVMediaType)mediaType alertMessage:(NSString *)alertMessage permissionsBlock:(PermissionsBlock)permissionsBlock {
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if (status == AVAuthorizationStatusRestricted || status == AVAuthorizationStatusDenied) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Permission Reminder" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *ensureAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];
        [alert addAction:cancelAction];
        [alert addAction:ensureAction];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
        if (permissionsBlock) {
            permissionsBlock(NO);
        }
    } else if (status == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            if (permissionsBlock) {
                permissionsBlock(granted);
            }
        }];
    } else {
        if (permissionsBlock) {
            permissionsBlock(YES);
        }
    }
}
@end
