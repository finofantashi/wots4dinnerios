//
//  NSDictionary+SafeValues.h
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSDictionary (NSDictionary_SafeValues)

- (NSString*) safeStringForKey:(id)key;
- (NSNumber*) safeNumberForKey:(id)key;
- (NSArray*)  safeArrayForKey:(id)key;
- (NSDictionary*) safeDictionaryForKey:(id)key;

@end
