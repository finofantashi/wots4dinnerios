
#import "NSString+StringAdditions.h"

@implementation NSString (StringAdditions)
-(BOOL)isHaveSpecialChar{
    NSCharacterSet *notAllowedChars = [NSCharacterSet characterSetWithCharactersInString:@"~`!$%^&*()+=-/;:\"\'{}[]<>^?,@"];
    return !([[[self componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""] isEqualToString:self]);
}

-(BOOL)isHaveCommaChar{
    NSCharacterSet *notAllowedChars = [NSCharacterSet characterSetWithCharactersInString:@","];
    return !([[[self componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""] isEqualToString:self]);
}

- (NSString *)formatCurrency:(NSString *)code
                      symbol:(NSString *)symbol
                       price:(float)price {
    if(!code || [code isEqualToString:@""]){
        code = @"USD";
    }
    if(!symbol || [symbol isEqualToString:@""]){
        symbol = @"$";
    }
    NSDictionary *localeInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                code, NSLocaleCurrencyCode,
                                [[NSLocale preferredLanguages] objectAtIndex:0], NSLocaleLanguageCode,
                                nil];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:
                        [NSLocale localeIdentifierFromComponents:localeInfo]];
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [fmt setLocale:locale];
    [fmt setCurrencySymbol:@""];

    return [NSString stringWithFormat:@"%@%@ %@",symbol,[fmt stringFromNumber:[NSNumber numberWithFloat:price]], code];
}

- (NSString *)formatCurrencySymbol:(NSString *)code
                      symbol:(NSString *)symbol
                       price:(float)price {
    if(!code || [code isEqualToString:@""]){
        code = @"USD";
    }
    if(!symbol || [symbol isEqualToString:@""]){
        symbol = @"$";
    }
    NSDictionary *localeInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                code, NSLocaleCurrencyCode,
                                [[NSLocale preferredLanguages] objectAtIndex:0], NSLocaleLanguageCode,
                                nil];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:
                        [NSLocale localeIdentifierFromComponents:localeInfo]];
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [fmt setLocale:locale];
    [fmt setCurrencySymbol:@""];
    
    return [NSString stringWithFormat:@"%@%@",symbol,[fmt stringFromNumber:[NSNumber numberWithFloat:price]]];
}
@end
