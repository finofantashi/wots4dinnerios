//
//  UIImage+Additions.h
//  Lead Capture
//
//  Created by Hoa Truong on 5/4/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)
- (UIImage *)applyLightEffect;
- (UIImage *)applySemiLightEffect;
- (UIImage *)applyExtraLightEffect;
- (UIImage *)applyDarkEffect;
- (UIImage *)applyTintEffectWithColor:(UIColor *)tintColor;
- (UIImage *)applyBlurWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;
- (UIImage *)renderAtSize:(const CGSize) size;
- (UIImage *)maskWithImage:(const UIImage *) maskImage;
- (UIImage *)maskWithColor:(UIColor *) color;
- (UIImage *)renderAtSizeForLargeImage:(const CGSize) size;
+ (UIImage *)compressImage:(UIImage *)image;
+ (UIImage *) imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;
+ (NSData*) reduceCapacity:(UIImage*)image;
@end
