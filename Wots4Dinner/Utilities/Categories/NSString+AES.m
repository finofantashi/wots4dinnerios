//
//  NSString+AES.m
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//

#import "NSString+AES.h"
#import "NSData+Base64.h"
#import <CommonCrypto/CommonCryptor.h>

#define FBENCRYPT_ALGORITHM     kCCAlgorithmAES128
#define FBENCRYPT_BLOCK_SIZE    kCCBlockSizeAES128
#define FBENCRYPT_KEY_SIZE      kCCKeySizeAES256
static NSString const *KEY = @"P1OME0BQDK7SACBSAFI7KA4PLVCXMTTG";

@implementation NSString (AES)

- (NSString*)encryptAES {
    NSData* data = [NSString encryptData:[self dataUsingEncoding:NSUTF8StringEncoding]];
    return [data base64EncodedStringWithSeparateLines:YES];
}

#pragma mark - Helper
+ (NSData*)encryptData:(NSData*)data {
    NSData* result = nil;
    NSData* key = [KEY dataUsingEncoding:NSUTF8StringEncoding];
    
    // setup key
    unsigned char cKey[FBENCRYPT_KEY_SIZE];
    bzero(cKey, sizeof(cKey));
    [key getBytes:cKey length:FBENCRYPT_KEY_SIZE];
    
    // setup iv
    char cIv[FBENCRYPT_BLOCK_SIZE];
    bzero(cIv, FBENCRYPT_BLOCK_SIZE);
    
    // setup output buffer
    size_t bufferSize = [data length] + FBENCRYPT_BLOCK_SIZE;
    void *buffer = malloc(bufferSize);
    
    // do encrypt
    size_t encryptedSize = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,
                                          FBENCRYPT_ALGORITHM,
                                          kCCOptionPKCS7Padding,
                                          cKey,
                                          FBENCRYPT_KEY_SIZE,
                                          cIv,
                                          [data bytes],
                                          [data length],
                                          buffer,
                                          bufferSize,
                                          &encryptedSize);
    if (cryptStatus == kCCSuccess) {
        result = [NSData dataWithBytesNoCopy:buffer length:encryptedSize];
    }
    else {
        free(buffer);
        NSLog(@"[ERROR] failed to encrypt|CCCryptoStatus: %d", cryptStatus);
    }
    
    return result;
}

@end
