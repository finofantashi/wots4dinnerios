//
//  NSManagedObject+Serialization.m
//  Lead Capture
//
//  Created by HungHoang on 14/05/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//

#import "NSManagedObject+Serialization.h"

@implementation NSManagedObject (Serialization)

#pragma mark -
#pragma mark Dictionary conversion methods

- (NSDictionary*) toDictionaryWithTraversalHistory:(NSMutableSet*)traversalHistory {
    NSArray* attributes = [[[self entity] attributesByName] allKeys];
    NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:
                                 [attributes count] + 1];
    
    NSMutableSet *localTraversalHistory = nil;
    
    if (traversalHistory == nil) {
        localTraversalHistory = [NSMutableSet setWithCapacity:[attributes count] + 1];
    } else {
        localTraversalHistory = traversalHistory;
    }
    
    [localTraversalHistory addObject:self];
    
    for (NSString* attr in attributes) {
        NSObject* value = [self valueForKey:attr];
        
        if (value != nil) {
            if ([value isKindOfClass:[NSDate class]]) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
                NSString *dateAttr = [dateFormatter stringFromDate:(NSDate *)value];
                [dict setObject:dateAttr forKey:attr];
            } else {
                [dict setObject:value forKey:attr];
            }
        }
    }
    
    if (traversalHistory == nil) {
        [localTraversalHistory removeAllObjects];
    }
    
    return dict;
}

- (NSDictionary*) toDictionary {
    // Check to see there are any objects that should be skipped in the traversal.
    // This method can be optionally implemented by NSManagedObject subclasses.
    NSMutableSet *traversedObjects = nil;
    if ([self respondsToSelector:@selector(serializationObjectsToSkip)]) {
        traversedObjects = [self performSelector:@selector(serializationObjectsToSkip)];
    }
    return [self toDictionaryWithTraversalHistory:traversedObjects];
}
- (void)serializationObjectsToSkip{
    
}
@end
