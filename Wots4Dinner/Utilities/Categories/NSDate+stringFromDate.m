//
//  NSDate+stringFromDate.m
//  Lead Capture
//
//  Created by Hoa Truong on 5/15/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//

#import "NSDate+stringFromDate.h"

@implementation NSDate (stringFromDate)
-(NSString*)fromDateString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyddMMHHmm"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
    NSString *stringFromDate = [formatter stringFromDate:[NSDate date]];
    return stringFromDate;
}
@end
