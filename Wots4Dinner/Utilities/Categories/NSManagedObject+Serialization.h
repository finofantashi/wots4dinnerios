//
//  NSManagedObject+Serialization.h
//  Lead Capture
//
//  Created by HungHoang on 14/05/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface NSManagedObject (Serialization)

- (NSDictionary*) toDictionary;

@end
