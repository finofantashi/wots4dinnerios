//
//  UIFont+Style.h
//  My Menu
//
//  Created by nvnguyen on 11/17/13.
//  Copyright (c) 2013 My Menu, LLC. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface UIFont (Style)

+ (void) printAvailableFontNames;
+ (UIFont *)signikaBold:(int)size;
+ (UIFont *)signikaLight:(int)size;
+ (UIFont *)signikaRegular:(int)size;
+ (UIFont *)signikaSemiBold:(int)size;
+ (UIFont *)glyphicons:(int)size;

+ (UIFont *)openSanBold:(int)size;
+ (UIFont *)openSanLight:(int)size;
+ (UIFont *)openSanRegular:(int)size;
+ (UIFont *)openSanSemiBold:(int)size;
+ (UIFont *)openSanBoldItalic:(int)size;
+ (UIFont *)openSanExtraBoldItalic:(int)size;
+ (UIFont *)openSanExtraBold:(int)size;
+ (UIFont *)openSanItalic:(int)size;
+ (UIFont *)openSanLightItalic:(int)size;
+ (UIFont *)openSanSemiBoldItalic:(int)size;

@end
