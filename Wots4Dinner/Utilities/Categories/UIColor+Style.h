//
//  UIColor+Style.h
//  My Menu
//
//  Created by nvnguyen on 11/17/13.
//  Copyright (c) 2013 My Menu, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Style)

+ (UIColor *)RedWithAlpha:(CGFloat)alpha;
+ (UIColor *)randomColor;
+ (UIColor *)colorFromHexString:(NSString *)hexString withAlpha:(CGFloat)alpha;
+ (UIColor *)textEventBrown;
+ (UIColor *)deleteRedButton;
+ (UIColor *)textSynced;
+ (UIColor *)textReady;
+ (UIColor *)GreenWithAlpha:(CGFloat)alpha;
+ (UIColor *)OrangeWithAlpha:(CGFloat)alpha;
@end
