//
//  NSManagedObject+Additions.h
//  Overpass
//
//  Created by HungHoang on 2/17/16.
//  Copyright © 2016 Magrabbit Inc.,Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface NSManagedObject (Acani)

+ (NSString *)entityName;
+ (NSEntityDescription *)entityInManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSManagedObject *)firstInManagedObjectContext:(NSManagedObjectContext *)context;

@end