//
//  NSArray+SafeValues.h
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSArray (NSArray_SafeValues)

- (NSString*) safeStringAtIndex:(NSUInteger)index;
- (NSNumber*) safeNumberAtIndex:(NSUInteger)index;
- (NSDictionary*) safeDictionaryAtIndex:(NSUInteger)index;

@end
