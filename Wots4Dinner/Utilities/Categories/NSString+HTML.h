//
//  NSString+HTML.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/24/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HTML)

// Instance Methods
- (NSString *)stringByStrippingTags;
- (NSString *)stringWithNewLinesAsBRs;
- (NSString *)stringByRemovingNewLinesAndWhitespace;
- (NSString *)stringByDecodingHTMLEntities;
- (NSString *)stringByEncodingHTMLEntities;

@end

