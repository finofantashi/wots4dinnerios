

#import <Foundation/Foundation.h>

@interface NSString (StringAdditions)
- (BOOL)isHaveSpecialChar;
- (BOOL)isHaveCommaChar;
- (NSString*)formatCurrency:(NSString*) code
                    symbol:(NSString*) symbol
                     price:(float) price;
- (NSString*)formatCurrencySymbol:(NSString*) code
                     symbol:(NSString*) symbol
                      price:(float) price;
@end
