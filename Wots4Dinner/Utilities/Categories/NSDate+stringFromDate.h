//
//  NSDate+stringFromDate.h
//  Lead Capture
//
//  Created by Hoa Truong on 5/15/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (stringFromDate)
-(NSString*)fromDateString;
@end
