//
//  PhoneNumberFormatter.h
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface PhoneNumberFormatter : NSObject {   
    NSDictionary *predefinedFormats;    
}

- (id)init;
- (NSString *)format:(NSString *)phoneNumber withLocale:(NSString *)locale;
- (NSString *)strip:(NSString *)phoneNumber;
- (BOOL)canBeInputByPhonePad:(char)c;
@end