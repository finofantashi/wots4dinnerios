//
//  UIColor+Style.m
//  My Menu
//
//  Created by nvnguyen on 11/17/13.
//  Copyright (c) 2013 My Menu, LLC. All rights reserved.
//


#import "UIColor+Style.h"

@implementation UIColor (Style)

+ (UIColor *)RedWithAlpha:(CGFloat)alpha{
    return [UIColor colorFromHexString:@"D0202E" withAlpha:alpha];
}

+ (UIColor *)GreenWithAlpha:(CGFloat)alpha {
    return [UIColor colorFromHexString:@"4A9B47" withAlpha:alpha];
}

+ (UIColor *)OrangeWithAlpha:(CGFloat)alpha {
    return [UIColor colorFromHexString:@"eb7a08" withAlpha:alpha];
}

+ (UIColor *) randomColor {
    return [UIColor colorWithHue:(arc4random() % 256 / 256.0)
                      saturation:(arc4random() % 128 / 256.0) + 0.5
                      brightness:(arc4random() % 128 / 256.0) + 0.5
                           alpha:1];
}
+ (UIColor *)colorFromHexString:(NSString *)hexString withAlpha:(CGFloat)alpha
{
    if ([hexString length] != 6) {
        return nil;
    }
    
    // Brutal and not-very elegant test for non hex-numeric characters
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-fA-F|0-9]" options:0 error:NULL];
    NSUInteger match = [regex numberOfMatchesInString:hexString options:NSMatchingReportCompletion range:NSMakeRange(0, [hexString length])];
    
    if (match != 0) {
        return nil;
    }
    
    NSRange rRange = NSMakeRange(0, 2);
    NSString *rComponent = [hexString substringWithRange:rRange];
    unsigned rVal = 0;
    NSScanner *rScanner = [NSScanner scannerWithString:rComponent];
    [rScanner scanHexInt:&rVal];
    float rRetVal = (float)rVal / 255;
    
    
    NSRange gRange = NSMakeRange(2, 2);
    NSString *gComponent = [hexString substringWithRange:gRange];
    unsigned gVal = 0;
    NSScanner *gScanner = [NSScanner scannerWithString:gComponent];
    [gScanner scanHexInt:&gVal];
    float gRetVal = (float)gVal / 255;
    
    NSRange bRange = NSMakeRange(4, 2);
    NSString *bComponent = [hexString substringWithRange:bRange];
    unsigned bVal = 0;
    NSScanner *bScanner = [NSScanner scannerWithString:bComponent];
    [bScanner scanHexInt:&bVal];
    float bRetVal = (float)bVal / 255;
    
    return [UIColor colorWithRed:rRetVal green:gRetVal blue:bRetVal alpha:alpha];
}
+ (UIColor *)textEventBrown {
    return [UIColor colorWithRed:137./255.0 green:126./255.0 blue:122./255.0 alpha:1.0f];
}
+ (UIColor *)deleteRedButton {
    return [UIColor colorWithRed:208./255.0 green:17./255.0 blue:43./255.0 alpha:1.0f];
}

+ (UIColor *)textSynced {
    return [UIColor colorWithRed:0 green:123./255.0 blue:250./255.0 alpha:1.0f];
}

+ (UIColor *)textReady {
    return [UIColor colorWithRed:88./255.0 green:192./255.0 blue:46./255.0 alpha:1.0f];
}
@end
