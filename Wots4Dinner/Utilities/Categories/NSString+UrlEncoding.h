//
//  NSString+UrlEncoding.h
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 360 Appteam. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSString (NSString_UrlEncoding)

-(NSString*)urlEncodedString;

@end
