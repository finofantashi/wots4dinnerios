//
//  NSString+AES.h
//  Lead Capture
//
//  Created by Nguyen Nguyen on 4/6/15.
//  Copyright (c) 2015 Magrabbit Inc.,. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSString (AES)

- (NSString*)encryptAES;

@end
