//
//  AppDelegate.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/7/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "WDUser.h"
#import "WDSourceConfig.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <UserNotifications/UserNotifications.h>
#import <IQKeyboardManager/IQKeyboardManager.h>

@import GooglePlaces;
@import GoogleMaps;

@interface AppDelegate () <UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [self registerForRemoteNotification];
  [[FBSDKApplicationDelegate sharedInstance] application:application
                           didFinishLaunchingWithOptions:launchOptions];
  [Fabric with:@[[Crashlytics class]]];
  [GMSPlacesClient provideAPIKey:@"AIzaSyAZDt39UV2JIP_zlfaGOWNSy3CaVA7dX-0"];
  [GMSServices provideAPIKey:@"AIzaSyAZDt39UV2JIP_zlfaGOWNSy3CaVA7dX-0"];
  [[WDUser sharedInstance] setSellerMode:NO];
  [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
  [IQKeyboardManager sharedManager].enable = YES;
  [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
  [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
  
  
  
  NSDate *now = [NSDate date];
  NSDate *dateToFire = [now dateByAddingTimeInterval:1];
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  [FBSDKAppEvents activateApp];
  
}

- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
  //register to receive notifications
  [application registerForRemoteNotifications];
  [[NSNotificationCenter defaultCenter] postNotificationName:@"postNotification" object:nil];
}

#endif

- (NSString *)hexadecimalStringFromData:(NSData *)data
{
  NSUInteger dataLength = data.length;
  if (dataLength == 0) {
    return nil;
  }
  
  const unsigned char *dataBuffer = (const unsigned char *)data.bytes;
  NSMutableString *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
  for (int i = 0; i < dataLength; ++i) {
    [hexString appendFormat:@"%02x", dataBuffer[i]];
  }
  return [hexString copy];
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  NSString *dToken = [self hexadecimalStringFromData:deviceToken];
  NSLog(@"%@", dToken);
  [[NSUserDefaults standardUserDefaults] setObject:dToken forKey:kDeviceToken];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
  
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  NSNumber *countMessage = [[NSUserDefaults standardUserDefaults] valueForKey:kCountNotificationMessage];
  NSNumber *countOrder = [[NSUserDefaults standardUserDefaults] valueForKey:kCountNotificationOrder];
  NSDictionary *data = [userInfo valueForKey:@"aps"];
  if([countMessage isKindOfClass:[NSNull class]]){
    countMessage = [NSNumber numberWithInt:0];
  }
  
  if([countOrder isKindOfClass:[NSNull class]]){
    countOrder = [NSNumber numberWithInt:0];
  }
  
  if([[data valueForKey:@"type_notification"] isEqualToString:@"order"]){
    countOrder = [NSNumber numberWithInt:[countOrder intValue]+1];
  }else{
    countMessage = [NSNumber numberWithInt:[countMessage intValue]+1];
  }
  
  [[NSUserDefaults standardUserDefaults] setObject:countMessage forKey:kCountNotificationMessage];
  [[NSUserDefaults standardUserDefaults] setObject:countOrder forKey:kCountNotificationOrder];
  [[NSUserDefaults standardUserDefaults] synchronize];
  //[[UIApplication sharedApplication] cancelAllLocalNotifications];
  NSDate *now = [NSDate date];
  NSDate *dateToFire = [now dateByAddingTimeInterval:1];
  
  if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone localTimeZone]];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond|NSCalendarUnitTimeZone fromDate:dateToFire];
    
    UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
    objNotificationContent.body = [data valueForKey:@"message"];
    if([[data valueForKey:@"type_notification"] isEqualToString:@"order"]){
      objNotificationContent.sound = [UNNotificationSound soundNamed:@"notification.caf"];
    }else{
      objNotificationContent.sound = [UNNotificationSound soundNamed:@"message_alert.caf"];
    }
    
    /// 4. update application icon badge number
    objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    
    
    UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:NO];
    
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"ten"
                                                                          content:objNotificationContent trigger:trigger];
    /// 3. schedule localNotification
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
      if (!error) {
        NSLog(@"Local Notification succeeded");
      }
      else {
        NSLog(@"Local Notification failed");
      }
    }];
    return;
  } else{
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = dateToFire;
    localNotification.alertBody = [data valueForKey:@"message"];
    localNotification.soundName = @"notification.caf";
    localNotification.applicationIconBadgeNumber = [countOrder intValue]+[countMessage intValue]; // increment
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    completionHandler( UIBackgroundFetchResultNewData );
  }
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
  completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
  NSNumber *countMessage = [[NSUserDefaults standardUserDefaults] valueForKey:kCountNotificationMessage];
  NSNumber *countOrder = [[NSUserDefaults standardUserDefaults] valueForKey:kCountNotificationOrder];
  NSDictionary *data = [response valueForKey:@"aps"];
  if([countMessage isKindOfClass:[NSNull class]]){
    countMessage = [NSNumber numberWithInt:0];
  }
  
  if([countOrder isKindOfClass:[NSNull class]]){
    countOrder = [NSNumber numberWithInt:0];
  }
  
  if([[data valueForKey:@"type_notification"] isEqualToString:@"order"]){
    countOrder = [NSNumber numberWithInt:[countOrder intValue]+1];
  }else{
    countMessage = [NSNumber numberWithInt:[countMessage intValue]+1];
  }
  
  [[NSUserDefaults standardUserDefaults] setObject:countMessage forKey:kCountNotificationMessage];
  [[NSUserDefaults standardUserDefaults] setObject:countOrder forKey:kCountNotificationOrder];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
  if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
      if(!error ){
        [[UIApplication sharedApplication] registerForRemoteNotifications];
      }
    }];
  } else {
    UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
    UIUserNotificationSettings *setting = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:setting];
  }
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
  return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                        openURL:url
                                              sourceApplication:sourceApplication
                                                     annotation:annotation];
}

@end
