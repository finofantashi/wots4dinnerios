//
//  WDGallery.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WDGallery : NSObject
@property (nonatomic, assign) NSInteger galleryID;
@property (nonatomic, strong) NSURL *galleryImage;
@property (nonatomic, strong) UIImage* image;

- (void) setGalleryObject: (NSDictionary*)dict;

@end
