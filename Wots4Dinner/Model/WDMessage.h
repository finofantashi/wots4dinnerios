//
//  WDMessage.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/17/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WDReceiver.h"

@interface WDMessage : NSObject
@property (nonatomic, assign) NSInteger recordID;
@property (nonatomic, assign) NSInteger messageID;
@property (nonatomic, assign) NSInteger senderID;
@property (nonatomic, assign) NSInteger groupID;
@property (nonatomic, assign) NSInteger conversationID;
@property (nonatomic, assign) NSInteger receiverID;
@property (nonatomic, assign) NSInteger roleID;

@property (nonatomic, strong) NSString *contentMessage;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *kitchenName;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *createdAt;

@property (nonatomic, strong) NSMutableArray *receiver;

- (void) setMessageObject: (NSDictionary*)dict;

@end
