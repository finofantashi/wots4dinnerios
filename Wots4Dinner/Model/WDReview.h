//
//  WDSellerReview.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/27/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WDUserTemp;

@interface WDReview : NSObject

@property (nonatomic, assign) NSInteger reviewID;
@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, assign) NSInteger sellerID;
@property (nonatomic, assign) float rating;
@property (nonatomic, strong) NSString *review;
@property (nonatomic, strong) WDUserTemp *user;
@property (nonatomic, strong) NSString *updatedAt;

- (void) setReviewObject: (NSDictionary*)dict isSeller:(BOOL)isSeller;
@end
