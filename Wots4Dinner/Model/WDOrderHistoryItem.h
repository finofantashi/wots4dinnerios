//
//  WDOrderHistoryItem.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/18/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface WDOrderHistoryItem : NSObject
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, assign) NSInteger delivery;
@property (nonatomic, assign) double lng;
@property (nonatomic, assign) double lat;

@property (nonatomic, assign) float price;
@property (nonatomic, assign) float totalPrice;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *dishName;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;
@property (nonatomic, strong) NSString *kitchenName;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *specialOffer;
@property (nonatomic, strong) NSString *pickupTimeFrom;
@property (nonatomic, strong) NSString *pickupTimeTo;
@property (nonatomic, strong) NSString *status;

@property (nonatomic, strong) NSString *code;
@property (nonatomic, assign) float value;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, assign) float fee;
@property (nonatomic, assign) float deliveryFee;


- (void) setOrderHistoryItemObject: (NSDictionary*)dict;
@end
