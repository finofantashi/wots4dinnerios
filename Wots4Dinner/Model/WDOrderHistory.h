//
//  WDOrderHistory.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/18/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDOrderHistory : NSObject
@property (nonatomic, assign) NSInteger orderID;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *createAt;
@property (nonatomic, strong) NSMutableArray *listKitchen;
@property (nonatomic, strong) NSMutableArray *kitchenIndex;
@property (nonatomic, assign) NSInteger totalRow;

@property (nonatomic, assign) float couponValue;
@property (nonatomic, assign) float fee;

@property (nonatomic, strong) NSString *couponCode;
@property (nonatomic, strong) NSString *couponType;

- (void) setOrderHistoryObject:(NSDictionary *)dict;
@end
