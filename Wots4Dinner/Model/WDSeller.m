//
//  WDSeller.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDSeller.h"
#import "NSDictionary+SafeValues.h"

@implementation WDSeller

- (void) setSellerObject: (NSDictionary*)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        if([[dict valueForKey:@"id"] isKindOfClass:[NSString class]]){
            [self setSellerID:[[dict safeStringForKey:@"id"] stringByRemovingPercentEncoding]];
        }else{
            [self setSellerID:[[dict safeNumberForKey:@"id"] stringValue]];
        }
        [self setKitchenName:[[dict safeStringForKey:@"kitchen_name"] stringByRemovingPercentEncoding]];
        [self setLocation:[[dict safeStringForKey:@"location"] stringByRemovingPercentEncoding]];
        NSNumber *latNumber = [dict safeNumberForKey:@"lat"];
        NSNumber *lngNumber = [dict safeNumberForKey:@"lng"];
        [self setLat:[latNumber doubleValue]];
        [self setLng:[lngNumber doubleValue]];
        [self setSellerDescription:[[dict safeStringForKey:@"description"] stringByRemovingPercentEncoding]];
        [self setCategory:[[[dict valueForKey:@"category"] safeStringForKey:@"name"] stringByRemovingPercentEncoding]];
        [self setCategoryID:[[dict safeNumberForKey:@"category_id"] integerValue]];
        [self setEmail:[[dict safeStringForKey:@"email"] stringByRemovingPercentEncoding]];
        NSNumber *certified = [dict safeNumberForKey:@"certified"];
        [self setCertified:[certified integerValue]];
    }
}

@end
