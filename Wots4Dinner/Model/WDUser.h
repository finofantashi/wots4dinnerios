//
//  WDUser.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kUserID          @"WD_USER_ID"
#define kUserFirstName   @"WD_FIRST_NAME"
#define kUserLastName    @"WD_LAST_NAME"
#define kUserPhonePrefix @"WD_PHONE_PREFIX"
#define kUserPhoneNumber @"WD_PHONE_NUMBER"
#define kUserToken       @"WD_USER_TOKEN"
#define kUserPassword    @"WD_PASSWORD"
#define kUserName        @"WD_USER_NAME"
#define kUserEmail       @"WD_EMAIL"
#define kUserRoleID      @"WD_ROLE_ID"
#define kUserAvatarURL   @"WD_AVATAR_URL"
#define kUserGender      @"WD_GENDER"
#define kUserBirthday    @"WD_BIRTHDAY"
#define kUserSince       @"WD_SINCE"
#define kUserLiving      @"WD_LIVING"
#define kUserAbout       @"WD_ABOUT"
#define kSellerMode      @"WD_SELLER_MODE"

@class WDUserTemp;

@interface WDUser : NSObject
@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *phonePrefix;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *userName;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) NSInteger roleID;
@property (nonatomic, strong) NSString *avatarURL;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *sinceDate;
@property (nonatomic, strong) NSString *living;
@property (nonatomic, strong) NSString *about;
@property (nonatomic, assign) BOOL isSellerMode;

+ (WDUser*) sharedInstance;
- (void) deleteAllData;
- (void) setUserObject: (NSDictionary*)dict;
- (void) setUser:(WDUserTemp*)user;
- (void) getUserFromDB;

- (NSString*) getUserName;
- (void) setUserName:(NSString *)userName;

- (NSInteger) getUserID;
- (void) setUserID:(NSInteger)userID;

- (NSString*) getFirstName;
- (void) setFirstName:(NSString *)firstName;

- (NSString*) getLastName;
- (void) setLastName:(NSString *)lastName;

- (NSString*) getPhonePrefix;
- (void) setPhonePrefix: (NSString*)phonePrefix;

- (NSString*) getPhoneNumber;
-(void) setPhoneNumber: (NSString*)phoneNumber;

- (NSString*) getToken;
- (void) setToken: (NSString*)token;

- (void) setPassword:(NSString *)password;
- (NSString*) getPassword;

- (void) setSellerMode:(BOOL)isSellerMode;
- (BOOL) isSellerMode;

- (NSString*) getEmail;
- (NSInteger) getRoleID;
- (NSString*) getAvatarURL;
- (NSString*) getGender;
- (NSString*) getBirthday;
- (NSString*) getSinceday;
- (NSString*) getLiving;
- (NSString*) getAbout;

- (NSString*) getFullName;
- (NSString *)stringWithDate:(NSString *)date;
- (NSString *)stringWithSinceDate:(NSString *)date;
@end
