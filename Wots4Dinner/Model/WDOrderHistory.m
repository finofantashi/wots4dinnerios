//
//  WDOrderHistory.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/18/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDOrderHistory.h"
#import "NSDictionary+SafeValues.h"

@implementation WDOrderHistory
- (void) setOrderHistoryObject:(NSDictionary *)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        [self setOrderID:[[dict safeNumberForKey:@"id"] integerValue]];
        [self setCode:[[dict safeStringForKey:@"code"] stringByRemovingPercentEncoding]];
        [self setCreateAt:[[dict safeStringForKey:@"created_at"] stringByRemovingPercentEncoding]];
        
        [self setCouponValue:[[dict safeNumberForKey:@"coupon_value"] floatValue]];
        [self setCouponCode:[[dict safeStringForKey:@"coupon_code"] stringByRemovingPercentEncoding]];
        [self setCouponType:[[dict safeStringForKey:@"coupon_type"] stringByRemovingPercentEncoding]];
        [self setFee:[[dict safeNumberForKey:@"fee"] floatValue]];

    }
}
@end
