//
//  WDShipping.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDShipping.h"
#import "NSDictionary+SafeValues.h"

@implementation WDShipping

- (void) setShippingObject: (NSDictionary*)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        NSNumber *shipID = [dict safeNumberForKey:@"id"];
        NSNumber *userID = [dict safeNumberForKey:@"user_id"];

        [self setShipID:[shipID integerValue]];
        [self setUserID:[userID integerValue]];
        [self setName:[[dict safeStringForKey:@"name"] stringByRemovingPercentEncoding]];
        [self setPhone:[[dict safeStringForKey:@"phone"] stringByRemovingPercentEncoding]];
        [self setAddress:[[dict safeStringForKey:@"address"] stringByRemovingPercentEncoding]];
    }
}

@end
