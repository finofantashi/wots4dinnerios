//
//  WDFoodType.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDDish.h"
#import "NSDictionary+SafeValues.h"
#import "WDUserTemp.h"
#import "WDDelivery.h"
#import "WDADS.h"

@implementation WDDish

- (void) setDishObject: (NSDictionary*)dict{
  
  if([dict isKindOfClass:[NSDictionary class]]) {
    NSLog(@"%@", dict);
    NSNumber *dishID = [dict safeNumberForKey:@"id"];
    
    [self setDishID:[dishID integerValue]];
    NSNumber *foodTypeId = [dict safeNumberForKey:@"food_type_id"];
    [self setFoodTypeID:[foodTypeId integerValue]];
    if([dict valueForKey:@"food_type"]&&![[dict valueForKey:@"food_type"] isKindOfClass:[NSNull class]]){
      [self setFoodTypeName:[[dict valueForKey:@"food_type"] safeStringForKey:@"name"]];
    }
    NSNumber *currencyId = [dict safeNumberForKey:@"currency_id"];
    [self setCurrencyID:[currencyId integerValue]];
    if([dict valueForKey:@"currency"]&&![[dict valueForKey:@"currency"] isKindOfClass:[NSNull class]]){
      [self setCurrencyCode:[[dict valueForKey:@"currency"] safeStringForKey:@"code"]];
      [self setCurrencySymbol:[[dict valueForKey:@"currency"] safeStringForKey:@"symbol"]];
      NSNumber *fee = [[dict valueForKey:@"currency"] safeNumberForKey:@"fee"];
      [self setFee:[fee floatValue]];
    }
    [self setDishName:[[dict safeStringForKey:@"name"] stringByRemovingPercentEncoding]];
    [self setDishSlug:[[dict safeStringForKey:@"slug"] stringByRemovingPercentEncoding]];
    [self setDishImage:[[dict safeStringForKey:@"image"] stringByRemovingPercentEncoding]];
    NSNumber *priceNumber = [dict safeNumberForKey:@"price"];
    self.price = [priceNumber floatValue];
    [self setQuantity:[[dict safeNumberForKey:@"quantity"] integerValue]];
    [self setDishDescription:[[dict safeStringForKey:@"description"] stringByRemovingPercentEncoding]];
    [self setStatus:[[dict safeStringForKey:@"status"] stringByRemovingPercentEncoding]];
    [self setPickupTimeTo:[[dict safeStringForKey:@"pickup_time_to"] stringByRemovingPercentEncoding]];
    [self setPickupTimeFrom:[[dict safeStringForKey:@"pickup_time_from"] stringByRemovingPercentEncoding]];
    [self setServingDate:[[dict safeStringForKey:@"serving_date"] stringByRemovingPercentEncoding]];
    [self setRepeat:[[dict safeStringForKey:@"repeat"] stringByRemovingPercentEncoding]];
    WDUserTemp *user = [[WDUserTemp alloc] init];
    [user setUserObject:[dict valueForKey:@"user"]];
    [self setUser:user];
        
    self.deliveries = [[NSMutableArray alloc] init];
    for (NSDictionary *object in [dict valueForKey:@"deliveries"]) {
      WDDelivery *delivery = [[WDDelivery alloc] init];
      [delivery setDeliveryObject:object];
      [self.deliveries addObject:delivery];
    }
    
    [self setNumOrders:[[dict safeNumberForKey:@"num_orders"] integerValue]];
    
    if(![dict valueForKey:@"has_wishlist"]||[[dict valueForKey:@"has_wishlist"] isKindOfClass:[NSNull class]]){
      [self setHasWishList:0];
    }else{
      NSNumber *hasWishList = [[dict valueForKey:@"has_wishlist"] safeNumberForKey:@"id"];
      [self setHasWishList:[hasWishList integerValue]];
    }
  }
}

@end
