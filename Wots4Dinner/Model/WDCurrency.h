//
//  WDCurrency.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/20/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDCurrency : NSObject
@property (nonatomic, assign) NSInteger currencyID;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *symbol;

- (void) setCurrencyObject: (NSDictionary*)dict;

@end
