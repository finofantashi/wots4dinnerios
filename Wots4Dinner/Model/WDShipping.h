//
//  WDShipping.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/11/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDShipping : NSObject

@property (nonatomic, assign) NSInteger shipID;
@property (nonatomic, assign) NSInteger userID;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *phone;

- (void) setShippingObject: (NSDictionary*)dict;

@end
