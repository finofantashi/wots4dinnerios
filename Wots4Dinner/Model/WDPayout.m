//
//  WDPayout.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDPayout.h"
#import "NSDictionary+SafeValues.h"

@implementation WDPayout

-(void)setPayoutObject:(NSDictionary *)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSNumber *payoutID = [dict safeNumberForKey:@"id"];
        [self setPayoutID:[payoutID integerValue]];
        NSNumber *userID = [dict safeNumberForKey:@"user_id"];
        [self setUserID:[userID integerValue]];
        [self setPayoutDefault:[[dict safeNumberForKey:@"default"] integerValue]];

        [self setAccount:[dict safeStringForKey:@"account"]];
        [self setMethod:[dict safeStringForKey:@"method"]];
        [self setCurrency:[dict safeStringForKey:@"currency"]];
    }
}
@end
