//
//  WDUserTemp.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/30/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WDUser.h"

@class WDSeller, WDDelivery;
@interface WDUserTemp : NSObject

@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *phonePrefix;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *userName;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) NSInteger roleID;
@property (nonatomic, strong) NSString *avatarURL;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *sinceday;
@property (nonatomic, strong) NSString *living;
@property (nonatomic, strong) NSString *about;
@property (nonatomic, strong) WDSeller *seller;

//New
@property (nonatomic, assign) float rating;
@property (nonatomic, assign) NSInteger countReviews;
@property (nonatomic, assign) NSInteger hasFavorite;
@property (nonatomic, assign) NSInteger hasRating;
@property (nonatomic, assign) NSInteger numberOrders;
@property (nonatomic, assign) NSInteger hasOrder;
@property (nonatomic, strong) NSMutableArray *dishes;
@property (nonatomic, strong) NSString *videoURL;
@property (nonatomic, strong) WDDelivery *delivery;

- (void) setUser: (WDUser*)user;
- (void) setUserObject: (NSDictionary*)dict;
@end
