//
//  WDFavoriteSellers.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WDUserTemp.h"

@interface WDFavoriteSellers : NSObject
@property (nonatomic, assign) NSInteger favoriteSellerID;
@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, assign) NSInteger sellerID;
@property (nonatomic, strong) WDUserTemp *userSeller;

- (void) setFavoriteSellersObject: (NSDictionary*)dict;

@end
