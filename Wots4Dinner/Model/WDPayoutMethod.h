//
//  WDPayoutMethod.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDPayoutMethod : NSObject
@property (nonatomic, assign) NSInteger countryID;

@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *zip;
@property (nonatomic, strong) NSString *method;
@property (nonatomic, strong) NSString *account;
@property (nonatomic, strong) NSString *currency;

@property (nonatomic, strong) NSString *bankSwift;
@property (nonatomic, strong) NSString *bankAddress;
@property (nonatomic, strong) NSString *bankAccountName;
@property (nonatomic, strong) NSString *bankName;


@end
