//
//  WDADS.h
//  MealsAround
//
//  Created by Macbook on 7/29/20.
//  Copyright © 2020 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDADS : NSObject

@property (nonatomic, assign) NSInteger adsID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *mime_type;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *status;

- (void) setAdsObject:(NSDictionary *)dict;
@end
