//
//  WDCountry.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDCountry : NSObject
@property (nonatomic, assign) NSInteger countryID;

@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *phonePrefix;

- (void) setCountryObject: (NSDictionary*)dict;
@end
