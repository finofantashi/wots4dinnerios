//
//  WDComment.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WDUserTemp;

@interface WDComment : NSObject

@property (nonatomic, assign) NSInteger commentID;
@property (nonatomic, strong) NSString *postID;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) WDUserTemp *user;

- (void) setCommentObject: (NSDictionary*)dict;

@end
