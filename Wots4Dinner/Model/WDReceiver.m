//
//  WDReceiver.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/17/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDReceiver.h"
#import "NSDictionary+SafeValues.h"

@implementation WDReceiver

- (void) setReceiverObject:(NSDictionary *)dict {
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);

        [self setReceiverID:[[dict safeNumberForKey:@"receiver"] integerValue]];
        if(self.receiverID==0){
            [self setReceiverID:[[dict safeNumberForKey:@"id"] integerValue]];
        }
        [self setRoleID:[[dict safeNumberForKey:@"role_id"] integerValue]];
        [self setReceiverReadAt:[dict safeStringForKey:@"receiver_read_at"]];
        [self setFirstName:[dict safeStringForKey:@"first_name"]];
        [self setLastName:[dict safeStringForKey:@"last_name"]];
        [self setKitchenName:[dict safeStringForKey:@"kitchen_name"]];
        [self setAvatar:[dict safeStringForKey:@"avatar"]];
    }
}

- (NSString*)getFullName {
    return [NSString stringWithFormat:@"%@ %@",self.firstName, self.lastName];
}

@end
