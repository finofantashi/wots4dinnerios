//
//  WDMyOrderItem.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMyOrderItem.h"
#import "NSDictionary+SafeValues.h"

@implementation WDMyOrderItem
- (void) setMyOrderItemObject:(NSDictionary *)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        [self setDishID:[[dict safeNumberForKey:@"id"] integerValue]];
        [self setQuantity:[[dict safeNumberForKey:@"quantity"] integerValue]];
        [self setPrice:[[dict safeNumberForKey:@"price"] floatValue]];
        [self setTotalPrice:[[dict safeNumberForKey:@"total_price"] floatValue]];
        [self setNote:[[dict safeStringForKey:@"note"] stringByRemovingPercentEncoding]];
        [self setDishName:[[dict safeStringForKey:@"dish_name"] stringByRemovingPercentEncoding]];
        [self setCurrencyCode:[[dict safeStringForKey:@"currency_code"] stringByRemovingPercentEncoding]];
        [self setCurrencySymbol:[[dict safeStringForKey:@"currency_symbol"] stringByRemovingPercentEncoding]];
        [self setStatus:[[dict safeStringForKey:@"status"] stringByRemovingPercentEncoding]];
        [self setDeliveryFee:[[dict safeNumberForKey:@"delivery_fee"] floatValue]];
    }
}
@end
