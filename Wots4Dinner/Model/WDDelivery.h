//
//  WDDeliverie.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDDelivery : NSObject

@property (nonatomic, assign) NSInteger deliveryID;
@property (nonatomic, strong) NSString *recommendation;
@property (nonatomic, strong) NSString *specialOffer;
@property (nonatomic, strong) NSString *drone;
@property (nonatomic, assign) double fee;
@property (nonatomic, assign) double deliveryFee;
@property (nonatomic, strong) NSString *radius;
@property (nonatomic, assign) NSInteger currencyID;
@property (nonatomic, assign) NSInteger isDelivery;

- (void) setDeliveryObject: (NSDictionary*)dict;

@end
