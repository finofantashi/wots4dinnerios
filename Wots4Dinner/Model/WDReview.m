//
//  WDSellerReview.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/27/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDReview.h"
#import "NSDictionary+SafeValues.h"
#import "WDUserTemp.h"

@implementation WDReview

- (void) setReviewObject:(NSDictionary *)dict isSeller:(BOOL)isSeller {
    
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        NSNumber *reviewID = [dict safeNumberForKey:@"id"];
        NSNumber *userID = [dict safeNumberForKey:@"user_id"];
        NSNumber *sellerID = [dict safeNumberForKey:@"seller_id"];

        [self setReviewID:[reviewID integerValue]];
        [self setUserID:[userID integerValue]];
        [self setSellerID:[sellerID integerValue]];
        [self setRating:[[dict safeNumberForKey:@"rating"] floatValue]];

        if(isSeller){
            [self setReview:[dict safeStringForKey:@"review"]];
        }else{
            [self setReview:[dict safeStringForKey:@"content"]];
        }
        
        WDUserTemp *user = [[WDUserTemp alloc] init];
        [user setUserObject:[dict valueForKey:@"user"]];
        [self setUser:user];
        [self setUpdatedAt:[[dict safeStringForKey:@"updated_at"] stringByRemovingPercentEncoding]];
    }
}


@end
