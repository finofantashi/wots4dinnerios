//
//  WDDishSearch.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDDishSearch : NSObject
@property (nonatomic, assign) NSInteger dishID;
@property (nonatomic, strong) NSString *dishName;
@property (nonatomic, strong) NSString *dishThumb;
@property (nonatomic, strong) NSString *foodTypeName;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, strong) NSString *dishStatus;
@property (nonatomic, strong) NSString *dishUpdate;
@property (nonatomic, strong) NSString *kitchenName;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;

@property (nonatomic, assign) float price;
@property (nonatomic, assign) double lat;
@property (nonatomic, assign) double lng;
@property (nonatomic, assign) double distance;
@property (nonatomic, assign) double ratingValue;

- (void) setDishObject: (NSDictionary*)dict;
@end
