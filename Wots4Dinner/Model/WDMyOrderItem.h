//
//  WDMyOrderItem.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDMyOrderItem : NSObject
@property (nonatomic, assign) NSInteger dishID;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, assign) float price;
@property (nonatomic, assign) float totalPrice;
@property (nonatomic, assign) float deliveryFee;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *dishName;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;
@property (nonatomic, strong) NSString *status;

- (void) setMyOrderItemObject: (NSDictionary*)dict;

@end
