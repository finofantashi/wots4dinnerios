//
//  WDCard.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/19/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDCard : NSObject
@property (nonatomic, assign) NSInteger cardID;
@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, assign) NSInteger expireYear;
@property (nonatomic, assign) NSInteger expireMonth;
@property (nonatomic, assign) NSInteger cardDefault;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *cardType;
@property (nonatomic, strong) NSString *country;

- (void) setCardObject: (NSDictionary*)dict;

@end
