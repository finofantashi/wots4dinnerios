//
//  WDUserTemp.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/30/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDUserTemp.h"
#import "WDSeller.h"
#import "NSDictionary+SafeValues.h"
#import "WDDelivery.h"

@implementation WDUserTemp

- (void) setUserObject: (NSDictionary*)dict{
  if([dict isKindOfClass:[NSDictionary class]]) {
    NSLog(@"%@", dict);
    
    [self setUserID:[[dict safeNumberForKey:@"id"] integerValue]];
    [self setFirstName:[[dict safeStringForKey:@"first_name"] stringByRemovingPercentEncoding]];
    [self setLastName:[[dict safeStringForKey:@"last_name"] stringByRemovingPercentEncoding]];
    [self setPhoneNumber:[[dict safeStringForKey:@"phone"] stringByRemovingPercentEncoding]];
    [self setUserName:[[dict safeStringForKey:@"username"] stringByRemovingPercentEncoding]];
    [self setEmail:[[dict safeStringForKey:@"email"] stringByRemovingPercentEncoding]];
    [self setRoleID:[[dict safeNumberForKey:@"role_id"] integerValue]];
    [self setAvatarURL:[[dict safeStringForKey:@"avatar"] stringByRemovingPercentEncoding]];
    [self setGender:[[dict safeStringForKey:@"gender"] stringByRemovingPercentEncoding]];
    [self setBirthday:[[dict safeStringForKey:@"birthday"] stringByRemovingPercentEncoding]];
    [self setSinceday:[[dict safeStringForKey:@"created_at"] stringByRemovingPercentEncoding]];
    if([[[dict safeStringForKey:@"living_in"] stringByRemovingPercentEncoding] containsString:@"null"]){
      [self setLiving:@"Location"];
    }else{
      [self setLiving:[[dict safeStringForKey:@"living_in"] stringByRemovingPercentEncoding]];
    }
    [self setAbout:[[dict safeStringForKey:@"about"] stringByRemovingPercentEncoding]];
    [self setPhonePrefix:[[[dict valueForKey:@"country_code"] safeStringForKey:@"phone_prefix"] stringByRemovingPercentEncoding]];
    WDSeller *seller = [[WDSeller alloc] init];
    [seller setSellerObject:[dict valueForKey:@"seller"]];
    [self setSeller:seller];
    WDDelivery *delivery = [[WDDelivery alloc] init];
    [delivery setDeliveryObject: [dict valueForKey:@"delivery"]];
    [self setDelivery:delivery];
    
    //New
    if(![dict valueForKey:@"rating"]||[[dict valueForKey:@"rating"] isKindOfClass:[NSNull class]]){
      [self setRating:0];
    }else{
      NSString *rating = [[dict valueForKey:@"rating"] safeStringForKey:@"average"];
      [self setRating:[rating floatValue]];
    }
    
    if(![dict valueForKey:@"count_reviews"]||[[dict valueForKey:@"count_reviews"] isKindOfClass:[NSNull class]]){
      [self setCountReviews:0];
    }else{
      NSNumber *countReviews = [[dict valueForKey:@"count_reviews"] safeNumberForKey:@"num_reviews"];
      [self setCountReviews:[countReviews integerValue]];
    }
    
    NSNumber *numberOrder = [dict safeNumberForKey:@"num_orders"];
    [self setNumberOrders:[numberOrder integerValue]];
    
    NSNumber *hasOrder = [dict safeNumberForKey:@"has_orders"];
    [self setHasOrder:[hasOrder integerValue]];
    
    if(![dict valueForKey:@"has_favourite"]||[[dict valueForKey:@"has_favourite"] isKindOfClass:[NSNull class]]){
      [self setHasFavorite:0];
    }else{
      NSNumber *hasFavorite = [[dict valueForKey:@"has_favourite"] safeNumberForKey:@"id"];
      [self setHasFavorite:[hasFavorite integerValue]];
    }
    
    if(![dict valueForKey:@"has_rating"]||[[dict valueForKey:@"has_rating"] isKindOfClass:[NSNull class]]){
      [self setHasRating:0];
    }else{
      NSNumber *hasRating = [[dict valueForKey:@"has_rating"] safeNumberForKey:@"id"];
      [self setHasRating:[hasRating integerValue]];
    }
    
    self.dishes = [[NSMutableArray alloc] init];
    for (NSDictionary *object in [dict valueForKey:@"dishes"]) {
      [self.dishes addObject:[object valueForKey:@"name"]];
    }
    
    if(![[dict valueForKey:@"video"] isKindOfClass:[NSNull class]]){
      [self setVideoURL:[[dict valueForKey:@"video"] safeStringForKey:@"url"]];
    }
  }
}

- (void) setUser:(WDUser*)user{
  [self setFirstName:user.firstName];
  [self setLastName:user.lastName];
  [self setPhoneNumber:user.phoneNumber];
  [self setUserName:user.userName];
  [self setEmail:user.email];
  [self setRoleID:user.roleID];
  [self setAvatarURL:user.avatarURL];
  [self setGender:user.gender];
  [self setBirthday:user.birthday];
  [self setBirthday:user.sinceDate];
  [self setLiving:user.living];
  [self setAbout:user.about];
  [self setPhonePrefix:user.phonePrefix];
}

@end
