//
//  WDDeliverie.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDDelivery.h"
#import "NSDictionary+SafeValues.h"

@implementation WDDelivery

- (void) setDeliveryObject: (NSDictionary*)dict{
  if([dict isKindOfClass:[NSDictionary class]]) {
    NSLog(@"%@", dict);
    [self setDeliveryID:[[dict safeNumberForKey:@"id"] integerValue]];
    [self setSpecialOffer:[[dict safeStringForKey:@"special_offer"] stringByRemovingPercentEncoding]];
    [self setRecommendation:[[dict safeStringForKey:@"recommendation"] stringByRemovingPercentEncoding]];
    [self setDrone:[[dict safeStringForKey:@"drone"] stringByRemovingPercentEncoding]];
    [self setDeliveryFee:[[dict safeNumberForKey:@"delivery_fee"] floatValue]];
    [self setFee:[[dict safeNumberForKey:@"fee"] floatValue]];
    [self setRadius:[[dict safeStringForKey:@"radius"] stringByRemovingPercentEncoding]];
    [self setCurrencyID:[[dict safeNumberForKey:@"currency_id"] integerValue]];
    [self setIsDelivery:[[dict safeNumberForKey:@"is_delivery"] integerValue]];

  }
}

@end
