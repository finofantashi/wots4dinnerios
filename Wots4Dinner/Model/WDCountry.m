//
//  WDCountry.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/3/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDCountry.h"
#import "NSDictionary+SafeValues.h"

@implementation WDCountry

-(void)setCountryObject:(NSDictionary *)dict {
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSNumber *currencyID = [dict safeNumberForKey:@"id"];
        [self setCountryID:[currencyID integerValue]];
        
        [self setCountryName:[dict safeStringForKey:@"country"]];
        [self setCountryCode:[dict safeStringForKey:@"code"]];
        [self setPhonePrefix:[dict safeStringForKey:@"phone_prefix"]];
    }
}

@end
