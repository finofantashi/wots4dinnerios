//
//  WDUser.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDUser.h"
#import "NSDictionary+SafeValues.h"
#import "WDUserDefaultsManager.h"
#import "WDSourceConfig.h"
#import "WDUserTemp.h"

@implementation WDUser

+ (WDUser*)sharedInstance {
    static WDUser *sharedUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUser = [[self alloc] init];
        [sharedUser getUserFromDB];
    });
    return sharedUser;
}

- (void) setUserObject: (NSDictionary*)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        [self setUserID:[[dict safeNumberForKey:@"id"] integerValue]];
        
        [self setFirstName:[[dict safeStringForKey:@"first_name"] stringByRemovingPercentEncoding]];
        [self setLastName:[[dict safeStringForKey:@"last_name"] stringByRemovingPercentEncoding]];
        [self setPhoneNumber:[[dict safeStringForKey:@"phone"] stringByRemovingPercentEncoding]];
        [self setUserName:[[dict safeStringForKey:@"username"] stringByRemovingPercentEncoding]];
        [self setEmail:[[dict safeStringForKey:@"email"] stringByRemovingPercentEncoding]];
        [self setRoleID:[[dict safeNumberForKey:@"role_id"] integerValue]];
        [self setAvatarURL:[[dict safeStringForKey:@"avatar"] stringByRemovingPercentEncoding]];
        [self setGender:[[dict safeStringForKey:@"gender"] stringByRemovingPercentEncoding]];
        [self setBirthday:[[dict safeStringForKey:@"birthday"] stringByRemovingPercentEncoding]];
        [self setSinceDate:[[dict safeStringForKey:@"created_at"] stringByRemovingPercentEncoding]];
        if([[[dict safeStringForKey:@"living_in"] stringByRemovingPercentEncoding] containsString:@"null"]){
            [self setLiving:@"Location"];
        }else{
            [self setLiving:[[dict safeStringForKey:@"living_in"] stringByRemovingPercentEncoding]];
        }
        
        [self setAbout:[[dict safeStringForKey:@"about"] stringByRemovingPercentEncoding]];
        
        [self setPhonePrefix:[[[dict valueForKey:@"country_code"] safeStringForKey:@"phone_prefix"] stringByRemovingPercentEncoding]];
        
    }
}

- (void) setUser:(WDUserTemp*)user{
        [self setUserID:user.userID];
        [self setFirstName:user.firstName];
        [self setLastName:user.lastName];
        [self setEmail:user.email];
        [self setGender:user.gender];
        [self setBirthday:user.birthday];
        [self setLiving:user.living];
        [self setAbout:user.about];
}

- (void) getUserFromDB{
    _userID         = [self getUserID];
    _firstName      = [self getFirstName];
    _lastName       = [self getLastName];
    _phonePrefix    = [self getPhonePrefix];
    _phoneNumber    = [self getPhoneNumber];
    _token          = [self getToken];
    _password       = [self getPassword];
    _email          = [self getEmail];
    _roleID         = [self getRoleID];
    _avatarURL      = [self getAvatarURL];
    _gender         = [self getGender];
    _birthday       = [self getBirthday];
    _living         = [self getLiving];
    _about          = [self getAbout];
    _sinceDate      = [self getSinceday];
    _isSellerMode   = [self isSellerMode];
    
}

- (NSString*) getFullName{
    NSString *fullName = [NSString stringWithFormat:@"%@ %@",[self getFirstName],[self getLastName]];
    return fullName;
}

- (NSString *)stringWithDate:(NSString *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:date];
    NSString *stringDate = [NSDateFormatter localizedStringFromDate:dateFromString
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:NSDateFormatterNoStyle];
    if(!stringDate){
        stringDate = @"";
    }
    return stringDate;
}

- (NSString *)stringWithSinceDate:(NSString *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:date];
    NSString *stringDate = [NSDateFormatter localizedStringFromDate:dateFromString
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:NSDateFormatterNoStyle];
    if(!stringDate){
        stringDate = @"";
    }
    return stringDate;
}

- (NSInteger) getUserID{
    return [WDUserDefaultsManager valueForKey:kUserID]==nil?0:[[WDUserDefaultsManager valueForKey:kUserID] integerValue];
}
- (void) setUserID:(NSInteger)userID{
    if(!userID){
        userID = 0;
    }
    _userID = userID;
    [WDUserDefaultsManager setValue:[NSNumber numberWithInteger:userID] forKey:kUserID];
}

//User Name
- (NSString*) getUserName{
    return [WDUserDefaultsManager valueForKey:kUserName]==nil?@"":[WDUserDefaultsManager valueForKey:kUserName];
}
- (void) setUserName:(NSString *)userName{
    if(!userName){
        userName = @"";
    }
    _userName = userName;
    [WDUserDefaultsManager setValue:userName forKey:kUserName];
    
}
//Password
- (void) setPassword:(NSString *)password{
    if(!password){
        password = @"";
    }
    _password = password;
    [WDUserDefaultsManager setValue:password forKey:kUserPassword];
    
}
- (NSString*) getPassword{
    return [WDUserDefaultsManager valueForKey:kUserPassword]==nil?@"":[WDUserDefaultsManager valueForKey:kUserPassword];
}
//First Name
- (NSString*) getFirstName{
    return [WDUserDefaultsManager valueForKey:kUserFirstName]==nil?@"":[WDUserDefaultsManager valueForKey:kUserFirstName];
}
- (void) setFirstName:(NSString *)firstName{
    if(!firstName){
        firstName = @"";
    }
    _firstName = firstName;
    [WDUserDefaultsManager setValue:firstName forKey:kUserFirstName];
}
//Last Name
- (NSString*) getLastName{
    return [WDUserDefaultsManager valueForKey:kUserLastName]==nil?@"":[WDUserDefaultsManager valueForKey:kUserLastName];
}
- (void) setLastName:(NSString *)lastName{
    if(!lastName){
        lastName = @"";
    }
    _lastName = lastName;
    [WDUserDefaultsManager setValue:lastName forKey:kUserLastName];
}
//Phone Prefix
- (NSString*) getPhonePrefix{
    return [WDUserDefaultsManager valueForKey:kUserPhonePrefix]==nil?@"":[WDUserDefaultsManager valueForKey:kUserPhonePrefix];
}
- (void) setPhonePrefix:(NSString *)phonePrefix{
    if(!phonePrefix){
        phonePrefix = @"";
    }
    _phonePrefix = phonePrefix;
    [WDUserDefaultsManager setValue:phonePrefix forKey:kUserPhonePrefix];
}
//Phone Number
- (NSString*) getPhoneNumber{
    return [WDUserDefaultsManager valueForKey:kUserPhoneNumber]==nil?@"":[WDUserDefaultsManager valueForKey:kUserPhoneNumber];
}
- (void) setPhoneNumber:(NSString *)phoneNumber{
    if(!phoneNumber){
        phoneNumber = @"";
    }
    _phoneNumber = phoneNumber;
    [WDUserDefaultsManager setValue:phoneNumber forKey:kUserPhoneNumber];
}
//Token
- (NSString*) getToken{
    return [WDUserDefaultsManager valueForKey:kUserToken]==nil?@"":[WDUserDefaultsManager valueForKey:kUserToken];
}
-(void) setToken: (NSString*)token{
    if(!token){
        token = @"";
    }
    _token = token;
    [WDUserDefaultsManager setValue:token forKey:kUserToken];
}
//Email
- (NSString*) getEmail{
    return [WDUserDefaultsManager valueForKey:kUserEmail]==nil?@"":[WDUserDefaultsManager valueForKey:kUserEmail];
}
-(void) setEmail: (NSString*)email{
    if(!email){
        email = @"";
    }
    _email = email;
    [WDUserDefaultsManager setValue:email forKey:kUserEmail];
}
//Role ID
- (NSInteger) getRoleID{
    return [WDUserDefaultsManager valueForKey:kUserRoleID]==nil?0:[[WDUserDefaultsManager valueForKey:kUserRoleID] integerValue];
}
-(void) setRoleID:(NSInteger )roleID{
    if(!roleID){
        roleID = 0;
    }
    _roleID = roleID;
    [WDUserDefaultsManager setValue:[NSNumber numberWithInteger:roleID] forKey:kUserRoleID];
}
//Avatar URL
- (NSString*) getAvatarURL{
    return [WDUserDefaultsManager valueForKey:kUserAvatarURL]==nil?@"":[WDUserDefaultsManager valueForKey:kUserAvatarURL];
}
-(void) setAvatarURL:(NSString *)avatarURL{
    if(!avatarURL){
        avatarURL = @"";
    }
    _avatarURL = avatarURL;
    [WDUserDefaultsManager setValue:avatarURL forKey:kUserAvatarURL];
}
//Gender
- (NSString*) getGender{
    return [WDUserDefaultsManager valueForKey:kUserGender]==nil?@"":[WDUserDefaultsManager valueForKey:kUserGender];
}
-(void) setGender:(NSString *)gender{
    if(!gender){
        gender = @"";
    }
    _gender = gender;
    [WDUserDefaultsManager setValue:gender forKey:kUserGender];
}
//Birthday
- (NSString*) getBirthday{
    return [WDUserDefaultsManager valueForKey:kUserBirthday]==nil?@"":[WDUserDefaultsManager valueForKey:kUserBirthday];
}
-(void) setBirthday:(NSString *)birthday{
    if(!birthday){
        birthday = @"";
    }
    _birthday = birthday;
    [WDUserDefaultsManager setValue:birthday forKey:kUserBirthday];
}

//Since day
- (NSString*) getSinceday {
    return [WDUserDefaultsManager valueForKey:kUserSince]==nil?@"":[WDUserDefaultsManager valueForKey:kUserSince];
}
-(void) setSinceDate:(NSString *)sinceDate{
    if(!sinceDate){
        sinceDate = @"";
    }
    _sinceDate = sinceDate;
    [WDUserDefaultsManager setValue:sinceDate forKey:kUserSince];
}
//Living
- (NSString*) getLiving{
    return [WDUserDefaultsManager valueForKey:kUserLiving]==nil?@"":[WDUserDefaultsManager valueForKey:kUserLiving];
}
-(void) setLiving:(NSString *)living{
    if(!living){
        living = @"";
    }
    _living = living;
    [WDUserDefaultsManager setValue:living forKey:kUserLiving];
}
//About
- (NSString*) getAbout{
    return [WDUserDefaultsManager valueForKey:kUserAbout]==nil?@"":[WDUserDefaultsManager valueForKey:kUserAbout];
}
-(void) setAbout:(NSString *)about{
    if(!about){
        about = @"";
    }
    _about = about;
    [WDUserDefaultsManager setValue:about forKey:kUserAbout];
}

//Seller Mode
- (BOOL) isSellerMode{
    return [WDUserDefaultsManager boolForKey:kSellerMode];
}

- (void) setSellerMode:(BOOL)isSellerMode{
    _isSellerMode = isSellerMode;
    [WDUserDefaultsManager setBool:isSellerMode forKey:kSellerMode];
}

- (void) deleteAllData {
    [self setFirstName:nil];
    [self setLastName:nil];
    [self setPhonePrefix:nil];
    [self setPhoneNumber:nil];
    [self setToken:nil];
    [self setPassword:nil];
    [self setEmail:nil];
    [self setRoleID:0];
    [self setAvatarURL:nil];
    [self setGender:nil];
    [self setBirthday:nil];
    [self setLiving:nil];
    [self setAbout:nil];
    [self setSinceDate:nil];
    [self setSellerMode:NO];
    [WDUserDefaultsManager setValue:[NSNumber numberWithBool:NO] forKey:kSessionSignIn];
    [WDUserDefaultsManager setValue:[NSNumber numberWithInt:0] forKey:kCountNotificationMessage];
    [WDUserDefaultsManager setValue:[NSNumber numberWithInt:0] forKey:kCountNotificationOrder];
}

@end
