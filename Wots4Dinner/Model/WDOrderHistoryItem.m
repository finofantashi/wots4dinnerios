//
//  WDOrderHistoryItem.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/18/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDOrderHistoryItem.h"
#import "NSDictionary+SafeValues.h"

@implementation WDOrderHistoryItem
- (void) setOrderHistoryItemObject:(NSDictionary *)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        [self setQuantity:[[dict safeNumberForKey:@"quantity"] integerValue]];
        [self setPrice:[[dict safeNumberForKey:@"price"] floatValue]];
        [self setTotalPrice:[[dict safeNumberForKey:@"total_price"] floatValue]];
        [self setNote:[[dict safeStringForKey:@"note"] stringByRemovingPercentEncoding]];
        [self setDishName:[[dict safeStringForKey:@"dish_name"] stringByRemovingPercentEncoding]];
        [self setCurrencyCode:[[dict safeStringForKey:@"currency_code"] stringByRemovingPercentEncoding]];
        [self setCurrencySymbol:[[dict safeStringForKey:@"currency_symbol"] stringByRemovingPercentEncoding]];
        [self setKitchenName:[[dict safeStringForKey:@"kitchen_name"] stringByRemovingPercentEncoding]];
        [self setLocation:[[dict safeStringForKey:@"location"] stringByRemovingPercentEncoding]];
        [self setSpecialOffer:[[dict safeStringForKey:@"special_offer"] stringByRemovingPercentEncoding]];
        [self setPickupTimeFrom:[[dict safeStringForKey:@"pickup_time_from"] stringByRemovingPercentEncoding]];
        [self setPickupTimeTo:[[dict safeStringForKey:@"pickup_time_to"] stringByRemovingPercentEncoding]];
        [self setDelivery:[[dict safeNumberForKey:@"delivery"] integerValue]];
        [self setStatus:[[dict safeStringForKey:@"status"] stringByRemovingPercentEncoding]];
        [self setLng:[[dict safeNumberForKey:@"lng"] doubleValue]];
        [self setLat:[[dict safeNumberForKey:@"lat"] doubleValue]];
        
        [self setValue:[[dict safeNumberForKey:@"coupon_value"] floatValue]];
        [self setCode:[[dict safeStringForKey:@"coupon_code"] stringByRemovingPercentEncoding]];
        [self setType:[[dict safeStringForKey:@"coupon_type"] stringByRemovingPercentEncoding]];
        [self setFee:[[dict safeNumberForKey:@"fee"] floatValue]];
        [self setDeliveryFee:[[dict safeNumberForKey:@"delivery_fee"] floatValue]];
    }
}
@end
