//
//  WDCard.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 11/19/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDCard.h"
#import "NSDictionary+SafeValues.h"

@implementation WDCard

-(void)setCardObject:(NSDictionary *)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSNumber *cardID = [dict safeNumberForKey:@"id"];
        [self setCardID:[cardID integerValue]];
        NSNumber *userID = [dict safeNumberForKey:@"user_id"];
        [self setUserID:[userID integerValue]];
        [self setFirstName:[dict safeStringForKey:@"first_name"]];
        [self setLastName:[dict safeStringForKey:@"last_name"]];
        [self setCardNumber:[dict safeStringForKey:@"card_number"]];
        [self setCardType:[dict safeStringForKey:@"card_type"]];
        [self setExpireYear:[[dict safeNumberForKey:@"expire_year"] integerValue]];
        [self setExpireMonth:[[dict safeNumberForKey:@"expire_month"] integerValue]];
        [self setCountry:[dict safeStringForKey:@"country"]];
        [self setCardDefault:[[dict safeNumberForKey:@"default"] integerValue]];
    }
}
@end
