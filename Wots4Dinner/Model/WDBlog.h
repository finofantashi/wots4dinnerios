//
//  WDBlog.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WDUserTemp, WDDelivery;
@interface WDBlog : NSObject

@property (nonatomic, assign) NSInteger blogID;
@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *excerpt;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, assign) NSInteger numComments;

- (void) setBlogObject: (NSDictionary*)dict;

@end
