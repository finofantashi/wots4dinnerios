//
//  WDCoupon.h
//  MealsAround
//
//  Created by Hung Hoang on 6/23/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDCoupon : NSObject

@property (nonatomic, assign) NSInteger couponID;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, assign) float value;
@property (nonatomic, strong) NSString *type;


- (void) setCouponObject:(NSDictionary *)dict;
@end
