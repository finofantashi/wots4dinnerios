//
//  WDCoupon.m
//  MealsAround
//
//  Created by Hung Hoang on 6/23/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDCoupon.h"
#import "NSDictionary+SafeValues.h"

@implementation WDCoupon

- (void) setCouponObject:(NSDictionary *)dict{
    
    if([dict isKindOfClass:[NSDictionary class]]) {
        [self setCouponID:[[dict safeNumberForKey:@"id"] integerValue]];
        [self setValue:[[dict safeNumberForKey:@"value"] floatValue]];
        [self setCode:[[dict safeStringForKey:@"code"] stringByRemovingPercentEncoding]];
        [self setType:[[dict safeStringForKey:@"type"] stringByRemovingPercentEncoding]];
    }
}
@end
