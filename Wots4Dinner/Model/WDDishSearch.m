//
//  WDDishSearch.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/14/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDDishSearch.h"
#import "NSDictionary+SafeValues.h"

@implementation WDDishSearch
- (void) setDishObject: (NSDictionary*)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        [self setDishID:[[dict safeNumberForKey:@"dish_id"]integerValue]];
        [self setDishName:[[dict safeStringForKey:@"dish_name"] stringByRemovingPercentEncoding]];
        [self setDishThumb:[[dict safeStringForKey:@"dish_thumb"] stringByRemovingPercentEncoding]];
        [self setFoodTypeName:[[dict safeStringForKey:@"food_type_name"] stringByRemovingPercentEncoding]];
        NSNumber *priceNumber = [dict safeNumberForKey:@"price"];
        self.price = [priceNumber floatValue];
        [self setQuantity:[[dict safeNumberForKey:@"quantity"] integerValue]];
        [self setFirstName:[[dict safeStringForKey:@"seller_first_name"] stringByRemovingPercentEncoding]];
        [self setLastName:[[dict safeStringForKey:@"seller_last_name"] stringByRemovingPercentEncoding]];
        [self setKitchenName:[[dict safeStringForKey:@"seller_kitchen_name"] stringByRemovingPercentEncoding]];
        
        [self setDishStatus:[[dict safeStringForKey:@"dish_status"] stringByRemovingPercentEncoding]];
        [self setDishUpdate:[[dict safeStringForKey:@"dishes_update"] stringByRemovingPercentEncoding]];
        [self setCurrencyCode:[[dict safeStringForKey:@"currency_code"] stringByRemovingPercentEncoding]];
        [self setCurrencySymbol:[[dict safeStringForKey:@"currency_symbol"] stringByRemovingPercentEncoding]];

        [self setLat:[[dict safeNumberForKey:@"lat"] doubleValue]];
        [self setLng:[[dict safeNumberForKey:@"lng"] doubleValue]];
        [self setDistance:[[[dict safeStringForKey:@"distance"] stringByRemovingPercentEncoding] doubleValue]];
        
        [self setRatingValue:[[[[dict valueForKey:@"ratingValue"] safeStringForKey:@"average"] stringByRemovingPercentEncoding] doubleValue]];
        
    }
}

@end
