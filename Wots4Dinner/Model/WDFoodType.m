//
//  WDFoodType.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDFoodType.h"
#import "NSDictionary+SafeValues.h"

@implementation WDFoodType

- (void) setFoodTypeObject: (NSDictionary*)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        [self setFoodTypeID:[[dict safeNumberForKey:@"id"] integerValue]];
        [self setFoodTypeName:[[dict safeStringForKey:@"name"] stringByRemovingPercentEncoding]];
        [self setFoodTypeSlug:[[dict safeStringForKey:@"slug"] stringByRemovingPercentEncoding]];
        [self setFoodTypeImage:[[dict safeStringForKey:@"image"] stringByRemovingPercentEncoding]];
        [self setDishCount:[[dict safeNumberForKey:@"num_dish"] integerValue]];

    }
}

@end
