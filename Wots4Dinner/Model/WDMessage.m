//
//  WDMessage.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/17/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMessage.h"
#import "NSDictionary+SafeValues.h"

@implementation WDMessage

- (void) setMessageObject:(NSDictionary *)dict {
    if([dict isKindOfClass:[NSDictionary class]]) {
        [self setRecordID:[[dict safeNumberForKey:@"id"] integerValue]];
        [self setSenderID:[[dict safeNumberForKey:@"sender"] integerValue]];
        [self setRoleID:[[dict safeNumberForKey:@"role_id"] integerValue]];
        [self setGroupID:[[dict safeNumberForKey:@"group_id"] integerValue]];
        [self setConversationID:[[dict safeNumberForKey:@"conversation_id"] integerValue]];
        [self setReceiverID:[[dict safeNumberForKey:@"receiver"] integerValue]];
        [self setMessageID:[[dict safeNumberForKey:@"message_id"] integerValue]];
        
        [self setFirstName:[dict safeStringForKey:@"first_name"]];
        [self setLastName:[dict safeStringForKey:@"last_name"]];
        [self setKitchenName:[dict safeStringForKey:@"kitchen_name"]];
        [self setAvatar:[dict safeStringForKey:@"avatar"]];
        [self setCreatedAt:[dict safeStringForKey:@"created_at"]];
        [self setContentMessage:[dict safeStringForKey:@"content"]];

        self.receiver = [[NSMutableArray alloc] init];
        for (NSDictionary *object in [dict valueForKey:@"receivers"]) {
            WDReceiver *receiver = [[WDReceiver alloc] init];
            [receiver setReceiverObject:object];
            [self.receiver addObject:receiver];
        }
    }
}

@end
