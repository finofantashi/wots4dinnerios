//
//  WDGallery.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 3/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDGallery.h"
#import "NSDictionary+SafeValues.h"
#import "WDSourceConfig.h"

@implementation WDGallery
-(void)setGalleryObject:(NSDictionary *)dict {
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSString *stringURL = [[NSString stringWithFormat:@"%@%@",kImageDomain,[dict safeStringForKey:@"image"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        [self setGalleryID:[[dict safeNumberForKey:@"id"] integerValue]];
        [self setGalleryImage:[NSURL URLWithString:stringURL]];
    }
}
@end
