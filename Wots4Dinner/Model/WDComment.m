//
//  WDComment.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 8/13/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDComment.h"
#import "NSDictionary+SafeValues.h"
#import "WDUserTemp.h"

@implementation WDComment

- (void) setCommentObject: (NSDictionary*)dict{
    
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        NSNumber *commentID = [dict safeNumberForKey:@"id"];
        [self setCommentID:[commentID integerValue]];
        [self setPostID:[[dict safeStringForKey:@"post_id"] stringByRemovingPercentEncoding]];
        WDUserTemp *user = [[WDUserTemp alloc] init];
        [user setUserObject:[dict valueForKey:@"user"]];
        [self setUser:user];
        [self setContent:[[dict safeStringForKey:@"content"] stringByRemovingPercentEncoding]];
        [self setCreatedAt:[[dict safeStringForKey:@"created_at"] stringByRemovingPercentEncoding]];
    }
}
@end
