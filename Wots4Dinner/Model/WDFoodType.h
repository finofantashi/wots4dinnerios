//
//  WDFoodType.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDFoodType : NSObject
@property (nonatomic, assign) NSInteger foodTypeID;
@property (nonatomic, strong) NSString *foodTypeName;
@property (nonatomic, strong) NSString *foodTypeSlug;
@property (nonatomic, strong) NSString *foodTypeImage;
@property (nonatomic, assign) NSInteger dishCount;

- (void) setFoodTypeObject: (NSDictionary*)dict;
@end
