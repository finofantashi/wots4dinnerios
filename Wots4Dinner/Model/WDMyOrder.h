//
//  WDMyOrder.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface WDMyOrder : NSObject
@property (nonatomic, assign) NSInteger orderID;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *shippingName;
@property (nonatomic, strong) NSString *shippingAddress;
@property (nonatomic, strong) NSString *shippingPhone;
@property (nonatomic, strong) NSString *createAt;
@property (nonatomic, strong) NSString *reateAt;

@property (nonatomic, strong) NSString *pickupFrom;
@property (nonatomic, strong) NSString *pickupTo;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, assign) float price;
@property (nonatomic, assign) float totalPrice;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *dishName;
@property (nonatomic, strong) NSMutableArray *listOrderItem;

- (void) setMyOrderObject: (NSDictionary*)dict;
@end
