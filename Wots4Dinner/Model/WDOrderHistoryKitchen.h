//
//  WDOrderHistoryKitchen.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 4/19/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDOrderHistoryKitchen : NSObject
@property (nonatomic, strong) NSString *kitchenName;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, assign) NSInteger delivery;
@property (nonatomic, assign) double lng;
@property (nonatomic, assign) double lat;

@property (nonatomic, strong) NSMutableArray *listOrderItem;

@end
