//
//  WDADS.m
//  Meals Around
//
//  Created by Macbook on 7/29/20.
//  Copyright © 2020 Hung Hoang. All rights reserved.
//

#import "WDADS.h"
#import "NSDictionary+SafeValues.h"

@implementation WDADS

- (void) setAdsObject:(NSDictionary *)dict{
  
  if([dict isKindOfClass:[NSDictionary class]]) {
    [self setAdsID:[[dict safeNumberForKey:@"id"] integerValue]];
    [self setUrl:[[dict safeStringForKey:@"url"] stringByRemovingPercentEncoding]];
    [self setTitle:[[dict safeStringForKey:@"title"] stringByRemovingPercentEncoding]];
    [self setMime_type:[[dict safeStringForKey:@"mime_type"] stringByRemovingPercentEncoding]];
    [self setStatus:[[dict safeStringForKey:@"status"] stringByRemovingPercentEncoding]];
    [self setLocation:[[dict safeStringForKey:@"location"] stringByRemovingPercentEncoding]];
  }
}
@end
