//
//  WDQuestion.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDQuestion.h"
#import "NSDictionary+SafeValues.h"

@implementation WDQuestion

- (void) setQuestionObject:(NSDictionary *)dict {
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        NSNumber *questionID = [dict safeNumberForKey:@"id"];
        NSNumber *userID = [dict safeNumberForKey:@"user_id"];
        
        [self setQuestionID:[questionID integerValue]];
        [self setUserID:[userID integerValue]];
        [self setQuestion:[dict safeStringForKey:@"question"]];
        [self setAnswer:[dict safeStringForKey:@"answer"]];
        [self setSection:[dict safeStringForKey:@"section"]];

    }
}

@end
