
#import "WDMessageData.h"
#import "UIColor+Style.h"

/**
 *  This is for demo/testing purposes only.
 *  This object sets up some fake model data.
 *  Do not actually do anything like this.
 */

@implementation WDMessageData

- (instancetype)init {
    self = [super init];
    if (self) {
        self.messages = [[NSMutableArray alloc] init];
        self.avatars = [[NSMutableDictionary alloc] init];
        /**
         *  Create message bubble images objects.
         *
         *  Be sure to create your bubble images one time and reuse them for good performance.
         *
         */
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor colorFromHexString:@"5b98f4" withAlpha:1.0]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    }
    
    return self;
}

@end
