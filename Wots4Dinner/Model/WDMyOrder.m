//
//  WDMyOrder.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/22/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDMyOrder.h"
#import "NSDictionary+SafeValues.h"

@implementation WDMyOrder
- (void) setMyOrderObject:(NSDictionary *)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        [self setOrderID:[[dict safeNumberForKey:@"id"] integerValue]];
        [self setCode:[[dict safeStringForKey:@"code"] stringByRemovingPercentEncoding]];
        [self setFirstName:[[dict safeStringForKey:@"first_name"] stringByRemovingPercentEncoding]];
        [self setLastName:[[dict safeStringForKey:@"last_name"] stringByRemovingPercentEncoding]];
        [self setCreateAt:[[dict safeStringForKey:@"created_at"] stringByRemovingPercentEncoding]];
        [self setReateAt:[[dict safeStringForKey:@"read_at"] stringByRemovingPercentEncoding]];
        [self setShippingName:[[dict safeStringForKey:@"name"] stringByRemovingPercentEncoding]];
        [self setShippingAddress:[[dict safeStringForKey:@"address"] stringByRemovingPercentEncoding]];
        [self setShippingPhone:[[dict safeStringForKey:@"phone"] stringByRemovingPercentEncoding]];

        [self setPickupFrom:[[dict safeStringForKey:@"pickup_from"] stringByRemovingPercentEncoding]];
        [self setPickupTo:[[dict safeStringForKey:@"pickup_to"] stringByRemovingPercentEncoding]];
        [self setQuantity:[[dict safeNumberForKey:@"quantity"] integerValue]];
        [self setPrice:[[dict safeNumberForKey:@"price"] floatValue]];
        [self setTotalPrice:[[dict safeNumberForKey:@"total_price"] floatValue]];
        [self setNote:[[dict safeStringForKey:@"note"] stringByRemovingPercentEncoding]];
        [self setDishName:[[dict safeStringForKey:@"dish_name"] stringByRemovingPercentEncoding]];
    }
}
@end
