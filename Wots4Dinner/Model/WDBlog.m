//
//  WDFoodType.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDBlog.h"
#import "NSDictionary+SafeValues.h"
#import "WDUserTemp.h"
#import "WDDelivery.h"

@implementation WDBlog

- (void) setBlogObject: (NSDictionary*)dict{
    
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        NSNumber *blogID = [dict safeNumberForKey:@"id"];
        
        [self setBlogID:[blogID integerValue]];
        [self setUserID:[[dict safeNumberForKey:@"user_id"] integerValue]];
        [self setTitle:[[dict safeStringForKey:@"title"] stringByRemovingPercentEncoding]];
        [self setImage:[[dict safeStringForKey:@"image"] stringByRemovingPercentEncoding]];
        [self setExcerpt:[[dict safeStringForKey:@"excerpt"] stringByRemovingPercentEncoding]];
        if(![[dict valueForKey:@"comments_count"] isKindOfClass:[NSNull class]]){
            NSInteger numberComment = [[[dict valueForKey:@"comments_count"] safeNumberForKey:@"num_comments"] integerValue];
            self.numComments = numberComment;
        }
        [self setContent:[[dict safeStringForKey:@"content"] stringByRemovingPercentEncoding]];
        [self setCreatedAt:[[dict safeStringForKey:@"created_at"] stringByRemovingPercentEncoding]];
        
    }
}

@end
