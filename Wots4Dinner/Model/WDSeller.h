//
//  WDSeller.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WDPayoutMethod.h"

@interface WDSeller : NSObject
@property (nonatomic, strong) NSString *sellerID;
@property (nonatomic, strong) NSString *kitchenName;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) NSInteger categoryID;
@property (nonatomic, assign) double lat;
@property (nonatomic, assign) double lng;
@property (nonatomic, strong) NSString *sellerDescription;
@property (nonatomic, strong) WDPayoutMethod *payoutMethod;
@property (nonatomic, assign) NSInteger certified;


- (void) setSellerObject: (NSDictionary*)dict;

@end
