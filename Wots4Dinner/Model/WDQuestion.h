//
//  WDQuestion.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/26/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDQuestion : NSObject
@property (nonatomic, assign) NSInteger questionID;
@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, strong) NSString *question;
@property (nonatomic, strong) NSString *answer;
@property (nonatomic, strong) NSString *section;

- (void) setQuestionObject: (NSDictionary*)dict;

@end
