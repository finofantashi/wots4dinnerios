//
//  WDFavoriteSellers.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/23/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDFavoriteSellers.h"
#import "NSDictionary+SafeValues.h"

@implementation WDFavoriteSellers

- (void) setFavoriteSellersObject: (NSDictionary*)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        NSNumber *favoriteID = [dict safeNumberForKey:@"id"];
        NSNumber *userID = [dict safeNumberForKey:@"user_id"];
        NSNumber *sellerID = [dict safeNumberForKey:@"seller_id"];

        [self setFavoriteSellerID:[favoriteID integerValue]];
        [self setUserID:[userID integerValue]];
        [self setSellerID:[sellerID integerValue]];
        WDUserTemp *userSeller = [[WDUserTemp alloc] init];
        if([dict objectForKey:@"seller"]){
            [userSeller setUserObject:[dict valueForKey:@"seller"]];
        }else{
            [userSeller setUserObject:[dict valueForKey:@"user"]];
        }
        [self setUserSeller:userSeller];
    }
}

@end
