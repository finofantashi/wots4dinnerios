//
//  WDReceiver.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 10/17/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDReceiver : NSObject
@property (nonatomic, assign) NSInteger receiverID;
@property (nonatomic, assign) NSInteger roleID;

@property (nonatomic, strong) NSString *receiverReadAt;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *kitchenName;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong, getter=getFullName) NSString *fullName;

- (void) setReceiverObject: (NSDictionary*)dict;
- (NSString*) getFullName;

@end
