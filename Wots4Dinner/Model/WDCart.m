//
//  WDCart.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDCart.h"
#import "NSDictionary+SafeValues.h"
#import "WDDish.h"
#import "WDCoupon.h"

@implementation WDCart

- (void) setCartObject: (NSDictionary*)dict{
    
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        NSNumber *cartID = [dict safeNumberForKey:@"id"];
        NSNumber *userID = [dict safeNumberForKey:@"user_id"];
        NSNumber *dishID = [dict safeNumberForKey:@"dish_id"];
        NSString *quantity  = [[dict safeStringForKey:@"quantity"] stringByRemovingPercentEncoding];
        if(quantity.length>0){
            [self setQuantity:[[[dict safeStringForKey:@"quantity"] stringByRemovingPercentEncoding] floatValue]];
        }else{
            NSNumber *quantity = [dict safeNumberForKey:@"quantity"];
            [self setQuantity:[quantity integerValue]];;

        }
        [self setCartID:[cartID integerValue]];
        [self setUserID:[userID integerValue]];
        [self setDishID:[dishID integerValue]];
        [self setNote:[dict safeStringForKey:@"note"]];
        WDDish *dish = [[WDDish alloc] init];
        [dish setDishObject:[dict valueForKey:@"dish"]];
        [self setDish:dish];
        WDCoupon *coupon = [[WDCoupon alloc] init];
        [coupon setCouponObject:[dict valueForKey:@"coupon"]];
        [self setCoupon:coupon];
    }
}

@end
