//
//  WDCategory.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import "WDCategory.h"
#import "NSDictionary+SafeValues.h"

@implementation WDCategory
- (void) setCategoryObject:(NSDictionary *)dict{
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSLog(@"%@", dict);
        [self setCategoryID:[[dict safeNumberForKey:@"id"] integerValue]];
        [self setCategoryName:[[dict safeStringForKey:@"name"] stringByRemovingPercentEncoding]];
    }
}
@end
