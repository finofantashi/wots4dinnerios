//
//  WDCategory.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 12/15/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDCategory : NSObject
@property (nonatomic, assign) NSInteger categoryID;
@property (nonatomic, strong) NSString *categoryName;

- (void) setCategoryObject: (NSDictionary*)dict;
@end
