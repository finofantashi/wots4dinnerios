//
//  WDDish.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 7/31/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WDUserTemp, WDDelivery, WDADS;
@interface WDDish : NSObject

@property (nonatomic, assign) NSInteger dishID;
@property (nonatomic, assign) NSInteger foodTypeID;
@property (nonatomic, assign) NSInteger currencyID;
@property (nonatomic, strong) NSString *foodTypeName;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *currencySymbol;
@property (nonatomic, assign) float fee;
@property (nonatomic, strong) NSString *dishName;
@property (nonatomic, strong) NSString *dishSlug;
@property (nonatomic, strong) NSString *dishImage;
@property (nonatomic, strong) NSString *dishThumb;
@property (nonatomic, assign) float price;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, strong) NSString *dishDescription;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *pickupTimeFrom;
@property (nonatomic, strong) NSString *pickupTimeTo;
@property (nonatomic, strong) NSString *servingDate;
@property (nonatomic, strong) NSString *repeat;
@property (nonatomic, strong) WDUserTemp *user;
@property (nonatomic, strong) NSMutableArray *deliveries;
@property (nonatomic, assign) NSInteger hasWishList;
@property (nonatomic, assign) NSInteger numOrders;

@property (nonatomic, strong) NSString *kitchenNameWithPopular;

- (void) setDishObject: (NSDictionary*)dict;

@end
