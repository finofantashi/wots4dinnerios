//
//  WDCart.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 9/10/16.
//  Copyright © 2016 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WDDish, WDCoupon;

@interface WDCart : NSObject
@property (nonatomic, assign) NSInteger cartID;
@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, assign) NSInteger dishID;
@property (nonatomic, assign) NSInteger quantity;
@property (nonatomic, strong) WDDish   *dish;
@property (nonatomic, strong) NSString   *note;
@property (nonatomic, strong) WDCoupon   *coupon;

- (void) setCartObject: (NSDictionary*)dict;

@end
