//
//  WDCurrency.m
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/20/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import "WDCurrency.h"
#import "NSDictionary+SafeValues.h"

@implementation WDCurrency

-(void)setCurrencyObject:(NSDictionary *)dict {
    if([dict isKindOfClass:[NSDictionary class]]) {
        NSNumber *currencyID = [dict safeNumberForKey:@"id"];
        [self setCurrencyID:[currencyID integerValue]];
        
        [self setName:[dict safeStringForKey:@"name"]];
        [self setCode:[dict safeStringForKey:@"code"]];
        [self setSymbol:[dict safeStringForKey:@"symbol"]];
    }
}

@end
