//
//  WDPayout.h
//  Wots4Dinner
//
//  Created by Hung Hoang on 1/15/17.
//  Copyright © 2017 Hung Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WDPayout : NSObject

@property (nonatomic, assign) NSInteger payoutID;
@property (nonatomic, assign) NSInteger userID;
@property (nonatomic, assign) NSInteger payoutDefault;

@property (nonatomic, strong) NSString *account;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *method;

- (void) setPayoutObject: (NSDictionary*)dict;

@end
